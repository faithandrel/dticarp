-- MySQL dump 10.13  Distrib 5.6.16, for Win32 (x86)
--
-- Host: localhost    Database: dticarp
-- ------------------------------------------------------
-- Server version	5.6.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `activities` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `venue` varchar(200) DEFAULT NULL,
  `man_months` double DEFAULT NULL,
  `date_constructed` date DEFAULT NULL,
  `date_finished` date DEFAULT NULL,
  `intervention_id` int(8) NOT NULL,
  `province_id` int(8) DEFAULT NULL,
  `region_id` int(8) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
INSERT INTO `activities` VALUES (1,'Tagum Trade Fair','Trade Center, Tagum City',0,'2015-02-02','2015-02-06',1,2,0,4,NULL,'2015-02-20 02:14:32','2015-02-20 02:14:32'),(2,'Veggie Noodles Production','Trade Center, Tagum City',0,'2015-01-26','2015-02-25',9,2,0,4,NULL,'2015-02-20 02:20:11','2015-02-20 02:20:11'),(3,'Bookkeeping Refresher Course 1','Trade Center, Tagum City',NULL,'2015-02-09','2015-02-13',8,2,0,4,NULL,'2015-02-20 04:21:50','2015-02-20 04:21:50'),(4,'Consultancy on Copra Quality','City Hall, Tagum City',0.1818,'2015-02-21','2015-02-23',13,2,0,4,4,'2015-02-25 09:29:23','2015-02-20 04:24:27'),(5,'Bookkeeping Refresher Course 2','Trade Center, Tagum City',NULL,'2015-02-18','2015-02-19',8,2,0,4,NULL,'2015-02-20 06:56:49','2015-02-20 06:56:49'),(6,'Pinoy SME Trade Fair','Abreeza Davao',NULL,'2015-01-26','2015-01-30',1,NULL,1,3,NULL,'2015-02-20 09:39:44','2015-02-20 09:39:44'),(7,'With the Mati Polytechnic College','Trade Center, Tagum City',NULL,'2015-01-14','2015-01-16',2,2,0,4,NULL,'2015-02-23 00:55:53','2015-02-23 00:55:53'),(8,'Christmas Decors','',NULL,'2015-02-16','2015-02-18',5,2,0,4,NULL,'2015-02-24 08:09:47','2015-02-24 08:09:47'),(9,'Brown Rice','',NULL,'2015-02-18','2015-02-20',7,2,0,4,NULL,'2015-02-24 08:14:21','2015-02-24 08:14:21'),(10,'on business management','City Hall, Tagum City',0.045,'2015-02-17','2015-02-19',13,2,0,4,4,'2015-02-25 09:29:50','2015-02-25 09:25:46');
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `arcs`
--

DROP TABLE IF EXISTS `arcs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `arcs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `brgy` varchar(50) DEFAULT NULL,
  `district` int(4) DEFAULT NULL,
  `province_id` int(8) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `arcs`
--

LOCK TABLES `arcs` WRITE;
/*!40000 ALTER TABLE `arcs` DISABLE KEYS */;
INSERT INTO `arcs` VALUES (2,'DUJALI ARC',NULL,NULL,2,4,4,'2015-02-02 10:16:50','2015-01-28 23:55:42'),(3,'SAMPAO CLUSTER ARC',NULL,NULL,2,4,NULL,'2015-01-30 08:30:26','2015-01-30 08:30:26'),(6,'KAPUTIAN',NULL,NULL,2,4,4,'2015-02-02 10:17:06','2015-01-30 16:45:02'),(7,'TAMBO',NULL,NULL,2,4,NULL,'2015-02-02 10:17:19','2015-02-02 10:17:19'),(8,'PAGKAIN NG BAYAN',NULL,NULL,2,4,NULL,'2015-02-02 10:17:58','2015-02-02 10:17:58'),(9,'CABAYWA',NULL,NULL,2,4,NULL,'2015-02-03 19:49:03','2015-02-03 19:49:03');
/*!40000 ALTER TABLE `arcs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `attendances`
--

DROP TABLE IF EXISTS `attendances`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `attendances` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_male` int(8) DEFAULT NULL,
  `fb_female` int(8) DEFAULT NULL,
  `lo_male` int(8) DEFAULT NULL,
  `lo_female` int(8) DEFAULT NULL,
  `ncb_male` int(8) DEFAULT NULL,
  `ncb_female` int(8) DEFAULT NULL,
  `cost_carp` decimal(10,0) DEFAULT NULL,
  `cost_others` decimal(10,0) DEFAULT NULL,
  `sales` decimal(10,0) DEFAULT NULL,
  `remarks` text,
  `msme_id` int(11) NOT NULL,
  `activity_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `attendances`
--

LOCK TABLES `attendances` WRITE;
/*!40000 ALTER TABLE `attendances` DISABLE KEYS */;
INSERT INTO `attendances` VALUES (1,10,5,5,5,0,0,500,0,2000,'sold banana flour',13,1,4,4,'2015-02-20 05:00:02','2015-02-20 02:17:52'),(2,6,9,0,0,0,0,1000,300,0,'participants from Mabini',13,2,4,4,'2015-02-20 05:00:26','2015-02-20 04:29:57'),(3,7,9,4,7,0,0,500,200,0,'participants from Apokon',13,3,4,4,'2015-02-23 00:06:00','2015-02-20 06:35:11'),(4,5,8,0,0,0,0,600,300,0,'',13,5,4,NULL,'2015-02-20 06:57:22','2015-02-20 06:57:22'),(5,9,10,0,0,0,0,3000,500,10000,'',13,6,4,NULL,'2015-02-20 09:56:54','2015-02-20 09:56:54'),(6,0,0,1,1,0,0,200,100,5000,'for abaca fiber',13,7,4,NULL,'2015-02-23 00:56:56','2015-02-23 00:56:56'),(7,1,2,0,0,0,0,200,100,2000,'dagmay novelty items',8,7,4,NULL,'2015-02-23 00:58:07','2015-02-23 00:58:07'),(8,6,12,0,0,0,0,500,0,0,'',13,8,4,NULL,'2015-02-24 08:10:29','2015-02-24 08:10:29'),(9,5,9,5,5,0,0,500,0,0,'',13,9,4,4,'2015-02-24 09:01:36','2015-02-24 08:14:52'),(10,9,12,0,0,0,0,800,200,0,'',8,3,4,NULL,'2015-02-25 04:39:28','2015-02-25 04:39:28');
/*!40000 ALTER TABLE `attendances` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `entrepreneurs`
--

DROP TABLE IF EXISTS `entrepreneurs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `entrepreneurs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msme_id` int(11) NOT NULL,
  `first_name` varchar(100) NOT NULL,
  `last_name` varchar(100) NOT NULL,
  `project` varchar(150) NOT NULL,
  `date_developed` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `entrepreneurs`
--

LOCK TABLES `entrepreneurs` WRITE;
/*!40000 ALTER TABLE `entrepreneurs` DISABLE KEYS */;
INSERT INTO `entrepreneurs` VALUES (1,13,'Faith','Macad','coco buttons','2015-02-02',4,0,'2015-02-20 02:46:06','2015-02-20 02:46:06');
/*!40000 ALTER TABLE `entrepreneurs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `igp_sales`
--

DROP TABLE IF EXISTS `igp_sales`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `igp_sales` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `msme_id` int(11) NOT NULL,
  `monitored` decimal(10,0) NOT NULL,
  `established` decimal(10,0) NOT NULL,
  `remarks` text,
  `report_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `igp_sales`
--

LOCK TABLES `igp_sales` WRITE;
/*!40000 ALTER TABLE `igp_sales` DISABLE KEYS */;
INSERT INTO `igp_sales` VALUES (1,5,1000,500,'','2015-01-30',4,0,'2015-02-20 00:46:01','2015-02-20 00:46:01'),(2,13,1000,300,'','2015-02-06',4,4,'2015-02-25 03:50:14','2015-02-20 02:44:45'),(3,8,1000,300,NULL,'2015-02-06',4,4,'2015-02-23 04:21:01','2015-02-23 01:15:19'),(4,8,700,1000,NULL,'2015-02-13',4,4,'2015-02-23 04:21:01','2015-02-23 01:15:19'),(5,8,8000,2300,NULL,'2014-12-26',4,0,'2015-02-23 04:21:01','2015-02-23 04:21:01');
/*!40000 ALTER TABLE `igp_sales` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `igprojects`
--

DROP TABLE IF EXISTS `igprojects`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `igprojects` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `igp_name` varchar(100) NOT NULL,
  `igp_products` text NOT NULL,
  `product_category` text NOT NULL,
  `product_remarks` text NOT NULL,
  `product_sales` decimal(10,0) NOT NULL,
  `jobs` int(11) NOT NULL,
  `remarks` text,
  `date_started` date DEFAULT NULL,
  `msme_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `igprojects`
--

LOCK TABLES `igprojects` WRITE;
/*!40000 ALTER TABLE `igprojects` DISABLE KEYS */;
INSERT INTO `igprojects` VALUES (3,'pangkabuhayan package','','','',0,0,'','2015-02-04',5,4,4,'2015-02-03 05:00:00','2015-01-30 16:51:48'),(8,'rice production, organic fertilizer','','','',0,0,'','0000-00-00',6,4,4,'2015-02-12 22:26:24','2015-02-02 10:33:16'),(11,'seaweed production','','','',0,0,'','0000-00-00',9,4,NULL,'2015-02-02 10:36:33','2015-02-02 10:36:33'),(9,'coco coir twining project','','','',0,0,'','2015-01-29',8,4,4,'2015-02-12 16:53:50','2015-02-02 10:34:24'),(10,'copra production (new land)','','','',0,0,'','2015-01-28',8,4,4,'2015-02-12 16:53:56','2015-02-02 10:34:46'),(12,'rice milling facility','','','',0,0,'','2014-11-02',10,4,4,'2015-02-02 12:04:13','2015-02-02 10:37:44'),(13,'banana fiber project-decorticator','','','',0,0,'','0000-00-00',12,4,NULL,'2015-02-02 10:38:47','2015-02-02 10:38:47'),(14,'rice production and marketing','','','',0,0,'','2014-12-17',10,4,4,'2015-02-02 12:04:22','2015-02-02 10:39:48'),(16,'BANANA FLOUR','BANANA FLOUR','','',0,0,'','2015-02-04',13,4,NULL,'2015-02-03 19:53:03','2015-02-03 19:53:03'),(17,'coco buttons','','','',0,0,'','2015-02-15',6,4,NULL,'2015-02-17 21:37:24','2015-02-17 21:37:24');
/*!40000 ALTER TABLE `igprojects` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `inter_categories`
--

DROP TABLE IF EXISTS `inter_categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `inter_categories` (
  `id` int(4) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `inter_categories`
--

LOCK TABLES `inter_categories` WRITE;
/*!40000 ALTER TABLE `inter_categories` DISABLE KEYS */;
INSERT INTO `inter_categories` VALUES (1,'Market Development'),(2,'Product Development Activities'),(3,'Trainings and Seminars'),(4,'Studies'),(5,'Consultancy');
/*!40000 ALTER TABLE `inter_categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `interventions`
--

DROP TABLE IF EXISTS `interventions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `interventions` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `intervention` varchar(100) NOT NULL,
  `w_sales` tinyint(1) NOT NULL DEFAULT '0',
  `inter_category_id` int(4) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `interventions`
--

LOCK TABLES `interventions` WRITE;
/*!40000 ALTER TABLE `interventions` DISABLE KEYS */;
INSERT INTO `interventions` VALUES (1,'Trade Fairs',1,1,2,'2015-02-20 02:02:31','2015-02-20 01:59:29'),(2,'Market Matching',1,1,2,'2015-02-20 02:02:23','2015-02-20 02:00:05'),(3,'Selling Missions',1,1,2,'2015-02-20 02:02:03','2015-02-20 02:02:03'),(4,'Promo Collateral',0,1,2,'2015-02-20 02:06:12','2015-02-20 02:06:12'),(5,'Product Developed',0,2,2,'2015-02-20 02:09:39','2015-02-20 02:09:39'),(6,'Prototype Executed',0,2,2,'2015-02-20 02:10:34','2015-02-20 02:10:01'),(7,'Packaging and Label Developed',0,2,2,'2015-02-20 02:10:48','2015-02-20 02:10:48'),(8,'Managerial/Entrep/Institutional Trainings',0,3,2,'2015-02-20 02:11:43','2015-02-20 02:11:34'),(9,'Skills Training',0,3,2,'2015-02-20 02:11:57','2015-02-20 02:11:57'),(10,'Productivity Training',0,3,2,'2015-02-20 02:12:26','2015-02-20 02:12:26'),(11,'Technology Mission',0,3,2,'2015-02-20 02:12:43','2015-02-20 02:12:43'),(12,'Studies',0,4,2,'2015-02-20 02:13:18','2015-02-20 02:13:18'),(13,'Consultancy',0,5,2,'2015-02-20 02:13:28','2015-02-20 02:13:28');
/*!40000 ALTER TABLE `interventions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investments`
--

DROP TABLE IF EXISTS `investments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `investment_source` varchar(200) NOT NULL,
  `investment_amount` decimal(10,0) NOT NULL,
  `igp_id` int(11) NOT NULL,
  `date_monitored` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investments`
--

LOCK TABLES `investments` WRITE;
/*!40000 ALTER TABLE `investments` DISABLE KEYS */;
INSERT INTO `investments` VALUES (1,'DOLE',150000,3,'2015-02-02',0,NULL,'2015-02-02 12:00:40','2015-02-02 10:27:24'),(11,'coop equity',160000,3,'2015-02-02',0,NULL,'2015-02-02 12:00:40','2015-02-02 10:59:53'),(3,'members equity',2000000,8,'2015-02-13',0,NULL,'2015-02-12 22:26:24','2015-02-02 10:33:56'),(4,'DTI coco coir fund',50000,9,'0000-00-00',0,NULL,'2015-02-02 10:35:47','2015-02-02 10:35:23'),(5,'members\' equity',600000,10,'0000-00-00',0,NULL,'2015-02-02 10:36:08','2015-02-02 10:36:08'),(6,'DTI seaweed fund',35000,11,'0000-00-00',0,NULL,'2015-02-02 10:37:22','2015-02-02 10:37:22'),(7,'LGU DN',600000,12,'0000-00-00',0,NULL,'2015-02-02 10:38:23','2015-02-02 10:38:23'),(8,'coop equity',200000,12,'0000-00-00',0,NULL,'2015-02-02 10:38:23','2015-02-02 10:38:23'),(9,'DOLE',450000,13,'0000-00-00',0,NULL,'2015-02-02 10:39:14','2015-02-02 10:39:14'),(10,'loan from LBP',4000000,14,'0000-00-00',0,NULL,'2015-02-02 10:40:18','2015-02-02 10:40:18'),(12,'MEMBER\'S EQUITY',10000,16,'2015-02-04',0,NULL,'2015-02-03 19:54:28','2015-02-03 19:54:28'),(13,'COOPS',20000,16,'2015-02-04',0,NULL,'2015-02-03 19:54:28','2015-02-03 19:54:28'),(14,'member\'s equity',10000,17,'2015-02-15',0,NULL,'2015-02-17 21:37:24','2015-02-17 21:37:24');
/*!40000 ALTER TABLE `investments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msme_members`
--

DROP TABLE IF EXISTS `msme_members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msme_members` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fb_male` int(8) NOT NULL,
  `fb_female` int(8) NOT NULL,
  `lo_male` int(8) NOT NULL,
  `lo_female` int(8) NOT NULL,
  `ncb_male` int(8) NOT NULL,
  `ncb_female` int(8) NOT NULL,
  `msme_id` int(11) NOT NULL,
  `report_date` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msme_members`
--

LOCK TABLES `msme_members` WRITE;
/*!40000 ALTER TABLE `msme_members` DISABLE KEYS */;
/*!40000 ALTER TABLE `msme_members` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `msmes`
--

DROP TABLE IF EXISTS `msmes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `msmes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `region_id` int(11) NOT NULL,
  `province_id` int(8) NOT NULL,
  `cb_arc` varchar(10) NOT NULL,
  `arc_id` int(11) DEFAULT NULL,
  `msme_name` varchar(100) NOT NULL,
  `address_region` varchar(100) NOT NULL,
  `address_province` varchar(100) NOT NULL,
  `address_city` varchar(100) NOT NULL,
  `address_brgy` varchar(100) NOT NULL,
  `date_assisted` date NOT NULL,
  `remarks` varchar(255) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `msmes`
--

LOCK TABLES `msmes` WRITE;
/*!40000 ALTER TABLE `msmes` DISABLE KEYS */;
INSERT INTO `msmes` VALUES (5,0,2,'ARC',3,'AMSEFFPCO','Region XI','Davao del sur','','','0000-00-00','',4,'2015-02-02 10:18:45','2015-01-30 12:00:44',4),(6,0,2,'ARC',2,'PUBANECA','Region XI','Davao del sur','Davao City','Matina','0000-00-00','remarks',4,'2015-02-02 10:19:05','2015-01-30 12:52:15',4),(8,0,2,'ARC',6,'SARBEMCO','Region XI','Davao del Norte','','','0000-00-00','',4,'2015-02-02 10:19:38','2015-01-30 16:48:26',4),(9,1,2,'ARC',7,'TASEGDECO','Region 2','Davao del Norte','','','2015-02-02','',4,'2015-02-02 10:19:53','2015-02-02 10:19:53',NULL),(10,1,2,'ARC',2,'DUFFAMCO','Region 2','Davao del Norte','','','2015-02-02','',4,'2015-02-02 10:20:36','2015-02-02 10:20:05',4),(12,1,2,'ARC',8,'MARBCO','Region 2','Davao del Norte','','','2015-02-02','',4,'2015-02-02 10:20:50','2015-02-02 10:20:50',NULL),(13,1,2,'ARC',9,'AMSEFCO','Region 2','Davao del Norte','','','2015-02-04','',4,'2015-02-03 19:52:09','2015-02-03 19:52:09',NULL);
/*!40000 ALTER TABLE `msmes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `provinces`
--

DROP TABLE IF EXISTS `provinces`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `provinces` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `province` varchar(100) NOT NULL,
  `region_id` int(8) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `provinces`
--

LOCK TABLES `provinces` WRITE;
/*!40000 ALTER TABLE `provinces` DISABLE KEYS */;
INSERT INTO `provinces` VALUES (1,'Davao Oriental',1,3,NULL,'2015-01-27 23:01:11','2015-01-27 23:01:11'),(2,'Davao del Norte',1,3,NULL,'2015-01-27 23:58:28','2015-01-27 23:58:28'),(3,'Zambaonga del Sur',3,5,2,'2015-01-30 13:59:22','2015-01-28 00:31:40'),(5,'Misamis Oriental',5,2,NULL,'2015-01-30 17:03:28','2015-01-30 17:03:28');
/*!40000 ALTER TABLE `provinces` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `regions`
--

DROP TABLE IF EXISTS `regions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `regions` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `region` varchar(100) NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) DEFAULT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `regions`
--

LOCK TABLES `regions` WRITE;
/*!40000 ALTER TABLE `regions` DISABLE KEYS */;
INSERT INTO `regions` VALUES (1,'Region 2',2,NULL,'2015-01-27 22:34:45','2015-01-27 22:34:45'),(2,'Region 3',2,NULL,'2015-01-27 22:45:05','2015-01-27 22:45:05'),(3,'Region 5',2,NULL,'2015-01-27 22:56:22','2015-01-27 22:56:22'),(5,'Region 10',2,NULL,'2015-01-30 12:19:56','2015-01-30 12:19:56');
/*!40000 ALTER TABLE `regions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `targets`
--

DROP TABLE IF EXISTS `targets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province_id` int(8) NOT NULL,
  `year` year(4) NOT NULL,
  `investments` decimal(10,0) NOT NULL,
  `sales` decimal(10,0) NOT NULL,
  `jobs` int(4) NOT NULL,
  `entrepreneurs` int(8) NOT NULL,
  `arcs_assisted` int(8) NOT NULL,
  `non_arcs_assisted` int(8) NOT NULL,
  `msmes_dev` int(8) NOT NULL,
  `msmes_dev_fb` int(8) NOT NULL,
  `msmes_dev_lo` int(8) NOT NULL,
  `established` decimal(10,0) NOT NULL,
  `msmes_dev_arcs` int(8) NOT NULL,
  `msmes_asst` int(8) NOT NULL,
  `msmes_asst_fb` int(8) NOT NULL,
  `msmes_asst_lo` int(8) NOT NULL,
  `monitored` decimal(10,0) NOT NULL,
  `msmes_asst_arcs` int(8) NOT NULL,
  `man_months` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `updated_by` int(11) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `year` (`year`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `targets`
--

LOCK TABLES `targets` WRITE;
/*!40000 ALTER TABLE `targets` DISABLE KEYS */;
INSERT INTO `targets` VALUES (1,2,2015,10000000,300000,0,0,0,0,10,0,0,0,0,0,0,0,10000,0,0.5,4,4,'2015-02-25 09:44:04','2015-02-22 00:41:14'),(2,2,2014,156000,560000,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,'2015-02-23 04:36:00','2015-02-23 04:36:00');
/*!40000 ALTER TABLE `targets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `targets_interventions`
--

DROP TABLE IF EXISTS `targets_interventions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `targets_interventions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `target_id` int(11) NOT NULL,
  `intervention_id` int(8) NOT NULL,
  `number` int(8) NOT NULL,
  `fbs` int(8) NOT NULL,
  `los` int(8) NOT NULL,
  `arcs` int(8) NOT NULL,
  `sales` decimal(10,0) NOT NULL,
  `man_months` float NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `targets_interventions`
--

LOCK TABLES `targets_interventions` WRITE;
/*!40000 ALTER TABLE `targets_interventions` DISABLE KEYS */;
INSERT INTO `targets_interventions` VALUES (1,1,1,0,0,0,0,87000,0),(2,1,2,0,0,0,0,0,0),(3,1,3,0,0,0,0,0,0),(4,1,4,0,0,0,0,0,0),(5,1,5,0,0,0,0,0,0),(6,1,6,0,0,0,0,0,0),(7,1,7,0,0,0,0,0,0),(8,1,8,5,0,0,0,0,0),(9,1,9,8,0,0,0,0,0),(10,1,10,0,0,0,0,0,0),(11,1,11,0,0,0,0,0,0),(12,1,12,0,0,0,0,0,0),(13,1,13,5,0,0,0,0,0),(14,2,1,0,0,0,0,100000,0),(15,2,2,0,0,0,0,0,0),(16,2,3,0,0,0,0,0,0),(17,2,4,0,0,0,0,0,0),(18,2,5,0,0,0,0,0,0),(19,2,6,0,0,0,0,0,0),(20,2,7,0,0,0,0,0,0),(21,2,8,0,0,0,0,0,0),(22,2,9,0,0,0,0,0,0),(23,2,10,0,0,0,0,0,0),(24,2,11,0,0,0,0,0,0),(25,2,12,0,0,0,0,0,0),(26,2,13,0,0,0,0,0,0);
/*!40000 ALTER TABLE `targets_interventions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(60) NOT NULL,
  `password` varchar(60) NOT NULL,
  `status` tinyint(1) NOT NULL,
  `access_level` int(4) NOT NULL,
  `account_name` varchar(200) NOT NULL,
  `created_by` int(11) DEFAULT NULL,
  `region_id` int(8) DEFAULT NULL,
  `province_id` int(8) DEFAULT NULL,
  `remember_token` varchar(100) NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'faith','$2y$10$QRcXyFWwFJiAta6WZRHmueuBMGTutOOPEbxGna9SiQCR.6YgchPwO',1,3,'Faith Andrel',NULL,NULL,NULL,'xi41oC8gOT9imMl2LG4CQMVwrXFqGyf88ZgQPx5NtTzaQ29A7DVYZyYcxB8F','2015-01-30 03:15:28','2015-01-26 21:47:31'),(2,'lyndelle','$2y$10$DtUD8aWXYO1VEIuNRlVF5.49FjkGqkH2Sfs5EeC.tlilbDnNdO7Vi',1,1,'Lyndelle Lianne',NULL,NULL,NULL,'JwWfHxJeeRezmYA0hEl2mKGTX0G6SVhPmEuV62c8MFMnlwPp2sx4Zu1tpvz4','2015-02-11 00:47:07','2015-01-26 23:29:46'),(3,'freddie','$2y$10$K0EC8udekZPiGY9kDw.J6usjXxkQOn3HGbzNy5MA4igeZ4kBUQM5i',1,2,'Freddie McClair',NULL,1,NULL,'ghuumECbC60PFGOW57SlBpxN6Il0cdaBJh95mcHTBXpxhlp8koQ4qwRJEQjh','2015-02-20 09:28:38','2015-01-27 19:24:46'),(4,'tony','$2y$10$.9kYsOl4n11uHKnb0CaCHuhb6FkxtG7dtTIEj96.7VechqmZmChoa',1,3,'Tony Stonem',NULL,1,2,'rJWqM7NtAx9jy8uPOIO1SjDXckXYlY6ZjJ7tAC4Jz116FIpS8UU2kuwmO6hm','2015-02-25 05:46:12','2015-01-28 00:04:39'),(5,'effy','$2y$10$jBCGJVf6qX0oW.bkcocdSeNoGB555PeJrCKnkBgXJUrPcKG3QkIX6',1,2,'Elizabeth Stonem',NULL,3,1,'CreR5SP3p6BVr3voF0pQmeB9071HQFkQAXbX2V0z9LiRVAZMwo35X1ew2Vyg','2015-02-02 12:15:52','2015-01-28 00:30:44'),(6,'test','$2y$10$GLBrHK981hDVP0hJ/8/XQuNeSgljdvZs/TS5/srm297AexhJSwZPa',1,3,'test',NULL,1,2,'','2015-01-28 16:01:46','2015-01-28 16:01:46'),(7,'cassie','$2y$10$NPpQKvrK09cCOXIUUoSvGuEilzMK/x4jNQhbHPeUIkyCiQPbgzaLS',1,2,'Cassie Ainsworth',2,2,1,'','2015-01-30 13:52:02','2015-01-30 13:25:08'),(8,'emily','$2y$10$eXkyPSBQgh.61VvVF36sousUhkpN0MEPWXu6GtmXpMhv7cAGqjgY2',1,3,'Emily Fitch',2,1,1,'','2015-02-02 22:03:08','2015-01-30 13:59:52'),(9,'dardz','$2y$10$Of/B4cIWz.bWVpzlfGbN2e.nMgBCwyXqhaQLbt2wXP3rQqvo5nC06',1,3,'Kuya Dardz',2,1,1,'B55261iWygpb3SxvVPGzYHD1CbgFxg07u5uVKHZtci180pPVMvxMwBFH4NCp','2015-02-02 22:09:41','2015-01-30 17:04:14');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-02-26  2:29:23
