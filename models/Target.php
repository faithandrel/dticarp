<?php

class Target extends Eloquent
{
    protected $fillable = array('year', 'investments', 'sales', 'jobs', 'entrepreneurs', 'arcs_assisted', 'non_arcs_assisted',
                                'msmes_dev', 'msmes_dev_fb', 'msmes_dev_lo', 'established', 'msmes_dev_arcs',
                                'msmes_asst', 'msmes_asst_fb', 'msmes_asst_lo', 'monitored', 'msmes_asst_arcs');
    
    public function province()
    {
        return $this->belongsTo('Province');
    }
    
}