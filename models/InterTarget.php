<?php

class InterTarget extends Eloquent
{
    protected $table = 'targets_interventions';
    protected $fillable = array('number', 'fbs', 'los', 'arcs', 'sales', 'man_months');
    public $timestamps = false;
    
    public function target()
    {
        return $this->belongsTo('Target');
    }
    
    public function intervention()
    {
        return $this->belongsTo('Intervention');
    }
    
}