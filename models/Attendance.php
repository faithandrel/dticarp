<?php

class Attendance extends Eloquent
{
     protected $fillable = array('fb_male', 'fb_female', 'lo_male', 'lo_female', 'ncb_female', 'ncb_male',
                                'cost_carp', 'cost_others', 'sales', 'remarks');
     
    public function activity()
    {
        return $this->belongsTo('Activity');
    }
    
    public function msme()
    {
        return $this->belongsTo('Msme');
    }
    
}