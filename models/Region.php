<?php

class Region extends Eloquent
{
    public function provinces()
    {
        return $this->hasMany('Province');
    }
}