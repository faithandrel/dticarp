<?php

class Msme extends Eloquent
{
    public function province()
    {
        return $this->belongsTo('Province');
    }
    
    public function igprojects()
    {
        return $this->hasMany('Igproject');
    }
	
	public function msme_members()
    {
        return $this->hasMany('MsmeMember');
    }
	
	public function arc()
    {
        return $this->belongsTo('Arc');
    }
	
	public function igp_sales()
    {
        return $this->hasMany('MsmeMember');
    }
	
	public function jobs()
    {
        return $this->hasMany('Job');
    }
}