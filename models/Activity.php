<?php

class Activity extends Eloquent
{
    public function intervention()
    {
        return $this->belongsTo('Intervention');
    }
    
    public function attendances()
    {
        return $this->hasMany('Attendance');
    }
    
}