<?php

class Arc extends Eloquent
{
    public function province()
    {
        return $this->belongsTo('Province');
    }
    
    public function msmes()
    {
        return $this->hasMany('Msme');
    }
    
}