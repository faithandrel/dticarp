<?php

class Province extends Eloquent
{
    public function arcs()
    {
        return $this->hasMany('Arc');
    }
    
    public function msmes()
    {
        return $this->hasMany('Msme');
    }
    
    public function region()
    {
        return $this->belongsTo('Region');
    }
	
    public function user()
    {
        return $this->hasMany('User');
    }
}