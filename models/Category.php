<?php

class Category extends Eloquent
{
    protected $table = 'inter_categories';
    
    public function interventions()
    {
        return $this->hasMany('Intervention', 'inter_category_id');
    }
    
}