<?php

class Intervention extends Eloquent
{
    public function activities()
    {
        return $this->hasMany('Activity');
    }
    
}