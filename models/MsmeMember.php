<?php

class MsmeMember extends Eloquent
{
   public function msme()
    {
        return $this->belongsTo('Msme');
    }
}