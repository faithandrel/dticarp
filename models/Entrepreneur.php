<?php

class Entrepreneur extends Eloquent
{
    public function msme()
    {
        return $this->belongsTo('Msme');
    }
}