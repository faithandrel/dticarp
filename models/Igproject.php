<?php

class Igproject extends Eloquent
{
    public function msme()
    {
        return $this->belongsTo('Msme');
    }
    
	 public function investments()
    {
        return $this->hasMany('Investment');
    }
}