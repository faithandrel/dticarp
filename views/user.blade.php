@section('inner_content')
	<div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-angle-double-right"></i>
			@if(Session::get('access_level') == 2)
			<a href="{{ url('regional/home')}}">{{ Session::get('region') }}</a>&nbsp;
			<i class="fa fa-angle-right"></i>
			@endif
			<span>Users</span>
			</h3>					
		</div>
	</div>
	<div class="row">
	    <div class="col-lg-12 table-users-wrapper">
			<div class="dataTable_wrapper ">
			    <table class="table table-striped table-bordered table-users table-hover" id="dataTables-example">
				<thead>
				    <tr>
					<th class="actions-1"></th>
					<th>Username</th>
					<th>Name</th>
					<th>Access</th>
					<th>Region</th>
					<th>Province</th>
					<th>Status</th>
					<th class="actions-1"></th>
					<th class="actions-1"></th>
				    </tr>
				</thead>
				<tbody id="char">
				    @foreach($users as $user)
					<tr class="odd gradeX">
						<td>
							<span data-toggle="tooltip" title={{ "'Reset Password for User: ".$user->username."'" }} >
								<button type="button" data-toggle="modal" data-target="#change-pass-admin-modal" class="btn btn-primary change-pass-admin-trigger" user-id={{ $user->id }}><i class="fa fa-key"></i></button>
							</span>	
						</td>
					    <td>{{ $user->username }}</td>
					    <td>{{ $user->account_name }}</td>
					    <td>
						@if($user->access_level == 1)
						    National
						@elseif($user->access_level == 2)
						    Regional
						@else($user->access_level == 3)
						    Provincial
						@endif
					    </td>
					    <td>{{ $user->region()->pluck('region') }}</td>
					    <td>{{ $user->province()->pluck('province') }}</td>
						<td>@if($user->status) Active @else Inactive @endif</td>
					    <td>
						@if($user->status == 1)
						{{ Form::open(array('url' => 'deactivate_user/'.$user->id, 'method' => 'post')) }}
							<span data-toggle="tooltip" title={{ "'Deactivate User".$user->username."'" }} >
								<button type="submit" class="btn btn-warning" onclick="return confirm('Deactivate {{ $user->username }}?' )" ><i class="fa fa-ban"></i></button>
							</span>	
					    </td>
						{{ Form::close() }}
						
						@else
						{{ Form::open(array('url' => 'activate_user/'.$user->id, 'method' => 'post')) }}
							<span data-toggle="tooltip" title={{ "'Activate User ".$user->username."'" }} >
								<button type="submit" class="btn btn-warning" onclick="return confirm('Activate {{ $user->username }}?' )" ><i class="fa fa-check-circle"></i></button>
							</span>	
					    </td>
						{{ Form::close() }}
						@endif
						<td>
						@if(Session::get('access') == 1)
					        {{ Form::open(array('url' => 'national/users/'.$user->id, 'method' => 'delete')) }}
					    @endif
					    @if(Session::get('access') == 2)
						{{ Form::open(array('url' => 'regional/users/'.$user->id, 'method' => 'delete')) }}
					    @endif	
							<span data-toggle="tooltip" title={{ "'Delete ".$user->username."'" }} >
								<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $user->username }}?' )" ><i class="fa fa-trash"></i></button>
							</span>	
					    </td>
					    {{ Form::close() }}
					</tr>
				    @endforeach
				    
				</tbody>
			    </table>
			</div>
	    </div>
	</div>
	
	
	<!------------- user profile ---------------->
		<div id="change-pass-admin-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-sm">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-gear fa-fw"></i><span> Reset Password</span></h4>
			    </div>
				{{ Form::open(array('url' => 'users/reset_pass', 'method' => 'post')) }}
			     <div class="modal-body">
					<ul class="list-group">
					  <li class="list-group-item">
					  {{ Form::label('username', 'Username', array('class'=>'control-label')) }}
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-user"></i></span>
								{{ Form::text('username', NULL, array('class'=>'form-control username', 'readonly'=>'readonly')) }}
						</div>
					  					 
					  <li class="list-group-item">
						{{ Form::label('new_pass', 'New Password', array('class'=>'control-label')) }}
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-sign-in"></i></span>
								{{ Form::text('new_pass', NULL, array('class'=>'form-control newpass')) }}
								{{ Form::hidden('id', NULL, array('class'=>'form-control user_id')) }}
						</div>
					  </li>
					</ul>
				 </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
				</div>
				 {{ Form::close() }}
			    </div>
		    </div>
		</div>
@stop
@section('additional_scripts')
 @parent	
<script>

    $(".change-pass-admin-trigger").click( function() {
        var user_id = $(this).attr("user-id");

	@if(Session::get('access') == 1)
	    var link = '{{ url('national/users') }}' + '/' + user_id
	@endif
			    
	@if(Session::get('access') == 2)
	    var link = '{{ url('regional/users') }}' + '/' + user_id
	@endif
	var randomstring = Math.random().toString(36).slice(-8);
        $.ajax({ 
            type: 'GET', 
            url:  link ,
            dataType: 'json',
            success: function (data) { 
				$( ".username" ).val( data.username );
				$( ".newpass" ).val( randomstring );
				$( ".user_id" ).val( user_id );
            }
        });
    });

</script>
@stop
