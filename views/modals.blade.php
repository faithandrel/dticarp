@extends('modules')

@section('provincial_modal')	
	<div id="add-arc-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
		    <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <h4 class="modal-title">Add ARC</h4>
			</div>
			<div class="modal-body" style="padding: 30px;">
			    {{ Form::open(array('url' => 'provincial/arcs', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="form-group">
				    {{ Form::label('arc_name', 'Name', array('class'=>'control-label')) }}
				    {{ Form::text('arc_name', NULL, array('class'=>'form-control')) }}
				</div>
			</div>
			<div class="modal-footer">
			    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    <button type="submit" class="btn btn-primary">Add</button>
			    {{ Form::close() }}
			</div>
		    </div>
		</div>
	    </div>
	    
	    <div id="edit-arc-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
		    <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <h4 class="modal-title">Edit ARC</h4>
			</div>
			<div class="modal-body" style="padding: 30px;">
			    {{ Form::open(array('url' => 'provincial/arcs', 'method' => 'put', 'id'=>'edit-arc-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="form-group">
				    {{ Form::label('edit_arc_name', 'Name', array('class'=>'control-label')) }}
				    {{ Form::text('edit_arc_name', NULL, array('class'=>'form-control')) }}
				</div>
			</div>
			<div class="modal-footer">
			    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    <button type="submit" class="btn btn-primary">Edit</button>
			    {{ Form::close() }}
			</div>
		    </div>
		</div>
	    </div>
@show


@section('regional_modal')	
	
@show


@section('provincial_modal')	
	
@show