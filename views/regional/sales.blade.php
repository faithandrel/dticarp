@extends('regional/modules')
		
@section('inner_content')
            <div class="row">
				<div class="col-lg-12">
					<h3 class="page-header"><i class="fa fa-angle-double-right"></i> <span>Reports</span><i class="fa fa-angle-double-right"></i>
					<span>Sales Generated</span>
					<button type="submit" onclick="$('#report').submit();" class="btn btn-warning btn-report"><i class="fa fa-refresh fa-fw"></i> Create Report</button>
					</h3>	
				</div>
			</div>
	    <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Sales
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body"> 
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>ARC</th>
                                            <th>MSME</th>
                                            <th>IGP</th>
					    <th>Product Sales</th>
                                            @foreach($interventions as $intervention)
					        <th>{{ $intervention->intervention }}</th>
					    @endforeach
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($msmes as $msme)
					    <tr class="odd gradeX">
                                                <td>{{ $msme->name }}</td>
                                                <td>{{ $msme->msme_name }}</td>
						<td>
                                                    @foreach($msme->igp as $igproject)
                                                       <li>{{ $igproject->igp_name }}</li>
                                                    @endforeach
						    
                                                </td>
					        <td>
					            @foreach($msme->igp as $igproject)
                                                       <li>{{ $igproject->product_sales }}</li>
                                                    @endforeach
						</td>
					        @foreach($interventions as $intervention)
					             <td>
					            @foreach($msme->attn[$intervention->id] as $attendances)
					               @if($attendances->count())
							    @foreach($attendances as $attendance)
							         <li>{{ $attendance->sales }}</li>
							    @endforeach
					               @endif
                                                    @endforeach
						     </td>
					        @endforeach
					    </tr>
					@endforeach
					@if(!empty($totals))
					<tr>
					     <td><strong>Total</strong></td><td></td><td></td><td><strong>{{ $totals['igp_sales'] }}</strong></td>
					     @foreach($interventions as $intervention)
						    <td><strong>{{ $totals[$intervention->id] }}</strong></td>
					     @endforeach
					</tr>
					@endif
                                    </tbody>
                                </table>
		    {{ var_dump($totals) }}  <br/>
                    {{ var_dump($msmes) }}  
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
@stop
