@extends('base')
@section('sidebar')
	    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
						<li class="sidebar-account">
							<a href="#">
								<table>
									<tr>
										<td rowspan="2"><i class="fa fa-user fa-4x" style="color: #ec6967"></i> </td>
										<td><span class="account-name">{{ Session::get('username') }}</span></td>
									</tr>
									<tr>
										<td><span class="account-label">{{ Session::get('region') }}</span></td>
										<span class="caret-profile" ><i class="fa fa-caret-down" style="float: right"></i></span>
									</tr>
								</table>
								
							</a>
							 <ul class="nav nav-second-level sidebar-account-second-level">
									<li><a href="#/" data-toggle="modal" data-target="#profile-modal"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
									<li><a href="#/" data-toggle="modal" data-target="#change-pass-modal"><i class="fa fa-gear fa-fw"></i> Change Password</a></li>
									<li class="divider"></li>
									<li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
									</li>
								</ul>	
						</li>
						<li>
							<a  href="{{ url('regional/home') }}" ><i class="fa fa-trophy fa-fw"></i>&nbsp; ACCOMPLISHMENTS<span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="/#" ><i class="fa fa-bar-chart fa-fw"></i>&nbsp; REPORTS<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a target="_blank" href="{{ url('regional/reports/accomplishment-arcs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Accomplishments Per Arc</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/summary-accomplishments') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Accomplishments</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/trainings') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Trainings</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/entrepreneurs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Entrepreneurs Developed</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/jobs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Jobs Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/sales') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Sales Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/investments') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Investments Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/gender-disaggregated') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Gender-Disaggregated Data on Program Services</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/assisted-arcs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; List of Assisted ARCs</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('regional/reports/trade_fairs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Trade Fairs</a>
									</li>
								</ul>
							</li>
							<li>
							<a href="#" ><i class="fa fa-archive fa-fw"></i>&nbsp; USER LIBRARY<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a href="{{ url('regional/users') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View Users</a>
									</li>
									<li>
										<a href="#" class="add-user-trigger" data-toggle="modal" data-target="#add-user-modal"><i class="fa fa-user-plus fa-fw"></i>&nbsp; Add User</a>
									</li>
								</ul>
							</li>
							<li>
								<a href="#" ><i class="fa fa-archive fa-fw"></i>&nbsp; ACTIVITY LIBRARY<span class="fa arrow"></span></a>
									 <ul class="nav nav-second-level">
										<li>
											<a href="{{ url('regional/activities') }} "><i class="fa fa-table fa-fw"></i>&nbsp; View Activities</a>
										</li>
										<li>
											<a href="#" data-toggle="modal" data-target="#add-act-modal"><i class="fa fa-plus fa-fw"></i>&nbsp; Add Activity</a>
										</li>
									</ul> 
							</li>	
							</div>
							<!-- /.sidebar-collapse -->
						</div>
						<!-- /.navbar-static-side -->
        </nav>
        <!-- add activity -->
        <div id="add-act-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                    <div class="modal-content">
                    <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New Activity</span></h4>
                    </div>
                        {{ Form::open(array('url' => 'regional/activities', 'class'=>'form-horizontal', 'role'=>'form')) }}
                    <div class="modal-body">
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                        {{ Form::label('act_inter', 'Intervention', array('class'=>'control-label')) }}
                                                        {{ Form::select('act_inter', $inter_dropdown, NULL,  array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                        {{ Form::label('act_name', 'Name', array('class'=>'control-label')) }}
                                                        {{ Form::text('act_name', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-6">
                                                <div class="form-group">
                                                        {{ Form::label('date_constructed', 'Date Conducted', array('class'=>'control-label')) }}
                                                        {{ Form::text('date_constructed', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                        <div class="col-md-6">
                                                <div class="form-group">	
                                                        {{ Form::label('date_finished', 'Date Finished', array('class'=>'control-label')) }}
                                                        {{ Form::text('date_finished', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                                <div id="man-months" class="row" style="display:none;">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                        {{ Form::label('manmonths', 'Man-Months', array('class'=>'control-label')) }}
                                                        {{ Form::text('manmonths', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-md-12">
                                                <div class="form-group">
                                                        {{ Form::label('venue', 'Venue', array('class'=>'control-label')) }}
                                                        {{ Form::text('venue', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
                    </div>
                        {{ Form::close() }}
                    </div>
            </div>
        </div>
	@include('usermodals')
@stop		
@section('content')
	<div id="page-wrapper">
            @if($message = Session::get('message'))
                <div class="alert-wrapper">
						<div  class="alert alert-{{ $message['type'] }} alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ $message['content'] }}
						</div>
					</div>
            @endif
        @yield('inner_content')
        </div>
@stop

@section('additional_scripts')
    <script>
	$('select[name="act_inter"]').change( function() {
	    check_intervention($(this).val());
	});
	$('select[name="edit_act_inter"]').change( function() {
	    check_intervention($(this).val());
	});
	function check_intervention(inter_id) {
	    if (inter_id == '13') {
		$('#man-months').show();
		$('#edit-man-months').show();
	    }
	    else
	    {
		$('#man-months').hide();
		$('#edit-man-months').hide();
	    }
	}
    </script>
@stop
