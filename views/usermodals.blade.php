   
    <div id="add-user-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
	    <div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New User</span></h4>
		</div>
		<div class="modal-body">
		    @if(Session::get('access') == 1)
			{{ Form::open(array('url' => 'national/users', 'name'=>'pass_admin_form', 'class'=>'form-horizontal', 'role'=>'form')) }}
		    @endif
					
		    @if(Session::get('access') == 2)
			{{ Form::open(array('url' => 'regional/users', 'name'=>'pass_admin_form', 'class'=>'form-horizontal', 'role'=>'form')) }}
		    @endif
					@if(Session::get('access') == 1)   
                    <div class="form-group">
                        {{ Form::label('access', 'Access Level', array('class'=>'control-label')) }}
                        {{ Form::select('access', array( 2=>'Regional', 3=>'Provincial'),
                                        1,  array('class'=>'form-control', 'required'=>'required')) }}
                    </div>
                    <div class="reg-dropdown form-group" class="form-group">
                        {{ Form::label('region', 'Region', array('class'=>'control-label')) }}
                        {{ Form::select('region', $regions_dropdown, NULL,  array('class'=>'form-control', 'required'=>'required')) }}
                    </div>
                    @endif
                    
                    @if(Session::get('access') == 1)
						<div class="prov-dropdown form-group" style="display:none;" class="form-group">
						@endif		
						@if(Session::get('access') == 2)
						<div class="prov-dropdown form-group" class="form-group">
						@endif
							{{ Form::label('province', 'Province', array('class'=>'control-label')) }}
							{{ Form::select('province', $provinces_dropdown, NULL,  array('class'=>'form-control', 'required'=>'required')) }}
						</div>
						
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">	
							{{ Form::label('username', 'Username', array('class'=>'control-label')) }}
							{{ Form::text('username', NULL, array('class'=>'form-control', 'required'=>'required')) }}
								<div class="check_username"></div>
							</div>
						</div>			

						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('name', 'Name', array('class'=>'control-label')) }}
								{{ Form::text('name', NULL, array('class'=>'form-control', 'required'=>'required')) }}
							</div>
						</div>
					</div>
					<!--
					<div class="form-group pass">
						{{ Form::label('password', 'Password', array('class'=>'control-label')) }}
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-lock"></i></span>
								  {{ Form::password('pass_admin', array('onKeyUp' => 'verify.check()', 'required'=>'required', 'class' => 'form-control', 'placeholder'=>'Enter Password', 'required' => 'required')) }}                               
						</div>
                    </div>
					<div class="form-group pass">
						{{ Form::label('confirm_password', 'Confirm Password', array('class'=>'control-label')) }}
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
								  {{ Form::password('confim_pass_admin', array('onKeyUp' => 'verify.check()', 'required'=>'required', 'class' => 'form-control', 'placeholder'=>'Enter Password', 'required' => 'required')) }}                               
						</div>
                    </div>	
					<div class="form-group">	
						<div style="margin-top: -15px; float: right; font-size: 10pt"id="password_result_admin">&nbsp;</div>
					</div>-->
					<div class="form-group">	
					{{ Form::label('pass_admin', 'Password', array('class'=>'control-label')) }}
						<div class="input-group">
							<span class="input-group-addon"><i class="fa fa-sign-in"></i></span>
								{{ Form::text('pass_admin', NULL, array('class'=>'form-control', 'required'=>'required')) }}
						</div>
					</div>
					<div class="form-group">
						{{ Form::label('status', 'Active?') }}
						{{ Form::checkbox('status', '1', true) }}
					</div>
		</div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			<button type="submit" id="add-user" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
		    {{ Form::close() }}
		</div>
	    </div>
	</div>
    </div>
        
    <div id="edit-user-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm">
	    <div class="modal-content">
		<div class="modal-header">
		    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
		    <h4 class="modal-title">Edit User</h4>
		</div>
		<div class="modal-body" style="padding: 30px;">
					@if(Session::get('access') == 1)
					{{ Form::open(array('url' => 'national/users', 'method' => 'put', 'id'=>'edit-user-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
					@endif
							
					@if(Session::get('access') == 2)
					{{ Form::open(array('url' => 'regional/users', 'method' => 'put', 'id'=>'edit-user-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
					@endif
					
                    <div class="form-group">
                        {{ Form::label('edit_username', 'Username', array('class'=>'control-label')) }}
                        {{ Form::text('edit_username', NULL, array('class'=>'form-control')) }}
                    </div>
					<div class="form-group">
                        {{ Form::label('edit_name', 'Name', array('class'=>'control-label')) }}
                        {{ Form::text('edit_name', NULL, array('class'=>'form-control')) }}
            
                    </div>
                    <div class="form-group">

                        {{ Form::label('edit_password', 'Password', array('class'=>'control-label')) }}
                        {{ Form::password('edit_password', array('class'=>'form-control')) }}
                    </div>
                    
                    <div class="form-group">
                        {{ Form::label('edit_status', 'Active?') }}
                        {{ Form::checkbox('edit_status', '1', true) }}
                    </div>
                    
                    @if(Session::get('access') == 1)   
                    <div class="form-group">
                        {{ Form::label('edit_access', 'Access Level', array('class'=>'control-label')) }}
                        {{ Form::select('edit_access', array(1=>'National', 2=>'Regional', 3=>'Provincial'),
                                        1,  array('class'=>'form-control')) }}
                    </div>
                    <div class="reg-dropdown form-group" style="display:none;" class="form-group">
                        {{ Form::label('edit_region', 'Region', array('class'=>'control-label')) }}
                        {{ Form::select('edit_region', $regions_dropdown, NULL,  array('class'=>'form-control')) }}
                    </div>
                    @endif
                    @if(Session::get('access') == 1)
			<div class="prov-dropdown form-group" style="display:none;" class="form-group">
		    @endif		
		    @if(Session::get('access') == 2)
			<div class="prov-dropdown form-group" class="form-group">
		    @endif
                        {{ Form::label('edit_province', 'Province', array('class'=>'control-label')) }}
                        {{ Form::select('edit_province', $provinces_dropdown, NULL,  array('class'=>'form-control')) }}
                    </div>
		</div>
		<div class="modal-footer">
		    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		    <button type="submit" class="btn btn-primary">Edit</button>
		    {{ Form::close() }}
		</div>
	    </div>
	</div>
    </div>
@section('additional_scripts')
    @parent	
<script>
	$( "#add-user" ).click(function( event ) {
		event.preventDefault();
	});
	//---------------- change pass
	/*verify = new verifynotify();
	verify.field1 = document.pass_admin_form.pass_admin;
	verify.field2 = document.pass_admin_form.confim_pass_admin;
	verify.result_id = "password_result_admin";
	verify.match_html = "<span style=\"color:black\">Thank you, your passwords match!<\/span>";
	verify.nomatch_html = "<span style=\"color:red\">Please make sure your passwords match.<\/span>";
			*/
	// Update the result message
	verify.check();
	
	$( ".add-user-trigger" ).click(function( event ) {
		$( "#pass_admin" ).val(Math.random().toString(36).slice(-8));
	});

	$( "#username" ).keyup(function() {
	 
	if($( "#username" ).val() == '') $( ".check_username" ).hide();
	else if( $("#username").val().length < 3 )
	{
		$( ".check_username" ).show();		
		$( ".check_username" ).html('<span class="error"><i class="fa fa-times"></i> Too Short</span>')
	}
	else 
	{
		$( ".check_username" ).show();		
		
		var link = '{{ url('check_username') }}' + '/' + $( "#username" ).val();
		$.get( link, function( data ) {
				if (data > 0)
				{
					$( ".check_username" ).html('<span class="error"><i class="fa fa-times"></i> Not Available</span>');
				}
				else  
				{
					$( ".check_username" ).html('<i class="fa fa-check"></i> Available');
					//if(pass_admin_form.pass_admin.value == pass_admin_form.confim_pass_admin.value)
					//{
						$( "#add-user" ).unbind('click');
					//}
				}
				
		});
		
	}
		
	});
	
		
</script>
@stop