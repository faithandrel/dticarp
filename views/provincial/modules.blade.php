@extends('base')
@section('sidebar')
			<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
						<li class="sidebar-account">
							<a href="#">
								<table>
									<tr>
										<td rowspan="3"><i class="fa fa-user fa-4x" style="color: #ec6967"></i> </td>
										<td><span class="account-name">{{ Session::get('username') }}</span></td>
									</tr>
									
									<tr>
										<td><span class="account-label">{{ Session::get('province') }}</span></td>
									</tr>
									<tr>
										<td><span class="account-label">{{ Session::get('region') }}</span></td>
										<span class="caret-profile" ><i class="fa fa-caret-down" style="float: right"></i></span>
									</tr>
									
								</table>
								
							</a>
							 <ul class="nav nav-second-level sidebar-account-second-level">
									<li><a href="#/" data-toggle="modal" data-target="#profile-modal"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
									<li><a href="#/" data-toggle="modal" data-target="#change-pass-modal"><i class="fa fa-gear fa-fw"></i> Change Password</a></li>
									<li class="divider"></li>
									<li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
									</li>
								</ul>	
						</li>
						
						<li>
							<a  href="{{ url('provincial/accomplishments') }}" ><i class="fa fa-trophy fa-fw"></i>&nbsp; ACCOMPLISHMENTS</a>
						</li>
						
						<li>
							<a href="/#" ><i class="fa fa-bar-chart fa-fw"></i>&nbsp; REPORTS<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a target="_blank" href="{{ url('provincial/reports/accomplishment-arcs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Accomplishments Per Arc</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/summary-accomplishments') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Accomplishments</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/trainings') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Trainings</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/entrepreneurs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Entrepreneurs Developed</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/jobs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Jobs Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/sales') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Sales Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/investments') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Investments Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/gender-disaggregated') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Gender-Disaggregated Data on Program Services</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/assisted-arcs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; List of Assisted ARCs</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('provincial/reports/trade_fairs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Trade Fairs</a>
									</li>
								</ul>
						</li>
						<li>
							<a href="{{ url('provincial/targets') }}" ><i class="fa fa-clipboard fa-fw"  ></i>&nbsp; ANNUAL TARGETS</a>
								
						</li>
						<li>
							<a href="/#" ><i class="fa fa-archive fa-fw"></i>&nbsp; ACTIVITY LIBRARY<span class="fa arrow"></span></a>
								 <ul class="nav nav-second-level">
									<li>
										<a href="{{ url('provincial/activities') }} "><i class="fa fa-table fa-fw"></i>&nbsp; View Activities</a>
									</li>
									<li>
										<a href="/#" data-toggle="modal" data-target="#add-act-modal"><i class="fa fa-plus fa-fw"></i>&nbsp; Add Activity</a>
									</li>
								</ul> 
						</li>
						<li>
							<a  href="{{ url('provincial/igp') }}" ><i class="fa fa-archive fa-fw"></i>&nbsp; IGP LIBRARY</a>
						</li>
						
						<li>
							<a href="/#"  ><i class="fa fa-archive fa-fw"></i>&nbsp; MSME LIBRARY<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a href="{{ url('provincial/msmes') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View MSMEs</a>
									</li>
									<li>
										<a href="/#" data-toggle="modal" data-target="#add-msme-modal"><i class="fa fa-plus fa-fw"></i>&nbsp; Add MSME</a>
									</li>
									
									
								</ul> 
						</li>
						<li>
							<a  href="/#" ><i class="fa fa-archive fa-fw"></i>&nbsp; ARC LIBRARY<span class="fa arrow"></span></a>
								 <ul class="nav nav-second-level">
									 <li>
										<a href="{{ url('provincial/arcs') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View ARCS</a>
									</li> 
									<li>
										<a  href="/#" data-toggle="modal" data-target="#add-arc-modal"><i class="fa fa-plus fa-fw"></i>&nbsp; Add ARC</a>
									</li>
								</ul>
						</li>
						
						
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>

		<!-------------------------------- modals ------------------------->
		<!--- ARC -->
		<!-- add arc -->
		<div id="add-arc-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New ARC</span></h4>
				</div>
				{{ Form::open(array('url' => 'provincial/arcs', 'class'=>'form-horizontal', 'role'=>'form')) }}				
				<div class="modal-body">
					<div class="form-group">
						{{ Form::label('arc_name', 'Name', array('class'=>'control-label')) }}
						{{ Form::text('arc_name', NULL, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
				</div>
				{{ Form::close() }}
				</div>
			</div>
		</div>
		
		<!--- MSME Profile-->
		<!-- add msme -->
		<div id="add-msme-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
				
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New MSME</span></h4>
				</div>
				{{ Form::open(array('url' => 'provincial/msmes', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
									{{ Form::label('msme_fullname', 'MSME Name', array('class'=>'control-label')) }}
									{{ Form::text('msme_fullname', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
									{{ Form::label('msme_name', 'MSME Name Abbreviation', array('class'=>'control-label')) }}
									{{ Form::text('msme_name', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('date_assisted', 'Date Assisted', array('class'=>'control-label')) }}
								{{ Form::text('date_assisted', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								{{ Form::label('arc_id', 'ARC', array('class'=>'control-label')) }}
								{{ Form::select('arc_id', $arc_dropdown, 1,  array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('industry', 'Industry Focus', array('class'=>'control-label')) }}
								{{ Form::text('industry', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">	
								{{ Form::label('address_region', 'Region', array('class'=>'control-label')) }}
								{{ Form::text('address_region', Session::get('region'), array('class'=>'form-control', 'readonly'=>'readonly')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('address_province', 'Province', array('class'=>'control-label')) }}
								{{ Form::text('address_province', Session::get('province'), array('class'=>'form-control', 'readonly'=>'readonly')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('address_city', 'City', array('class'=>'control-label')) }}
								{{ Form::text('address_city', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('address_brgy', 'Barangay/Municipality', array('class'=>'control-label')) }}
								{{ Form::text('address_brgy', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('district', 'Legislative District', array('class'=>'control-label')) }}
								{{ Form::text('district', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<div class="form-group">		
								{{ Form::label('remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('remarks', NULL, array('class'=>'form-control', 'rows'=>'4')) }}
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
				</div>
				{{ Form::close() }}
				</div>
			</div>
		</div>
	
<!-------------- ADD IGP MODAL ------------------->
		<div id="add-igp-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title" ><i class="fa fa-plus fa-fw"></i><span id="add-igp-title"> </span></h4>
				</div>
				{{ Form::open(array('url' => 'provincial/igp', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('igp_name', 'IGP Name', array('class'=>'control-label')) }}
								{{ Form::text('igp_name', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">	
								{{ Form::label('date_started', 'Date Started', array('class'=>'control-label')) }}
								{{ Form::text('date_started', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="form-group">	
								{{ Form::label('igp_end_date', 'Date Finished', array('class'=>'control-label')) }}
								{{ Form::text('igp_end_date', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('igp_products', 'Products', array('class'=>'control-label')) }}
								{{ Form::text('igp_products', NULL, array('class'=>'form-control', 'placeholder'=>'e.g. copra, cacao beans, tablea')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('remarks', NULL, array('class'=>'form-control','rows'=>'1')) }}
							</div>
						</div>
					<!--	<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('product_remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('product_remarks', NULL, array('class'=>'form-control','rows'=>'1')) }}
							</div>
						</div> -->
					</div>
					
	<!--------------- funds source ------------------->
					<div class="row">
						<div class="col-xs-4">
							<div class="form-group">		
								{{ Form::label('investment_source_1', 'Investment Source', array('class'=>'control-label')) }}
								{{ Form::text('investment_source_1', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">	
								{{ Form::label('investment_amount_1', 'Investment Amount', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon">₱</span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control number')) }}
								</div>
							</div>
						</div>
						<div class="col-xs-4">
							<div class="form-group">		
								{{ Form::label('date_monitored_1', 'Date Monitored', array('class'=>'control-label')) }}
								{{ Form::text('date_monitored_1', NULL, array('class'=>'form-control')) }}
								{{ Form::hidden('igp_id', NULL, array('id' => 'igp_id', 'class'=>'form-control')) }}
								{{ Form::hidden('source_count', 1, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="investment"></div>
					<div class="form-group">	
						<button class="add_investment btn btn-default" type="button"><i class="fa fa-plus"></i> Funds Source</button>
						
					</div>

						{{ Form::hidden('msme_id', NULL, array('id'=>'igp_msme_id', 'class'=>'form-control')) }}
		
					<div class="product"></div>
					<div class="form-group">
					</div>
				</div>
				
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
				</div>
				{{ Form::close() }}
				</div>
			</div>
		</div>
<!---------------------- edit igp ----------------------->
		<div id="edit-igp-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i> IGP 
					@if(Request::path()  == 'provincial/accomplishments')
						<span>and @if (Session::get('acc_quarter') == 1)  1st Quarter
						@elseif (Session::get('acc_quarter') == 2)  2nd Quarter
						@elseif (Session::get('acc_quarter') == 3)  3rd Quarter
						@elseif (Session::get('acc_quarter') == 4)  4th Quarter
						@endif
						</span>
						@endif
							 Investments Source </h4>
				</div>

				{{ Form::open(array('url' => 'provincial/igp', 'method' => 'put', 'id'=>'edit-igp-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
						
								{{ Form::label('edit_igp_name', 'IGP Name', array('class'=>'control-label'))  }}
									{{ Form::text('edit_igp_name', NULL, array('class'=>'form-control')) }}
									{{ Form::hidden('url', Request::segment(2), array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">	
								{{ Form::label('edit_date_started', 'Date Started', array('class'=>'control-label')) }}
									{{ Form::text('edit_date_started', NULL, array('class'=>'form-control')) }}				
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">	
								{{ Form::label('edit_igp_end_date', 'Date Finished', array('class'=>'control-label')) }}
								{{ Form::text('edit_igp_end_date', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						
					</div>
					
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Products', array('class'=>'control-label')) }}
										{{ Form::text('edit_igp_products', NULL, array('class'=>'form-control', 'placeholder'=>'e.g. copra, cacao beans, tablea')) }}
							

							</div>
						</div>
						
						<!--<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('edit_igp_product_remarks', 'Remarks', array('class'=>'control-label')) }}
										{{ Form::textarea('edit_igp_product_remarks', NULL, array('class'=>'form-control','rows'=>'1')) }}
							
							</div>
						</div>-->
						
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('edit_remarks', 'Remarks', array('class'=>'control-label')) }}
									{{ Form::textarea('edit_remarks', NULL, array('class'=>'form-control','rows'=>'1')) }}
								
							</div>
						</div>
					</div>
					
					<div class="row inv-label">
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Investments Source', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Amount', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Date Monitored', array('class'=>'')) }}
							</div>
						</div>

					</div>
					
					<div class="edit-investment"></div>	
					<div class="new-investment"></div>
					
					<div class="form-group">	
						<button class="edit_add_investment btn btn-default" type="button"><i class="fa fa-plus"></i> Funds Source</button>
					
					</div>
						{{ Form::hidden('msme_id', NULL, array('id'=>'igp_msme_id', 'class'=>'form-control')) }}
					<div class="product"></div>
					<div class="form-group">
					</div>
				</div>
					{{ Form::hidden('edit_msme_id', NULL, array('id' => 'edit_msme_id', 'class'=>'form-control')) }}
				<div class="modal-footer">
					<button type="button" onclick="location.reload()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					{{ Form::close() }}
				</div>
				</div>
			</div>
		</div>
<!----------------------------- Activity ------------------------------>
		<!-- add activity -->
		<div id="add-act-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-sm">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New Activity</span></h4>
			    </div>
				{{ Form::open(array('url' => 'provincial/activities', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('act_inter', 'Intervention', array('class'=>'control-label')) }}
								{{ Form::select('act_inter', $inter_dropdown, NULL,  array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('act_name', 'Name', array('class'=>'control-label')) }}
								{{ Form::textarea('act_name', NULL, array('class'=>'form-control', 'rows'=>'3')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('date_constructed', 'Date Conducted', array('class'=>'control-label')) }}
								{{ Form::text('date_constructed', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">	
								{{ Form::label('date_finished', 'Date Finished', array('class'=>'control-label')) }}
								{{ Form::text('date_finished', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div id="man-months" class="row" style="display:none;">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('manmonths', 'Man-Months', array('class'=>'control-label')) }}
								{{ Form::text('manmonths', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('venue', 'Venue', array('class'=>'control-label')) }}
								{{ Form::text('venue', NULL, array('class'=>'form-control')) }}
								{{ Form::hidden('path', Request::path(), array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>

		

@stop

@section('content')
	<div id="page-wrapper">
            @if($message = Session::get('message'))
                
					<div class="alert-wrapper">
						<div  class="alert alert-{{ $message['type'] }} alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ $message['content'] }}
						</div>
					</div>
				
            @endif
        @yield('inner_content')
     </div>
@stop

@section('additional_scripts')
    <script>
	$('select[name="act_inter"]').change( function() {
	    check_intervention($(this).val());
	});
	$('select[name="edit_act_inter"]').change( function() {
	    check_intervention($(this).val());
	});
	function check_intervention(inter_id) {
	    if (inter_id == '13') {
		$('#man-months').show();
		$('#edit-man-months').show();
	    }
	    else
	    {
		$('#man-months').hide();
		$('#edit-man-months').hide();
	    }
	}
	
	//------------------------ I G P -------------------------------
		// --------------- add igp trigger
		$(".add-igp-trigger").click( function() {
			 var msme_id = $(this).attr("msme-id");
			 $("#igp_msme_id").val(msme_id);
				$('#add-igp-title').html(" Add IGP for "+$(this).attr("msme-name"));
		});
		
		// --------------- add igp source
		var c = 1;
		
		$(".add_investment").click( function() { 
			c++;
			var html = '<div class="row">';
			html += '<div class="col-lg-4"><div class="form-group"><div class="input-group"><span class="input-group-btn"><button onclick = "$(this).parent().parent().parent().parent().parent().remove()" class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button></span>';
			html += '<input class="form-control"  name="investment_source_' + c + '" type="text" id="investment_amount"></div></div></div>';
			html += '<div class="col-lg-4"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
			html += '<input class="form-control number"  name="investment_amount_' + c + '" type="text" id="investment_amount"></div>';
			html += '<input type="hidden" value="'+ c +'" name="source_count"></div></div>';
			html += '<div class="col-lg-4"><div class="form-group">';
			html += '<input class="form-control" name="date_monitored_' + c + '" type="text" id="date_monitored_'+ c +'"></div></div>';
						
			$('.investment').append(html); 
			
			$( "#date_monitored_"+c ).datepicker( {
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			}); 
			
			$('input.number').keyup(function (event) {
				if (event.which >= 37 && event.which <= 40) {
				event.preventDefault();
				}
				var currentVal = $(this).val();
				var testDecimal = testDecimals(currentVal);
					if (testDecimal.length > 1) {
						onsole.log("You cannot enter more than one decimal point");
						currentVal = currentVal.slice(0, -1);
					}
					$(this).val(replaceCommas(currentVal));
			});
		});
		
		//-------------- add funds source trigger
		$(".add-source-trigger").click( function() {
			 var igp_id = $(this).attr("igp-id");
			 $("#igp_id").val(igp_id);
		});
		
		//----------------- edit igp trigger
		$(".edit-igp-trigger").click( function() { 
        var igp_id = $(this).attr("igp-id");
		var link = '{{ url('provincial/igp') }}' + '/' + igp_id;
			$.ajax({ 
				type: 'GET', 
				url:  link + '/edit',
				dataType: 'json',
				success: function (data) {  
					$("#edit_igp_name").val(data.igp_name);
					$("#edit_msme_id").val(data.msme_id);
						date = new Date(data.date_started);
						date = new Date(date.setDate(date.getDate()));
					$("#edit_date_started").val($.datepicker.formatDate('M d, yy', date));
						date = new Date(data.date_finished);
						date = new Date(date.setDate(date.getDate()));
					if($.datepicker.formatDate('M d, yy', date) != 'Jan 1, 2999') 
					{	$("#edit_igp_end_date").val($.datepicker.formatDate('M d, yy', date)); }
					else {
						$("#edit_igp_end_date").val('');
						$("#edit_igp_end_date").attr("placeholder", "(Ongoing)");
					}
					$("#edit_remarks").val(data.remarks);
					$("#edit_igp_msme_id").val(data.igp_msme_id);
					$("#edit_igp_products").val(data.igp_products);
					$("#edit_igp_product_category").val(data.product_category);
					//$("#edit_igp_product_sales").val(data.product_sales);
					$("#edit_igp_product_remarks").val(data.product_remarks);
					$("#edit-igp-form").attr('action', link);
				}
			});
		});
	
		//edit fund source
		$(".edit-igp-trigger").click( function() { 
			
			$( ".edit-investment" ).empty();
			var msme_id = $(this).attr("msme-id");
			var igp_id = $(this).attr("igp-id"); 
			
			var link = '{{ url('provincial/investments') }}' + '/' + igp_id; 
			if(window.location.pathname == '/provincial/igp') 
			{
				var link = '{{ url('provincial/investments') }}' + '/' + igp_id + '/edit';
			}
		    $.ajax({ 
				type: 'GET', 
				url:  link ,
				dataType: 'json',
				success: function (data) {  
					var x = 1;
					var total = 0;
					for (var c = 0; c < data.length; c++)
					{	
						date = new Date(data[c].date_monitored);
						date = new Date(date.setDate(date.getDate()));
						html = '<div class="row">';
						html += '<div class="col-md-4"><div class="form-group"><div class="input-group"><span class="input-group-btn">';
						html += '<button id="delete-source-trigger-'+ c +'" src-id="'+data[c].id +'"  class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button></span>';
						html += '<input class="form-control"  value="'+data[c].investment_source+'"  name="edit_investment_source_' + c + '" type="text" id="investment_amount"></div></div></div>';
						html += '<div class="col-md-4"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
						html += '<input class="form-control number"  value="'+numberWithCommas(data[c].investment_amount) +'" name="edit_investment_amount_' + c + '" type="text" id="investment_amount"></div>';
						html += '<input type="hidden" value="'+ data[c].id +'" name="id_'+ c +'">';
						html += '<input type="hidden" value="'+ x +'" name="edit_source_count"></div></div>';
						html += '<input type="hidden" value="'+ msme_id +'" name="msme_id">';
						html += '<div class="col-md-4"><div class="form-group">';
						html += '<input class="form-control"  value="'+$.datepicker.formatDate('M d, yy', date) +'" name="edit_date_monitored_' + c + '" type="text" id="edit_date_monitored_'+c+'"></div></div>';
						
					
						x++;
						
						
						$('.edit-investment').append(html);
					
						
						total += Number( data[c].investment_amount );
						$("#total_source").val(numberWithCommas(total));
						
						$( "#edit_date_monitored_"+c ).datepicker({
							showButtonPanel: true,
							dateFormat: "M d, yy",
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
						}); 
						
						$('input.number').keyup(function (event) {
							if (event.which >= 37 && event.which <= 40) {
								event.preventDefault();
							}
							var currentVal = $(this).val();
							var testDecimal = testDecimals(currentVal);
							if (testDecimal.length > 1) {
								onsole.log("You cannot enter more than one decimal point");
								currentVal = currentVal.slice(0, -1);
							}
							$(this).val(replaceCommas(currentVal));
						});
						
						$("#delete-source-trigger-" +c).click( function() {
							
							var id = $(this).attr("src-id"); 
							var link = '{{ url('provincial/investments') }}' + '/' + id; 
							
							if (confirm('Delete ??'))
							{
								$.ajax({ 
									type: 'DELETE', 
									url:  link,
									success: function (msg) {  
									}
									});
								 $(this).parent().parent().parent().parent().parent().empty();
							 }
						});
					}
					
					if ( c == 0 ) { $('.inv-label').hide();  }
					$("#edit-source-form").attr('action', link);	
				}
			});
		});
		// ------------------ add igp source (edit modal)
		var z = 0;
		
		$(".edit_add_investment").click( function() { 
			z++;
			$('.inv-label').show();
			var html = '<div class="row">';
			html += '<div class="col-lg-4"><div class="form-group"><div class="input-group"><span class="input-group-btn"><button onclick = "$(this).parent().parent().parent().parent().parent().remove()" class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button></span>';
			html += '<input class="form-control"  name="investment_source_' + z + '" type="text" id="investment_amount"></div></div></div>';
			html += '<div class="col-lg-4"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
			html += '<input class="form-control number"  name="investment_amount_' + z + '" type="text" id="investment_amount"></div>';
			html += '<input type="hidden" value="'+ z +'" name="source_count"></div></div>';
			html += '<div class="col-lg-4"><div class="form-group">';
			html += '<input class="form-control" name="date_monitored_' + z + '" type="text" id="add_edit_date_monitored_'+ z +'"></div></div>';
						
			$('.new-investment').append(html); 
			
			$( "#add_edit_date_monitored_"+z ).datepicker({
							showButtonPanel: true,
							dateFormat: "M d, yy",
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			}); 
			
			$('input.number').keyup(function (event) {
							if (event.which >= 37 && event.which <= 40) {
								event.preventDefault();
							}
							var currentVal = $(this).val();
							var testDecimal = testDecimals(currentVal);
							if (testDecimal.length > 1) {
								onsole.log("You cannot enter more than one decimal point");
								currentVal = currentVal.slice(0, -1);
							}
							$(this).val(replaceCommas(currentVal));
							
			});
			
		}); 
    </script>
@stop