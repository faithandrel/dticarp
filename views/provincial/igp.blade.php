@extends('provincial/modules')
@section('inner_content')
    <div class="row">
		<div class="col-lg-12">
			<h3 class="page-header"><i class="fa fa-angle-double-right"></i><a href="{{ url('provincial/home')}}">{{ Session::get('province') }}</a>&nbsp;
				<i class="fa fa-angle-right"></i><span>Income Generating Projects</span>
				<i class="fa fa-angle-right"></i><span id="msme_head"></span>
			</h3>					
		</div>
	</div>
    <div class="row form-select" >
		{{ Form::open(array('url' => 'provincial/igp', 'method' => 'get', 'class'=>'form-horizontal', 'role'=>'form')) }}
			<div class="form-group-sm col-lg-12">
				{{ Form::label('msme', 'MSME', array('class'=>'control-label')) }}
                {{ Form::select('msme', $msme_dropdown , date('Y'),  array('class'=>'form-control', 'id'=>'msme', 'onchange'=>'this.form.submit()')) }}
			</div>
		{{ Form::close();}}
	</div> 

	@if($igps)
	<div class="row">
					<div class="col-lg-12 table-igp-wrapper">
                            <div class="dataTable_wrapper ">
                                <table class="table table-striped responsive table-bordered table-igp table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
											<th class="actions-1"></th>
											<th class="th-msme">MSME</th>
                                            <th>Income Generating Projects</th>
											<th>Products</th>
											<th>Sources of Funds</th>
											<th>Date Started</th>
											<th>Status</th>
											<th class="actions-1"></th>
											<th class="actions-1"></th>											
                                        </tr>
                                    </thead>
                                    <tbody id="char">
							
					@foreach($igps as $igp)
					    <tr class="odd gradeX">
						<td>
								<span data-toggle="tooltip" title={{ "'Edit ".$igp->igp_name."'" }}>
									<button type="button" data-toggle="modal" data-target="#edit-igp-modal" class="btn btn-primary edit-igp-trigger " igp-id={{ $igp->id }}><i class="fa fa-pencil-square-o"></i></button>
								</span> 
							</td>
						<td class="td-msme">{{ $igp->msme_name }}</td>
						<td >{{ $igp->igp_name }}</td>
						
						<td>{{ $igp->igp_products }}</td>
						
						<td>	
							<div class="source-list">
							<?php $c[$igp->id] = NULL; ?>
							@foreach($sources as $source)
								@if ($source->igp_id == $igp->id) 
									<?php $c[$igp->id] = 1; ?>
									<li> {{ $source->investment_source }} </li> 
								@endif	
							@endforeach
							</div>
							<td>{{ date_format(date_create($igp->date_started), 'M d, Y') }}</td>
							<td>@if( $igp->date_finished <= date("Y-m-d") )
									Project Ended : <br> &nbsp; {{ date_format(date_create($igp->date_finished), 'M d, Y') }} 
								@elseif( $igp->date_finished == '2999-01-01') Ongoing
								@elseif( $igp->date_started > date("Y-m-d")) To be conducted  <br> &nbsp; &nbsp; ( Ends on {{  date_format(date_create($igp->date_finished), 'M d, Y') }} )
								@else  Ongoing ( Ends on <br> &nbsp; &nbsp;{{  date_format(date_create($igp->date_finished), 'M d, Y') }} )
								@endif
								 
							</td>
							<!-- @if ($c[$igp->id])
								<span data-toggle="modal" data-target="#edit-source-modal" >
									<button type="button" data-toggle="tooltip" title="Edit Funds Source" class="btn btn-default edit-source-trigger" igp-id={{ $igp->id }}><i class="fa fa-pencil-square-o"></i></button>
								</span>
							@endif	-->
						</td>
						<td class="actions-1">
							<span data-toggle="modal" data-target="#deact-igp-modal">
								@if( $igp->date_finished <= date("Y-m-d") )
									<button type="submit" igp-id="{{ $igp->id }}" end-date="{{ date_format(date_create($igp->date_finished), 'M d, Y') }}"  data-toggle="tooltip" title={{ "'Click to extend Project: ".$igp->igp_name."'" }} class="btn btn-warning end-trigger" >
										<i class="fa fa-long-arrow-right"></i>
									</button>								
								@elseif( $igp->date_finished == '2999-01-01')
									<button type="submit" igp-id="{{ $igp->id }}"  data-toggle="tooltip" title={{ "'Click to end Project: ".$igp->igp_name."'" }} class="btn btn-warning end-trigger" >
										<i class="fa fa-ban"></i>
									</button>
								@else  
									<button type="submit" igp-id="{{ $igp->id }}" end-date="{{ date_format(date_create($igp->date_finished), 'M d, Y') }}" data-toggle="tooltip" title={{ "'Click to edit ".$igp->igp_name." End Date '" }} class="btn btn-warning end-trigger" >
										<i class="fa fa-pencil"></i>
									</button>
								@endif
							</span>
							<!-- <span data-toggle="tooltip" title={{ "'Add Funds Source to ".$igp->igp_name."'" }}>
								<button type="button" data-toggle="modal" data-target="#add-source-modal" class="btn btn-warning add-source-trigger" igp-id={{ $igp->id }}><i class="fa fa-plus"></i> Funds Source</button>
							</span> -->
							
						</td>
					{{ Form::open(array('url' => 'provincial/igp/'.$igp->id, 'method' => 'delete')) }}
						{{ Form::hidden('msme_id', Input::get('msme'), array('class'=>'form-control')) }}
						<td class="actions-1">
							<span data-toggle="tooltip" title={{ "'Delete ".$igp->igp_name."'" }}>
								<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $igp->igp_name }}?' )" ><i class="fa fa-trash"></i></button>
							</span>
						</td>
						{{ Form::close() }}
					@endforeach
                                        
                         </tbody>
                        </table>
                       </div>
				</div>
                <!-- /.col-lg-12 -->
            </div>	
			<!-------------igp deactivate ---------------->
			<div id="deact-igp-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
				<div class="modal-dialog modal-sm">
					<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title"><i class="fa fa-ban fa-fw"></i><span> End Project </span></h4>
					</div>
					{{ Form::open(array('url' => 'provincial/deactivate_igp', 'id'=>'end_form', 'class'=>'form-horizontal', 'role'=>'form')) }}
					 <div class="modal-body">
						<div class="form-group">
							{{ Form::hidden('msme_id', Input::get('msme'), array('class'=>'form-control')) }}
							{{ Form::label('date_ended', 'Date Ended') }}
							{{ Form::text('date_ended', NULL, array('class'=>'form-control')) }}
						</div>
					 </div>
					<div class="modal-footer">
						<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
						<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					</div>
					{{ Form::close() }}
					</div>
				</div>
			</div>
	@endif	
@stop
@section('additional_scripts')
@parent
<script>
 $(".end-trigger").click( function() {
	var end_date = $(this).attr("end-date");
	var igp_id = $(this).attr("igp-id");
	$('#end_form').attr('action', "deactivate_igp/"+igp_id);
	$("#date_ended").val(end_date);
  });
  
  $("#msme_head").html($("#msme option:selected" ).text());
  if($("#msme option:selected").val()) 
  {
	$(".th-msme").hide();  
	$(".td-msme").hide(); 
  }
</script>
@stop