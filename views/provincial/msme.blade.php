@extends('provincial/modules')
@section('inner_content')
    <div class="row">
		<div class="col-lg-12">
			<h3 class="page-header"><i class="fa fa-angle-double-right"></i><a href="{{ url('provincial/home')}}">{{ Session::get('province') }}</a>&nbsp;
			<i class="fa fa-angle-right"></i><span> Micro Small and Medium Enterprises (MSME)</span>				
			</h3>	
		</div>
			
	    </div>
            <!-- /.row -->
	    <div class="row">
                <div class="col-lg-12 table-msme-wrapper">
                            <div class="dataTable_wrapper ">
                                <table class="table table-striped table-bordered table-hover table-msme" id="dataTables-example">
                                    <thead>
                                        <tr>
											<th class="actions-1"></th>
											<th>Abbreviation</th>
                                            <th>MSME Name</th>
                                            <th>ARC</th>
											<th class="actions-1"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="char">
					@foreach($msmes as $msme)
					    <tr class="odd gradeX">
						<!-- <td class="td-msme" data-toggle="tooltip" title={{ "'Click to view ".$msme->msme_name." IGPs and Activities'" }} onclick="window.location.href ='{{ 'msmes/'.$msme->id }}'">{{ $msme->msme_name }}</td> -->
						<td>
							<!-- <span data-toggle="tooltip" title={{ "'View ".$msme->msme_name." IGPs'" }}>
								<a href={{ 'msmes/'.$msme->id }} type="button" class="btn btn-primary" ><i class="fa fa-file-text"></i></a>
							</span>  -->
							<span data-toggle="modal" data-target="#edit-msme-modal" >
								<button type="button"  data-toggle="tooltip" title={{ "'Edit ".$msme->msme_name."'" }} class="btn btn-primary edit-msme-trigger" msme-name={{ "'".$msme->msme_name."'" }} msme-id={{ $msme->id }}><i class="fa fa-pencil-square-o"></i></button>
							</span>
						</td>
						<td>{{ $msme->msme_name }}</td>
						<td>{{ $msme->msme_fullname }}</td>
						<td>
						@if($msme->arc()->pluck('name'))
							{{ $msme->arc()->pluck('name') }}
						@else 
							Non-ARC
						@endif
						</td>
						{{ Form::open(array('url' => 'provincial/msmes/'.$msme->id, 'method' => 'delete')) }}
						<td class="actions-4">
						
						
						<!-- <span data-toggle="tooltip" title={{ "'Add IGP for ".$msme->msme_name."'" }}>
							<button type="button" data-toggle="modal" data-target="#add-igp-modal"  class="btn btn-warning add-igp-trigger" msme-id={{ $msme->id }} ><i class="fa fa-plus"></i> IGP</button>
						</span>	 -->
						<!-- <span data-toggle="tooltip" title={{ "'Add Activity for ".$msme->msme_name."'" }}>
							<button type="button" data-toggle="modal" data-target="#add-igp-modal"  class="btn btn-warning add-igp-trigger" msme-id={{ $msme->id }} ><i class="fa fa-plus"></i> Activity</button>
						</span>	-->
						<span data-toggle="tooltip" title={{ "'Delete ".$msme->msme_fullname."(".$msme->msme_name.")'" }}>
							<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $msme->msme_fullname.' ('.$msme->msme_name }})?' )" ><i class="fa fa-trash"></i></button>
						</span>
						</td>
						{{ Form::close() }}
					    </tr>
					@endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
<!--  EDIT MSME MODAL  -->
		<div id="edit-msme-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit MSME</span></h4>
				</div>
				{{ Form::open(array('url' => 'provincial/msmes', 'method' => 'put', 'id'=>'edit-msme-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="modal-body">
					<div class="row">
						<div class="col-sm-12">
							<div class="form-group">
									{{ Form::label('edit_msme_fullname', 'MSME Name', array('class'=>'control-label')) }}
									{{ Form::text('edit_msme_fullname', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
									{{ Form::label('edit_msme_name', 'MSME Name Abbreviation', array('class'=>'control-label')) }}
									{{ Form::text('edit_msme_name', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('edit_date_assisted', 'Date Assisted', array('class'=>'control-label')) }}
								{{ Form::text('edit_date_assisted', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-sm-8">
							<div class="form-group">
								{{ Form::label('edit_arc_id', 'ARC', array('class'=>'control-label')) }}
								{{ Form::select('edit_arc_id', $arc_dropdown, 1,  array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_industry', 'Industry Focus', array('class'=>'control-label')) }}
								{{ Form::text('edit_industry', NULL, array('class'=>'form-control')) }}
							</div>
						</div>	
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">	
								{{ Form::label('edit_address_region', 'Region', array('class'=>'control-label')) }}
								{{ Form::text('edit_address_region', Session::get('region'), array('class'=>'form-control', 'readonly'=>'readonly')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">		
								{{ Form::label('edit_address_province', 'Province', array('class'=>'control-label')) }}
								{{ Form::text('edit_address_province', Session::get('province'), array('class'=>'form-control', 'readonly'=>'readonly')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('edit_address_city', 'City', array('class'=>'control-label')) }}
								{{ Form::text('edit_address_city', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('edit_address_brgy', 'Barangay/Municipality', array('class'=>'control-label')) }}
								{{ Form::text('edit_address_brgy', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">		
								{{ Form::label('edit_district', 'Legislative District', array('class'=>'control-label')) }}
								{{ Form::text('edit_district', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-12">
							<div class="form-group">		
								{{ Form::label('edit_msme_remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('edit_msme_remarks', NULL, array('class'=>'form-control', 'rows'=>'2')) }}
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					{{ Form::close() }}
				</div>
				</div>
			</div>
		</div>
		
@stop

@section('additional_scripts')
@parent
<script>
	// EDIT MSME
    $(".edit-msme-trigger").click( function() {
        var msme_id = $(this).attr("msme-id"); 
		var link = '{{ url('provincial/msmes') }}' + '/' + msme_id;
		
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) { 
				$("#edit_msme_fullname").val(data.msme_fullname);
				$("#edit_msme_name").val(data.msme_name);
				$("#edit_address_region").val(data.address_region);
				$("#edit_address_province").val(data.address_province);
				$("#edit_address_city").val(data.address_city);
				$("#edit_district").val(data.district);
				$("#edit_industry").val(data.industry);
				$("#edit_address_brgy").val(data.address_brgy);
				date = new Date(data.date_assisted);
				date = new Date(date.setDate(date.getDate()));
				$("#edit_date_assisted").val($.datepicker.formatDate('M d, yy', date));
				$("#edit_msme_remarks").val(data.remarks);
				$("#edit_arc_id").val(data.arc_id);
				$("#edit-msme-form").attr('action', link); 

            }
        });
    });
	
	
</script>
@stop