@extends('provincial/modules')
@section('inner_content')
    <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header">
			
			<i class="fa fa-angle-double-right"></i><a href="{{ url('provincial/home') }}">{{ Session::get('province') }} &nbsp;</a>
		@if ( isset($msme) )	
			<i class="fa fa-angle-double-right"></i> <span>Accomplishments &nbsp;</span>		
			<i class="fa fa-angle-right"></i><span>@if($msme->arc()->pluck('name')) {{ $msme->arc()->pluck('name') }} @else Non-ARC @endif &nbsp;</span>
			<i class="fa fa-angle-right"></i><span>{{ $msme['msme_name'] }}</span>
			<i class="fa fa-angle-right"></i><span>
			@if (Session::get('acc_quarter') == 1)  1st Quarter
			@elseif (Session::get('acc_quarter') == 2)  2nd Quarter
			@elseif (Session::get('acc_quarter') == 3)  3rd Quarter
			@elseif (Session::get('acc_quarter') == 4)  4th Quarter
			@endif
			&nbsp;</span>
			<i class="fa fa-angle-right"></i><span>{{ Session::get('acc_year') }}</span>
		@else	
			<i class="fa fa-angle-right"></i><span href="{{ url('provincial/home') }}">Welcome {{ Session::get('username') }} !&nbsp;</span>
		@endif
			</h3>	
		</div>
		
	    </div>
            <!-- /.row -->
	    <div class="row header-form" >
			{{ Form::open(array('url' => 'provincial/accomplishments', 'class'=>'form-horizontal', 'role'=>'form')) }}
			<div class="form-group-sm col-md-3 col-xs-6">
                {{ Form::label('arc_dropdown', 'ARC', array('class'=>'control-label')) }}
                {{ Form::select('arc_dropdown', $arc_dropdown , NULL,  array('class'=>'form-control', 'id'=>'arc_dropdown', 'onchange'=>'repair()' )) }}
			</div>
            <div class="form-group-sm col-md-3 col-xs-6">
                {{ Form::label('msme_dropdown', 'MSME', array('class'=>'control-label')) }}
                {{ Form::select('msme_dropdown',  array('0'=>'') , NULL,  array('class'=>'form-control', 'id'=>'msme_dropdown')) }}
			</div>
			<div class="form-group-sm col-md-2 col-xs-4 ">
                {{ Form::label('acc_quarter', 'Quarter', array('class'=>'control-label')) }}
                {{ Form::select('acc_quarter', $quarter_dropdown , ceil(date('m')/3),  array('class'=>'form-control', 'id'=>'acc_year')) }}
			</div>
			 <div class="form-group-sm col-md-2 col-xs-4">
                {{ Form::label('acc_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('acc_year', $years_dropdown , date('Y'),  array('class'=>'form-control', 'id'=>'acc_year')) }}
			</div>
			  <div class="col-md-2 col-xs-4 btn-accomplishment">
                <button type="submit" class="btn btn-danger active btn-go"> &nbsp; Go &nbsp;<i class="fa fa-arrow-circle-right fa-fw"></i> </button>
			</div> 
			{{ Form::close();}}
		</div>
		@if ( isset($msme) )
		<div class="row">
				<div class="btn-group toggle-menu-res col-sm-12 toggle-menu btn-group-justified" role="group" aria-label="...">
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						 <i class="fa fa-bars fa-fw"></i>  Menu &nbsp;
						  <span class="caret"></span>
						</button>
						<ul class="dropdown-menu side-menu-drop" role="menu">
						  <li class="members-trigger" type="button" data-toggle="modal" data-target="#members-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><a href="#"><i class="fa fa-pencil fa-fw "></i> Members</a></li> <li class="divider"></li>
						  <li class="add-igp-trigger" type="button" data-toggle="modal" data-target="#add-igp-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><a href="#"><i class="fa fa-plus fa-fw"></i> IGP</a></li> <li class="divider"></li>
						   <li class="jobs-trigger" type="button" data-toggle="modal" data-target="#jobs-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><a href="#"><i class="fa fa-pencil fa-fw"></i> Jobs</a></li> <li class="divider"></li>
						   <li class="sales-trigger" type="button" data-toggle="modal" data-target="#sales-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><a href="#"><i class="fa fa-pencil fa-fw"></i> Sales</a></li> <li class="divider"></li>
						   <li class="entrep-trigger" type="button" data-toggle="modal" data-target="#entrep-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><a href="#"><i class="fa fa-pencil fa-fw"></i> Entrepreneurs</a></li> 
						</ul>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						  <i class="fa fa-bars fa-fw"></i> IGPs &nbsp;
						  <span class="caret"></span>
						</button>
						<ul class="dropdown-menu centerDropdown side-menu-drop" role="menu">
						 <li role="presentation" class="dropdown-header"><i class="fa fa-pencil fa-fw"></i> <b> IGPs & Investments Source </b></li>
						
						  @foreach ($igps as $igp)
							<li data-toggle="modal" data-target="#edit-igp-modal" igp-id={{ $igp->id }} msme-id={{ $msme['id'] }} class="edit-igp-trigger" ><a href="#">{{ $igp->igp_name }}</i></a></li>
							@endforeach
						</ul>
					</div>
					<div class="btn-group" role="group">
						<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-bars fa-fw"></i>  Activities &nbsp;
						  <span class="caret"></span>
						</button>
						<ul class="dropdown-menu dropdown-menu-right side-menu-drop" role="menu">
						@foreach($category as $key => $value) 
							@if($value !='[]')
							 <li role="presentation" class="dropdown-header"><i class="fa fa-pencil fa-fw"></i> <b> {{ $key }} </b></li>
								@foreach($value as $v)
									<li><a href="" class="add-attendance-trigger" data-toggle="modal" w-sales={{ $v->w_sales }} act-name="{{ $v->name }} " act-id={{ $v->id }} data-target="#add-attendance-modal">{{ $v->name }}</a></li>
								@endforeach
							<li class="divider"></li>
							@endif
						@endforeach
						</ul>
					</div> 
				</div>
		</div>
		@endif
		@if ( isset($msme) )
		<div id="table-wrapper"> 
		<div class="row table-wrapper-accomplishment ">
			<div class="col-lg-10 responsive-wrapper">
			<div class="scroll-right">scroll <i class="fa fa-arrow-right"></i></div>
				<table class="table-bordered responsive table-hover table-accomplishment">
					<thead>
						<tr>
							<th class="headcol" width="35%" rowspan="3"><b>ARC NAME / TITLE OF ACTIVITY </b></th>
							<th width="10%" rowspan="3"><b>Date of Conduct </b></th>
							<th colspan="4"><b>ARBs </b></th>
							<th colspan="2"><b>COST </b></th>
							<th rowspan="3"><b>Remarks </b></th>
						</tr>
						<tr>
							<th colspan="2"><b>FBs </b></th>
							<th colspan="2"><b>LOs </b></th>
							<th  width="8%" rowspan="2"><b>FUND 158 </b></th>
							<th  width="8%" rowspan="2"><b>Others </b></th>
						</tr>
						<tr>
							<th width="4%" width="1"><b>M</b></th>
							<th width="4%"><b>F</b></th>
							<th width="4%"><b>M</b></th>
							<th width="4%"><b>F</b></th>
						</tr>
					</thead>
					<tr>
						<td class="headcol" ><b>Name of ARC: 
						@if($msme->arc()->pluck('name'))
							{{ $msme->arc()->pluck('name') }}
						@else 
							Non-ARC
						@endif</b>
						</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						<td><b>Members Assisted:</b></td>
					</tr>
					<tr id="data">
						<td>Barangay/Municipality:  {{ $msme['address_brgy'] }}</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						<td>FBs: &nbsp; &nbsp; M = {{ $fb_male }} &nbsp; ; &nbsp; F = {{ $fb_female }} ; &nbsp; Total = {{ $fb_total }}</td>
					</tr>
					<tr>
						<td>Legislative District:  {{ $msme['district'] }}</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						<td>LOs: &nbsp; &nbsp;   M = {{ $lo_male }} &nbsp; ; &nbsp; F = {{ $lo_female }} ; &nbsp; Total = {{ $lo_total }}</td>
					</tr>
					<tr>
						<td><b>MSME:  {{ $msme['msme_fullname'] }} ({{ $msme['msme_name'] }})</b></td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						<td>NCBs: &nbsp; M = {{ $ncb_male }} &nbsp; ; &nbsp; F = {{ $ncb_female }} ; &nbsp; Total = {{ $ncb_total }}</td>
					</tr>
					<tr>
					<?php
						$d1 = new DateTime(date("Y-m-d"));
						$d2 = new DateTime($msme['date_assisted']);
						$diff = $d2->diff($d1); 
					?>
						<td>No. of years assisted:  @if($diff->y == 0) New @else {{ $diff->y }} @endif</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						
					</tr>
					<tr>
						<td>Industry Focus: {{ $msme['industry'] }}</td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr>
						<td><b>Income Generating Projects (IGPs): </b></td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr> 
					<?php $a = 'a'; ?>
					@foreach ($igps as $igp)
						<tr data-toggle="modal" data-target="#edit-igp-modal" igp-id={{ $igp->id }} msme-id={{ $msme['id'] }} class="edit-igp-trigger tr-edit ">
							<td style="padding-left: 20px;">{{ $a.".  ".$igp->igp_name }}</td>
							<td align="center"></td>
							<td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						</tr>
					<?php  $a++; ?>
					@endforeach 
					<?php $intervention = 0; ?>
					@foreach($activities as $key => $value)
						<?php $c = 1; ?>
						@if($value != '[]') 
						<tr>
							<td><b>{{ $key }}</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
						</tr>
							
							@foreach($value as $v)
								
								@if($v->intervention_id != $intervention)
								<tr>
									<td style="padding-left: 20px;"><b>{{ $c.".  ".$v->intervention }}</b></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
								</tr>
								<?php  $a = 'a'; $c++; ?>
								@endif
								<?php  $intervention = $v->intervention_id ?>
								<tr class="tr-edit add-attendance-trigger" data-target="#add-attendance-modal" data-toggle="modal" w-sales={{ $v->w_sales }} act-name="{{ $v->name }} " act-id={{ $v->act_id }} >
									<td style="padding-left: 40px;">{{ $a.".  ".$v->name }}</td>
									<td align="center">
										@if ($v->date_constructed == $v->date_finished)
											{{ date_format(date_create($v->date_constructed), 'M d, Y') }}
										@else
											{{ date_format(date_create($v->date_constructed), 'M d, Y') }}
											-
											{{ date_format(date_create($v->date_finished), 'M d, Y') }}
										@endif
									</td>
									<td align="center">{{ number_format($v->fb_male) }}</td>
									<td align="center">{{ number_format($v->fb_female) }}</td>
									<td align="center">{{ number_format($v->lo_male) }}</td>
									<td align="center">{{ number_format($v->lo_female) }}</td>
									<td align="right">{{ '₱ ' . number_format($v->cost_carp, 2) }}</td>
									<td align="right">{{ '₱ ' . number_format($v->cost_others, 2) }}</td>
									<td>{{ $v->remarks }}</td>
								</tr>
								<?php $a++; ?>
							@endforeach
						@endif
					@endforeach
					
					<tr>
						<td><b>Investments: {{ '₱ ' . number_format($investments, 2)  }}</b></td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr class="tr-edit sales-trigger" type="button" data-toggle="modal" data-target="#sales-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}>
						<td><b>Sales Generated: {{ '₱ ' . number_format($sales, 2)  }}</b></td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr class="tr-edit jobs-trigger" type="button" data-toggle="modal" data-target="#jobs-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}>
						<td><b>Jobs Generated: {{ number_format($total_jobs) }} </b></td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<tr class="tr-edit entrep-trigger" data-toggle="modal" data-target="#entrep-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}>
						<td><b>Entrepreneurs Developed: {{ number_format($entrepreneurs) }}  </b></td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
				</table>
			</div>
			<div class="col-lg-2 col-xs-12 btn-side btn-group-vertical btn-accomplishment" role="group" aria-label="...">
				<button class="non-split btn btn-primary members-trigger" type="button" data-toggle="modal" data-target="#members-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><i class="fa fa-pencil fa-fw"></i>&nbsp; Members</button>
				<button class="non-split btn btn-primary add-igp-trigger" type="button" data-toggle="modal" data-target="#add-igp-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><i class="fa fa-plus fa-fw"></i>&nbsp; IGP</button>
				<div class="btn-group toggle-menu ">
					@if ($igps == '[]')
					<button type="button" class="non-split btn btn-primary  dropdown-toggle active" data-toggle="dropdown" aria-expanded="false">
					@else
					<button type="button" class="non-split btn btn-primary  dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
					@endif
						<i class="fa fa-bars fa-fw"></i>&nbsp; IGPs &nbsp; <span class="caret"></span>
					</button>
					<ul class="dropdown-menu side-menu-drop dropdown-menu-right"  role="menu">
					<li role="presentation" class="dropdown-header"><i class="fa fa-pencil fa-fw"></i> <b> IGPs & Investments Source </b></li>
						@foreach ($igps as $igp)
							<li data-toggle="modal" data-target="#edit-igp-modal" igp-id={{ $igp->id }} msme-id={{ $msme['id'] }} class="edit-igp-trigger" ><a href="#">{{ $igp->igp_name }}</i></a></li>
						@endforeach
					</ul>
				</div>
				<button class="non-split btn btn-primary jobs-trigger" type="button" data-toggle="modal" data-target="#jobs-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><i class="fa fa-pencil fa-fw"></i>&nbsp; Jobs</button>
				<button class="non-split btn btn-primary sales-trigger" type="button" data-toggle="modal" data-target="#sales-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><i class="fa fa-pencil fa-fw"></i>&nbsp; Sales</button>
				<button class="non-split btn btn-primary entrep-trigger" type="button" data-toggle="modal" data-target="#entrep-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><i class="fa fa-pencil fa-fw"></i>&nbsp; Entrepreneurs</button>
				
				@foreach($category as $key => $value) 
					<div class="btn-group toggle-menu dropup">
						<button type="button" class="non-split btn btn-primary dropdown-toggle @if($value=='[]') active @endif"  data-toggle="dropdown" aria-expanded="false">
							<i class="fa fa-bars fa-fw"></i>&nbsp; 
							{{ substr($key, 0, 10) }}
							@if( strlen($key) > 9)..@endif &nbsp; <span class="caret"></span>
						</button>
						<ul class="dropdown-menu side-menu-drop dropdown-menu-right list-group" role="menu">
							<li role="presentation" class="dropdown-header"><i class="fa fa-pencil fa-fw"></i> <b>{{ $key }}</b></li>
								@foreach($value as $v)
									<li><a href="" class="add-attendance-trigger" data-toggle="modal" w-sales={{ $v->w_sales }} act-name="{{ $v->name }} " act-id={{ $v->id }} data-target="#add-attendance-modal">{{ $v->name }}</a></li>
								@endforeach
						</ul>
					</div>
				@endforeach
			</div>
		</div>
		</div>
				
		@else 
		<div class="col-lg-12 home-logo" >
			<img class="dti-logo" src="{{ url('/img/dti-logo.jpg') }}"> <br>
			<img class="carp-logo" src="{{ url('/img/carp-logo.jpg') }}">
	    </div>
		@endif
		
<!-------------------- modal ---------------------->
	
	<!----------------- SALES -------------------->
	<div id="sales-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Sales</span></h4>
			    </div>
				 {{ Form::open(array('url' => 'provincial/igp_sales', 'class'=>'form-horizontal', 'role'=>'form')) }} 
			    <div class="modal-body">
					<div class="row inv-label">
						<div class="col-md-1">
							<div class="form-group">		
								{{ Form::label('', '', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('report_date_1', 'Date', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								{{ Form::label('monitored_1', 'Monitored Sales', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('established_1', 'Established Sales', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">		
								{{ Form::label('remarks_1', 'Remarks', array('class'=>'')) }}
							</div>
						</div>
					</div>
					<div class="row sales" >
						<div class="col-md-1 col-del">
							<button disabled="disabled" class="btn btn-default btn-del"><i class="fa fa-trash"></i></button>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								{{ Form::text('report_date_1', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="input-group">
								  <span class="input-group-addon">₱</span>
								  {{ Form::text('monitored_1', NULL, array('class'=>'form-control number')) }}
								</div>
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								<div class="input-group">
								  <span class="input-group-addon">₱</span>
								  {{ Form::text('established_1', NULL, array('class'=>'form-control number')) }}
								</div>
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								  {{ Form::textarea('remarks_1', NULL, array('class'=>'form-control', 'rows' => '1')) }}
							</div>
						</div>
					</div>
					@if ( isset($msme['id']) )
					{{ Form::hidden('msme_id', $msme['id'], array('class'=>'form-control')) }}
					{{ Form::hidden('sales_count', 1, array('class'=>'form-control')) }}
					<div class="new-sales"></div>
					<div class="form-group">	
					<button class="add_sales btn btn-default"  msme-id={{ $msme['id'] }} type="button"><i class="fa fa-plus"></i> Sales</button>
					</div>
					@endif
			    </div>
			    <div class="modal-footer">
				    <button type="button" onclick="reload_page_wrapper()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>

	<!----------------- Entrepreneurs -------------------->
	<div id="entrep-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Entrepreneurs</span></h4>
			    </div>
				 {{ Form::open(array('url' => 'provincial/entrepreneurs', 'class'=>'form-horizontal', 'role'=>'form')) }} 
			    <div class="modal-body">
					<div class="row inv-label">
						<div class="col-md-1">
							<div class="form-group">		
								{{ Form::label('', '', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('date_developed_1', 'Date Developed', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								{{ Form::label('first_name_1', 'First Name', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('last_name_1', 'Last Name', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('project_1', 'Project', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('fb_1', 'Farmer Beneficiaries (FB)', array('class'=>'')) }}
							</div>									
						</div>
					</div>
					<div class="row entrep" >

							<div class="col-md-1 col-del">
								<button disabled="disabled" class="btn btn-default btn-del"><i class="fa fa-trash"></i></button>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									{{ Form::text('date_developed_1', NULL, array('class'=>'form-control')) }}
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									  {{ Form::text('first_name_1', NULL, array('class'=>'form-control')) }}
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									  {{ Form::text('last_name_1', NULL, array('class'=>'form-control')) }}
								</div>
							</div>
							<div class="col-md-2">
								<div class="form-group">
									  {{ Form::text('project_1', NULL, array('class'=>'form-control')) }}
								</div>
							</div>
							<div class="col-md-3">
								<div class="form-group ">
									<div class="input-group">
									  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
									  {{ Form::text('fb_male_1', NULL, array('class'=>'form-control number','placeholder'=>'Male', 'id'=>'fb_male_1' )) }}
										  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
									  {{ Form::text('fb_female_1', NULL, array('class'=>'form-control number','placeholder'=>'Female', 'id'=>'fb_female_1')) }}
									</div>
								</div>
							</div>
					</div>
					@if ( isset($msme['id']) )
					{{ Form::hidden('msme_id', $msme['id'], array('class'=>'form-control')) }}
					{{ Form::hidden('entrep_count', 1, array('class'=>'form-control')) }}
					<div class="new-entrep"></div>
					<div class="form-group">	
					<button class="add_entrep btn btn-default"  msme-id={{ $msme['id'] }} type="button"><i class="fa fa-plus"></i> Entrepreneur</button>
					</div>
					@endif
			    </div>
			    <div class="modal-footer">
				    <button type="button" onclick="reload_page_wrapper()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
		
	<!----------------- Members -------------------->
	<div id="members-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-lg">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Members Assisted Log</span></h4>
			    </div>
				{{ Form::open(array('url' => 'provincial/msme_members', 'class'=>'form-horizontal', 'role'=>'form')) }}
				  <div class="modal-body">
					<div id="loader"><i class="fa fa-spinner fa-pulse"></i></div>
					<div class="row inv-label">
						<div class="col-md-1">
							<div class="form-group">		
								{{ Form::label('', '', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('report_date_member_1', 'Date', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('fb', 'Farmer Beneficiaries (FB)', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::label('lo', 'Lot Owners (LO)', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('ncb', 'Non-CARP Business (NCB)', array('class'=>'')) }}
							</div>
						</div>
					</div>
					<div class="row log" >
						<div class="col-md-1 col-del">
							<button disabled="disabled" class="btn btn-default btn-del"><i class="fa fa-trash"></i></button>
						</div>
						<div class="col-md-2">
							<div class="form-group">
								{{ Form::label('', 'Date', array('class'=>'res-label')) }}
								{{ Form::text('report_date_member_1', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group members">
								{{ Form::label('', 'Farmer Beneficiaries (FB)', array('class'=>'res-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('fb_male_1', NULL, array('class'=>'form-control number','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('fb_female_1', NULL, array('class'=>'form-control number','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-3">
							<div class="form-group members">
							{{ Form::label('', 'Lot Owners (LO)', array('class'=>'res-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('lo_male_1', NULL, array('class'=>'form-control number','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('lo_female_1', NULL, array('class'=>'form-control number','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-3">
							<div class="form-group members">
								{{ Form::label('', 'Non-CARP Business (NCB)', array('class'=>'res-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('ncb_male_1', NULL, array('class'=>'form-control number','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('ncb_female_1', NULL, array('class'=>'form-control number','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
					</div>
					@if ( isset($msme['id']) )
					{{ Form::hidden('msme_id', $msme['id'], array('class'=>'form-control')) }}
					{{ Form::hidden('log_count', 1, array('class'=>'form-control')) }}
					<div class="new-log"></div>
					<div class="form-group">	
						<button class="add_log btn btn-default" type="button"><i class="fa fa-plus"></i> Log</button>
					
					</div>
					@endif
					
			    </div>
			    <div class="modal-footer"><!--onclick="location.reload()"--> 
				    <button type="button" onclick="reload_page_wrapper()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
		
	<!----------------- JOBS -------------------->
	<div id="jobs-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-md">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Jobs Developed Log</span></h4>
			    </div>
				{{ Form::open(array('url' => 'provincial/jobs', 'class'=>'form-horizontal', 'role'=>'form')) }}
				  <div class="modal-body">
					<div class="row inv-label">
						<div class="col-md-1">
							<div class="form-group">		
								{{ Form::label('', '', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('report_date_job_1', 'Date', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
								{{ Form::label('igp', 'Activity', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('number', 'No. of Jobs', array('class'=>'')) }}
							</div>
						</div>
						
						
					</div>
					<div class="row job" >
						<div class="col-md-1 col-del">
							<button disabled="disabled" class="btn btn-default btn-del"><i class="fa fa-trash"></i></button>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::text('report_date_job_1', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-5">
							<div class="form-group">
							@if ( isset($msme['id']) )	{{ Form::select('igp_id_1', array(1 => ''), NULL,  array('id'=>'igp_dropdown', 'class'=>'form-control')) }} @endif
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">
								{{ Form::text('number_1', NULL, array('class'=>'form-control number')) }}
							</div>
						</div>
						
					</div>
					@if ( isset($msme['id']) )
					{{ Form::hidden('msme_id', $msme['id'], array('class'=>'form-control')) }}
					{{ Form::hidden('job_count', 1, array('class'=>'form-control')) }}
					<div class="new-job"></div>
					<div class="form-group">	
						<button class="add_job btn btn-default" type="button"><i class="fa fa-plus"></i> Log</button>
					</div>
					@endif
					
			    </div>
			    <div class="modal-footer">
				    <button type="button" onclick="reload_page_wrapper()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
		
	<!----------------- activities - attendance  -------------------->
	<div id="add-attendance-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-md">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span id="add-attendance-title"> </span></h4>
			    </div>
			
				{{ Form::open(array('url' => 'provincial/attendances', 'method'=>'put', 'id'=>'add-attendances-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="modal-body">
					<div class="row">
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('fb', 'Farmer Beneficiaries (FB)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('fb_male', NULL, array('class'=>'form-control attn1 number','placeholder'=>'Male', 'id'=>'fb_male' )) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('fb_female', NULL, array('class'=>'form-control attn2 number','placeholder'=>'Female', 'id'=>'fb_female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('lo', 'Lot Owners (LO)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('lo_male', NULL, array('class'=>'form-control attn3 number','placeholder'=>'Male', 'id'=>'lo_male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('lo_female', NULL, array('class'=>'form-control attn4 number','placeholder'=>'Female', 'id'=>'lo_female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('ncb', 'Non-CARP Business (NCB)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('ncb_male', NULL, array('class'=>'form-control attn5 number','placeholder'=>'Male', 'id'=>'ncb_male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('ncb_female', NULL, array('class'=>'form-control attn6 number','placeholder'=>'Female', 'id'=>'ncb_female')) }}
								</div>
							</div>
						</div>
						
					</div>
				       <div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('cost_carp', 'CARP (Cost)', array('class'=>'control-label')) }}
								<div class="input-group">
									<span class="input-group-addon">₱</span>
									{{ Form::text('cost_carp', NULL, array('class'=>'form-control number')) }}
								</div>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">	
								{{ Form::label('cost_others', 'Others (Cost)', array('class'=>'control-label')) }}
								<div class="input-group">
									<span class="input-group-addon">₱</span>
									{{ Form::text('cost_others', NULL, array('class'=>'form-control number')) }}
								</div>
							</div>
						</div>
					</div>
					<div class="row row-sales">
						<div class="col-md-12">
							<div class="form-group ">
								{{ Form::label('sales', 'Sales', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon">₱</span>
								  {{ Form::text('sales', NULL, array('class'=>'form-control number', 'id'=>'sales')) }}
								
								</div>
							</div>
						</div>
					</div>
					<div class="row">	
						<div class="col-md-12">
							<div class="form-group ">
								{{ Form::label('attn_remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('attn_remarks', NULL, array('class'=>'form-control','rows'=>'4')) }}
								 {{ Form::hidden('activity_id', NULL, array('class'=>'form-control', 'id'=>'activity_id')) }}
							</div>
						</div>
					</div>
				
			    </div>
			    <div class="modal-footer">
					<!-- <button type="button"  class="btn btn-warning del-attn-trigger"><i class="fa fa-trash-o fa-fw"></i> Delete</button> -->
					<button type="button"   class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary submit-attn"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					
			    </div> <!-- onclick="location.reload()" -->
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
	<!-------------------------------- consultancy
		<div id="add-con-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-md">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Consultancy </span></h4>
			    </div>
				{{ Form::open(array('url' => 'provincial/activities', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group ">
								{{ Form::label('c', 'Consultancy', array('class'=>'control-label')) }}
								{{ Form::textarea('sales', NULL, array('class'=>'form-control', 'rows'=>'2')) }}
								
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group ">
								{{ Form::label('sales', 'Date', array('class'=>'control-label')) }}
								{{ Form::text('sales', NULL, array('class'=>'form-control')) }}
								
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group ">
								{{ Form::label('sales', 'Man-hours', array('class'=>'control-label')) }}
								{{ Form::text('sales', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Farmer Beneficiaries (FB)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Lot Owners (LO)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Non-CARP Business (NCB)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="row">	
						<div class="col-md-12">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('investment_amount_1', NULL, array('class'=>'form-control','rows'=>'4')) }}
							</div>
						</div>
					</div>
				
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
		<!-------------------------------- study 
		<div id="add-study-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-md">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Study </span></h4>
			    </div>
				{{ Form::open(array('url' => 'provincial/activities', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group ">
								{{ Form::label('sales', 'Title', array('class'=>'control-label')) }}
								{{ Form::textarea('sales', NULL, array('class'=>'form-control', 'rows'=>'2')) }}
								
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('sales', 'Date', array('class'=>'control-label')) }}
								{{ Form::text('sales', NULL, array('class'=>'form-control')) }}
								
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('sales', 'Source', array('class'=>'control-label')) }}
								{{ Form::text('sales', NULL, array('class'=>'form-control')) }}
								
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('sales', 'Project Cost', array('class'=>'control-label')) }}
								{{ Form::text('sales', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						
					</div>
					<div class="row">
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Farmer Beneficiaries (FB)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Lot Owners (LO)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-4">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Non-CARP Business (NCB)', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Male')) }}
									  <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control','placeholder'=>'Female')) }}
								</div>
							</div>
						</div>
						
					</div>
					
					<div class="row">	
						<div class="col-md-12">
							<div class="form-group ">
								{{ Form::label('investment_amount_1', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('investment_amount_1', NULL, array('class'=>'form-control','rows'=>'4')) }}
							</div>
						</div>
					</div>
				
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div> ----------------------->	
@stop

@section('additional_scripts')
@parent
<script>
	
	 $(document).ready(function($){
		$( "#date_monitored_1" ).datepicker({
			  showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			});
			$( "#report_date_1" ).datepicker({
			 showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			});
			$( "#date_developed_1" ).datepicker({
			  showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			});
			$( "#report_date_member_1" ).datepicker({
			 showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			});
			$( "#report_date_job_1" ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			});
	 
		
		$.get("{{ url('provincial/repairdropdown')}}", { option: $('#arc_dropdown').val() }, 
			
            function(data) {

                var msme = $('#msme_dropdown');
					
                    msme.empty();

                    $.each(data, function(key, value) {   

              msme

              .append($("<option></option>")
			  .attr( "key", key )	
              .attr( "value", value )
			  
			 

              .text(key)); 
              });
            });
		$( ".btn-go" ).removeClass('active');
	//	$('#arc_dropdown').val('');
		$('#arc_dropdown').change(function(){ 
			//$( ".btn-go" ).focus();
			$.get("{{ url('provincial/repairdropdown')}}", { option: $('#arc_dropdown').val() }, 
			
            function(data) {

                var msme = $('#msme_dropdown');

                    msme.empty();

                    $.each(data, function(key, value) {   

              msme

              .append($("<option></option>")
			  .attr( "key", key )	
              .attr( "value", value )
			  
			 

              .text(key)); 
              });
            });
		
		});
	});

// --------------------------- SALES ---------------------------
		$(".sales-trigger").click( function() { 
			$( ".new-sales" ).empty();
			var msme_id = $(this).attr("msme-id");
			var link = '{{ url('provincial/igp_sales') }}' + '/' + msme_id; 
		   $.ajax({ 
				type: 'GET', 
				url:  link + '/edit',
				dataType: 'json',
				success: function (data) {  
					var x = 1;
					
					for (var c = 0; c < data.length; c++)
					{	
						date = new Date(data[c].report_date);
						date = new Date(date.setDate(date.getDate()));
						html = '<div class="row">';
						html += '<div class="col-md-1 col-del"><button type="button" src-date="'+ $.datepicker.formatDate('M d, yy', date ) +'" src-id="'+ data[c].id +'" id="delete-source-trigger-' + x + '" class="btn btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
						html += '<div class="col-md-2"><div class="form-group">';
						html += '<input class="form-control"  value="'+ $.datepicker.formatDate('M d, yy', date ) +'"  name="edit_report_date_' + x + '" type="text" id="edit_report_date_' + x + '"></div></div>';
						html += '<div class="col-md-2"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
						html += '<input class="form-control number"  value="'+numberWithCommas(data[c].monitored) +'" name="edit_monitored_' + x + '" type="text" id="edit_monitored_"></div>';
						html += '<input type="hidden" value="'+ data[c].id +'" name="id_'+ x +'" >';
						html += '<input type="hidden" value="'+ x +'" name="edit_sales_count"></div></div>';
						html += '<input type="hidden" value="'+ msme_id +'" name="msme_id">';
						html += '<div class="col-md-2"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
						html += '<input class="form-control number"  value="'+numberWithCommas(data[c].established) +'" name="edit_established_' + x + '" type="text" id="edit_established_'+x+'"></div></div></div>';
						html += '<div class="col-md-5"><div class="form-group"><input class="form-control"  value="'+data[c].remarks +'" name="edit_remarks_' + x + '" type="text" id="edit_remarks_'+x+'"></div></div></div>';
						$('.new-sales').append(html); 
						
						$("#edit_report_date_"+x ).datepicker({
							showButtonPanel: true,
							dateFormat: "M d, yy",
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
						});
						
						$('input.number').keyup(function (event) {
							if (event.which >= 37 && event.which <= 40) {
								event.preventDefault();
							}
							var currentVal = $(this).val();
							var testDecimal = testDecimals(currentVal);
							if (testDecimal.length > 1) {
								onsole.log("You cannot enter more than one decimal point");
								currentVal = currentVal.slice(0, -1);
							}
							$(this).val(replaceCommas(currentVal));
						});
						
						$("#delete-source-trigger-"+x).click( function() {
							var id = $(this).attr("src-id"); 
							var date = $(this).attr("src-date"); 
							var link = '{{ url('provincial/igp_sales') }}' + '/' + id; 

							if (confirm('Delete log date '+date+' ?'))
							{
								$.ajax({ 
									type: 'DELETE', 
									url:  link,
									success: function (msg) {  
									}
									});
								 $(this).parent().parent().empty();
							 }
						});
						
						x++;
					}
					if ( x > 1 ) { $( ".sales" ).empty();  }
				}
			});
		});
		//--------------- add sales
		var a = 1;
		
		$(".add_sales").click( function() { 
			a++;
			var html = '<div class="row">';
			html += '<div class="col-md-1 col-del"><button type="button" onclick = "$(this).parent().parent().remove()" class="btn btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
			html += '<div class="col-lg-2"><div class="form-group">';
			html += '<input class="form-control"  name="report_date_' + a + '" type="text" id="report_date_' + a + '"></div></div>';
			html += '<div class="col-lg-2"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
			html += '<input class="form-control number"  name="monitored_' + a + '" type="text"></div>';
			html += '<input type="hidden" value="'+ a +'" name="sales_count"></div></div>';
			html += '<div class="col-lg-2"><div class="form-group"><div class="input-group"><span class="input-group-addon">₱</span>';
			html += '<input class="form-control number" name="established_' + a + '" type="text"></div></div></div>';
			html += '<div class="col-md-5"><div class="form-group"><input class="form-control" name="remarks_' + a + '" type="text" id="remarks_'+a+'"></div></div></div>';
			
			$('.new-sales').append(html); 
			$( "#report_date_"+a ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			}); 
			
			$('input.number').keyup(function (event) {
				if (event.which >= 37 && event.which <= 40) {
					event.preventDefault();
				}
				var currentVal = $(this).val();
				var testDecimal = testDecimals(currentVal);
				if (testDecimal.length > 1) {
					onsole.log("You cannot enter more than one decimal point");
					currentVal = currentVal.slice(0, -1);
				}
				$(this).val(replaceCommas(currentVal));
			});
		});

// --------------------------- jobs   ---------------------------
		$(".jobs-trigger").click( function() { 
			$.get("{{ url('provincial/igp_dropdown')}}", 
			 function(data) {
				var igp = $('#igp_dropdown');
					igp.empty();
					$.each(data, function(key, value) {   
						igp
						.append($("<option></option>")
						.attr( "key", key )	
						.attr( "value", value )
						.text(key)); 
					});
					});
					
			var  igp_value = new Array();
			
			$( ".new-job" ).empty();
			
			var msme_id = $(this).attr("msme-id");
			var link = '{{ url('provincial/jobs') }}' + '/' + msme_id; 
		   $.ajax({ 
				type: 'GET', 
				url:  link ,
				dataType: 'json',
				success: function (data) {  
					var x = 1;
					for (var c = 0; c < data.length; c++)
					{	
						date = new Date(data[c].report_date);
						date = new Date(date.setDate(date.getDate()));
						var html = '<div class="row">';
						html += '<div class="col-md-1 col-del"><button src-date="'+ $.datepicker.formatDate('M d, yy', date) +'" src-id="'+ data[c].id +'" id="delete-job-trigger-' + x + '" type="button" class="btn  btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
						html += '<div class="col-lg-3"><div class="form-group">';
						html += '<input class="form-control"  value="'+$.datepicker.formatDate('M d, yy', date)+'" name="edit_report_date_job_' + x + '" type="text" id="edit_report_date_job_' + x + '"></div></div>';
						html += '<div class="col-md-5"><div class="form-group">';
						html += '<select class="form-control"  id="edit_igp_dropdown_'+ x +'" name="edit_igp_id_' + x + '" ></select></div></div>';
						html += '<div class="col-lg-3"><div class="form-group">';
						html += '<input class="form-control number" value="'+ numberWithCommas(data[c].number) +'" name="edit_number_' + x + '" type="text" id="edit_number_' + x + '"></div></div>';
						html += '<input type="hidden" value="'+ data[c].id +'" name="id_' + x + '">';
						html += '<input type="hidden" value="'+ x +'" name="edit_job_count">';
						
						$('.new-job').append(html); 
					   
					    igp_value[x] = data[c].igp_id;
						
						$("#edit_igp_dropdown_" + c).val(data[c].igp_id);
						
						$("#edit_report_date_job_"+ x ).datepicker({
							showButtonPanel: true,
							dateFormat: "M d, yy",
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
						});
						
						
						$('input.number').keyup(function (event) {
							if (event.which >= 37 && event.which <= 40) {
								event.preventDefault();
							}
								var currentVal = $(this).val();
								var testDecimal = testDecimals(currentVal);
								if (testDecimal.length > 1) {
									console.log("You cannot enter more than one decimal point");
									currentVal = currentVal.slice(0, -1);
								}
								$(this).val(replaceCommas(currentVal));
						});
						
						$("#delete-job-trigger-"+x).click( function() {
							var id = $(this).attr("src-id"); 
							var date = $(this).attr("src-date"); 
							var link = '{{ url('provincial/jobs') }}' + '/' + id; 
							
							if (confirm('Delete log date '+date+' ?'))
							{
								$.ajax({ 
									type: 'DELETE', 
									url:  link,
									success: function (msg) {  
									}
									});
								 $(this).parent().parent().empty();
							 }
						});
						
						x++;
					}
					if ( x > 1 ) { $( ".job" ).empty();  } 
						
						
						$.get("{{ url('provincial/igp_dropdown')}}", 
							 function(data) { 
								for (var c = 1; c <= x; c++)
								{		
									var igp = $("#edit_igp_dropdown_" + c); 
									igp.empty();	
									$.each(data, function(key, value) {   
										igp
										.append($("<option></option>")
										.attr( "key", key )	
										.attr( "value", value )
										.text(key)); 
									});
									$("#edit_igp_dropdown_" + c).val(igp_value[c]); 
								}
							});
					
				}
				
			});
		});
		//--------------- add log
		var e = 1;
		
		$(".add_job").click( function() { 
			e++;
			var html = '<div class="row">';
			html += '<div class="col-md-1 col-del"><button type="button" onclick = "$(this).parent().parent().remove()" class="btn btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
			html += '<div class="col-lg-3"><div class="form-group">';
			html += '<input class="form-control"  name="report_date_job_' + e + '" type="text" id="report_date_job_' + e + '"></div></div>';
			html += '<div class="col-md-5"><div class="form-group">';
			html += '<select class="form-control" name="igp_id_' + e + '" id="igp_dropdown_' + e + '"><option></option></select></div></div>';
			html += '<div class="col-lg-3"><div class="form-group">';
			html += '<input class="form-control"  name="number_' + e + '" type="text" id="number_' + e + '"></div></div>';
			html += '<input type="hidden" value="'+ e +'" name="job_count">';
			$('.new-job').append(html); 
			$( "#report_date_job_"+e ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			});
			$('input.number').keyup(function (event) {
				if (event.which >= 37 && event.which <= 40) {
					event.preventDefault();
				}
				var currentVal = $(this).val();
				var testDecimal = testDecimals(currentVal);
				if (testDecimal.length > 1) {
					console.log("You cannot enter more than one decimal point");
					currentVal = currentVal.slice(0, -1);
				}
				$(this).val(replaceCommas(currentVal));
			});	
			$.get("{{ url('provincial/igp_dropdown')}}", 
			 function(data) {
				var igp = $("#igp_dropdown_" + e );
					igp.empty();
					$.each(data, function(key, value) {   
						igp
						.append($("<option></option>")
						.attr( "key", key )	
						.attr( "value", value )
						.text(key)); 
					});
					});
			
				});
		
// --------------------------- Msme members  ---------------------------
		$(".members-trigger").click( function() { 

			$( ".new-log" ).empty();
			var msme_id = $(this).attr("msme-id");
			var link = '{{ url('provincial/msme_members') }}' + '/' + msme_id; 
		   $.ajax({ 
				type: 'GET', 
				url:  link + '/edit',
				dataType: 'json',
				success: function (data) {  
					$( "#loader" ).hide();
					var x = 1;
					for (var c = 0; c < data.length; c++)
					{	
						date = new Date(data[c].report_date);
						date = new Date(date.setDate(date.getDate()));
						var html = '<div class="row">';
						html += '<div class="col-md-1 col-del"><button src-date="'+$.datepicker.formatDate('M d, yy', date)+'" src-id="'+ data[c].id +'" id="delete-log-trigger-' + x + '" type="button" class="btn  btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
						html += '<div class="col-md-2"><div class="form-group">';
						html += '<label for="" class="res-label">Date</label><input class="form-control"  value="'+$.datepicker.formatDate('M d, yy', date)+'" name="edit_report_date_member_' + x + '" type="text" id="edit_report_date_member_' + x + '"></div></div>';
						html += '<div class="col-md-3"><div class="form-group members"><label for="" class="res-label">Farmer Beneficiaries (FB)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
						html += '<input class="form-control number"  value="'+numberWithCommas(data[c].fb_male)+'" name="edit_fb_male_' + x + '" placeholder="Male" type="text">';
						html += '<span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span><input class="form-control number"  value="'+numberWithCommas(data[c].fb_female)+'" name="edit_fb_female_' + x + '" placeholder="Female" type="text"></div></div></div>';
						html += '<div class="col-md-3"><div class="form-group members"><label for="" class="res-label">Lot Owners (LO)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
						html += '<input class="form-control number" value="'+numberWithCommas(data[c].lo_male)+'" name="edit_lo_male_' + x + '" placeholder="Male" type="text">';
						html += '<span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span><input class="form-control number" value="'+numberWithCommas(data[c].lo_female)+'" name="edit_lo_female_' + x + '" placeholder="Female" type="text"></div></div></div>';
						html += '<div class="col-md-3"><div class="form-group members"><label for="" class="res-label">Non-CARP Business (NCB)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
						html += '<input class="form-control number" value="'+numberWithCommas(data[c].ncb_male)+'" name="edit_ncb_male_' + x + '" placeholder="Male" type="text">';
						html += '<span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span><input class="form-control number" value="'+numberWithCommas(data[c].ncb_female)+'" name="edit_ncb_female_' + x + '" placeholder="Female" type="text"></div></div></div>';
						html += '<input type="hidden" value="'+ data[c].id +'" name="edit_id_' + x + '">';
						html += '<input type="hidden" value="'+ d +'" name="edit_log_count">';
						
						$('.new-log').append(html); 
						
						$("#edit_report_date_member_"+x ).datepicker({
							showButtonPanel: true,
							dateFormat: "M d, yy",
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
						});
						
						$('input.number').keyup(function (event) {
							if (event.which >= 37 && event.which <= 40) {
								event.preventDefault();
							}
								var currentVal = $(this).val();
								var testDecimal = testDecimals(currentVal);
								if (testDecimal.length > 1) {
									console.log("You cannot enter more than one decimal point");
									currentVal = currentVal.slice(0, -1);
								}
								$(this).val(replaceCommas(currentVal));
						});
						
						$("#delete-log-trigger-"+x).click( function() {
							var id = $(this).attr("src-id"); 
							var date = $(this).attr("src-date"); 
							
							var link = '{{ url('provincial/msme_members') }}' + '/' + id; 
							
							if (confirm('Delete log date '+date+' ?'))
							{
								$.ajax({ 
									type: 'DELETE', 
									url:  link,
									success: function (msg) {  
									}
									});
								 $(this).parent().parent().empty();
							 }
						});
						
						x++;
					}
					if ( x > 1 ) { $( ".log" ).empty();  }
				}
			});
		});
		//--------------- add log
		var d = 1;
		
		$(".add_log").click( function() { 
			d++;
			var html = '<div class="row">';
			html += '<div class="col-md-1 col-del"><button type="button" onclick = "$(this).parent().parent().remove()" class="btn btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
			html += '<div class="col-md-2"><div class="form-group">';
			html += '<label for="" class="res-label">Date</label><input class="form-control"  name="report_date_member_' + d + '" type="text" id="report_date_member_' + d + '"></div></div>';
			html += '<div class="col-md-3"><div class="form-group members"><label for="" class="res-label">Farmer Beneficiaries (FB)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
			html += '<input class="form-control number"  name="fb_male_' + d + '" placeholder="Male" type="text">';
			html += '<span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span><input class="form-control number"  name="fb_female_' + d + '" placeholder="Female" type="text"></div></div></div>';
			html += '<div class="col-md-3"><div class="form-group members"><label for="" class="res-label">Lot Owners (LO)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
			html += '<input class="form-control number"  name="lo_male_' + d + '" placeholder="Male" type="text">';
			html += '<span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span><input class="form-control number"  name="lo_female_' + d + '" placeholder="Female" type="text"></div></div></div>';
			html += '<div class="col-md-3"><div class="form-group members"><label for="" class="res-label">Non-CARP Business (NCB)</label><div class="input-group"><span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
			html += '<input class="form-control number"  name="ncb_male_' + d + '" placeholder="Male" type="text">';
			html += '<span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span><input class="form-control number"  name="ncb_female_' + d + '" placeholder="Female" type="text"></div></div></div>';
			
			html += '<input type="hidden" value="'+ d +'" name="log_count">';
			$('.new-log').append(html); 
			$( "#report_date_member_"+d ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			}); 
			$('input.number').keyup(function (event) {
				if (event.which >= 37 && event.which <= 40) {
					event.preventDefault();
				}
				var currentVal = $(this).val();
				var testDecimal = testDecimals(currentVal);
				if (testDecimal.length > 1) {
					console.log("You cannot enter more than one decimal point");
					currentVal = currentVal.slice(0, -1);
				}
				$(this).val(replaceCommas(currentVal));
			});			
			
		});
		
// --------------------------- Entrepreneurs ---------------------------
		$(".entrep-trigger").click( function() { 
			$( ".new-entrep" ).empty();
			var msme_id = $(this).attr("msme-id");
			var link = '{{ url('provincial/entrepreneurs') }}' + '/' + msme_id; 
			
		   $.ajax({ 
				type: 'GET', 
				url:  link + '/edit',
				dataType: 'json',
				success: function (data) {  
					var x = 1;
					
					for (var c = 0; c < data.length; c++)
					{	
						date = new Date(data[c].date_developed)
						date = new Date(date.setDate(date.getDate()));
						html = '<div class="row">';
						html += '<div class="col-md-1 col-del"><button type="button" src-name="'+data[c].first_name+' '+data[c].last_name+'" src-id="'+ data[c].id +'" id="delete-entrep-trigger-' + x + '" class="btn btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
						html += '<div class="col-md-2"><div class="form-group">';
						html += '<input class="form-control"  value="'+$.datepicker.formatDate('M d, yy', date)+'"  name="edit_date_developed_' + x + '" type="text" id="edit_date_developed_' + x + '"></div></div>';
						html += '<div class="col-md-2"><div class="form-group">';
						html += '<input class="form-control"  value="'+data[c].first_name+'" name="edit_first_name_' + x + '" type="text" ></div></div>';
						html += '<input type="hidden" value="'+ data[c].id +'" name="id_'+ x +'">';
						html += '<input type="hidden" value="'+ x +'" name="edit_entrep_count">';
						html += '<input type="hidden" value="'+ msme_id +'" name="msme_id">';
						html += '<div class="col-md-2"><div class="form-group">';
						html += '<input class="form-control"  value="'+data[c].last_name+'" name="edit_last_name_' + x + '" type="text"></div></div>';
						html += '<div class="col-md-2"><div class="form-group">';
						html += '<input class="form-control"  value="'+data[c].project+'" name="edit_project_' + x + '" type="text"></div></div>';
						html += '<div class="col-md-3"><div class="form-group "><div class="input-group"> <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
						html += '<input class="form-control number"  value="'+data[c].fb_male+'" placeholder="Male" id="fb_male" name="edit_fb_male_' + x + '" type="text">';
						html += ' <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>';
						html += '<input class="form-control number" value="'+data[c].fb_female+'" placeholder="Female" id="fb_male" name="edit_fb_female_' + x + '" type="text"></div></div></div>';
						html += '</div>';
						
						$('.new-entrep').append(html); 
						$("#edit_date_developed_"+x ).datepicker({
							showButtonPanel: true,
							dateFormat: "M d, yy",
							changeMonth: true,
							changeYear: true,
							showOtherMonths: true,
							selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
						});
						$('input.number').keyup(function (event) {
							if (event.which >= 37 && event.which <= 40) {
								event.preventDefault();
							}
							var currentVal = $(this).val();
							var testDecimal = testDecimals(currentVal);
							if (testDecimal.length > 1) {
								console.log("You cannot enter more than one decimal point");
								currentVal = currentVal.slice(0, -1);
							}
							$(this).val(replaceCommas(currentVal));
						});	
						$("#delete-entrep-trigger-"+x).click( function() {
							var id = $(this).attr("src-id"); 
							var name = $(this).attr("src-name"); 
							var link = '{{ url('provincial/entrepreneurs') }}' + '/' + id; 
							
							if (confirm('Delete Entrepreneur '+name+' ?'))
							{
								$.ajax({ 
									type: 'DELETE', 
									url:  link,
									success: function (msg) {  
									}
									});
								 $(this).parent().parent().empty();
							 }
						});
						
						x++;
					}
					if ( x > 1 ) { $( ".entrep" ).empty();  }
				}
			});
		});
		//--------------- add entrep
		var b = 1;
		
		$(".add_entrep").click( function() { 
			b++;
			var html = '<div class="row">';
			html += '<div class="col-md-1 col-del"><button type="button" onclick = "$(this).parent().parent().remove()" class="btn btn-default btn-del"><i class="fa fa-trash-o"></i></button></div>';
			html += '<div class="col-lg-2"><div class="form-group">';
			html += '<input class="form-control"  name="date_developed_' + b + '" type="text" id="date_developed_' + b + '"></div></div>';
			html += '<div class="col-lg-2"><div class="form-group">';
			html += '<input class="form-control"  name="first_name_' + b + '" type="text"></div>';
			html += '<input type="hidden" value="'+ b +'" name="entrep_count"></div>';
			html += '<div class="col-lg-2"><div class="form-group">';
			html += '<input class="form-control" name="last_name_' + b + '" type="text"></div></div>';
			html += '<div class="col-lg-2"><div class="form-group">';
			html += '<input class="form-control"  name="project_' + b + '" type="text"></div></div>';
			html += '<div class="col-md-3"><div class="form-group "><div class="input-group"> <span class="input-group-addon"><i class="fa fa-male fa-fw"></i></span>';
			html += '<input class="form-control number" placeholder="Male" id="fb_male" name="fb_male_' + b + '" type="text">';
			html += ' <span class="input-group-addon"><i class="fa fa-female fa-fw"></i></span>';
			html += '<input class="form-control number" placeholder="Female" id="fb_male" name="fb_female_' + b + '" type="text"></div></div></div>';
			html += '</div>';
							
			$('.new-entrep').append(html); 
			$( "#date_developed_"+b ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true,
				minDate: new Date({{Session::get('acc_year')}}, {{Session::get('start_month')}} - 1),
				maxDate: new Date({{Session::get('acc_year')}}, {{Session::get('end_month')}} - 1, 31)
			}); 
			
		});
//----------------------------------------------------- ACTIVITIES -------------------------------------
		$(".add-attendance-trigger").click( function() {

			$(".submit-attn").click(function( event ) {
				if($(".attn1").val() != ''|| $(".attn2").val() != '' || $(".attn3").val() != '' || $(".attn4").val() != '' ||
									$(".attn5").val() != '' || $(".attn6").val() != ''  )
				{
					$( "submit-attn" ).unbind('click');
				}
				else{
					event.preventDefault();
					alert('Enter Attendance!');
				}
			});
            var w_sales = $(this).attr("w-sales");
			var act_id = $(this).attr("act-id"); 
			var act_name = $(this).attr("act-name"); 
			$(".del-attn-trigger").hide();
			
			if (w_sales=='1') { 
				$('.row-sales').show();
				$("#add-attendance-title").html(' ' +act_name+ 'Sales & Attendance');
			}
			else {
				$('.row-sales').hide();
				$("#add-attendance-title").html(' ' +act_name+ ' Attendance');
			}
			 var link = '{{ url('provincial/attendances') }}' + '/' + act_id;
			 $.ajax({ 
                type: 'GET', 
                url:  link,
                dataType: 'json',
                success: function (data) { 
						if (data!='')
						{	
							$.each(data, function(index, value) {
								
								$("#fb_male").val(numberWithCommas(value.fb_male)); 
								$("#fb_female").val(numberWithCommas(value.fb_female));
								$("#lo_male").val(numberWithCommas(value.lo_male));
								$("#lo_female").val(numberWithCommas(value.lo_female));
								$("#ncb_male").val(numberWithCommas(value.ncb_male));
								$("#ncb_female").val(numberWithCommas(value.ncb_female));		
								$("#sales").val(numberWithCommas(value.sales));
								$("#cost_carp").val(numberWithCommas(value.cost_carp));
								$("#cost_others").val(numberWithCommas(value.cost_others));
								$("#attn_remarks").val(value.remarks);
								$("#add-attendances-form").attr('action', '{{ url('provincial/attendances') }}' + '/' + value.id);
								
								$(".del-attn-trigger").show();
								$(".del-attn-trigger").click( function() {
									if (confirm('Are you sure you want to delete the attendance₱'))
									{	
										$('input[name=_method]').val('DELETE');
										//$("#add-attendances-form").submit();
										
										$.ajax({ 
											type: 'DELETE', 
											url:  '{{ url('provincial/attendances') }}' + '/' + value.id,
											success: function (msg) {  
												$("#fb_male").val(null);
												$("#fb_female").val(null);
												$("#lo_male").val(null);
												$("#lo_female").val(null);
												$("#ncb_male").val(null);
												$("#ncb_female").val(null);		
												$("#sales").val(null);
												$("#cost_carp").val(null);
												$("#cost_others").val(null);
												$("#attn_remarks").val(null);
											}
										});
									 }
									
								});
							});
							
						}
						else {
								$('input').val("");
								$("#activity_id").val(act_id);
								$('input[name=_method]').remove();
								$('input[name=_token]').remove();
								$("#add-attendances-form").attr('action', '{{ url('provincial/attendances') }}');
						}
                }
            });
        });
		
function reload_page_wrapper() 
{
	$('#table-wrapper').load(document.URL +  ' #table-wrapper');

}
</script>
@stop