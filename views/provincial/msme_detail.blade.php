@extends('provincial/modules')
@section('inner_content')
            <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header">
				<i class="fa fa-angle-double-right"></i><span><a href="../msmes">MSME</a></span>
				<i class="fa fa-angle-double-right"></i><span>{{ $msme['msme_name'] }}</span>
			</h3>					
		</div>
	    </div>
           
	    <div class="row table-wrapper-msme-details">
			<div class="col-lg-10">
				<table class="table-bordered table-msme-details">
					<tr>
						<td><b>MSME:<b>
						{{ $msme['msme_name'] }}</td>
					</tr>
					<tr>
						<td><b>ARC:
						 @if ($msme->arc()->pluck('name')) 
							{{ $msme->arc()->pluck('name') }}
							 @else Non-ARC
							 @endif </b> </td>
						</tr>
				</table>
			</div>
			<div class="col-lg-2">
				<button class="btn btn-primary add-igp-trigger" type="button" data-toggle="modal" data-target="#add-igp-modal"  msme-name="{{ $msme['msme_name']}}" msme-id={{ $msme['id'] }}><i class="fa fa-plus fa-fw"></i> IGP</button>
			</div>
		</div>
	
				<div class="row">
					<div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
											<th class="actions-1"></th>
                                            <th>Income Generating Projects</th>
                                            <th>Date Started</th>
											<th>Products</th>
											<th>Sales</th>
											<th>Sources of Funds</th>
											<th class="actions-1"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="char">
							
					@foreach($igps as $igp)
					    <tr class="odd gradeX">
						<td>
							<!-- <span data-toggle="tooltip" title={{ "'Edit ".$igp->igp_name."'" }}>
								<button type="button" data-toggle="modal" data-target="#edit-igp-modal" class="btn btn-primary edit-igp-trigger " igp-id={{ $igp->id }}><i class="fa fa-pencil-square-o"></i></button>
							</span> -->
						</td>
						<td >{{ $igp->igp_name }}</td>
						<td>{{ $igp->date_started }}</td>
						<td>{{ $igp->igp_products }}</td>
						<td>{{ $igp->product_sales }}</td>
						<td>	
							<div class="source-list">
							<?php $c[$igp->id] = NULL; ?>
							@foreach($sources as $source)
								@if ($source->igp_id == $igp->id) 
									<?php $c[$igp->id] = 1; ?>
									<li> {{ $source->investment_source }} </li> 
								@endif	
							@endforeach
							</div>
							<!-- @if ($c[$igp->id])
								<span data-toggle="modal" data-target="#edit-source-modal" >
									<button type="button" data-toggle="tooltip" title="Edit Funds Source" class="btn btn-default edit-source-trigger" igp-id={{ $igp->id }}><i class="fa fa-pencil-square-o"></i></button>
								</span>
							@endif	-->
						</td>
						{{ Form::open(array('url' => 'provincial/igp/'.$igp->id, 'method' => 'delete')) }}
						{{ Form::hidden('msme_id', $msme['id'], array('class'=>'form-control')) }}
						<td class="actions-1">
							<span data-toggle="tooltip" title={{ "'Delete ".$igp->igp_name."'" }}>
								<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $igp->igp_name }}?' )" ><i class="fa fa-trash"></i></button>
							</span>
							<!-- <span data-toggle="tooltip" title={{ "'Add Funds Source to ".$igp->igp_name."'" }}>
								<button type="button" data-toggle="modal" data-target="#add-source-modal" class="btn btn-warning add-source-trigger" igp-id={{ $igp->id }}><i class="fa fa-plus"></i> Funds Source</button>
							</span> -->
						</td>
						{{ Form::close() }}
					@endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
				</div>
                <!-- /.col-lg-12 -->
            </div>
			
		<!----------- EDIT IGP MODAL -------------->
		<!-- <div id="edit-igp-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit Income Generating Project (IGP)</h4>
				</div>

				{{ Form::open(array('url' => 'provincial/igp', 'method' => 'put', 'id'=>'edit-igp-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('edit_igp_name', 'IGP Name', array('class'=>'control-label')) }}
								{{ Form::text('edit_igp_name', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">	
								{{ Form::label('edit_date_started', 'Date Started', array('class'=>'control-label')) }}
								{{ Form::text('edit_date_started', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('edit_remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('edit_remarks', NULL, array('class'=>'form-control','rows'=>'1')) }}
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Products', array('class'=>'control-label')) }}
								{{ Form::text('edit_igp_products', NULL, array('class'=>'form-control', 'placeholder'=>'e.g. copra, cacao beans, tablea')) }}
							</div>
						</div>
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('edit_product_category', 'Category', array('class'=>'control-label')) }}
								{{ Form::text('edit_product_category', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						
						<div class="col-md-2">
							<div class="form-group">	
								{{ Form::label('edit_product_sales', 'Product Sales', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon">?</span>
								  {{ Form::text('edit_product_sales', NULL, array('class'=>'form-control')) }}
								</div>
							</div>
						</div>
						
						<div class="col-md-3">
							<div class="form-group">		
								{{ Form::label('edit_product_remarks', 'Remarks', array('class'=>'control-label')) }}
								{{ Form::textarea('edit_product_remarks', NULL, array('class'=>'form-control','rows'=>'1')) }}
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'No. of Jobs', array('class'=>'control-label')) }}
								{{ Form::text('edit_igp_products', NULL, array('class'=>'form-control')) }}
							</div>

						</div>
						<div class="col-md-10">
							<div class="form-group">		
								{{ Form::label('edit_product_category', 'Location', array('class'=>'control-label')) }}
								{{ Form::text('edit_product_category', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-2">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'No. of Entrepreneurs', array('class'=>'control-label')) }}
								{{ Form::text('edit_igp_products', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-10">
							<div class="form-group">		
								{{ Form::label('edit_product_category', 'Location', array('class'=>'control-label')) }}
								{{ Form::text('edit_product_categorys', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					
					<div class="row inv-label">
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Investments Source', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Amount', array('class'=>'')) }}
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">		
								{{ Form::label('edit_igp_products', 'Date Monitored', array('class'=>'')) }}
							</div>
						</div>
						
					</div>
					
					<div class="edit-investment"></div>	
					<div class="new-investment"></div>
					
					<div class="form-group">	
						<button class="edit_add_investment btn btn-default" type="button"><i class="fa fa-plus"></i> Funds Source</button>
					</div>
					
						{{ Form::hidden('msme_id', NULL, array('id'=>'igp_msme_id', 'class'=>'form-control')) }}
		
					<div class="product"></div>
					<div class="form-group">
					</div>
				</div>
				
				
					{{ Form::hidden('edit_msme_id', NULL, array('id' => 'edit_msme_id', 'class'=>'form-control')) }}
				<div class="modal-footer">
					<button type="button" onclick="location.reload()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					{{ Form::close() }}
				</div>
				</div>
			</div>
		</div>
		
		<!----------- ADD FUNDS SOURCE MODAL 
		<div id="add-source-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
				
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New Funds Source</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(array('url' => 'provincial/investments', 'class'=>'form-horizontal', 'role'=>'form')) }}
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">		
								{{ Form::label('investment_source_1', 'Investment Source', array('class'=>'control-label')) }}
								{{ Form::text('investment_source_1', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">	
								{{ Form::label('investment_amount_1', 'Investment Amount', array('class'=>'control-label')) }}
								<div class="input-group">
								  <span class="input-group-addon">?</span>
								  {{ Form::text('investment_amount_1', NULL, array('class'=>'form-control')) }}
								</div>
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">		
								{{ Form::label('date_monitored_1', 'Date Monitored', array('class'=>'control-label')) }}
								{{ Form::text('date_monitored_1', NULL, array('class'=>'form-control')) }}
								{{ Form::hidden('igp_id', NULL, array('id' => 'igp_id', 'class'=>'form-control')) }}
								{{ Form::hidden('msme_id', $msme['id'], array('class'=>'form-control')) }}
								{{ Form::hidden('source_count', 1, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="investment"></div>
						<div class="form-group">	
							<button class="add_investment btn btn-default" type="button"><i class="fa fa-plus"></i> Funds Source</button>
						</div>
				</div>
				
					
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					{{ Form::close() }}
				</div>
				</div>
			</div>
		</div> -------------->
		
		<!----------- EDIT FUNDS SOURCE MODAL
		<div id="edit-source-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
				
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit Funds Source</h4>
				</div>
				<div class="modal-body">
					{{ Form::open(array('url' => 'provincial/investments', 'method' => 'put', 'id'=>'edit-source-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
					<div class="row">
						<div class="col-lg-4">
							<div class="form-group">{{ Form::label('investment_source', 'Investments Source', array('class'=>'control-label')) }}</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">{{ Form::label('investment_amount', 'Amount', array('class'=>'control-label')) }}</div>
						</div>
						
						<div class="col-lg-4">
							<div class="form-group">{{ Form::label('date_monitored', 'Date Monitored', array('class'=>'control-label')) }}</div>
						</div>
					</div>
						
					<div class="edit-investment"></div>	
					
					
				</div>
				<div class="modal-footer">
					<button type="button" onclick="location.reload()" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					
					{{ Form::close() }}
				</div>
				</div>
			</div>
		</div>  -------------->
		
@stop
@section('additional_scripts')
@parent
<script>
 /*   $(".edit-igp-trigger").click( function() { 
        var igp_id = $(this).attr("igp-id"); 
	var link = '{{ url('provincial/igp') }}' + '/' + igp_id;
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) {  
				$("#edit_igp_name").val(data.igp_name);
				$("#edit_msme_id").val(data.msme_id);
				$("#edit_date_started").val(data.date_started);
				$("#edit_igp_remarks").val(data.remarks);
				$("#edit_igp_igp_msme_id").val(data.igp_msme_id);
				$("#edit_igp_products").val(data.igp_products);
				$("#edit_igp_product_category").val(data.product_category);
				$("#edit_igp_product_sales").val(data.product_sales);
				$("#edit_igp_product_remarks").val(data.product_remarks);
                $("#edit-igp-form").attr('action', link);
            }
        });
    });
	
	//edit fund source
	//$(".edit-source-trigger").click( function() { 
		
	$(".edit-igp-trigger").click( function() { 
		$( ".edit-investment" ).empty();
        var igp_id = $(this).attr("igp-id"); 
		var link = '{{ url('provincial/investments') }}' + '/' + igp_id; 
	   $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) {  
				var x = 1;
				for (var c = 0; c < data.length; c++)
				{	
					html = '<div class="row">';
					html += '<div class="col-md-4"><div class="form-group"><div class="input-group"><span class="input-group-btn">';
					html += '<button id="delete-source-trigger-'+ c +'" src-id="'+ data[c].id +'"  class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button></span>';
					html += '<input class="form-control"  value="'+data[c].investment_source+'"  name="edit_investment_source_' + c + '" type="text" id="investment_amount"></div></div></div>';
					html += '<div class="col-md-4"><div class="form-group"><div class="input-group"><span class="input-group-addon">?</span>';
					html += '<input class="form-control"  value="'+data[c].investment_amount+'" name="edit_investment_amount_' + c + '" type="text" id="investment_amount"></div>';
					html += '<input type="hidden" value="'+ data[c].id +'" name="id_'+ c +'">';
					html += '<input type="hidden" value="'+ x +'" name="edit_source_count"></div></div>';
					html += '<input type="hidden" value="{{ $msme['id'] }}" name="msme_id">';
					html += '<div class="col-md-4"><div class="form-group">';
					html += '<input class="form-control"  value="'+data[c].date_monitored+'" name="date_monitored_' + c + '" type="text" id="edit_date_monitored_'+c+'"></div></div>';
					x++;
					$('.edit-investment').append(html); 
					$( "#edit_date_monitored_"+c ).datepicker(); 
					$("#delete-source-trigger-" +c).click( function() {
						
						var id = $(this).attr("src-id"); 
						var link = '{{ url('provincial/investments') }}' + '/' + id; 
						
						if (confirm('Delete ?'))
						{
							$.ajax({ 
								type: 'DELETE', 
								url:  link,
								success: function (msg) {  
								}
								});
							 $(this).parent().parent().parent().parent().parent().empty();
						 }
					});
				}
				if ( c == 0 ) { $('.inv-label').hide();  }
				$("#edit-source-form").attr('action', link);	
            }
        });
		
    });
	
// ------------------ ADD IGP SOURCE
		var z = 0;
		
		$(".edit_add_investment").click( function() { 
			z++;
			$('.inv-label').show();
			var html = '<div class="row">';
			html += '<div class="col-lg-4"><div class="form-group"><div class="input-group"><span class="input-group-btn"><button onclick = "$(this).parent().parent().parent().parent().parent().remove()" class="btn btn-default" type="button"><i class="fa fa-trash-o"></i></button></span>';
			html += '<input class="form-control"  name="investment_source_' + z + '" type="text" id="investment_amount"></div></div></div>';
			html += '<div class="col-lg-4"><div class="form-group"><div class="input-group"><span class="input-group-addon"></span>';
			html += '<input class="form-control"  name="investment_amount_' + z + '" type="text" id="investment_amount"></div>';
			html += '<input type="hidden" value="'+ z +'" name="source_count"></div></div>';
			html += '<div class="col-lg-4"><div class="form-group">';
			html += '<input class="form-control" name="date_monitored_' + z + '" type="text" id="add_edit_date_monitored_'+ z +'"></div></div>';
						
			$('.new-investment').append(html); 
			$( "#add_edit_date_monitored_"+z ).datepicker(); 
		
		});
		
		
	$(".add-igp-trigger").click( function() {
			 var msme_id = $(this).attr("msme-id");
			 $("#igp_msme_id").val(msme_id);
				 
				$('#add-igp-title').html(" Add IGP for "+$(this).attr("msme-name"));
		});
	*/

</script>
@stop