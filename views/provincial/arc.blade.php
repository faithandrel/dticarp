@extends('provincial/modules')

@section('inner_content')

     <div class="row">
		<div class="col-lg-12">
			<h3 class="page-header"><i class="fa fa-angle-double-right"></i><a href="{{ url('provincial/home')}}">{{ Session::get('province') }}</a>&nbsp;
			<i class="fa fa-angle-right"></i><span>Agrarian Reform Communities (ARC)</span></h3>					
		</div>
	    </div>
            <!-- /.row -->
	    <div class="row">
                <div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
											<th class="actions-1"></th>
                                            <th>ARC Name</th>
                                            <th>Province</th>
											<th class="actions-1"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="char">
							
					@foreach($arcs as $arc)
					    <tr class="odd gradeX">
						<td class="actions-2" >
							<span data-toggle="tooltip" title={{ "'Edit ".$arc->name."'" }} >
								<button type="button" data-toggle="modal" data-target="#edit-arc-modal" class="btn btn-primary edit-arc-trigger" arc-id={{ $arc->id }}><i class="fa fa-pencil-square-o"></i></button>
							</span>
						</td>
						<td>{{ $arc->name }}</td>
						<td>{{ $arc->province()->pluck('province') }}</td>
						{{ Form::open(array('url' => 'provincial/arcs/'.$arc->id, 'method' => 'delete')) }}
						
						<td>
							<span data-toggle="tooltip" title={{ "'Delete ".$arc->name."'" }} >	
								<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $arc->name }}?' )" ><i class="fa fa-trash "></i></button>
							</span>
						</td>
						{{ Form::close() }}
					    </tr>
					@endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
			
	<!-- EDIT ARC MODAL -->
		<div id="edit-arc-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-sm">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit ARC</span></h4>
				</div>
				{{ Form::open(array('url' => 'provincial/arcs', 'method' => 'put', 'id'=>'edit-arc-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="modal-body">
					<div class="form-group">
						{{ Form::label('edit_arc_name', 'Name', array('class'=>'control-label')) }}
						{{ Form::text('edit_arc_name', NULL, array('class'=>'form-control')) }}
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
				</div>
				{{ Form::close() }}
				</div>
			</div>
		</div>
@stop

@section('additional_scripts')
@parent
<script>
    $(".edit-arc-trigger").click( function() {
        var arc_id = $(this).attr("arc-id");
	var link = '{{ url('provincial/arcs') }}' + '/' + arc_id
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) { 
				$("#edit_arc_name").val(data.name);
                $("#edit-arc-form").attr('action', link);
            }
        });
    });
</script>
@stop