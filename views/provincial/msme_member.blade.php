@extends('provincial/modules')
@section('inner_content')
        <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-angle-double-right"></i><span>MSME Members</span></h3>					
		</div>
	    </div>
            <!-- /.row -->
	    <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Gender</th>
											<th>Contact_No</th>
											<th>Membership</th>
											<th>Status</th>
											<th class="actions-2">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody id="char">
							
					@foreach($members as $member)
					    <tr class="odd gradeX">
						<td >{{ $member->name }}</td>
						<td >{{ $member->gender }}</td>
						<td >{{ $member->contact_no }}</td>
						<td >{{ $member->membership }}</td>
						<td >{{ $member->status }}</td>
						{{ Form::open(array('url' => 'provincial/msme_members/'.$member->id, 'method' => 'delete')) }}
						<td class="actions-2">
								<span data-toggle="tooltip" title={{ "'Edit Member ".$member->name."'" }} >
									<button data-toggle="modal" data-target="#edit-member-modal" type="button"  class="btn btn-edit edit-member-trigger" member-id={{ $member->id }}>
										<i  class="fa fa-pencil-square-o"></i>
									</button>
								</span>
								<span  data-toggle="tooltip" title={{ "'Delete Member ".$member->name."'" }}>
									<button class="btn btn-danger" type="submit" onclick="return confirm('Delete {{ $member->name }}?' )" >
										<i  class="fa fa-trash"></i>
									</button>
								</span>
						</td>
						{{ Form::close() }}
					    </tr>
					@endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
			
		<!-- edit member -->
		<div id="edit-member-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg">
				<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i> Edit MSME Member</h4>
				</div>
				<div class="modal-body" >
					{{ Form::open(array('url' => 'provincial/msme_members', 'method' => 'put', 'id'=>'edit-member-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
					<div class="row">
						<div class="col-lg-8">
							<div class="form-group">
								{{ Form::label('edit_name', 'Name', array('class'=>'control-label')) }}
								{{ Form::text('edit_name', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-lg-4">
							<div class="form-group">		
								{{ Form::label('edit_gender', 'Gender', array('class'=>'control-label')) }}
								{{ Form::select('edit_gender', array('M'=>'Male', 'F'=>'Female'), 1,  array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					
					<div class="form-group">	
						{{ Form::label('edit_address', 'Address', array('class'=>'control-label')) }}
						{{ Form::text('edit_address', NULL, array('class'=>'form-control')) }}
					</div>
					<div class="row">
						<div class="col-lg-3">
							<div class="form-group">		
								{{ Form::label('edit_contact_no', 'Contact No.', array('class'=>'control-label')) }}
								{{ Form::text('edit_contact_no', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">		
								{{ Form::label('edit_date_registered', 'Date Registered', array('class'=>'control-label')) }}
								{{ Form::text('edit_date_registered',  date("F j, Y"), array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">		
								{{ Form::label('edit_membership', 'Membership', array('class'=>'control-label')) }}
								{{ Form::select('edit_membership', array('FB'=>'FB', 'LO'=>'LO', 'NCB'=>'NCB'), 1,  array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-lg-3">
							<div class="form-group">		
								{{ Form::label('edit_status', 'Status', array('class'=>'control-label')) }}
								{{ Form::select('edit_status', array('ACTIVE'=>'ACTIVE', 'INACTIVE'=>'INACTIVE'), 1,  array('class'=>'form-control')) }}
							
							</div>
						</div>
					</div>
					<div class="form-group">		
						{{ Form::label('edit_remarks', 'Remarks', array('class'=>'control-label')) }}
						{{ Form::text('remarks', NULL, array('class'=>'form-control')) }}
					</div>
					<div class="form-group">	
						{{ Form::label('edit_developed', 'Developed?') }}
                        {{ Form::checkbox('edit_developed', 'Developed?', true) }}
					
					</div>
					<div class="form-group">		
						{{ Form::label('edit_date_developed', 'Date Developed', array('class'=>'control-label')) }}
						{{ Form::text('edit_date_developed', NULL, array('class'=>'form-control')) }}
					</div>			
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
					{{ Form::close() }}
				</div>
				</div>
			</div>
		</div>
			
			
@stop

@section('additional_scripts')
@parent
<script>
    $(".edit-member-trigger").click( function() {
        var member_id = $(this).attr("member-id");
	var link = '{{ url('provincial/msme_members') }}' + '/' + member_id;
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) { 
				$("#edit_name").val(data.name);
				$("#edit_gender").val(data.gender);
				$("#edit_address").val(data.address);
				$("#edit_contact_no").val(data.contact_no);
				$("#edit_date_registered").val(data.date_registered);
				$("#edit_membership").val(data.membership);
				$("#edit_status").val(data.status);
				$("#edit_remarks").val(data.remarks);
				$("#edit_developed").val(data.developed); 
				$("#edit_date_developed").val(data.date_developed);
                $("#edit-member-form").attr('action', link);
            }
        });
    });
	
	
</script>
@stop