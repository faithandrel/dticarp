@extends('provincial/modules')
@section('inner_content')

		 <div class="row">
			<div class="col-lg-12">
			<h3 class="page-header"><i class="fa fa-angle-double-right"></i><a href="{{ url('provincial/home')}}">{{ Session::get('province') }}</a>&nbsp;
				<i class="fa fa-angle-right"></i><span> Annual Targets</span>	
				<i class="fa fa-angle-right"></i><span id="year_head"></span></h3>
			</div>
		</div>
        <div class="row form-select">
			{{ Form::open(array('url' => 'provincial/targets', 'method' => 'get', 'class'=>'form-horizontal', 'role'=>'form')) }}
			
	@if ( $targets != '[]')		
			 <div class="form-group-sm col-xs-8 col-md-10">
                {{ Form::label('year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('year', $years_dropdown , date('Y'),  array('class'=>'form-control number', 'id'=>'year', 'onchange'=>'this.form.submit()')) }}
			</div>
			 <div class="col-xs-4 col-md-2 btn-target">
                 <button class="btn btn-primary target-trigger" type="button" data-toggle="modal" data-target="#add-target-modal"><i class="fa fa-pencil fa-fw"></i> Annual Target</button>
			</div>
			{{ Form::close();}}
		</div>    

    <div class="row table-wrapper-target">
		<div class="col-lg-12">
			<table class="table-bordered  table-hover table-target">
				<thead>
					<tr>
						<th>Indicators</th>
						<th>Annual Target</th>
					</tr>
				</head>
				<tbody>
				
				@foreach ($targets as $target)
					<tr>
						<td>Investments (₱ M)</td><td align="right">{{ '₱ ' . number_format($target->investments, 2)  }}</td>
					</tr>
					<tr>
						<td>Sales (₱ M)</td><td align="right">{{ '₱ ' . number_format($target->sales, 2)  }}</td>
					</tr>
					<tr>
						<td>Jobs Generated</td><td align="right">{{ number_format($target->jobs) }}</td>
					</tr>
					<tr>
						<td>Entreprenuers Developed</td><td align="right">{{  number_format($target->entrepreneurs) }}</td>
					</tr>
					<tr>
						<td>Number of ARCs Assisted</td><td align="right">{{  number_format($target->arcs_assisted) }}</td>
					</tr>
					<tr>
						<td>Number of Non-ARCs Assisted</td><td align="right">{{  number_format($target->non_arcs_assisted) }}</td>
					</tr>
					<tr>
						<td>Number of MSMEs Developed</td><td align="right">{{  number_format($target->msmes_dev) }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp; Farmer-beneficiaries served</td><td align="right">{{  number_format($target->msmes_dev_fb) }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp; Landowners served</td><td align="right">{{  number_format($target->msmes_dev_lo) }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp;  Sales Generated</td><td align="right">{{  '₱ ' . number_format($target->established, 2)  }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp;  ARCs served</td><td align="right">{{  number_format($target->msmes_dev_arcs) }}</td>
					</tr>
					<tr>
						<td>Number of MSMEs Assisted</td><td align="right">{{  number_format($target->msmes_asst) }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp; Farmer-beneficiaries served</td><td align="right">{{  number_format($target->msmes_asst_fb) }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp; Landowners served</td><td align="right">{{  number_format($target->msmes_asst_lo) }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp;  Sales Generated</td><td align="right">{{ '₱ ' . number_format($target->monitored, 2)  }}</td>
					</tr>
					<tr>
						<td> &nbsp; &nbsp;  ARCs served</td><td align="right">{{  number_format($target->msmes_asst_arcs) }}</td>
					</tr>
				
					@foreach ($inter_targets as $inter_target)
                                                @if($inter_target->intervention_id == 13)
                                                <tr>
							<td>Number of Man-months Consultancy</td><td align="right">{{  $target->man_months }}</td>
						</tr>
                                                @else
						<tr>
							<td>Number of {{ $inter_target->intervention()->pluck('intervention') }}</td><td align="right">{{  number_format($inter_target->number) }}</td>
						</tr>
                                                @endif
						<tr>
							<td> &nbsp; &nbsp; Farmer-beneficiaries served</td><td align="right">{{  number_format($inter_target->fbs) }}</td>
						</tr>
						<tr>
							<td> &nbsp; &nbsp; Landowners served</td><td align="right">{{  number_format($inter_target->los)  }}</td>
						</tr>
						@if($inter_target->intervention()->pluck('w_sales'))
						<tr>
							<td> &nbsp; &nbsp;  Sales Generated</td><td align="right">{{ '₱ ' . number_format($inter_target->sales, 2)  }}</td>
						</tr>
						@endif
						<tr>
							<td> &nbsp; &nbsp;  ARCs served</td><td align="right">{{  number_format($inter_target->arcs) }}</td>
						</tr>
					@endforeach
				@endforeach
				</tbody>
			</table>
		</div>
	</div>
	@else
	
	<div class="form-group-sm col-lg-12">
                {{ Form::label('year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('year', $years_dropdown , date('Y'),  array('class'=>'form-control number', 'id'=>'year', 'onchange'=>'this.form.submit()')) }}
			</div>
			
			{{ Form::close();}}
		</div>    

	<div class="no-data">
		<i class="fa fa-ban"></i>
		<br><span>No data available</span>
		<br>
        <button class="add-target-trigger btn btn-primary" type="button" data-toggle="modal" data-target="#add-target-modal"><i class="fa fa-plus fa-fw"></i> Annual Target</button>
		
	</div>
	@endif
	
    <!-- add target -->
    <div id="add-target-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-md">
                    <div class="modal-content">
                    <div class="modal-header">
                    
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-plus"></i><span id="add-attn-title"> Annual Target </span></h4>
                    </div>
                    <div class="modal-body">
                            {{ Form::open(array('url' => 'provincial/targets', 'method'=>'put', 'id'=>'add-targets-form',  'class'=>'form-horizontal', 'role'=>'form')) }}
							{{ Form::hidden('year', NULL, array('class'=>'form-control number', 'id'=>'target_year')) }}
                            <div class="row">
                                <div class="col-lg-4">
                                        {{ Form::label('investments', 'Investments', array('class'=>'control-label')) }}
                                        <div class="input-group">
                                            <span class="input-group-addon">₱</span>
                                            {{ Form::text('investments', NULL, array('class'=>'form-control number')) }}
                                        </div>
                                </div>
                                <div class="col-lg-4">
                                        {{ Form::label('sales', 'Sales', array('class'=>'control-label')) }}
                                        <div class="input-group">
                                            <span class="input-group-addon">₱</span>
                                            {{ Form::text('sales', NULL, array('class'=>'form-control number')) }}
                                        </div>
                                </div>
                                 <div class="col-lg-4">
                                        {{ Form::label('jobs', 'Jobs', array('class'=>'control-label')) }}
                                        {{ Form::text('jobs', NULL, array('class'=>'form-control number')) }}
                                </div>
                            </div>
							<div class="row">
								<div class="col-lg-4">
                                        {{ Form::label('entrepreneurs', 'Entrepreneurs', array('class'=>'control-label')) }}
                                        {{ Form::text('entrepreneurs', NULL, array('class'=>'form-control number')) }}
                                </div>
                                <div class="col-lg-4">
                                        {{ Form::label('arcs_assisted', 'ARCs Assisted', array('class'=>'control-label')) }}
                                        {{ Form::text('arcs_assisted', NULL, array('class'=>'form-control number')) }}
                                </div>
                                <div class="col-lg-4">
                                        {{ Form::label('non_arcs_assisted', 'Non-ARCs Assisted', array('class'=>'control-label')) }}
                                        {{ Form::text('non_arcs_assisted', NULL, array('class'=>'form-control number')) }}
                                </div>
                            </div>
                               <br> 
                            <!--Number of MSMEs Developed-->
							<div class="panel panel-default">
							  <div class="panel-heading"><b>MSMEs Developed</b></div>
							  <div class="panel-body">
								<div class="row">
									<div class="col-lg-7">
											<div class="form-group-sm">	
													{{ Form::label('msmes_dev', 'Number of MSMEs Developed', array('class'=>'control-label')) }}
													{{ Form::text('msmes_dev', NULL, array('class'=>'form-control number')) }}
											</div>
									</div>
									<div class="col-lg-5">
											<div class="form-group-sm">	
													{{ Form::label('msmes_dev_fb', 'Farmer-beneficiaries served', array('class'=>'control-label')) }}
													{{ Form::text('msmes_dev_fb', NULL, array('class'=>'form-control number')) }}
											</div>
									</div>
								</div>
								 <div class="row">
									<div class="col-lg-7">
											<div class="form-group-sm">		
													{{ Form::label('established', 'Sales Generated', array('class'=>'control-label')) }}
													<div class="input-group">
														<span class="input-group-addon">₱</span>
														{{ Form::text('established', NULL, array('class'=>'form-control number')) }}
													</div>
											</div>
									</div>
									
									<div class="col-lg-5">
											<div class="form-group-sm">		
													{{ Form::label('msmes_dev_lo', 'Landowners served', array('class'=>'control-label')) }}
													{{ Form::text('msmes_dev_lo', NULL, array('class'=>'form-control number')) }}
											</div>
									</div>
								</div>
								 <div class="row">
									<div class="col-lg-7">
											<div class="form-group-sm">		
												
											</div>
									</div>
									
									<div class="col-lg-5">
											<div class="form-group-sm">		
													{{ Form::label('msmes_dev_arcs', 'ARCs served', array('class'=>'control-label')) }}
													{{ Form::text('msmes_dev_arcs', NULL, array('class'=>'form-control number')) }}
											</div>
									</div>
								</div>
							  </div>
							</div>
                            
                            <!--Number of MSMEs Assisted-->
							<div class="panel panel-default">
								<div class="panel-heading"><b>MSMEs Assisted</b></div>
									 <div class="panel-body">
									 <div class="row">
										<div class="col-lg-7">
												<div class="form-group-sm">	
														{{ Form::label('msmes_asst', 'Number of MSMEs Assisted', array('class'=>'control-label')) }}
														{{ Form::text('msmes_asst', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
										<div class="col-lg-5">
												<div class="form-group-sm">	
														{{ Form::label('msmes_asst_fb', 'Farmer-beneficiaries served', array('class'=>'control-label')) }}
														{{ Form::text('msmes_asst_fb', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
										
									</div>
									<div class="row">
										<div class="col-lg-7">
												<div class="form-group-sm">
														{{ Form::label('monitored', 'Monitored Sales', array('class'=>'control-label')) }}		
													<div class="input-group">
														<span class="input-group-addon">₱</span>
														{{ Form::text('monitored', NULL, array('class'=>'form-control number')) }}
													</div>
												</div>
										</div>
										<div class="col-lg-5">
												<div class="form-group-sm">		
														{{ Form::label('msmes_asst_lo', 'Landowners served', array('class'=>'control-label')) }}
														{{ Form::text('msmes_asst_lo', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-7">
												
										</div>
										
										<div class="col-lg-5">
												<div class="form-group-sm">		
														{{ Form::label('msmes_asst_arcs', 'ARCs served', array('class'=>'control-label')) }}
														{{ Form::text('msmes_asst_arcs', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
									</div>
								</div>
							</div>
                           
                                
                            
                            <!--Interventions-->
                            @foreach ($interventions as $intervention)
							<div class="panel panel-default">
								<div class="panel-heading"><b>{{ $intervention->intervention }}</b></div>
									 <div class="panel-body">
									 <div class="row">
										<div class="col-lg-7">
												<div class="{{ $intervention->id }}id"></div>
												<div class="form-group-sm">
                                                                                                        @if($intervention->id == 13) 
                                                                                                        {{ Form::label('man_months', 'Number of Man-months Consultancy', array('class'=>'control-label')) }}
													{{ Form::text('man_months', NULL, array('class'=>'form-control number')) }}
                                                                                                        @else
													{{ Form::label($intervention->id.'number', 'Number of '.$intervention->intervention, array('class'=>'control-label')) }}
													{{ Form::text($intervention->id.'number', NULL, array('class'=>'form-control number')) }}
                                                                                                        @endif
												</div>
										</div>
										<div class="col-lg-5">
												<div class="form-group-sm">	
													{{ Form::label($intervention->id.'fbs', 'Farmer-beneficiaries served', array('class'=>'control-label')) }}
													{{ Form::text($intervention->id.'fbs', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
										
									</div>
									<div class="row">
											<div class="col-lg-7">
													<div class="form-group-sm">		
													@if($intervention->w_sales)
															{{ Form::label($intervention->id.'sales', 'Sales Generated', array('class'=>'control-label')) }}
														<div class="input-group">
															<span class="input-group-addon">₱</span>
															{{ Form::text($intervention->id.'sales', NULL, array('class'=>'form-control number')) }}
														</div>
													@endif
													</div>
											</div>
										<div class="col-lg-5">
												<div class="form-group-sm">		
													{{ Form::label($intervention->id.'los', 'Landowners served', array('class'=>'control-label')) }}
													{{ Form::text($intervention->id.'los', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
									</div>
									<div class="row">
										<div class="col-lg-7">
												
										</div>
										
										<div class="col-lg-5">
												<div class="form-group-sm">		
													{{ Form::label($intervention->id.'arcs', 'ARCs served', array('class'=>'control-label')) }}
													{{ Form::text($intervention->id.'arcs', NULL, array('class'=>'form-control number')) }}
												</div>
										</div>
									</div>
								</div>
							</div>
                            @endforeach
							
							
                    </div>
                    <div class="modal-footer">
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
							<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
                            {{ Form::close() }}
                    </div>
                    </div>
            </div>
    </div>
@stop
@section('additional_scripts')
@parent
<script>
	$(".target-trigger").click( function() {
			year = $("#year").val();
			var link = '{{ url('provincial/targets') }}' + '/' + year;
			 $.ajax({ 
                type: 'GET', 
                url:  link,
                dataType: 'json',
                success: function (data) { 
						if (data!='')
						{	
							$.each(data, function(index, value) { 
								$("#investments").val(numberWithCommas(value.investments));
								$("#sales").val(numberWithCommas(value.sales));
								$("#jobs").val(numberWithCommas(value.jobs));
								$("#entrepreneurs").val(numberWithCommas(value.entrepreneurs));
								$("#arcs_assisted").val(numberWithCommas(value.arcs_assisted));
								$("#non_arcs_assisted").val(numberWithCommas(value.non_arcs_assisted));
								$("#lo_male").val(numberWithCommas(value.entrepreneurs));
								$("#msmes_dev").val(numberWithCommas(value.msmes_dev));
								$("#msmes_dev_fb").val(numberWithCommas(value.msmes_dev_fb));
								$("#established").val(numberWithCommas(value.established));		
								$("#msmes_dev_lo").val(numberWithCommas(value.msmes_dev_lo));
								$("#msmes_dev_arcs").val(numberWithCommas(value.msmes_dev_arcs));
								$("#msmes_asst").val(numberWithCommas(value.msmes_asst));
								$("#msmes_asst_fb").val(numberWithCommas(value.msmes_asst_fb));
								$("#monitored").val(numberWithCommas(value.monitored));
								$("#msmes_asst_lo").val(numberWithCommas(value.msmes_asst_lo));
								$("#msmes_asst_arcs").val(numberWithCommas(value.msmes_asst_arcs));
                                                                $("#man_months").val(numberWithCommas(value.man_months));
								id = value.id; 
								var link_inter = '{{ url('provincial/inter_targets') }}' + '/' + id;
									$.ajax({ 
										type: 'GET', 
										url:  link_inter,
										dataType: 'json',
										success: function (inter_data) { 
											$.each(inter_data, function(index, value) {
												$("#"+value.intervention_id+"number").val(numberWithCommas(value.number));
												$("#"+value.intervention_id+"fbs").val(numberWithCommas(value.fbs));
												$("#"+value.intervention_id+"los").val(numberWithCommas(value.los));
												$("#"+value.intervention_id+"arcs").val(numberWithCommas(value.arcs));
												$("#"+value.intervention_id+"sales").val(numberWithCommas(value.sales));
												
												var html = '<input type="hidden" name="'+value.intervention_id+'id" value="'+value.id+'" >';
												$("."+value.intervention_id+"id").append(html);
											});
										
										}
									 });
								$("#add-targets-form").attr('action', '{{ url('provincial/targets') }}' + '/' + value.id);
							});
						}
						
                }
            });
		});
		$(".add-target-trigger").click( function() {
			$('input').val("");
			year = $("#year").val();
			$("#target_year").val(year);
			$('input[name=_method]').remove();
			$('input[name=_token]').remove();
			
			$("#add-attendances-form").attr('action', '{{ url('provincial/targets') }}');
		});
		
		 $("#year_head").html($("#year" ).val());
</script>
@stop

