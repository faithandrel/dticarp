@extends('modules')

@section('inner_content')
	    <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-file-text fa-fw"></i><span>Provinces</span></h3>					
		</div>
	    </div>
            <!-- /.row -->
	    <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Header
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Province</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody id="char">
					@foreach($provinces as $province)
					    <tr class="odd gradeX">
						<td data-toggle="modal" data-target="#edit-arc-modal" class="edit-arc-trigger" arc-id={{ $province->id }} >{{ $province->province }}</td>
						<td></td>
					    </tr>
					@endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
	    
	    <div id="add-province-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
		    <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <h4 class="modal-title">Add Province</h4>
			</div>
			<div class="modal-body" style="padding: 30px;">
			    {{ Form::open(array('url' => 'provincial/arcs', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="form-group">
				    {{ Form::label('arc_name', 'Name', array('class'=>'control-label')) }}
				    {{ Form::text('arc_name', NULL, array('class'=>'form-control')) }}
				</div>
			</div>
			<div class="modal-footer">
			    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    <button type="submit" class="btn btn-primary">Add</button>
			    {{ Form::close() }}
			</div>
		    </div>
		</div>
	    </div>
	    
	    <div id="edit-province-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		<div class="modal-dialog modal-sm">
		    <div class="modal-content">
			<div class="modal-header">
			    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			    <h4 class="modal-title">Edit Province</h4>
			</div>
			<div class="modal-body" style="padding: 30px;">
			    {{ Form::open(array('url' => 'provincial/arcs', 'method' => 'put', 'id'=>'edit-arc-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
				<div class="form-group">
				    {{ Form::label('edit_arc_name', 'Name', array('class'=>'control-label')) }}
				    {{ Form::text('edit_arc_name', NULL, array('class'=>'form-control')) }}
				</div>
			</div>
			<div class="modal-footer">
			    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
			    <button type="submit" class="btn btn-primary">Edit</button>
			    {{ Form::close() }}
			</div>
		    </div>
		</div>
	    </div>

@stop

@section('additional_scripts')
<script>
    $(".edit-arc-trigger").click( function() {
        var arc_id = $(this).attr("arc-id");
	var link = '{{ url('provincial/arcs') }}' + '/' + arc_id
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) {
		$("#edit_arc_name").val(data.name);
                $("#edit-arc-form").attr('action', link);
            }
        });
    });
</script>
@stop