@extends('modules')

@section('inner_content')
    <div class="row">
	<div class="col-lg-12">
	    <h3 class="page-header"><i class="fa fa-user-plus fa-fw"></i><span>Add User</span></h3>
                <div id="signup">
					@if(Session::get('access') == 1)
                    {{ Form::open(array('url' => 'national/users', 'class'=>'form-horizontal', 'role'=>'form')) }}
					@endif
					
					@if(Session::get('access') == 2)
                    {{ Form::open(array('url' => 'regional/users', 'class'=>'form-horizontal', 'role'=>'form')) }}
					@endif
					
                    <div class="form-group">
                        {{ Form::label('username', 's', array('class'=>'control-label')) }}
                        {{ Form::text('username', NULL, array('class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('password', 'Password', array('class'=>'control-label')) }}
                        {{ Form::password('password', array('class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('name', 'Name', array('class'=>'control-label')) }}
                        {{ Form::text('name', NULL, array('class'=>'form-control')) }}
            
                    </div>
                    <div class="form-group">
                        {{ Form::label('status', 'Active?') }}
                        {{ Form::checkbox('status', '1', true) }}
                    </div>
                    
                    @if(Session::get('access') == 1)   
                    <div class="form-group">
                        {{ Form::label('access', 'Access Level', array('class'=>'control-label')) }}
                        {{ Form::select('access', array(1=>'National', 2=>'Regional', 3=>'Provincial'),
                                        1,  array('class'=>'form-control')) }}
                    </div>
                    <div class="form-group">
                        {{ Form::label('region', 'Region', array('class'=>'control-label')) }}
                        {{ Form::select('region', $regions, NULL,  array('class'=>'form-control')) }}
                    </div>
                    @endif
                    
                    <div class="form-group">
                        {{ Form::label('province', 'Province', array('class'=>'control-label')) }}
                        {{ Form::select('province', $provinces, NULL,  array('class'=>'form-control')) }}
                    </div>
                        
                        {{ Form::submit('Add', array('class'=>'btn btn-default')) }}
                   
                    {{ Form::close() }}
                    
                    {{ var_dump($provinces) }}
                    <br/>
                    {{ var_dump($regions) }}
                </div>
        </div>
    </div>
@stop
