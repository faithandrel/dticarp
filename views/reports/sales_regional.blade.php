<html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Sales Generated</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/sales') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/sales') }}';
			@endif
			
			$("#report").attr('action', link);
		}
		function xls()
		{
				@if(Session::get('access') == 1)
					var link = '{{ url('national/reports/sales-xls') }}';
				@endif
									
				@if(Session::get('access') == 2)
					var link = '{{ url('regional/reports/sales-xls') }}';
				@endif
				
				$("#report").attr('action', link);
		}
		function pdf()
		{
				$('input[name=action]').val('pdf');
				$('#report').submit();
		}
	</script>
</head>
<body>
<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		{{ Form::open(array('url' => 'provincial/reports/trade_fairs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , $region,  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2 @else col-sm-2 @endif">
                {{ Form::label('report_start_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_start_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-2 @endif">
                {{ Form::label('report_end_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_end_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') == 1)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
		    @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		   
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>
<div class="table-report">
	<table class="table-hover">
		<thead class="border-less">
			<tr>
				<th colspan="9">
					COMPREHENSIVE AGRARIAN REFORM PROGRAM
				</th>
			</tr>
			<tr>
				<th colspan="9">
					Annex 4
				</th>
			</tr>
			<tr>
				<th colspan="9">
					Sales Generated
				</th>
			</tr>
			<tr>
				<th colspan="9">
                                    @if(Session::get('access') == 2)
					{{ Session::get('region') }}
                                    @else
                                        {{ Region::where('id', '=', $region)->pluck('region') }}
                                    @endif
				</th>
			</tr>
			<tr>
				<th colspan="9">
					@if(Input::get('report_start_month') == Input::get('report_end_month'))
						{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
					@else
						{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
					@endif
				</th>
			</tr>
			<tr>
				<th colspan="9">
				</th>
			</tr>
		</thead>
		@if (isset($sales))
		<thead class="tb-header">
                    <tr>
                        <td>ARC</td>
                        <td>MSME</th>
                        <td>IGP</td>
                        <td>Sales</td>
                        <?php  $interventions = current($sales); ?>
                        @foreach( $interventions['interventions'] as $intervention)
                            <td>{{ $intervention->intervention }}</td>
                        @endforeach
                        <td>Monitored</td>
                        <td>Established</td>
                    <tr>
		</thead>
                
		<tbody>
                @foreach ($sales as $province => $data)
                    <tr>
                        <td class="td-bold td-uppercase">{{ $province }}</td><td></td><td></td><td></td>
                        <?php $interventions = current($sales); ?>
                        @foreach( $interventions['interventions'] as $intervention)
                            <td></td>
                        @endforeach
                        <td></td><td></td>
                    </tr>
                    @foreach($sales[$province]['msmes'] as $msme)
					@if($msme->sales == 0) <?php continue; ?> @endif <!-- will not display msme with 0 sales -->
                        <tr>
                            <td>{{ $msme->name }}</td>
                            <td>{{ $msme->msme_name }}</td>
                            <td>
                                @foreach($msme->igp as $igproject)
                                   <li>{{ $igproject->igp_name }}</li>
                                @endforeach
                                
                            </td>
                            <td align="right">
                                {{ '₱ ' . number_format($msme->sales, 2)  }}
                            </td>
                            @foreach($sales[$province]['interventions']as $intervention)
                                 <td>
                                @foreach($msme->attn[$intervention->id] as $attendances)
                                   @if($attendances->count())
                                        @foreach($attendances as $attendance)
                                             <li align="right">{{ '₱ ' . number_format($attendance->sales, 2)  }}</li>
                                        @endforeach
                                   @endif
                                @endforeach
                                 </td>
                            @endforeach
                            <td align="right">{{ '₱ ' . number_format($msme->monitored, 2)  }}</td>
                            <td align="right">{{ '₱ ' . number_format($msme->established, 2)  }}</td>
                        </tr>
                    @endforeach
                    @if(!empty($sales[$province]['totals']))
                    <tr>
                         <td align="right"><b>Total</b></td><td></td><td></td><td align="right"><b>{{ '₱ ' . number_format($sales[$province]['totals']['igp_sales'], 2)  }}</b></td>
                         @foreach($sales[$province]['interventions'] as $intervention)
                                <td align="right"><b>{{ '₱ ' . number_format($sales[$province]['totals'][$intervention->id], 2)  }}</b></td>
                         @endforeach
                         <td align="right"><b>{{ '₱ ' . number_format($sales[$province]['totals']['monitored'], 2)  }}</b></td><td align="right"><b>{{  '₱ ' . number_format($sales[$province]['totals']['established'], 2)  }}<b></td>
                    </tr>
                    @endif
					<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
                @endforeach
                 <tr>
                    <td align="right"><b>GRAND TOTAL</b></td><td></td><td></td><td align="right"><b>{{ '₱ ' . number_format($totals['igp_sales'], 2)  }}</b></td>
                     @foreach($sales[$province]['interventions'] as $intervention )
                            <td align="right"><b>{{ $totals[$intervention->id] }}</b></td>
                     @endforeach
                     <td align="right"><b>{{ '₱ ' . number_format($totals['monitored'], 2)  }}</b></td><td align="right"><b>{{  '₱ ' . number_format($totals['established'], 2)  }}<b></td>
                </tr>
		</tbody>
		@endif
	</table>
</div>
</html>