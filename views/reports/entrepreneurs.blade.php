<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Entrepreneurs Developed</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			@if(Session::get('access') == 3)
				var link = '{{ url('provincial/reports/entrepreneurs') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/entrepreneurs') }}';
			@endif
			
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/entrepreneurs') }}';
			@endif
			$("#report").attr('action', link);
		}
		function xls()
		{
				@if(Session::get('access') == 3)
					var link = '{{ url('provincial/reports/entrepreneurs-xls') }}';
				@endif
									
				@if(Session::get('access') == 2)
					var link = '{{ url('regional/reports/entrepreneurs-xls') }}';
				@endif
				
				@if(Session::get('access') == 1)
					var link = '{{ url('national/reports/entrepreneurs-xls') }}';
				@endif
				$("#report").attr('action', link);
		}
		function pdf()
		{
				$('input[name=action]').val('pdf');
				$('#report').submit();
		}
	</script>
</head>
<body>
<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		{{ Form::open(array('url' => 'provincial/reports/accomplishment/arcs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , NULL,  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2 @else col-sm-2 @endif">
                {{ Form::label('report_start_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_start_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-2 @endif">
                {{ Form::label('report_end_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_end_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') == 1)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
		    @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		   
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>
	<div class="table-report">
	<table class="table-hover">
		<thead >
			<tr>
				<th class="border-less" colspan="4" >
					COMPREHENSIVE AGRARIAN REFORM PROGRAM
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="4">
					Annex 6
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="4">
					Entrepreneurs Developed
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="4">
					{{ Session::get('province') }}
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="4">
				@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@endif				
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="4">
				
				</th>
			</tr>	
			
		</thead>
		<thead>
			<tr>
				<td >ARC</td>
				<td >Name of Assn/Location</td>
				<td >Entrepreneur</td>
				<td >Operations/Activities</td>
				<td >Number</td>
			</tr>		
		</thead>
		
		<tbody> 
				@foreach($results['provinces'] as $province)
					@if(Session::get('access') != 3 )
					<tr class="tr-bold tr-uppercase">
						<td>{{ $province->province }}</td><td></td><td></td><td></td><td></td>
					</tr>
					@endif
					<?php $arc = 'arc';  ?> 
					@foreach($results['entrepreneurs'][$province->id] as $entrepreneur)
					<tr>
                        <td>
						@if($entrepreneur->arc_name != $arc)
							@if ($entrepreneur->arc_name == '') Non-ARC
							@else {{ $entrepreneur->arc_name  }} 
							@endif
							<?php $c = 1; ?>
						@endif
						</td>
                        <td>{{ $entrepreneur->msme_name }} </td>
                        <td>
                            {{ $entrepreneur->first_name." ".$entrepreneur->last_name }}
                        </td>
						<td>
                            {{ $entrepreneur->project }}
                        </td>
                        <td align="center">  1 </td>
                    </tr>
					
					<?php  $arc = $entrepreneur->arc_name; $c++; ?>
                    @endforeach
                    <tr><td></td><td></td><td></td><td align="right"><b> Total</b></td><td align="center"><b>{{ number_format($results['total'][$province->id]) }}</b></td></tr>
					<tr></td><td></td><td></td><td></td><td></td><td></td></tr>
				@endforeach
				@if(Session::get('access') != 3 )
				<tr><td></td><td></td><td></td><td align="right"><b> Grand Total</b></td><td align="center"><b>{{ number_format($results['grand_total']) }}</b></td></tr>
				@endif
		</tbody>
	</table>
	</table>
	</body>
</html>