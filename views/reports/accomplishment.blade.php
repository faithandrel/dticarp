<html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Summary of Accomplishments</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			var link = '{{ url('provincial/reports/summary-accomplishments') }}';

			$("#report").attr('action', link);
		}
		function xls()
		{
                        var link = '{{ url('provincial/reports/summary-accomplishments-xls') }}';
                        
                        $("#report").attr('action', link);
		}
		function pdf()
		{
				$('input[name=action]').val('pdf');
				$('#report').submit();
		}
		
	</script>
</head>
<body>
	<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
			@if(Session::get('access') == 3)
                {{ Form::open(array('url' => 'provincial/reports/sales', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
            @endif
            @if(Session::get('access') == 2)
                {{ Form::open(array('url' => 'regional/reports/sales', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
            @endif         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , $results['region'],  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-2 @endif">
                {{ Form::label('year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') == 1)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
		   @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>
<div class="table-report">
       <!-- Target: <br/>
        {{ var_dump($annual) }}
        <br/>
        To Date: <br/>
        {{ var_dump($todate) }}
        <br/>
        Percentage: <br/>
        {{ var_dump($percentage) }}
        <br/><br/>
       -->
        <table class="table-hover">
             <thead>
                <tr>
                        <th colspan="9">
                                COMPREHENSIVE AGRARIAN REFORM PROGRAM
                        </th>
                </tr>
                <tr>
                        <th colspan="9">
                                Annex 8
                        </th>
                </tr>
                <tr>
                        <th colspan="9">
                                Summary of Accomplishments
                        </th>
                </tr>
                <tr>
                        <th colspan="9">
                                {{ Session::get('province') }}
                        </th>
                </tr>
                <tr>
                        <th colspan="9">
                                @if(Input::get('report_start_month') == Input::get('report_end_month'))
                                        {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
                                @else
                                        {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
                                @endif
                        </th>
                </tr>
                <tr>
                        <th colspan="9">
                        </th>
                </tr>
        </thead>
                <thead>
                        <tr>
                                <td>Indicators</td>
                                <td>Annual Target</td>
                                <td>
                                    @if(Input::get('report_start_month') == Input::get('report_end_month'))
                                        {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
                                    @else
                                        {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
                                    @endif
                                </td>
                                <td>Accomp to Date</td>
                                <td>% of Accomp</td>
                        </tr>
                </thead>
                <tbody>
                
                        <tr>
                                <td>Investments (P M))</td>
								<td align="right">{{ '₱ ' . number_format($target->investments, 2)  }}</td>
                                 <td align="right">{{ '₱ ' . number_format($current['investments'], 2)  }}</td>
								 <td align="right">{{ '₱ ' . number_format($todate['investments'], 2)  }}</td>
								 <td align="right">{{ $percentage['investments'] }}%</td>
                        </tr>
                        <tr>
                                <td>Sales (P M)</td>
								<td align="right">{{ '₱ ' . number_format($target->sales , 2) }}</td>
                                <td align="right">{{ '₱ ' . number_format($current['sales'], 2)  }}</td>
								<td align="right">{{ '₱ ' . number_format($todate['sales'], 2)  }}</td>
								<td align="right">{{ $percentage['sales'] }}%</td>
                        </tr>
                        <tr>
                                <td>Jobs Generated</td>
								<td align="right">{{ number_format($target->jobs)  }}</td>
                                 <td align="right">{{ number_format($current['jobs'])  }}</td>
								 <td align="right">{{ number_format($todate['jobs'])   }}</td>
								 <td align="right">{{ $percentage['jobs'] }}%</td>
                        </tr>
                        <tr>
                                <td>Entrepreneurs Developed</td>
								<td align="right">{{ number_format($target->entrepreneurs)}}</td>
                                <td align="right">{{ number_format($current['entrepreneurs']) }}</td>
								<td align="right">{{ number_format($todate['entrepreneurs'])  }}</td>
								<td align="right">{{ $percentage['entrepreneurs'] }}%</td>
                        </tr>
                        <tr>
                                <td>Number of ARCs Assisted</td>
								<td align="right">{{ number_format($target->arcs_assisted) }}</td>
                                <td align="right">{{ number_format($current['arcs_assisted']) }}</td>
								<td align="right">{{ number_format($todate['arcs_assisted'])  }}</td>
								<td align="right">{{ $percentage['arcs_assisted'] }}%</td>
                                
                        </tr>
                        <tr>
                                <td>Number of Non-ARCs Assisted</td>
								<td align="right">{{ number_format($target->non_arcs_assisted)  }}</td>
                                <td align="right">{{ number_format($current['non_arcs_assisted']) }}</td>
								<td align="right">{{ number_format($todate['non_arcs_assisted'])  }}</td>
								<td align="right">{{ $percentage['non_arcs_assisted'] }}%</td>
                        </tr>
                        <tr>
                                <td>Number of MSMEs Developed</td>
								<td align="right">{{ number_format($target->msmes_dev) }}</td>
                                <td align="right">{{ number_format($current['msmes_dev']) }}</td>
								<td align="right">{{ number_format($todate['msmes_dev'])  }}</td>
								<td align="right">{{ $percentage['msmes_dev'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
								<td align="right">{{ number_format($target->msmes_dev_fb) }}</td>
                                <td align="right">{{ number_format($current['msmes_dev_fb']) }}</td>
								<td align="right">{{ number_format($todate['msmes_dev_fb'])  }}</td>
								<td align="right">{{ $percentage['msmes_dev_fb'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp; Landowners served</td>
								<td align="right">{{ number_format($target->msmes_dev_lo) }}</td>
                                <td></td><td></td><td></td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp;  Sales Generated</td>
								<td align="right">{{ '₱ ' . number_format($target->established, 2) }}</td>
                                <td align="right">{{ '₱ ' . number_format($current['established'], 2) }}</td>
								<td align="right">{{ '₱ ' . number_format($todate['established'], 2)  }}</td>
								<td align="right">{{ $percentage['established'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp;  ARCs served</td>
								<td align="right">{{ number_format($target->msmes_dev_arcs) }}</td>
                                <td align="right">{{ number_format($current['msmes_dev_arcs']) }}</td>
								<td align="right">{{ number_format($todate['msmes_dev_arcs'])  }}</td>
								<td align="right">{{ $percentage['msmes_dev_arcs'] }}%</td>
                        </tr>
                        <tr>
                                <td>Number of MSMEs Assisted</td>
								<td align="right">{{ number_format($target->msmes_asst) }}</td>
                                <td align="right">{{ number_format($current['msmes_asst']) }}</td>
								<td align="right">{{ number_format($todate['msmes_asst'])  }}</td>
								<td align="right">{{ $percentage['msmes_asst'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
								<td align="right">{{ number_format($target->msmes_asst_fb)  }}</td>
                                <td align="right">{{ number_format($current['msmes_asst_fb']) }}</td>
								<td align="right">{{ number_format($todate['msmes_asst_fb'])  }}</td>
								<td align="right">{{ $percentage['msmes_asst_fb'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp; Landowners served</td>
								<td align="right">{{ number_format($target->msmes_asst_lo) }}</td>
                                <td align="right">{{ number_format($current['msmes_asst_lo']) }}</td>
								<td align="right">{{ number_format($todate['msmes_asst_lo'])  }}</td>
								<td align="right">{{ $percentage['msmes_asst_lo'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp;  Monitored Sales</td>
								<td align="right">{{ '₱ ' . number_format($target->monitored, 2) }}</td>
                                <td align="right">{{ '₱ ' . number_format($current['monitored'], 2) }}</td>
								<td align="right">{{ '₱ ' . number_format($todate['monitored'], 2)  }}</td>
								<td align="right">{{ $percentage['monitored'] }}%</td>
                        </tr>
                        <tr>
                                <td> &nbsp; &nbsp;  ARCs served</td>
								<td align="right">{{ number_format($target->msmes_asst_arcs) }}</td>
                                <td align="right">{{ number_format($current['msmes_asst_arcs']) }}</td>
								<td align="right">{{ number_format($todate['msmes_asst_arcs'])  }}</td>
								<td align="right">{{ $percentage['msmes_asst_arcs'] }}%</td>
                        </tr>
                
                        @foreach ($inter_targets as $inter_target)
                                @if($inter_target->intervention_id == 13)
                                <tr>
                                        <td>Number of Man-months Consultancy</td>
										<td align="right">{{ number_format($target->man_months,5)  }}</td>
                                        <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
										<td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
                                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['manmonths'] }}%</td>
                                </tr>
                                @else
                                <tr>
                                        <td>Number of {{ $inter_target->intervention()->pluck('intervention') }}</td>
										<td align="right">{{ number_format($inter_target->number ) }}</td>
                                        <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['count'])  }}</td>
										<td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['count'])  }}</td>
                                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['count'] }}%</td>
                                </tr>
                                @endif
                                <tr>
                                        <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
										<td align="right">{{ number_format($inter_target->fbs)  }}</td>
                                        <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
										<td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['fbs'])  }}</td>
                                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['fbs'] }}%</td>
                                </tr>
                                <tr>
                                        <td > &nbsp; &nbsp; Landowners served</td>
										<td align="right">{{ number_format($inter_target->los) }}</td>
                                        <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['los'])  }}</td>
										<td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['los'])  }}</td>
                                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['los'] }}%</td>
                                @if($inter_target->intervention()->pluck('w_sales'))
                                <tr>
                                        <td> &nbsp; &nbsp;  Sales Generated</td>
										<td align="right">{{ '₱ ' . number_format($inter_target->sales, 2) }}</td>
                                        <td align="right">{{ '₱ ' . number_format($current['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
										<td align="right">{{ '₱ ' . number_format($todate['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
                                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['sales'] }}%</td>
                                </tr>
                                @endif
                                <tr>
                                        <td> &nbsp; &nbsp;  ARCs served</td>
										<td align="right">{{ number_format($inter_target->arcs)  }}</td>
                                        <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
										<td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['arcs']) }}</td>
                                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['arcs'] }}%</td>
                                </tr>
                        @endforeach
                
                </tbody>
        </table>
</div>
</html>