<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Trade Fairs</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			@if(Session::get('access') == 3)
				var link = '{{ url('provincial/reports/trade_fairs') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/trade_fairs') }}';
			@endif
			
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/trade_fairs') }}';
			@endif
			
			$("#report").attr('action', link);
		}
		function xls()
		{
				@if(Session::get('access') == 3)
					var link = '{{ url('provincial/reports/trade_fairs-xls') }}';
				@endif
									
				@if(Session::get('access') == 2)
					var link = '{{ url('regional/reports/trade_fairs-xls') }}';
				@endif
				
				@if(Session::get('access') == 1)
					var link = '{{ url('national/reports/trade_fairs-xls') }}';
				@endif
			
				$("#report").attr('action', link);
		}
		function pdf()
		{
				$('input[name=action]').val('pdf');
				$('#report').submit();
		}
	</script>
</head>
<body>
	<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		{{ Form::open(array('url' => 'provincial/reports/trade_fairs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , $results['region'],  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2 @else col-sm-2 @endif">
                {{ Form::label('report_start_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_start_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-2 @endif">
                {{ Form::label('report_end_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_end_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') == 1)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div> 
		   @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		    
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>
<div class="table-report">
	<table class="table-hover">
		<thead >
			<tr>
				<th class="border-less" colspan="11" >
					COMPREHENSIVE AGRARIAN REFORM PROGRAM
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="11">
					Trade Fairs
				</th>
			</tr>
			@if(Session::get('access') == 3)
			<tr>
				<th class="border-less" colspan="11">
					{{ Session::get('region') }}
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="11">
					{{ Session::get('province') }}
				</th>
			</tr>
			@else
				<tr>
				<th class="border-less" colspan="11">
					{{ Session::get('region') }}
				</th>
			</tr>
			@endif
			
			<tr>
				<th class="border-less" colspan="11">
					@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
					@else
						{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
					@endif	
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="11">
				
				</th>
			</tr>	
			
		</thead>
			
		<thead>
			
			<tr>
				<td rowspan="2">MSME</td>
				<td rowspan="2">ARC</td>
				<td  rowspan="2">Date</td>
				<td  rowspan="2">Trade Fair</td>
				<td  rowspan="2">Venue</td>
				<td rowspan="2">Cash Sales</td>
				<td colspan="2" >FBs</td>
				<td colspan="2">LOs</td>
				<td rowspan="2">Remarks</td>
			</tr>	
				
			<tr>
				<td >M</td>
				<td >F</td>
				<td >M</td>
				<td >F</td>
				
			</tr>		
		</thead>
		
					<?php $grand_c=0; ?> 
			@foreach($results['provinces'] as $province)
				@if(Session::get('access') != 3 && $results['count'][$province->id] )
				<tr class="tr-bold tr-uppercase">
					<td>{{ $province->province }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				@endif
				<?php $msme = ''; $arc = 'arc'; $c=0 ?> 
				@foreach( $results['tradefairs'][$province->id] as $tradefair )
				<tr>
					<td>
						@if($tradefair->msme_name != $msme)
							{{ $tradefair->msme_name  }} <?php $c++; $grand_c++; ?>
						@endif
					</td>
					<td>
						@if($tradefair->arc_name != $arc)
							@if($tradefair->arc_name != '') {{ $tradefair->arc_name  }}
							@else 
							Non-ARC
							@endif
						@endif
					</td>
					<td align="center">
					@if ($tradefair->date_constructed == $tradefair->date_finished)
						{{ date_format(date_create($tradefair->date_constructed), 'M d, Y') }}
					@else
						{{ date_format(date_create($tradefair->date_constructed), 'M d, Y') }}
							-
						{{ date_format(date_create($tradefair->date_finished), 'M d, Y') }}
					@endif
					</td>
					<td>{{ $tradefair->name }}</td>
					<td>{{ $tradefair->venue }}</td>
					<td align="right">{{ '₱ ' . number_format($tradefair->sales, 2)  }}</td>
					<td align="center">{{ number_format($tradefair->fb_male) }}</td>
					<td align="center">{{ number_format($tradefair->fb_female) }}</td>
					<td align="center">{{ number_format($tradefair->lo_male) }}</td>
					<td align="center">{{ number_format($tradefair->lo_female) }}</td>
					<td>{{ $tradefair->remarks }}</td>
					
				</tr>
				<?php  $msme = $tradefair->msme_name;  $arc = $tradefair->arc_name; ?>
				@endforeach
				<tr class="tr-bold">
					<td>Total</td>
					<td align="center">{{  number_format($c) }}</td><td></td>
					<td align="center">{{  number_format($results['count'][$province->id]) }}</td>
					<td></td>
					<td align="right">{{ '₱ ' . number_format($results['total_sales'][$province->id], 2)  }}</td>
					<td align="center">{{  number_format($results['total_fb_male'][$province->id]) }}</td>
					<td align="center">{{  number_format($results['total_fb_female'][$province->id]) }}</td>
					<td align="center">{{  number_format($results['total_lo_male'][$province->id]) }}</td>
					<td align="center">{{  number_format($results['total_lo_female'][$province->id]) }}</td>
					<td></td>
				</tr>
				<tr></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
			@endforeach
				@if(Session::get('access') != 3)
				<tr class="tr-bold tr-uppercase">
					<td>Grand Total</td>
					<td  align="center">{{  number_format($grand_c) }}</td><td></td>
					<td align="center">{{  number_format($results['grand_count']) }}</td> 
					<td></td>

					<td align="right">{{ '₱ ' . number_format($results['grand_total_sales'], 2)  }}</</td>
					<td align="center">{{  number_format($results['grand_total_fb_male']) }}</td>
					<td align="center">{{  number_format($results['grand_total_fb_female']) }}</td>
					<td align="center">{{  number_format($results['grand_total_lo_male']) }}</td>
					<td align="center">{{  number_format($results['grand_total_lo_female']) }}</td>
					<td></td>
				</tr>
				@endif
		<tbody>
	</table>
	</body>
</html>
