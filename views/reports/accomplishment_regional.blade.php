<html>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Summary of Accomplishments</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/summary-accomplishments') }}';
			@endif
								
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/summary-accomplishments') }}';
			@endif
			
			$("#report").attr('action', link);
		}
		function xls()
		{
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/summary-accomplishments-xls') }}';
			@endif
								
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/summary-accomplishments-xls') }}';
			@endif
			
			$("#report").attr('action', link);
		}
		function pdf()
		{
				$('input[name=action]').val('pdf');
				$('#report').submit();
		}
	</script>
</head>
<body>
<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		{{ Form::open(array('url' => 'provincial/reports/trade_fairs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $regions_dropdown , $region,  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
           
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-3 @endif">
                 {{ Form::label('year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
			</div>
			
			
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') < 3)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
		    @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		   
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>

            <div class="row table-wrapper-accomplishment">
                <table class="table-hover table-accomplishment">
			<thead >
				    <tr>
					    <th class="border-less" colspan="{{ 5 + count($provinces_current) }}" >
						    COMPREHENSIVE AGRARIAN REFORM PROGRAMs
					    </th>
				    </tr>
				    <tr>
					    <th class="border-less" colspan="{{ 5 + count($provinces_current) }}">
						    Annex 8
					    </th>
				    </tr>
				    <tr>
					    <th class="border-less" colspan="{{ 5 + count($provinces_current) }}">
						    Summary of Accomplishments
					    </th>
				    </tr>
				    <tr>
					    <th class="border-less" colspan="{{ 5 + count($provinces_current) }}">
						    @if(Session::get('access') == 2)
							{{ Session::get('region') }}
						    @else
							{{ Region::where('id', '=', $region)->pluck('region') }}
						    @endif
					    </th>
				    </tr>
				    <tr>
					    
				    </tr>
				    <tr>
					    <th class="border-less" colspan="{{ 5 + count($provinces_current)}}">
						@if(Input::get('report_start_month') == Input::get('report_end_month'))
							    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
						@else
							    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
						@endif
					    </th>
				    </tr>	
				    
			    </thead>
                        <thead>
                                <tr>
                                        <td rowspan="2">Indicators</td>
				        <td rowspan="2">Annual Target</td>
				        <td rowspan="2">This Quarter</th>
                                        <td colspan="{{ count($provinces_current) }}">
						@if(Input::get('report_start_month') == Input::get('report_end_month'))
							    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
						@else
							    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
						@endif
					</td>
					<td rowspan="2">Accomplishment to Date</td>
				        <td rowspan="2">% of Accomplishment</td>
                                </tr>
                                <tr>
				        @foreach($provinces_current as $key => $value)
                                        <td>{{ $key }}</td>
                                        @endforeach
                                </tr>
                        </head>
                        <tbody>
                                <tr>
				    <td>Investments (P M))</td>
					<td align="right">{{ '₱ ' . number_format($annual['investments'], 2)  }}</td>
				    <td align="right">{{ '₱ ' . number_format($current['investments'], 2)  }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ '₱ ' . number_format($value['investments'], 2)   }}</td>
				    @endforeach
				    <td align="right">{{ '₱ ' . number_format($todate['investments'], 2)  }}</td>
				    <td align="right">{{ $percentage['investments'] }}%</td>
                    </tr>
                    <tr>
				    <td>Sales (P M)</td>
				    <td align="right">{{ '₱ ' . number_format($annual['sales'], 2)  }}</td>
				    <td align="right">{{ '₱ ' . number_format($current['sales'], 2)  }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ '₱ ' . number_format($value['sales'], 2) }}</td>
				    @endforeach
				    <td align="right">{{ '₱ ' . number_format($todate['sales'], 2)  }}</td>
				    <td align="right">{{ $percentage['sales'] }}%</td>
                                </tr>
				<tr>
				    <td>Jobs Generated</td>
				    <td align="right">{{ number_format($annual['jobs'] )  }}</td>
				    <td align="right">{{ number_format($current['jobs'])  }}</td>
				    @foreach($provinces_current as $key => $value)
					<td align="right">{{ number_format($value['jobs'] )  }}</td>
				    @endforeach
				    <td align="right">{{ number_format($todate['jobs'])   }}</td>
				    <td align="right">{{ $percentage['jobs'] }}%</td>
                                </tr>
                                <tr>
				    <td>Entrepreneurs Developed</td>
				    <td align="right">{{ number_format($annual['entrepreneurs'])   }}</td>
				    <td align="right">{{ number_format($current['entrepreneurs'])   }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ number_format($value['entrepreneurs'])   }}</td>
				    @endforeach
				    <td align="right">{{ number_format($todate['entrepreneurs'])   }}</td>
				    <td align="right">{{ $percentage['entrepreneurs'] }}%</td>
                                </tr>
                                <tr>
				    <td>Number of ARCs Assisted</td>
				    <td align="right">{{ number_format($annual['arcs_assisted'])  }}</td>
				    <td align="right">{{ number_format($current['arcs_assisted'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ number_format($value['arcs_assisted'])  }}</td>
				    @endforeach
				    <td align="right">{{ number_format($todate['arcs_assisted'])  }}</td>
				    <td align="right">{{ $percentage['arcs_assisted'] }}%</td>
                                </tr>
                                <tr>
                                    <td>Number of Non-ARCs Assisted</td>
				     <td align="right">{{ number_format($annual['non_arcs_assisted']) }}</td>
				     <td align="right">{{ number_format($current['non_arcs_assisted'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ number_format($value['non_arcs_assisted'])  }}</td>
				    @endforeach
				    <td align="right">{{ number_format($todate['non_arcs_assisted'])  }}</td>
				    <td align="right" >{{ $percentage['non_arcs_assisted'] }}%</td>
						
                                </tr>
                                <tr>
                                    <td>Number of MSMEs Developed</td>
				     <td align="right">{{ number_format($annual['msmes_dev'])  }}</td>
				     <td align="right">{{ number_format($current['msmes_dev'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ number_format( $value['msmes_dev']) }}</td>
				    @endforeach
				     <td align="right">{{ number_format($todate['msmes_dev'])  }}</td>
				    <td align="right">{{ $percentage['msmes_dev'] }}%</td>
                                </tr>
                                <tr>
                                    <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
				    <td align="right">{{ number_format($annual['msmes_dev_fb'])  }}</td>
				    <td align="right">{{ number_format($current['msmes_dev_fb'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ number_format($value['msmes_dev_fb'])  }}</td>
				    @endforeach
				     <td align="right">{{ number_format($todate['msmes_dev_fb'])  }}</td>
				    <td align="right">{{ $percentage['msmes_dev_fb'] }}%</td>
                                </tr>
                                <tr>
                                        <td> &nbsp; &nbsp; Landowners served</td>
					<td align="right">{{ number_format($target->msmes_dev_lo)  }}</td>
					<td></td>
					@foreach($provinces_current as $key => $value)
					<td></td>
					@endforeach
					<td></td>
					<td></td>
				</tr>
                                <tr>
                                    <td> &nbsp; &nbsp;  Sales Generated</td>
				     <td align="right">{{ '₱ ' . number_format($annual['established'], 2)  }}</td>
				     <td align="right">{{ '₱ ' . number_format($current['established'], 2)  }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ '₱ ' . number_format($value['established'], 2)  }}</td>
				    @endforeach
				     <td align="right">{{ '₱ ' . number_format($todate['established'], 2)  }}</td>
				    <td align="right">{{ $percentage['established'] }}%</td>
						
                                </tr>
                                <tr>
                                    <td> &nbsp; &nbsp;  ARCs served</td>
				     <td align="right">{{ number_format( $annual['msmes_dev_arcs']) }}</td>
				     <td align="right">{{ number_format($current['msmes_dev_arcs'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ number_format($value['msmes_dev_arcs'])  }}</td>
				    @endforeach
				     <td align="right">{{ number_format($todate['msmes_dev_arcs'])  }}</td>
				    <td align="right">{{ $percentage['msmes_dev_arcs'] }}%</td>
                                </tr>
                                <tr>
                                    <td>Number of MSMEs Assisted</td>
				     <td align="right">{{ number_format($annual['msmes_asst'])  }}</td>
				     <td align="right">{{ number_format($current['msmes_asst'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ number_format($value['msmes_asst'])  }}</td>
				    @endforeach
				     <td align="right">{{ number_format($todate['msmes_asst'])  }}</td>
				    <td align="right">{{ $percentage['msmes_asst'] }}%</td>
                                </tr>
                                <tr>
                                    <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
				     <td align="right">{{ number_format($annual['msmes_asst_fb'] ) }}</td>
				    <td align="right">{{ number_format($current['msmes_asst_fb'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				    <td align="right">{{ number_format($value['msmes_asst_fb'])  }}</td>
				    @endforeach
				    <td align="right">{{ number_format($todate['msmes_asst_fb'])  }}</td>
				    <td align="right">{{ $percentage['msmes_asst_fb'] }}%</td>
                                </tr>
                                <tr>
                                    <td> &nbsp; &nbsp; Landowners served</td>
				     <td align="right">{{ number_format($annual['msmes_asst_lo'])  }}</td>
				     <td align="right">{{ number_format($current['msmes_asst_lo'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ number_format($value['msmes_asst_lo'])  }}</td>
				    @endforeach
				    <td align="right">{{ number_format($todate['msmes_asst_lo'])  }}</td>
				    <td align="right">{{ $percentage['msmes_asst_lo'] }}%</td>
                                </tr>
                                <tr>
                                    <td> &nbsp; &nbsp;  Monitored Sales</td>
				    <td align="right">{{ '₱ ' . number_format($annual['monitored'], 2)  }}</td>
				     <td align="right">{{ '₱ ' . number_format($current['monitored'], 2)  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ '₱ ' . number_format($value['monitored'], 2)  }}</td>
				    @endforeach
				    <td align="right">{{ '₱ ' . number_format($todate['monitored'], 2)  }}</td>
				    <td align="right">{{ $percentage['monitored'] }}%</td>
						
                                </tr>
                                <tr>
				    <td> &nbsp; &nbsp;  ARCs served</td>
				     <td align="right">{{ number_format($annual['msmes_asst_arcs'])  }}</td>
				     <td align="right">{{ number_format($current['msmes_asst_arcs'])  }}</td>
				    @foreach($provinces_current as $key => $value)
				     <td align="right">{{ number_format($value['msmes_asst_arcs'])  }}</td>
				    @endforeach
				     <td align="right">{{ number_format($todate['msmes_asst_arcs'])  }}</td>
				    <td align="right">{{ $percentage['msmes_asst_arcs'] }}%</td>
                                </tr>
                        
                                @foreach ($inter_targets as $inter_target)
                                    @if($inter_target->intervention_id == 13)
				    <tr>
						<td>Number of Man-months Consultancy</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['manmonths'],5) }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
						@foreach($provinces_current as $key => $value)
							     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
						@endforeach
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
						<td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['manmonths'] }}%</td>
				    </tr>
				    @else
				    <tr>
						<td>Number of {{ $inter_target->intervention()->pluck('intervention') }}</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['count'] )  }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['count']) }}</td>
						@foreach($provinces_current as $key => $value)
							     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['count'])  }}</td>
						@endforeach
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['count'])  }}</td>
						<td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['count'] }}%</td>
				    </tr>
				    @endif
				    <tr>
						<td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						@foreach($provinces_current as $key => $value)
							     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						@endforeach
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						 <td align="right">{{  $percentage['interattns'][$inter_target->intervention_id]['fbs'] }}%</td>
				    </tr>
				    <tr>
						<td> &nbsp; &nbsp; Landowners served</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['los'])  }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['los']) }}</td>
						@foreach($provinces_current as $key => $value)
							     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['los'] ) }}</td>
						@endforeach
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['los'])  }}</td>
						 <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['los']  }}%</td>
				    @if($inter_target->intervention()->pluck('w_sales'))
				    <tr>
						<td> &nbsp; &nbsp;  Sales Generated</td>
						 <td align="right">{{ '₱ ' . number_format($annual['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
						 <td align="right">{{ '₱ ' . number_format($current['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
						@foreach($provinces_current as $key => $value)
							    <td align="right">{{ '₱ ' . number_format($value['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
						@endforeach
						 <td align="right">{{ '₱ ' . number_format($todate['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
						<td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['sales'] }}%</td>
				    </tr>
				    @endif
				    <tr>
						 <td> &nbsp; &nbsp;  ARCs served</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
						@foreach($provinces_current as $key => $value)
							    <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['arcs']) }}</td>
						@endforeach
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
						<td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['arcs'] }}%</td>
				    </tr>
                                @endforeach
                        </tbody>
                </table>
            </div>
</html>