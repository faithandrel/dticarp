<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Summary of Trainings</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			@if(Session::get('access') == 3)
				var link = '{{ url('provincial/reports/trainings') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/trainings') }}';
			@endif
			
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/trainings') }}';
			@endif
			
			$("#report").attr('action', link);
		}
		function xls()
		{
			@if(Session::get('access') == 3)
				var link = '{{ url('provincial/reports/trainings-xls') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/trainings-xls') }}';
			@endif
			
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/trainings-xls') }}';
			@endif
			$("#report").attr('action', link);
		}
	</script>
</head>
<body>
<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		{{ Form::open(array('url' => 'provincial/reports/trainings', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , null,  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2 @else col-sm-2 @endif">
                {{ Form::label('report_start_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_start_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-2 @endif">
                {{ Form::label('report_end_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_end_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') == 1)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
		    @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		   
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>
	
<div class="table-report">
	<table class="table-hover">
		<thead >
			<tr>
				<th class="border-less" colspan="13" >
					COMPREHENSIVE AGRARIAN REFORM PROGRAM
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="13" >
					Annex 7
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="13">
					SUMMARY OF TRAININGS / MISSIONS CONDUCTED / FACILITATED
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="13">
					{{ Session::get('province') }}
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="13">
				@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@endif
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="13">
				
				</th>
			</tr>	
			
		</thead>
			
		<thead>
			
			<tr>
				<td rowspan="3">Title of Training/Proponent</td>
				<td rowspan="3">Date Conducted </td>
				<td  colspan="7" >Participants</td>
				<td  colspan="3" >Cost</td>
				<td rowspan="3">Venue/Remarks</td>
			</tr>	
			<tr>
				<td colspan="2" >FBs</td>
				<td colspan="2">LOs</td>
				<td colspan="2">Non-Carp</td>
				<td rowspan="2">Total</td>
				<td rowspan="2">Carp</td>
				<td rowspan="2">Others</td>
				<td rowspan="2">Total</td>
			</tr>	
			<tr>
				<td >M</td>
				<td >F</td>
				<td >M</td>
				<td >F</td>
				<td >M</td>
				<td >F</td>
			</tr>	
		</thead>
		
		<tbody>
		@foreach ($results['interventions'] as $intervention)
			
			<tr> <td class="td-bold td-uppercase">{{ $intervention->intervention }}</td>
				</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			
			<?php $c=1; ?>
			@foreach ($results['provinces'] as $province)
				@if($results['activities'][$intervention->id][$province->id] != '[]' && Session::get('access') != 3)
				<tr> <td class="td-bold">{{ $province->province }}</td>
					</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				@endif
				@foreach ($results['activities'][$intervention->id][$province->id] as $activity)
					<tr> 
						<td >&nbsp; &nbsp;{{ $activity->name }}
						<td align="center">
									@if ($activity->date_constructed == $activity->date_finished)
										{{ date_format(date_create($activity->date_constructed), 'M d, Y') }}
									@else
										{{ date_format(date_create($activity->date_constructed), 'M d, Y') }}
											-
										{{ date_format(date_create($activity->date_finished), 'M d, Y') }}
									@endif
						</td>
						<td align="center">{{ number_format($activity->fb_m) }}</td>
						<td align="center">{{ number_format($activity->fb_f) }}</td>
						<td align="center">{{ number_format($activity->lo_m) }}</td>
						<td align="center">{{ number_format($activity->lo_f) }}</td>
						<td align="center">{{ number_format($activity->ncb_m) }}</td>
						<td align="center">{{ number_format($activity->ncb_f) }}</td>
						<td align="center">{{ number_format($activity->total_attn) }}</td>
						<td align="right">{{ '₱ ' . number_format($activity->carp, 2)   }}</td>
						<td align="right">{{ '₱ ' . number_format($activity->others, 2)   }}</td>
						<td align="right">{{ '₱ ' . number_format($activity->total_cost, 2)	 }}</td>
						<td align="left">{{ $activity->venue.' '.$activity->remarks }}</td>
					</tr>
					@if($c == $results['count'][$activity->intervention_id])
						<tr class="tr-bold"><td>Total</td>
						<td align="center">{{ number_format($results['count'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['fb_m_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['fb_f_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['lo_m_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['lo_f_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['ncb_m_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['ncb_f_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['total_attn'][$intervention->id]) }}</td>
						<td align="right">{{ '₱ ' . number_format($results['carp'][$intervention->id], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($results['others'][$intervention->id], 2) 	 }}</td>
						<td align="right">{{ '₱ ' . number_format($results['total_cost'][$intervention->id], 2)	}}</td><td></td>
						</tr>
						<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><tr>
				@endif
				<?php $c++; ?>
				@endforeach
			@endforeach
		@endforeach
		
				<tr class="tr-bold tr-uppercase">
					<td>Grand Total</td>
					<td align="center">{{ number_format($results['grand_count']) }}</td>
					<td align="center">{{ number_format($results['grand_fb_m_total']) }}</td>
					<td align="center">{{ number_format($results['grand_fb_f_total']) }}</td>
					<td align="center">{{ number_format($results['grand_lo_m_total']) }}</td>
					<td align="center">{{ number_format($results['grand_lo_f_total']) }}</td>
					<td align="center">{{ number_format($results['grand_ncb_m_total']) }}</td>
					<td align="center">{{ number_format($results['grand_ncb_f_total']) }}</td>
					<td align="center">{{ number_format($results['grand_total_attn']) }}</td>
					<td align="right">{{ '₱ ' . number_format($results['grand_carp'], 2)  }}</td>
					<td align="right">{{ '₱ ' . number_format($results['grand_others'], 2) 	 }}</td>
					<td align="right">{{ '₱ ' . number_format($results['grand_total_cost'], 2)	}}</td><td></td>
					</tr>
			
	
		</tbody>
	</table>
</body>
</html>