<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Gender-Disaggregated Data on Program Services</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/gender-disaggregated') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/gender-disaggregated') }}';
			@endif
			
			$("#report").attr('action', link);
		}
		function xls()
		{
			@if(Session::get('access') == 1)
				var link = '{{ url('national/reports/gender-disaggregated-xls') }}';
			@endif
								
			@if(Session::get('access') == 2)
				var link = '{{ url('regional/reports/gender-disaggregated-xls') }}';
			@endif

			$("#report").attr('action', link);
		}
		function pdf()
		{
			$('input[name=action]').val('pdf');
			$('#report').submit();
		}
	</script>
</head>
<body>
<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		{{ Form::open(array('url' => 'provincial/reports/trade_fairs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , null,  array('class'=>'form-control input-sm' )) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2 @else col-sm-2 @endif">
                {{ Form::label('report_start_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_start_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-md-2 col-sm-2  @else col-sm-2 @endif">
                {{ Form::label('report_end_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_end_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') == 1)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
		     @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		  
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>

    <div class="table-report">
	    <table class="table-hover">
		<thead >
			<tr>
				<th class="border-less" colspan="{{ 4 + count($provinces)*3 }}" >
					COMPREHENSIVE AGRARIAN REFORM PROGRAM
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="{{ 4 + count($provinces)*3 }}">
					Annex 3
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="{{ 4 + count($provinces)*3 }}">
					Gender-Disaggregated Data on Program Services
				</th>
			</tr>
			
			<tr>
				<th class="border-less" colspan="{{ 4 + count($provinces)*3 }}">
                                    @if(Session::get('access') == 2)
					{{ Session::get('region') }}
                                    @else
                                        {{ Region::where('id', '=', $region)->pluck('region') }}
                                    @endif
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="{{ 4 + count($provinces)*3 }}">
				    @if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				    @else
					    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				    @endif	
				</th>
			</tr>	
			
		</thead>
			<thead>
			
			<tr>
				<td rowspan=2>Indicators</td>
				<td colspan=3>GRAND TOTAL</td>
				@foreach($provinces as $province)
					<td colspan=3>{{ $province }}</td>
				@endforeach
			</tr>
			<tr>
				<td >Total</td>
				<td >Male</td>
				<td >Female</td>
				@foreach($provinces as $province)
					<td >Total</td>
					<td >Male</td>
					<td >Female</td>
				@endforeach
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>No. of Entrepreneurs Developed</td>
				<td align="center">{{ $grand_total['entrepreneurs'] }}</td>
				<td></td>
				<td></td>
				@foreach($provinces as $province)
					<td  align="center">{{ $gender_disag[$province]['entrepreneurs'] }}</td>
					<td ></td>
					<td ></td>
				@endforeach
			</tr>
			<tr>
				<td>&nbsp&nbsp No. of FBs Served</td>
				<td align="center">{{ $grand_total['msmes_dev_fb_m'] + $grand_total['msmes_dev_fb_f'] }}</td>
				<td align="center">{{ $grand_total['msmes_dev_fb_m'] }}</td>
				<td align="center" >{{ $grand_total['msmes_dev_fb_f'] }}</td>
				@foreach($provinces as $province)
					<td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] + $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
				@endforeach
			</tr>
			
			<tr>
				<td>No. of MSMEs Developed</td>
				<td  align="center">{{ $grand_total['entrepreneurs'] }}</td>
				<td></td>
				<td></td>
				@foreach($provinces as $province)
					<td  align="center">{{ $gender_disag[$province]['entrepreneurs'] }}</td>
					<td ></td>
					<td ></td>
				@endforeach
			</tr>
			<tr>
				<td>&nbsp&nbsp No. of FBs Served</td>
				<td  align="center">{{ $grand_total['msmes_dev_fb_m'] + $grand_total['msmes_dev_fb_f'] }}</td>
				<td  align="center">{{ $grand_total['msmes_dev_fb_m'] }}</td>
				<td  align="center">{{ $grand_total['msmes_dev_fb_f'] }}</td>
				@foreach($provinces as $province)
					<td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] + $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
				@endforeach
			</tr>
			<tr>
				<td>&nbsp&nbsp No. of LOs/NCBs Served</td>
				<td></td>
				<td></td>
				<td></td>
				@foreach($provinces as $province)
					<td></td>
					<td></td>
					<td></td>
				@endforeach
			</tr>
			<tr>
				<td>No. of MSMEs Assisted</td>
				<td  align="center">{{ $grand_total['msmes_asst'] }}</td>
				<td></td>
				<td></td>
				@foreach($provinces as $province)
					<td align="center" >{{ $gender_disag[$province]['msmes_asst'] }}</td>
					<td ></td>
					<td ></td>
				@endforeach
			</tr>
			<tr>
				<td>&nbsp&nbsp No. of FBs Served</td>
				<td align="center">{{ $grand_total['msmes_asst_fb_m'] + $grand_total['msmes_asst_fb_f'] }}</td>
				<td align="center">{{ $grand_total['msmes_asst_fb_m'] }}</td>
				<td align="center">{{ $grand_total['msmes_asst_fb_f'] }}</td>
				@foreach($provinces as $province)
					<td  align="center">{{ $gender_disag[$province]['msmes_asst_fb_m'] + $gender_disag[$province]['msmes_asst_fb_f'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_asst_fb_m'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_asst_fb_f'] }}</td>
				@endforeach
			</tr>
			<tr>
				<td>&nbsp&nbsp No. of LOs/NCBs Served</td>
				<td align="center">{{ $grand_total['msmes_asst_lo_m'] + $grand_total['msmes_asst_lo_f'] }}</td>
				<td align="center">{{ $grand_total['msmes_asst_lo_m'] }}</td>
				<td align="center">{{ $grand_total['msmes_asst_lo_f'] }}</td>
				@foreach($provinces as $province)
					<td  align="center">{{ $gender_disag[$province]['msmes_asst_lo_m'] + $gender_disag[$province]['msmes_asst_lo_f'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_asst_lo_m'] }}</td>
					<td  align="center">{{ $gender_disag[$province]['msmes_asst_lo_f'] }}</td>
				@endforeach
			</tr>
			@foreach( $gender_disag[$province]['interattns']  as $intervention => $attendance )
				@if( !(in_array($intervention,array('Product Developed', 'Prototype Executed', 'Packaging and Label Developed'))) )
				<tr>
					@if($intervention == 'Consultancy')
					<td>No. of Man-months Consultancy</td>
					<td align="center">{{ $grand_total[$intervention]['manmonths'] }}</td>
					<td></td>
					<td></td>
						@foreach($provinces as $province)
							<td  align="center">{{  $gender_disag[$province]['interattns'][$intervention]['manmonths'] }}</td>
							<td ></td>
							<td ></td>
						@endforeach
					@else
					<td>No. of {{ $intervention }}</td>
					<td  align="center">{{ $grand_total[$intervention]['count'] }}</td>
					<td></td>
					<td></td>
						@foreach($provinces as $province)
							<td  align="center">{{ $gender_disag[$province]['interattns'][$intervention]['count'] }}</td>
							<td ></td>
							<td ></td>
						@endforeach
					@endif
				</tr>
				<tr>
					<td>&nbsp&nbsp No. of FBs Served</td>
					<td align="center">{{ $grand_total[$intervention]['fb_male'] + $grand_total[$intervention]['fb_female'] }}</td>
					<td align="center">{{ $grand_total[$intervention]['fb_male'] }}</td>
					<td align="center">{{ $grand_total[$intervention]['fb_female'] }}</td>
					@foreach($provinces as $province)
						<td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['fb_male'] + $gender_disag[$province]['interattns'][$intervention]['fb_female'] }}</td>
						<td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['fb_male']  }}</td>
						<td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['fb_female'] }}</td>
					@endforeach
					
				</tr>
				<tr>
					<td>&nbsp&nbsp No. of LOs/NCBs Served</td>
					<td align="center">{{ $grand_total[$intervention]['lo_male'] + $grand_total[$intervention]['lo_female'] }}</td>
					<td align="center">{{ $grand_total[$intervention]['lo_male'] }}</td>
					<td align="center">{{ $grand_total[$intervention]['lo_female'] }}</td>
					@foreach($provinces as $province)
						<td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['lo_male'] + $gender_disag[$province]['interattns'][$intervention]['lo_female'] }}</td>
						<td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['lo_male']  }}</td>
						<td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['lo_female'] }}</td>
					@endforeach
				</tr>
				@endif
			@endforeach
		</tbody>
	    </table>
        </div>
	</body>
</html>