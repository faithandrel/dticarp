<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Accomplishment Per ARC</title>

	{{ HTML::style('bower_components/bootstrap/dist/css/bootstrap.min.css') }}
	{{ HTML::style('bower_components/font-awesome/css/font-awesome.min.css') }}
	{{ HTML::style('dist/css/xls.css') }}
	{{ HTML::script('bower_components/jquery/dist/jquery.min.js') }}

	<script>
		function create()
		{	
			$('input[name=action]').val('create');
			$('#report').submit();
		}
		function xls()
		{
			$('input[name=action]').val('xls');
			$('#report').submit();
		}
		function pdf()
		{
			$('input[name=action]').val('pdf');
			$('#report').submit();
		}
	</script>
</head>
<body>

	<span  title="Show Report Navigation" class="no-print down"> <i class="fa fa-bars"></i></span>

	<div class="row container filter-wrapper">
		@if(Session::get('access') == 1)
		{{ Form::open(array('url' => 'national/reports/accomplishment-arcs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
		@elseif (Session::get('access') == 2)
		{{ Form::open(array('url' => 'regional/reports/accomplishment-arcs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
		@else
		{{ Form::open(array('url' => 'provincial/reports/accomplishment-arcs', 'class'=>'form-horizontal', 'id'=>'report', 'role'=>'form')) }}
		@endif
         <div class="col-xs-12 filter no-print" >
			<div class="col-sm-1 report-logo-wrapper @if(Session::get('access') == 1) report-logo-wrapper-nat @endif"><img class="report-logo " src="{{ url('/img/logo.png') }}"> </div>
			@if(Session::get('access') == 1)  
			<div class="form-group col-sm-2">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $region_dropdown , $region,  array('class'=>'form-control input-sm')) }}
			</div>
			@endif
			@if(Session::get('access') == 2 OR Session::get('access') == 1)  
            <div class="form-group col-sm-2" >
				{{ Form::label('province', 'Province', array('class'=>'control-label')) }}
				{{ Form::select('province', $province_dropdown , $province,  array('class'=>'form-control input-sm')) }}
			</div>
			@endif
            <div class="form-group col-sm-2" >
                {{ Form::label('report_start_month', 'Start Month', array('class'=>'control-label drop-report')) }}
                {{ Form::select('report_start_month', $months_dropdown , NULL,  array('class'=>'form-control input-sm')) }}

            </div>
            <div class="form-group @if(Session::get('access') == 1) col-sm-1 col-lg-1 @else col-sm-2 @endif">
                {{ Form::label('report_start_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_start_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			
            <div class="form-group col-sm-2">
                {{ Form::label('report_end_month', 'End Month', array('class'=>'control-label')) }}
                {{ Form::select('report_end_month',$months_dropdown, NULL,  array('class'=>'form-control input-sm')) }}
            </div>
            <div class="form-group @if(Session::get('access') == 1) col-sm-1 col-lg-1 @else col-sm-2 @endif">
                {{ Form::label('report_end_year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('report_end_year', $years_dropdown, date('Y'),  array('class'=>'form-control input-sm')) }}
            </div>
			{{ Form::hidden('action', '1') }}
			@if(Session::get('access') < 3)
			<div class="button ">
				<div class="btn-group short-nat col-md-1">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div>
			@else
			<div class="button ">
				<div class="btn-group col-md-2 col-lg-3 long ">
				  	<button onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> Create</button>
					<button onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> Save as xls</button>
					<button onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> Print</button>
				</div>
				<div class="btn-group col-md-2 short">
				  	<button title="Create Report" onclick="create()" class="btn  btn-default btn-create"><i class="fa fa-refresh fa-fw"></i> </button>
					<button title="Download Report" onclick="xls()" class="btn btn-default btn-xls"><i class="fa fa-download fa-fw"></i> </button>
					<button title="Print Report" onclick="window.print()" class="btn btn-default btn-print"><i class="fa fa-print fa-fw"></i> </button>
				</div>
           </div> 
		   @endif
		  <span title="Hide Report Navigation" class="up no-print" ><i class="fa fa-times"></i></span>
		   
        </div>
        {{ Form::close() }}
		<script>
			$( ".up" ).click(function() {
			  $( ".filter" ).fadeOut();
			  $( "table" ).css( "margin-top", "20px " );
			  $( "table" ).removeClass( "table-margin" );
			  $( "down" ).show();
			});
			$( ".down" ).click(function() {
			  $( ".filter" ).fadeIn();
			  $( "table" ).addClass( "table-margin" );
			// $( "table" ).css( "margin-top", "120px" );
			  $( "up" ).fadeIn();
			});
		</script>
     </div>


<div class="table-report">
	<table class="table-hover">
		<thead >
			<tr>
				<th class="border-less" colspan="9" >
					COMPREHENSIVE AGRARIAN REFORM PROGRAM
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="9">
					Annex 9
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="9">
					ACCOMPLISHMENT PER ARC
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="9">
					@if(Session::get('access') == 3)
						{{ Session::get('province') }}
					@else
						{{ Province::where('id', '=', $province)->pluck('province') }}
					@endif
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="9">
				@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@endif				
				</th>
			</tr>
			<tr>
				<th class="border-less" colspan="9">
				
				</th>
			</tr>		
			
		</thead>
			
		<thead class="tb-header">
			<tr>
				<td class="border-less-bottom"></td>
				<td class="border-less-bottom"></td>
				<td colspan="4" >ARBs</td>
				<td colspan="2"  >COST</td>
				<td rowspan="3" width="29%">Remarks</td>
			</tr>		
			<tr>
				<td class="border-less-top" align="top" rowspan="2" width="40%">ARC NAME/TITLE OF ACTIVITY</td>
				<td class="border-less-top" rowspan="2" width="20%">DATE OF CONDUCT</td>
				<td colspan="2" >FBs</td>
				<td colspan="2" >LOs</td>
				<td rowspan="2" width="15%">FUND 158</td>
				<td rowspan="2" width="15%">Others</td>
			</tr>
			<tr>
				<td>Male</td>
				<td>Female</td>
				<td>Male</td>
				<td>Female</td>
			</tr>
		</thead>
		<tbody>
			@foreach($msmes as $msme)
				<!-- will not display arc with no igp, act, inv, sales, entrep -->
				@if ($msme->projects == "[]" && $msme->attns == array() && $msme->investments == 0 && $msme->sales == 0
						&& $msme->jobs == 0 && $msme->entreps == 0)
						<?php continue; ?>
				@endif
				<!-- will not display arc with no igp, act, inv, sales, entrep -->
				
			<tr class="border-less-bottom"> 
				<td >
				<b>Name of Arc: @if($msme->arc_name == '') Non-ARC @else {{ $msme->arc_name }} @endif</b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top border-less-bottom">
				<td ><b>Barangay/Municipality:  </b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top border-less-bottom">
				<td><b>Legislative District:  </b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr> 
			<tr class="border-less-top border-less-bottom">
				<td><b>MSME:  {{ $msme->msme_name }}</b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top border-less-bottom">
				<?php
						$d1 = new DateTime(date("Y-m-d"));
						$d2 = new DateTime($msme->date_assisted);
						$diff = $d2->diff($d1); 
					?>
				<td><b>No. of years assisted:  @if($diff->y == 0) New @else {{ $diff->y }} @endif</b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top border-less-bottom">
				<td><b>Industry Focus:  </b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			
			<tr class="border-less-top border-less-bottom">
				<td><b>Income Generating Projects (IGPs):  </b></td>
				<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
				
			<?php $a = 'a'; ?>
			@foreach($msme->projects as $project)
				<tr class="border-less-top border-less-bottom">
					<td>&nbsp; {{ $a }}. {{ $project->igp_name }}</td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<?php $a++; ?>
			@endforeach
			
			@foreach($msme->attns as $category => $interventions)	
				<tr class="border-less-top border-less-bottom">
					<td><b>{{ $category }} </b></td>
					<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				<?php $c2 = 1; ?>
				@foreach( $interventions as $activity => $attendances)
					<tr class="border-less-top border-less-bottom">
						<td>&nbsp; <b> {{ $c2 }}. {{$activity}}  </b> </td>
						<td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
					</tr>
					<?php $a1 = 'a'; ?>
					@foreach($attendances as $attendance)
						<tr class="border-less-top border-less-bottom">
							<td style="padding-left: 30px;">{{ $a1.'. '.$attendance->activity }} </td>
							<td align="center">
								@if ($attendance->activity_date == $attendance->date_finished_end)
									{{ date_format(date_create($attendance->activity_date), 'M d, Y') }}
								@else
									{{ date_format(date_create($attendance->activity_date), 'M d, Y') }}
										-
									{{ date_format(date_create($attendance->activity_date_end), 'M d, Y') }}
								@endif
							</td>
							<td align="center">{{ number_format($attendance->fb_male) }}</td>
							<td align="center">{{ number_format($attendance->fb_female) }}</td>
							<td align="center">{{ number_format($attendance->lo_male) }}</td>
							<td align="center">{{ number_format($attendance->lo_female) }}</td>
							<td align="right">{{ '₱ ' . number_format($attendance->cost_carp, 2) }}</td>
							<td align="right">{{ '₱ ' . number_format($attendance->cost_others, 2) }}</td>
							<td>{{ $attendance->remarks }}</td>
						</tr>
					<?php $a1++; ?>
					@endforeach
				<?php $c2++; ?>
				@endforeach
			@endforeach
			<tr class="border-less-top border-less-bottom">
				<td><b>Investments: </b></td>
				<td align="center">{{'₱ ' . number_format($msme->investments, 2) }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top border-less-bottom">
				<td><b>Sales Generated: </b></td>
				<td align="center">{{ '₱ ' . number_format($msme->sales, 2)  }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top border-less-bottom">
				<td><b>Jobs Generated: </b></td>
				<td align="center">{{ number_format($msme->jobs)  }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			<tr class="border-less-top">
				<td><b>Entrepreneurs Developed: </b></td>
				<td align="center">{{ number_format($msme->entreps) }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			@endforeach

		</tbody>
	</table>
</div>
<script>
	@if(Session::get('access') == 1)
	$.get("{{ url('national/province-dropdown')}}", { option: $("select[name='region']").val() }, 
		function(data) {
		    var provinces = $("select[name='province']");
		    provinces.empty();
		    $.each(data, function(key, value) {   
		     provinces.append($("<option></option>").attr( "key", key ).attr( "value", value ).text(key)); 
		  });
	});
	$("select[name='region']").change(function(){
		$.get("{{ url('national/province-dropdown')}}", { option: $("select[name='region']").val() }, 
		    function(data) {
			var provinces = $("select[name='province']");
			provinces.empty();
			$.each(data, function(key, value) {   
			 provinces.append($("<option></option>").attr( "key", key ).attr( "value", value ).text(key)); 
		      });
		    });
	});
	@endif
</script>
</body>
</html>