<meta charset="utf-8">
<html>
    <style>
        td.header, tr.header > td {
           text-align: center;
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
	<tr><td class="header" colspan="9">COMPREHENSIVE AGRARIAN REFORM PROGRAM</td></tr>
        <tr><td class="header" colspan="9">Annex 4</td></tr>
        <tr><td class="header" colspan="9"><strong>Sales Generated</strong></td></tr>
        <tr><td class="header" colspan="9">{{ Session::get('province') }}</td></tr>
        <tr><td class="header" colspan="9">
            @if(Input::get('report_start_month') == Input::get('report_end_month'))
                    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @else
                    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @endif
        </td></tr>
	<tr><td class="header" colspan="9"></td></tr>		
   
        <tr class="header bold">
            
            <td width="30">ARC</td>
            <td width="30">Name of Assn/MSME</td>
            <td width="40">Project/IGPs</td>
            <td width="20">Sales</td>
            @foreach($interventions as $intervention)
                <td width="20">{{ $intervention->intervention }}</td>
            @endforeach
            <td width="20">Monitored</td>
            <td width="20">Established</td>
        </tr>
            @foreach($msmes as $msme)
            <tr>
              
                <td>{{ $msme->name }}</td>
                <td>{{ $msme->msme_name }}</td>
                <?php $igps = $msme->igp->slice(0,2); ?>
                <td>
                    @foreach($igps as $igp)
                         {{ $igp->igp_name }},
                    @endforeach
                </td>
                <td align="right">
                    {{ '₱ ' . number_format($msme->sales, 2)  }}
                </td>
                @foreach($interventions as $intervention)
                     <td align="right">
                    {{ '₱ ' . number_format($totals['activities'][$msme->id][$intervention->id], 2)  }}
                     </td>
                @endforeach
                <td align="right">{{ '₱ ' . number_format($msme->monitored, 2)  }}</td>
                <td align="right">{{ '₱ ' . number_format($msme->established, 2)  }}</td>
            </tr>
        @endforeach
        @if(!empty($totals))
        <tr class="bold">
            <td align="right">Total</td><td></td><td></td><td align="right">{{ '₱ ' . number_format($totals['igp_sales'], 2)  }}</td>
            @foreach($interventions as $intervention)
                   <td  align="right">{{ '₱ ' . number_format($totals[$intervention->id], 2)   }}</td>
            @endforeach
            <td align="right">{{ '₱ ' . number_format($totals['monitored'], 2)  }}</td>
			<td  align="right">{{ '₱ ' . number_format($totals['established'], 2)  }}</td>
        </tr>
        @endif
    </table>
</html>