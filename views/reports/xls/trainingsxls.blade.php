<meta charset="utf-8">
<html>
     <style>
         td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
		
    </style>
    <table>
		<tr><td class="header" colspan="13">COMPREHENSIVE AGRARIAN REFORM PROGRAM</td></tr>
        <tr><td class="header" colspan="13">Annex 7</td></tr>
        <tr><td class="header" colspan="13"><strong>SUMMARY OF TRAININGS / MISSIONS CONDUCTED / FACILITATED</strong></td></tr>
        <tr><td class="header" colspan="13">{{ Session::get('province') }}</td></tr>
        <tr><td class="header" colspan="13">@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@endif	</td></tr>
		<tr><td class="header" colspan="13"></td></tr>		
      
        <tr class="header bold">
			
                <td></td>
				<td></td>
                <td colspan="7">Participants</td>
                <td colspan="3">Cost</td>
                <td width="30" rowspan="3">Remarks</td>
        </tr>	
        <tr class="header bold">
				
				<td width="50" >Title of Training/Proponent</td>
                <td width="30" >Date Conducted </td>
                <td colspan="2">FBs</td>
                <td colspan="2">LOs</td>
                <td colspan="2">Non-Carp</td>
                <td rowspan="2">Total</td>
                <td width="15" rowspan="2">Carp</td>
                <td width="15" rowspan="2">Others</td>
                <td width="15" rowspan="2">Total</td>
        </tr>	
        <tr class="header bold">
				<td></td>
				<td></td>
                <td >M</td>
                <td >F</td>
                <td >M</td>
                <td >F</td>
                <td >M</td>
                <td >F</td>
        </tr>
		
@foreach ($results['interventions'] as $intervention)
			
			<tr> <td class="td-bold td-uppercase">{{ $intervention->intervention }}</td>
				</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
			</tr>
			
			<?php $c=1; ?>
			@foreach ($results['provinces'] as $province)
				@if($results['activities'][$intervention->id][$province->id] != '[]' && Session::get('access') != 3)
				<tr> <td class="td-bold">{{ $province->province }}</td>
					</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				@endif
				@foreach ($results['activities'][$intervention->id][$province->id] as $activity)
					<tr> 
						<td >&nbsp; &nbsp;{{ $activity->name }}
						<td align="center">
									@if ($activity->date_constructed == $activity->date_finished)
										{{ date_format(date_create($activity->date_constructed), 'M d, Y') }}
									@else
										{{ date_format(date_create($activity->date_constructed), 'M d, Y') }}
											-
										{{ date_format(date_create($activity->date_finished), 'M d, Y') }}
									@endif
						</td>
						<td align="center">{{ number_format($activity->fb_m) }}</td>
						<td align="center">{{ number_format($activity->fb_f) }}</td>
						<td align="center">{{ number_format($activity->lo_m) }}</td>
						<td align="center">{{ number_format($activity->lo_f) }}</td>
						<td align="center">{{ number_format($activity->ncb_m) }}</td>
						<td align="center">{{ number_format($activity->ncb_f) }}</td>
						<td align="center">{{ number_format($activity->total_attn) }}</td>
						<td align="right">{{ '₱ ' . number_format($activity->carp, 2)   }}</td>
						<td align="right">{{ '₱ ' . number_format($activity->others, 2)   }}</td>
						<td align="right">{{ '₱ ' . number_format($activity->total_cost, 2)	 }}</td>
						<td align="right">{{ $activity->remarks }}</td>
					</tr>
					@if($c == $results['count'][$activity->intervention_id])
						<tr class="tr-bold"><td>Total</td>
						<td align="center">{{ number_format($results['count'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['fb_m_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['fb_f_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['lo_m_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['fb_f_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['ncb_m_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['ncb_f_total'][$intervention->id]) }}</td>
						<td align="center">{{ number_format($results['total_attn'][$intervention->id]) }}</td>
						<td align="right">{{ '₱ ' . number_format($results['carp'][$intervention->id], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($results['others'][$intervention->id], 2) 	 }}</td>
						<td align="right">{{ '₱ ' . number_format($results['total_cost'][$intervention->id], 2)	}}</td><td></td>
						</tr>
						<tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><tr>
				@endif
				<?php $c++; ?>
				@endforeach
			@endforeach
		@endforeach
		
				<tr class="tr-bold tr-uppercase">
					<td>Grand Total</td>
					<td align="center">{{ number_format($results['grand_count']) }}</td>
					<td align="center">{{ number_format($results['grand_fb_m_total']) }}</td>
					<td align="center">{{ number_format($results['grand_fb_f_total']) }}</td>
					<td align="center">{{ number_format($results['grand_lo_m_total']) }}</td>
					<td align="center">{{ number_format($results['grand_fb_f_total']) }}</td>
					<td align="center">{{ number_format($results['grand_ncb_m_total']) }}</td>
					<td align="center">{{ number_format($results['grand_ncb_f_total']) }}</td>
					<td align="center">{{ number_format($results['grand_total_attn']) }}</td>
					<td align="right">{{ '₱ ' . number_format($results['grand_carp'], 2)  }}</td>
					<td align="right">{{ '₱ ' . number_format($results['grand_others'], 2) 	 }}</td>
					<td align="right">{{ '₱ ' . number_format($results['grand_total_cost'], 2)	}}</td><td></td>
					</tr>
			
			
    </table>
</html>