<meta charset="utf-8">
<html>
    <style>
        td.header, tr.header > td {
           text-align: center;
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
		<tr><td class="header" colspan="4">COMPREHENSIVE AGRARIAN REFORM PROGRAM</td></tr>
        <tr><td class="header" colspan="4">Annex 5</td></tr>
        <tr><td class="header" colspan="4"><strong>Jobs Generated</strong></td></tr>
        @if(Session::get('access') == 3)
			<tr>
				<td  class="header" colspan="4">
					{{ Session::get('region') }}
				</td>
			</tr>
			<tr>
				<td class="header" colspan="4">
					{{ Session::get('province') }}
				</td>
			</tr>
			@else
				<tr>
				<td  class="header" colspan="4">
					{{ Session::get('region') }}
				</td>
			</tr>
			@endif
        <tr><td class="header" colspan="4">
            @if(Input::get('report_start_month') == Input::get('report_end_month'))
                    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @else
                    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @endif
        </td></tr>
	<tr><td class="header" colspan="9"></td></tr>		
   
        <tr class="header bold">
            
            <td width="30">ARC</td>
            <td width="30">Name of Assn/MSME</td>
            <td width="40">Operations/Activities/IGP</td>
            <td width="20">Number</td>
           
        </tr>
          @foreach($results['provinces'] as $province)
					@if(Session::get('access') != 3 )
					<tr></td><td></td><td></td><td></td><td></td>
					<tr class="tr-bold tr-uppercase">
						<td>{{ $province->province }}</td><td></td><td></td><td></td>
					</tr>
					@endif
					@foreach($results['msmes'][$province->id] as $msme)
						<?php $arc = 'arc';   ?> 
						@foreach($results['jobs'][$province->id][$msme->id.'_'.$msme->igp_id] as $job)
						<tr>
							<td>
							@if($job->arc_name != $arc)
								@if ($job->arc_name == '') Non-ARC
								@else {{ $job->arc_name  }} 
								@endif
								<?php $c = 1; ?>
							@endif
							</td>
							<td>{{ $job->msme_name }} </td>
						   
							<td>
								{{ $job->igp_name }}
							</td>
							<td align="center"> {{ number_format($job->number) }}</ </td>
						</tr>
						
						<?php  $arc = $job->arc_name; $c++;  ?>
						@endforeach
					@endforeach
                    <tr><td></td><td></td><td align="right"><b> Total</b></td><td align="center"><b>{{ number_format($results['total'][$province->id]) }}</b></td></tr>
				@endforeach
				@if(Session::get('access') != 3 )
				<tr><td></td><td></td><td align="right"><b> Grand Total</b></td><td align="center"><b>{{ number_format($results['grand_total']) }}</b></td></tr>
				@endif
    </table>
</html>