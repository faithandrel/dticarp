<meta charset="utf-8">
<html>
    <style>
        td.header, tr.header > td {
           text-align: center;
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
	<tr>
                <th colspan="9">
                        COMPREHENSIVE AGRARIAN REFORM PROGRAM
                </th>
        </tr>
        <tr>
                <th colspan="9">
                        Annex 4
                </th>
        </tr>
        <tr>
                <th colspan="9">
                        Sales Generated
                </th>
        </tr>
        <tr>
                <th colspan="9">
                    @if(Session::get('access') == 2)
                        {{ Session::get('region') }}
                    @else
                        {{ Region::where('id', '=', $region)->pluck('region') }}
                    @endif
                </th>
        </tr>
        <tr>
                <th colspan="9">
                        @if(Input::get('report_start_month') == Input::get('report_end_month'))
                                {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                        @else
                                {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                        @endif
                </th>
        </tr>
        <tr>
                <th colspan="9">
                </th>
        </tr>
	<tr><td class="header" colspan="9"></td></tr>		
   
        <tr class="header bold">      
            <td width="30">ARC</td>
            <td width="30">Name of Assn/MSME</td>
            <td width="40">Project/IGPs</td>
            <td width="20">Sales</td>
            <?php $interventions = current($sales); ?>
            @foreach( $interventions['interventions'] as $intervention)
                <td width="20">{{ $intervention->intervention }}</td>
            @endforeach
            <td width="20">Monitored</td>
            <td width="20">Established</td>
        </tr>
        
        @foreach ($sales as $province => $data)
            <tr>
                <td class="td-bold td-uppercase">{{ $province }}</td><td></td><td></td><td></td>
                <?php $interventions = current($sales); ?>
                @foreach( $interventions['interventions'] as $intervention)
                    <td></td>
                @endforeach
                <td></td><td></td>
            </tr>
            @foreach($sales[$province]['msmes'] as $msme)
                <tr>
                    <td>{{ $msme->name }}</td>
                    <td>{{ $msme->msme_name }}</td>
                    <td>
                        @foreach($msme->igp as $igproject)
                           <li>{{ $igproject->igp_name }}</li>
                        @endforeach
                        
                    </td>
                    <td align="right">
                        {{ '? ' . number_format($msme->sales, 2)  }}
                    </td>
                    @foreach($sales[$province]['interventions'] as $intervention)
                         <td>
                            {{ $sales[$province]['totals']['activities'][$msme->id][$intervention->id] }} 
                         </td>
                    @endforeach
                    <td align="right">{{ '? ' . number_format($msme->monitored, 2)  }}</td>
                    <td align="right">{{ '? ' . number_format($msme->established, 2)  }}</td>
                </tr>
            @endforeach
            @if(!empty($sales[$province]['totals']))
            <tr>
                 <td align="right"><b>Total</b></td><td></td><td></td><td align="right"><b>{{ '? ' . number_format($sales[$province]['totals']['igp_sales'], 2)  }}</b></td>
                 @foreach($sales[$province]['interventions'] as $intervention)
                        <td align="right"><b>{{ '? ' . number_format($sales[$province]['totals'][$intervention->id], 2)  }}</b></td>
                 @endforeach
                 <td align="right"><b>{{ '? ' . number_format($sales[$province]['totals']['monitored'], 2)  }}</b></td><td align="right"><b>{{  '? ' . number_format($sales[$province]['totals']['established'], 2)  }}<b></td>
            </tr>
            @endif
                                <tr><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
        @endforeach
         <tr>
            <td align="right"><b>GRAND TOTAL</b></td><td></td><td></td><td align="right"><b>{{ '? ' . number_format($totals['igp_sales'], 2)  }}</b></td>
             @foreach($sales[$province]['interventions'] as $intervention )
                    <td align="right"><b>{{ $totals[$intervention->id] }}</b></td>
             @endforeach
             <td align="right"><b>{{ '? ' . number_format($totals['monitored'], 2)  }}</b></td><td align="right"><b>{{  '? ' . number_format($totals['established'], 2)  }}<b></td>
        </tr>
    </table>
</html>