<html>
    <style>
        td.header {
           text-align: center;
           
        }
    </style>
    <table>
        <tr><td class="header" colspan="{{ 4 + count($provinces)*3 }}">Annex 2</td></tr>
        <tr><td class="header" colspan="{{ 4 + count($provinces)*3 }}"><strong>Gender-Disaggregated Data on DTI-CARP Services</strong></td></tr>
	<tr><td class="header" colspan="{{ 4 + count($provinces)*3 }}">
            @if(Session::get('access') == 2)
                {{ Session::get('region') }}
            @else
                {{ Region::where('id', '=', $region)->pluck('region') }}
            @endif
        </td></tr>
        <tr><td class="header" colspan="{{ 4 + count($provinces)*3 }}">
            @if(Input::get('report_start_month') == Input::get('report_end_month'))
                    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @else
                    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @endif
        </td></tr>
	<tr><td class="header" colspan="{{ 4 + count($provinces)*3 }}"></td></tr>	
        <tr>
                <td class="header" rowspan=2>Indicators</td>
                <td class="header" colspan=3>GRAND TOTAL</td>
                @foreach($provinces as $province)
                        <td class="header" colspan=3>{{ $province }}</td>
                @endforeach
        </tr>
        <tr>
                <td class="header">Total</td>
                <td class="header">Male</td>
                <td class="header">Female</td>
                @foreach($provinces as $province)
                        <td class="header">Total</td>
                        <td class="header">Male</td>
                        <td class="header">Female</td>
                @endforeach
        </tr>
        <tr>
                <td>No. of Entrepreneurs Developed</td>
                <td align="center">{{ $grand_total['entrepreneurs'] }}</td>
                <td></td>
                <td></td>
                @foreach($provinces as $province)
                        <td  align="center">{{ $gender_disag[$province]['entrepreneurs'] }}</td>
                        <td ></td>
                        <td ></td>
                @endforeach
        </tr>
        <tr>
                <td>  No. of FBs Served</td>
                <td align="center">{{ $grand_total['msmes_dev_fb_m'] + $grand_total['msmes_dev_fb_f'] }}</td>
                <td align="center">{{ $grand_total['msmes_dev_fb_m'] }}</td>
                <td align="center" >{{ $grand_total['msmes_dev_fb_f'] }}</td>
                @foreach($provinces as $province)
                        <td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] + $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
                @endforeach
        </tr>
        
        <tr>
                <td>No. of MSMEs Developed</td>
                <td  align="center">{{ $grand_total['entrepreneurs'] }}</td>
                <td></td>
                <td></td>
                @foreach($provinces as $province)
                        <td  align="center">{{ $gender_disag[$province]['entrepreneurs'] }}</td>
                        <td ></td>
                        <td ></td>
                @endforeach
        </tr>
        <tr>
                <td>  No. of FBs Served</td>
                <td  align="center">{{ $grand_total['msmes_dev_fb_m'] + $grand_total['msmes_dev_fb_f'] }}</td>
                <td  align="center">{{ $grand_total['msmes_dev_fb_m'] }}</td>
                <td  align="center">{{ $grand_total['msmes_dev_fb_f'] }}</td>
                @foreach($provinces as $province)
                        <td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] + $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_m'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_dev_fb_f'] }}</td>
                @endforeach
        </tr>
        <tr>
                <td>  No. of LOs/NCBs Served</td>
                <td></td>
                <td></td>
                <td></td>
                @foreach($provinces as $province)
                        <td></td>
                        <td></td>
                        <td></td>
                @endforeach
        </tr>
        <tr>
                <td>No. of MSMEs Assisted</td>
                <td  align="center">{{ $grand_total['msmes_asst'] }}</td>
                <td></td>
                <td></td>
                @foreach($provinces as $province)
                        <td align="center" >{{ $gender_disag[$province]['msmes_asst'] }}</td>
                        <td ></td>
                        <td ></td>
                @endforeach
        </tr>
        <tr>
                <td>  No. of FBs Served</td>
                <td align="center">{{ $grand_total['msmes_asst_fb_m'] + $grand_total['msmes_asst_fb_f'] }}</td>
                <td align="center">{{ $grand_total['msmes_asst_fb_m'] }}</td>
                <td align="center">{{ $grand_total['msmes_asst_fb_f'] }}</td>
                @foreach($provinces as $province)
                        <td  align="center">{{ $gender_disag[$province]['msmes_asst_fb_m'] + $gender_disag[$province]['msmes_asst_fb_f'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_asst_fb_m'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_asst_fb_f'] }}</td>
                @endforeach
        </tr>
        <tr>
                <td>  No. of LOs/NCBs Served</td>
                <td align="center">{{ $grand_total['msmes_asst_lo_m'] + $grand_total['msmes_asst_lo_f'] }}</td>
                <td align="center">{{ $grand_total['msmes_asst_lo_m'] }}</td>
                <td align="center">{{ $grand_total['msmes_asst_lo_f'] }}</td>
                @foreach($provinces as $province)
                        <td  align="center">{{ $gender_disag[$province]['msmes_asst_lo_m'] + $gender_disag[$province]['msmes_asst_lo_f'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_asst_lo_m'] }}</td>
                        <td  align="center">{{ $gender_disag[$province]['msmes_asst_lo_f'] }}</td>
                @endforeach
        </tr>
        @foreach( $gender_disag[$province]['interattns']  as $intervention => $attendance )
                @if( !(in_array($intervention,array('Product Developed', 'Prototype Executed', 'Packaging and Label Developed'))) )
                <tr>
                        @if($intervention == 'Consultancy')
                        <td>No. of Man-months Consultancy</td>
                        <td align="center">{{ $grand_total[$intervention]['manmonths'] }}</td>
                        <td></td>
                        <td></td>
                                @foreach($provinces as $province)
                                        <td  align="center">{{  $gender_disag[$province]['interattns'][$intervention]['manmonths'] }}</td>
                                        <td ></td>
                                        <td ></td>
                                @endforeach
                        @else
                        <td>No. of {{ $intervention }}</td>
                        <td  align="center">{{ $grand_total[$intervention]['count'] }}</td>
                        <td></td>
                        <td></td>
                                @foreach($provinces as $province)
                                        <td  align="center">{{ $gender_disag[$province]['interattns'][$intervention]['count'] }}</td>
                                        <td ></td>
                                        <td ></td>
                                @endforeach
                        @endif
                </tr>
                <tr>
                        <td>  No. of FBs Served</td>
                        <td align="center">{{ $grand_total[$intervention]['fb_male'] + $grand_total[$intervention]['fb_female'] }}</td>
                        <td align="center">{{ $grand_total[$intervention]['fb_male'] }}</td>
                        <td align="center">{{ $grand_total[$intervention]['fb_female'] }}</td>
                        @foreach($provinces as $province)
                                <td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['fb_male'] + $gender_disag[$province]['interattns'][$intervention]['fb_female'] }}</td>
                                <td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['fb_male']  }}</td>
                                <td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['fb_female'] }}</td>
                        @endforeach
                        
                </tr>
                <tr>
                        <td>  No. of LOs/NCBs Served</td>
                        <td align="center">{{ $grand_total[$intervention]['lo_male'] + $grand_total[$intervention]['lo_female'] }}</td>
                        <td align="center">{{ $grand_total[$intervention]['lo_male'] }}</td>
                        <td align="center">{{ $grand_total[$intervention]['lo_female'] }}</td>
                        @foreach($provinces as $province)
                                <td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['lo_male'] + $gender_disag[$province]['interattns'][$intervention]['lo_female'] }}</td>
                                <td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['lo_male']  }}</td>
                                <td align="center">{{ $gender_disag[$province]['interattns'][$intervention]['lo_female'] }}</td>
                        @endforeach
                </tr>
                @endif
        @endforeach
    </table>
</html>