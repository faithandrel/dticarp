<meta charset="utf-8">
<html>
    <style>
      th.header, td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
            <tr>
                    <th class="header" colspan="5">
                            COMPREHENSIVE AGRARIAN REFORM PROGRAM
                    </th>
            </tr>
            <tr>
                    <th class="header" colspan="5">
                            Annex 3
                    </th>
            </tr>
            <tr>
                    <th class="header" colspan="5">
                            Investments Generated
                    </th>
            </tr>
            <tr>
                    <th class="header" colspan="5">
                        @if(Session::get('access') == 2)
                            {{ Session::get('region') }}
                        @else
                            {{ Region::where('id', '=', $region)->pluck('region') }}
                        @endif
                    </th>
            </tr>
            <tr>
                    <th class="header" colspan="5">
                            @if(Input::get('report_start_month') == Input::get('report_end_month'))
                                    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                            @else
                                    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                            @endif
                    </th>
            </tr>
            <tr>
                    <th colspan="5">
                    </th>
            </tr>
            <tr>
                <td>
                    ARC
                </td>
                <td>
                    Name of Assn/MSME
                </td>
                <td>
                    Project
                </td>
                <td>
                    Source of Funds
                </td>
                <td>
                    Amount
                </td>
            </tr>
            <?php $arc = 'arc'; ?> 
            @foreach ($investments as $province => $data)
                <tr><td>{{ $province }}</td><td></td><td></td><td></td><td></td></tr>
               
                @foreach ($data['projects'] as $project)
                    <?php $count=0; ?>
                    @foreach($project->investments as $investment)
                        <tr>
                            <td>
                                @if(!$count)
                                    @if($project->arc_name != $arc)
                                            @if($project->arc_name != '') {{ $project->arc_name  }}
                                            @else 
                                            Non-ARC
                                            @endif
                                    @endif
                                @endif
                            </td>
                            <td>
                                @if(!$count)
                                    {{ $project->msme_name }}
                                @endif
                            </td>
                            <td>
                                @if(!$count)
                                    {{ $project->igp_name }}
                                @endif
                            </td>
                            <td>{{ $investment->investment_source }}</td>
                            <td align="right">{{ '? ' . number_format($investment->investment_amount, 2) }}</td>
                        </tr>
                        <?php $count++; ?>
                    @endforeach
                @endforeach
                <tr><td></td><td></td><td></td><td><strong>Total</strong></td><td align="right"><strong>{{ '? ' . number_format($data['total'], 2)  }}</strong></td></tr>
                @endforeach
            <tr><td></td><td></td><td></td><td><strong>GRAND TOTAL</strong></td><td align="right"><strong>{{ '? ' . number_format($grand_total, 2)  }}</strong></td></tr>
    </table>
</html>