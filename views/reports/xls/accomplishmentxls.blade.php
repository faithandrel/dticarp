<html>
    <style>
        td.header {
           text-align: center;
           
        }
    </style>
    <table>
        <tr><td class="header" colspan="5">Annex 8</td></tr>
        <tr><td class="header" colspan="5"><strong>Summary of Accomplishments</strong></td></tr>
	<tr><td class="header" colspan="5">{{ Session::get('province') }}</td></tr>
        <tr><td class="header" colspan="5">
            @if(Input::get('report_start_month') == Input::get('report_end_month'))
		    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
	    @else
		    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
	    @endif
        </td></tr>
	<tr><td class="header" colspan="5"></td></tr>		
        <tr>
                <td ><strong>Indicators</strong></td>
                <td ><strong>Annual Target</strong></td>
                <td ><strong>
		    @if(Input::get('report_start_month') == Input::get('report_end_month'))
			    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
		    @else
			    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
		    @endif
		</strong></td>
                <td ><strong>Accomp to Date</strong></td>
		<td ><strong>% of Accomp</strong></td>
        </tr>
        <tr>
		<td>Investments (P M))</td><td>{{ $target->investments }}</td>
		<td>{{ $current['investments'] }}</td><td>{{ $todate['investments'] }}</td><td>{{ $percentage['investments']/100 }}</td>
	</tr>
	<tr>
		<td>Sales (P M)</td><td>{{ $target->sales }}</td>
		<td>{{ $current['sales'] }}</td><td>{{ $todate['sales'] }}</td><td>{{ $percentage['sales']/100 }}</td>
	</tr>
	<tr>
		<td>Jobs Generated</td><td>{{ $target->jobs }}</td>
		<td>{{ $current['jobs'] }}</td><td>{{ $todate['jobs'] }}</td><td>{{ $percentage['jobs']/100 }}</td>
	</tr>
	<tr>
		<td>Entrepreneurs Developed</td><td>{{ $target->entrepreneurs }}</td>
		<td>{{ $current['entrepreneurs'] }}</td><td>{{ $todate['entrepreneurs'] }}</td><td>{{ $percentage['entrepreneurs']/100 }}</td>
	</tr>
	<tr>
		<td>Number of ARCs Assisted</td><td>{{ $target->arcs_assisted }}</td>
		<td>{{ $current['arcs_assisted'] }}</td><td>{{ $todate['arcs_assisted'] }}</td><td>{{ $percentage['arcs_assisted']/100 }}</td>
		
	</tr>
	<tr>
		<td>Number of Non-ARCs Assisted</td><td>{{ $target->non_arcs_assisted }}</td>
		<td>{{ $current['non_arcs_assisted'] }}</td><td>{{ $todate['non_arcs_assisted'] }}</td><td>{{ $percentage['non_arcs_assisted']/100 }}</td>
	</tr>
	<tr>
		<td>Number of MSMEs Developed</td><td>{{ $target->msmes_dev }}</td>
		<td>{{ $current['msmes_dev'] }}</td><td>{{ $todate['msmes_dev'] }}</td><td>{{ $percentage['msmes_dev']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp; Farmer-beneficiaries served</td><td>{{ $target->msmes_dev_fb }}</td>
		<td>{{ $current['msmes_dev_fb'] }}</td><td>{{ $todate['msmes_dev_fb'] }}</td><td>{{ $percentage['msmes_dev_fb']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp; Landowners served</td><td>{{ $target->msmes_dev_lo }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp;  Sales Generated</td><td>{{ $target->established }}</td>
		<td>{{ $current['established'] }}</td><td>{{ $todate['established'] }}</td><td>{{ $percentage['established']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp;  ARCs served</td><td>{{ $target->msmes_dev_arcs }}</td>
		<td>{{ $current['msmes_dev_arcs'] }}</td><td>{{ $todate['msmes_dev_arcs'] }}</td><td>{{ $percentage['msmes_dev_arcs']/100 }}</td>
	</tr>
	<tr>
		<td>Number of MSMEs Assisted</td><td>{{ $target->msmes_asst }}</td>
		<td>{{ $current['msmes_asst'] }}</td><td>{{ $todate['msmes_asst'] }}</td><td>{{ $percentage['msmes_asst']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp; Farmer-beneficiaries served</td><td>{{ $target->msmes_asst_fb }}</td>
		<td>{{ $current['msmes_asst_fb'] }}</td><td>{{ $todate['msmes_asst_fb'] }}</td><td>{{ $percentage['msmes_asst_fb']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp; Landowners served</td><td>{{ $target->msmes_asst_lo }}</td>
		<td>{{ $current['msmes_asst_lo'] }}</td><td>{{ $todate['msmes_asst_lo'] }}</td><td>{{ $percentage['msmes_asst_lo']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp;  Monitored Sales</td><td>{{ $target->monitored }}</td>
		<td>{{ $current['monitored'] }}</td><td>{{ $todate['monitored'] }}</td><td>{{ $percentage['monitored']/100 }}</td>
	</tr>
	<tr>
		<td> &nbsp; &nbsp;  ARCs served</td><td>{{ $target->msmes_asst_arcs }}</td>
		<td>{{ $current['msmes_asst_arcs'] }}</td><td>{{ $todate['msmes_asst_arcs'] }}</td><td>{{ $percentage['msmes_asst_arcs']/100 }}</td>
	</tr>

	@foreach ($inter_targets as $inter_target)
		@if($inter_target->intervention_id == 13)
		<tr>
			<td>Number of Man-months Consultancy</td><td>{{ $target->man_months }}</td>
			<td>{{ $current['interattns'][$inter_target->intervention_id]['manmonths'] }}</td><td>{{ $todate['interattns'][$inter_target->intervention_id]['manmonths'] }}</td>
			<td>{{ $percentage['interattns'][$inter_target->intervention_id]['manmonths']/100 }}</td>
		</tr>
		@else
		<tr>
			<td>Number of {{ $inter_target->intervention()->pluck('intervention') }}</td><td>{{ $inter_target->number }}</td>
			<td>{{ $current['interattns'][$inter_target->intervention_id]['count'] }}</td><td>{{ $todate['interattns'][$inter_target->intervention_id]['count'] }}</td>
			<td>{{ $percentage['interattns'][$inter_target->intervention_id]['count']/100 }}</td>
		</tr>
		@endif
		<tr>
			<td> &nbsp; &nbsp; Farmer-beneficiaries served</td><td>{{ $inter_target->fbs }}</td>
			<td>{{ $current['interattns'][$inter_target->intervention_id]['fbs'] }}</td><td>{{ $todate['interattns'][$inter_target->intervention_id]['fbs'] }}</td>
			<td>{{ $percentage['interattns'][$inter_target->intervention_id]['fbs']/100 }}</td>
		</tr>
		<tr>
			<td> &nbsp; &nbsp; Landowners served</td><td>{{ $inter_target->los }}</td>
			<td>{{ $current['interattns'][$inter_target->intervention_id]['los'] }}</td><td>{{ $todate['interattns'][$inter_target->intervention_id]['los'] }}</td>
			<td>{{ $percentage['interattns'][$inter_target->intervention_id]['los']/100 }}</td>
		@if($inter_target->intervention()->pluck('w_sales'))
		<tr>
			<td> &nbsp; &nbsp;  Sales Generated</td><td>{{ $inter_target->sales }}</td>
			<td>{{ $current['interattns'][$inter_target->intervention_id]['sales'] }}</td><td>{{ $todate['interattns'][$inter_target->intervention_id]['sales'] }}</td>
			<td>{{ $percentage['interattns'][$inter_target->intervention_id]['sales']/100 }}</td>
		</tr>
		@endif
		<tr>
			<td> &nbsp; &nbsp;  ARCs served</td><td>{{ $inter_target->arcs }}</td>
			<td>{{ $current['interattns'][$inter_target->intervention_id]['arcs'] }}</td><td>{{ $todate['interattns'][$inter_target->intervention_id]['arcs'] }}</td>
			<td>{{ $percentage['interattns'][$inter_target->intervention_id]['arcs']/100 }}</td>
		</tr>
	@endforeach
    </table>
</html>