<meta charset="utf-8">
<html>
    <style>
      td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
		<tr><td class="header" colspan="5">COMPREHENSIVE AGRARIAN REFORM PROGRAM</td></tr>
        <tr><td class="header" colspan="5">Annex 7</td></tr>
        <tr><td class="header" colspan="5"><strong>Entrepreneurs Developed</strong></td></tr>
        <tr><td class="header" colspan="5">{{ Session::get('province') }}</td></tr>
		<tr><td class="header" colspan="5">
		@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@endif	</td></tr>
		<tr><td colspan="5"></td></tr>
		
        <tr class="header">
			
            <td width="20"><strong>ARC</strong></td>
            <td width="30"><strong>Name of Assn/MSME</strong></td>
            <td width="20"><strong>Entrepreneurs</strong></td>
            <td width="30"><strong>Operations/Activities</strong></td>
            <td width="20"><strong>Number</strong></td>
        </tr>
           		@foreach($results['provinces'] as $province)
					@if(Session::get('access') != 3 )
					<tr></td><td></td><td></td><td></td><td></td><td></td>
					<tr class="tr-bold tr-uppercase">
						<td>{{ $province->province }}</td><td></td><td></td><td></td><td></td>
					</tr>
					@endif
					<?php $arc = 'arc';  ?> 
					@foreach($results['entrepreneurs'][$province->id] as $entrepreneur)
					<tr>
                        <td>
						@if($entrepreneur->arc_name != $arc)
							@if ($entrepreneur->arc_name == '') Non-ARC
							@else {{ $entrepreneur->arc_name  }} 
							@endif
							<?php $c = 1; ?>
						@endif
						</td>
                        <td>{{ $entrepreneur->msme_name }} </td>
                        <td>
                            {{ $entrepreneur->first_name." ".$entrepreneur->last_name }}
                        </td>
						<td>
                            {{ $entrepreneur->project }}
                        </td>
                        <td align="center">  1 </td>
                    </tr>
					
					<?php  $arc = $entrepreneur->arc_name; $c++; ?>
                    @endforeach
                    <tr><td></td><td></td><td></td><td align="right"><b> Total</b></td><td align="center"><b>{{ number_format($results['total'][$province->id]) }}</b></td></tr>
				@endforeach
				@if(Session::get('access') != 3 )
				<tr><td></td><td></td><td></td><td align="right"><b> Grand Total</b></td><td align="center"><b>{{ number_format($results['grand_total']) }}</b></td></tr>
				@endif
    </table>
</html>