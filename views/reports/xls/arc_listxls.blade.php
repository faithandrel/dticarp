<meta charset="utf-8">
<html>
    <style>
      th.header, td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
        <tr>
                <th class="header" colspan="2" >
                        COMPREHENSIVE AGRARIAN REFORM PROGRAM
                </th>
        </tr>
        <tr>
                <th class="header" colspan="2">
                        List of Assisted ARCs
                </th>
        </tr>
        <tr>
                <th class="header" colspan="2">
                        {{ Session::get('province') }}
                </th>
        </tr>
        <tr>
                <th class="header" colspan="2">
                @if(Input::get('report_start_month') == Input::get('report_end_month'))
                        {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                @else
                        {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                @endif		
                </th>
        </tr>
        <tr>
                <th class="header" colspan="2">
                
                </th>
        </tr>
        <tr>
                <td>Name of ARC</td>
                <td>MSMEs Assisted</td>
        </tr>
        @foreach ($results['arcs'] as $arc => $msmes)
        <tr>
                <td>{{ $arc }}</td>
                <td>
                        <ul>
                        @foreach($msmes as $msme)
                                <li>{{ $msme }}</li>
                        @endforeach
                        </ul>
                </td>
        </tr>
        @endforeach
        <tr>
                <td><b>Total:  </b>{{ $results['grand_total'] }}</td>
                <td></td>
        </tr>
    </table>
</html>