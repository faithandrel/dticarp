<meta charset="utf-8">
<html>
    <style>
      td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
		<tr><td class="header" colspan="5">COMPREHENSIVE AGRARIAN REFORM PROGRAM</td></tr>
        <tr><td class="header" colspan="5">Annex 3</td></tr>
        <tr><td class="header" colspan="5"><strong>Investments Generated</strong></td></tr>
		<tr><td class="header" colspan="5">{{ Session::get('province') }}</td></tr>
        <tr><td class="header" colspan="5">@if(Input::get('report_start_month') == Input::get('report_end_month'))
					{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@endif	</td></tr>
		<tr><td class="header" colspan="5"></td></tr>		
        <tr class="header bold">
           
            <td width="30">ARC</td>
            <td width="30">Name of Assn/MSME</td>
            <td width="30">Project</td>
            <td width="20">Source of Funds</td>
            <td width="20">Amount</td>
        </tr>
        @foreach ($projects as $project)
            <?php $count=0; ?>
            @foreach($project->investments as $investment)
                <tr>
                    <td>
                        @if(!$count)
                            {{ $project->arc_name }}
                        @endif
                    </td>
                    <td>
                        @if(!$count)
                            {{ $project->msme_name }}
                        @endif
                    </td>
                    <td>
                        @if(!$count)
                            {{ $project->igp_name }}
                        @endif
                    </td>
                    <td>{{ $investment->investment_source }}</td>
                    <td align="right">{{ '₱ ' . number_format($investment->investment_amount, 2) }}</td>
                </tr>
                <?php $count++; ?>
            @endforeach
        @endforeach
        <tr class="bold"><td></td><td></td><td></td><td>Total</td><td align="right">{{ '₱ ' . number_format($total, 2)  }}</td></tr>
    </table>
</html>