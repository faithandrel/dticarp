<html>
    <style>
        td.header {
           text-align: center;
           
        }
    </style>
    <table>
        <tr><td class="header" colspan="{{ 5 + count($provinces_current) }}">Annex 8</td></tr>
        <tr><td class="header" colspan="{{ 5 + count($provinces_current) }}"><strong>Summary of Accomplishments</strong></td></tr>
	<tr><td class="header" colspan="{{ 5 + count($provinces_current) }}">
            @if(Session::get('access') == 2)
                {{ Session::get('region') }}
            @else
                {{ Region::where('id', '=', $region)->pluck('region') }}
            @endif
        </td></tr>
        <tr><td class="header" colspan="{{ 5 + count($provinces_current) }}">
            @if(Input::get('report_start_month') == Input::get('report_end_month'))
		    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
	    @else
		    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
	    @endif
        </td></tr>
        <tr>
            <tr>
                <td rowspan="2">Indicators</td>
                <td rowspan="2">Annual Target</td>
                <td rowspan="2">This Quarter</th>
                <td colspan="{{ count($provinces_current) }}">
                        @if(Input::get('report_start_month') == Input::get('report_end_month'))
                                    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
                        @else
                                    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('year') }}
                        @endif
                </td>
                <td rowspan="2">Accomplishment to Date</td>
                <td rowspan="2">% of Accomplishment</td>
            </tr>
            <tr>
                @foreach($provinces_current as $key => $value)
                <td>{{ $key }}</td>
                @endforeach
            </tr>
        </tr>
        
        <tr>
            <td>Investments (P M)</td>
                <td align="right">{{ '? ' . number_format($annual['investments'], 2)  }}</td>
            <td align="right">{{ '? ' . number_format($current['investments'], 2)  }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ '? ' . number_format($value['investments'], 2)   }}</td>
            @endforeach
            <td align="right">{{ '? ' . number_format($todate['investments'], 2)  }}</td>
            <td align="right">{{ $percentage['investments']/100 }}</td>
        </tr>
        <tr>
            <td>Sales (P M)</td>
            <td align="right">{{ '? ' . number_format($annual['sales'], 2)  }}</td>
            <td align="right">{{ '? ' . number_format($current['sales'], 2)  }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ '? ' . number_format($value['sales'], 2) }}</td>
            @endforeach
            <td align="right">{{ '? ' . number_format($todate['sales'], 2)  }}</td>
            <td align="right">{{ $percentage['sales']/100 }}</td>
        </tr>
        <tr>
            <td>Jobs Generated</td>
            <td align="right">{{ number_format($annual['jobs'] )  }}</td>
            <td align="right">{{ number_format($current['jobs'])  }}</td>
            @foreach($provinces_current as $key => $value)
                <td align="right">{{ number_format($value['jobs'] )  }}</td>
            @endforeach
            <td align="right">{{ number_format($todate['jobs'])   }}</td>
            <td align="right">{{ $percentage['jobs']/100 }}</td>
        </tr>
        <tr>
            <td>Entrepreneurs Developed</td>
            <td align="right">{{ number_format($annual['entrepreneurs'])   }}</td>
            <td align="right">{{ number_format($current['entrepreneurs'])   }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ number_format($value['entrepreneurs'])   }}</td>
            @endforeach
            <td align="right">{{ number_format($todate['entrepreneurs'])   }}</td>
            <td align="right">{{ $percentage['entrepreneurs']/100 }}</td>
        </tr>
        <tr>
            <td>Number of ARCs Assisted</td>
            <td align="right">{{ number_format($annual['arcs_assisted'])  }}</td>
            <td align="right">{{ number_format($current['arcs_assisted'])  }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ number_format($value['arcs_assisted'])  }}</td>
            @endforeach
            <td align="right">{{ number_format($todate['arcs_assisted'])  }}</td>
            <td align="right">{{ $percentage['arcs_assisted']/100 }}</td>
        </tr>
        <tr>
            <td>Number of Non-ARCs Assisted</td>
             <td align="right">{{ number_format($annual['non_arcs_assisted']) }}</td>
             <td align="right">{{ number_format($current['non_arcs_assisted'])  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ number_format($value['non_arcs_assisted'])  }}</td>
            @endforeach
            <td align="right">{{ number_format($todate['non_arcs_assisted'])  }}</td>
            <td align="right" >{{ $percentage['non_arcs_assisted']/100 }}</td>
                        
        </tr>
        <tr>
            <td>Number of MSMEs Developed</td>
             <td align="right">{{ number_format($annual['msmes_dev'])  }}</td>
             <td align="right">{{ number_format($current['msmes_dev'])  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ number_format( $value['msmes_dev']) }}</td>
            @endforeach
             <td align="right">{{ number_format($todate['msmes_dev'])  }}</td>
            <td align="right">{{ $percentage['msmes_dev']/100 }}</td>
        </tr>
        <tr>
            <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
            <td align="right">{{ number_format($annual['msmes_dev_fb'])  }}</td>
            <td align="right">{{ number_format($current['msmes_dev_fb'])  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ number_format($value['msmes_dev_fb'])  }}</td>
            @endforeach
             <td align="right">{{ number_format($todate['msmes_dev_fb'])  }}</td>
            <td align="right">{{ $percentage['msmes_dev_fb']/100 }}</td>
        </tr>
        <tr>
                <td> &nbsp; &nbsp; Landowners served</td>
                <td align="right">{{ number_format($target->msmes_dev_lo)  }}</td>
                <td></td>
                @foreach($provinces_current as $key => $value)
                <td></td>
                @endforeach
                <td></td>
                <td></td>
        </tr>
        <tr>
            <td> &nbsp; &nbsp;  Sales Generated</td>
             <td align="right">{{ '? ' . number_format($annual['established'], 2)  }}</td>
             <td align="right">{{ '? ' . number_format($current['established'], 2)  }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ '? ' . number_format($value['established'], 2)  }}</td>
            @endforeach
             <td align="right">{{ '? ' . number_format($todate['established'], 2)  }}</td>
            <td align="right">{{ $percentage['established']/100 }}</td>
                        
        </tr>
        <tr>
            <td> &nbsp; &nbsp;  ARCs served</td>
             <td align="right">{{ number_format( $annual['msmes_dev_arcs']) }}</td>
             <td align="right">{{ number_format($current['msmes_dev_arcs'])  }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ number_format($value['msmes_dev_arcs'])  }}</td>
            @endforeach
             <td align="right">{{ number_format($todate['msmes_dev_arcs'])  }}</td>
            <td align="right">{{ $percentage['msmes_dev_arcs']/100 }}</td>
        </tr>
        <tr>
            <td>Number of MSMEs Assisted</td>
             <td align="right">{{ number_format($annual['msmes_asst'])  }}</td>
             <td align="right">{{ number_format($current['msmes_asst'])  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ number_format($value['msmes_asst'])  }}</td>
            @endforeach
             <td align="right">{{ number_format($todate['msmes_asst'])  }}</td>
            <td align="right">{{ $percentage['msmes_asst']/100 }}</td>
        </tr>
        <tr>
            <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
             <td align="right">{{ number_format($annual['msmes_asst_fb'] ) }}</td>
            <td align="right">{{ number_format($current['msmes_asst_fb'])  }}</td>
            @foreach($provinces_current as $key => $value)
            <td align="right">{{ number_format($value['msmes_asst_fb'])  }}</td>
            @endforeach
            <td align="right">{{ number_format($todate['msmes_asst_fb'])  }}</td>
            <td align="right">{{ $percentage['msmes_asst_fb']/100 }}</td>
        </tr>
        <tr>
            <td> &nbsp; &nbsp; Landowners served</td>
             <td align="right">{{ number_format($annual['msmes_asst_lo'])  }}</td>
             <td align="right">{{ number_format($current['msmes_asst_lo'])  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ number_format($value['msmes_asst_lo'])  }}</td>
            @endforeach
            <td align="right">{{ number_format($todate['msmes_asst_lo'])  }}</td>
            <td align="right">{{ $percentage['msmes_asst_lo']/100 }}</td>
        </tr>
        <tr>
            <td> &nbsp; &nbsp;  Monitored Sales</td>
            <td align="right">{{ '? ' . number_format($annual['monitored'], 2)  }}</td>
             <td align="right">{{ '? ' . number_format($current['monitored'], 2)  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ '? ' . number_format($value['monitored'], 2)  }}</td>
            @endforeach
            <td align="right">{{ '? ' . number_format($todate['monitored'], 2)  }}</td>
            <td align="right">{{ $percentage['monitored']/100 }}</td>
                        
        </tr>
        <tr>
            <td> &nbsp; &nbsp;  ARCs served</td>
             <td align="right">{{ number_format($annual['msmes_asst_arcs'])  }}</td>
             <td align="right">{{ number_format($current['msmes_asst_arcs'])  }}</td>
            @foreach($provinces_current as $key => $value)
             <td align="right">{{ number_format($value['msmes_asst_arcs'])  }}</td>
            @endforeach
             <td align="right">{{ number_format($todate['msmes_asst_arcs'])  }}</td>
            <td align="right">{{ $percentage['msmes_asst_arcs']/100 }}</td>
        </tr>

        @foreach ($inter_targets as $inter_target)
            @if($inter_target->intervention_id == 13)
            <tr>
                        <td>Number of Man-months Consultancy</td>
                         <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['manmonths']) }}</td>
                         <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['manmonths'])  }}</td>
                        @foreach($provinces_current as $key => $value)
                                     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['manmonths'])  }}</td>
                        @endforeach
                         <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['manmonths'])  }}</td>
                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['manmonths']/100 }}</td>
            </tr>
            @else
            <tr>
                        <td>Number of {{ $inter_target->intervention()->pluck('intervention') }}</td>
                         <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['count'] )  }}</td>
                         <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['count']) }}</td>
                        @foreach($provinces_current as $key => $value)
                                     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['count'])  }}</td>
                        @endforeach
                         <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['count'])  }}</td>
                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['count']/100 }}</td>
            </tr>
            @endif
            <tr>
                        <td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
                         <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
                         <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
                        @foreach($provinces_current as $key => $value)
                                     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
                        @endforeach
                         <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
                         <td align="right">{{  $percentage['interattns'][$inter_target->intervention_id]['fbs']/100 }}</td>
            </tr>
            <tr>
                        <td> &nbsp; &nbsp; Landowners served</td>
                         <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['los'])  }}</td>
                         <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['los']) }}</td>
                        @foreach($provinces_current as $key => $value)
                                     <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['los'] ) }}</td>
                        @endforeach
                         <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['los'])  }}</td>
                         <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['los']/100  }}</td>
            @if($inter_target->intervention()->pluck('w_sales'))
            <tr>
                        <td> &nbsp; &nbsp;  Sales Generated</td>
                         <td align="right">{{ '? ' . number_format($annual['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
                         <td align="right">{{ '? ' . number_format($current['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
                        @foreach($provinces_current as $key => $value)
                                    <td align="right">{{ '? ' . number_format($value['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
                        @endforeach
                         <td align="right">{{ '? ' . number_format($todate['interattns'][$inter_target->intervention_id]['sales'], 2)  }}</td>
                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['sales']/100 }}</td>
            </tr>
            @endif
            <tr>
                         <td> &nbsp; &nbsp;  ARCs served</td>
                         <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
                         <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
                        @foreach($provinces_current as $key => $value)
                                    <td align="right">{{ number_format($value['interattns'][$inter_target->intervention_id]['arcs']) }}</td>
                        @endforeach
                         <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['arcs'])  }}</td>
                        <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['arcs']/100 }}</td>
            </tr>
        @endforeach
    </table>
</html>