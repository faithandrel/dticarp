<meta charset="utf-8">
<html>
    <style>
         td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
		<tr><td class="header"  colspan="11" >COMPREHENSIVE AGRARIAN REFORM PROGRAM</td>
		<tr><td  class="header"colspan="11"><strong>Trade Fairs</strong></td></tr>
		<tr>
			<td class="header"class="border-less" colspan="11">
				{{ Session::get('province') }} <br>
				{{ Session::get('region') }}
			</td>
		</tr>
		<tr>
			<td class="header" colspan="11">
				@if(Input::get('report_start_month') == Input::get('report_end_month'))
				{{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
				@else
					{{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
					@endif	
			</td>
		</tr>
		<tr>
			<td colspan="11">
				
			</td>
		</tr>	

			<tr class="header bold">
				<td width="30" rowspan="2">MSME</td>
				<td width="20" rowspan="2">ARC</td>
				<td width="30" rowspan="2">Date</td>
				<td width="20" rowspan="2">Trade Fair</td>
				<td width="30" rowspan="2">Venue</td>
				<td width="20" rowspan="2">Cash Sales</td>
				<td width="10"colspan="2" >FBs</td>
				<td width="10"colspan="2">LOs</td>
				<td width="20"  rowspan="2">Remarks</td>
			</tr>	
				
			<tr class="header bold">
				<td >M</td>
				<td >F</td>
				<td >M</td>
				<td >F</td>
			</tr>		
			
			<?php $grand_c=0; ?> 
			@foreach($results['provinces'] as $province)
				@if(Session::get('access') != 3)
				<tr></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr class="bold">
					<td>{{ $province->province }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
				</tr>
				@endif
				<?php $msme = ''; $arc = 'arc'; $c=0; ?> 
				@foreach( $results['tradefairs'][$province->id] as $tradefair )
				<tr>
					<td>
						@if($tradefair->msme_name != $msme)
							{{ $tradefair->msme_name  }} <?php $c++; $grand_c++; ?>
						@endif
					</td>
					<td>
						@if($tradefair->arc_name != $arc)
							@if($tradefair->arc_name != '') {{ $tradefair->arc_name  }}
							@else 
							Non-ARC
							@endif
						@endif
					</td>
					<td align="center">
					@if ($tradefair->date_constructed == $tradefair->date_finished)
						{{ date_format(date_create($tradefair->date_constructed), 'M d, Y') }}
					@else
						{{ date_format(date_create($tradefair->date_constructed), 'M d, Y') }}
							-
						{{ date_format(date_create($tradefair->date_finished), 'M d, Y') }}
					@endif
					</td>
					<td>{{ $tradefair->name }}</td>
					<td>{{ $tradefair->venue }}</td>
					<td align="right">{{ '₱ ' . number_format($tradefair->sales, 2)  }}</td>
					<td align="center">{{ number_format($tradefair->fb_male) }}</td>
					<td align="center">{{ number_format($tradefair->fb_female) }}</td>
					<td align="center">{{ number_format($tradefair->lo_male) }}</td>
					<td align="center">{{ number_format($tradefair->lo_female) }}</td>
					<td>{{ $tradefair->remarks }}</td>
					
				</tr>
				<?php  $msme = $tradefair->msme_name;  $arc = $tradefair->arc_name; ?>
				@endforeach
				<tr class="bold">
					<td>Total</td>
					<td align="center">{{  number_format($c) }}</td><td></td>
					<td align="center">{{  number_format($results['count'][$province->id]) }}</td>
					<td></td>
					<td align="right">{{ '₱ ' . number_format($results['total_sales'][$province->id], 2)  }}</td>
					<td align="center">{{  number_format($results['total_fb_male'][$province->id]) }}</td>
					<td align="center">{{  number_format($results['total_fb_female'][$province->id]) }}</td>
					<td align="center">{{  number_format($results['total_lo_male'][$province->id]) }}</td>
					<td align="center">{{  number_format($results['total_lo_female'][$province->id]) }}</td>
					<td></td>
				</tr>
			@endforeach
			@if(Session::get('access') != 3)
				<tr></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td></tr>
				<tr class="bold">
					<td>Grand Total</td>
					<td align="center">{{  number_format($grand_c) }}</td><td></td>
					<td align="center">{{  number_format($results['grand_count']) }}</td> 
					<td></td>

					<td align="right">{{ '₱ ' . number_format($results['grand_total_sales'], 2)  }}</</td>
					<td align="center">{{  number_format($results['grand_total_fb_male']) }}</td>
					<td align="center">{{  number_format($results['grand_total_fb_female']) }}</td>
					<td align="center">{{  number_format($results['grand_total_lo_male']) }}</td>
					<td align="center">{{  number_format($results['grand_total_lo_female']) }}</td>
					<td></td>
				</tr>
			@endif
	
			
    </table>
</html>