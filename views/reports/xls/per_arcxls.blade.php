<meta charset="utf-8">
<html>
    <style>
      th.header, td.header, tr.header > td {
           text-align: center;
           
        }
		tr.bold > td {
			font-weight: bold;
		}
    </style>
    <table>
	<tr>
                <th class="header" colspan="9" >
                        COMPREHENSIVE AGRARIAN REFORM PROGRAM
                </th>
        </tr>
        <tr>
                <th class="header" colspan="9">
                        Annex 9
                </th>
        </tr>
        <tr>
                <th class="header" colspan="9">
                        ACCOMPLISHMENT PER ARC
                </th>
        </tr>
        <tr>
                <th class="header" colspan="9">
                        @if(Session::get('access') == 3)
                                {{ Session::get('province') }}
                        @else
                                {{ Province::where('id', '=', $province)->pluck('province') }}
                        @endif
                </th>
        </tr>
        <tr>
                <th class="header" colspan="9">
                @if(Input::get('report_start_month') == Input::get('report_end_month'))
                        {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                @else
                        {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
                @endif				
                </th>
        </tr>
        <tr>
                <th  colspan="9">
                
                </th>
        </tr>		
        <tr>
                <td ></td>
                <td ></td>
                <td colspan="4" >ARBs</td>
                <td colspan="2"  >COST</td>
                <td rowspan="3" width="29%">Remarks</td>
        </tr>		
        <tr>
                <td  align="top" rowspan="2" width="40%">ARC NAME/TITLE OF ACTIVITY</td>
                <td  rowspan="2" width="20%">DATE OF CONDUCT</td>
                <td colspan="2" >FBs</td>
                <td colspan="2" >LOs</td>
                <td rowspan="2" width="15%">FUND 158</td>
                <td rowspan="2" width="15%">Others</td>
        </tr>
        <tr>
                <td>Male</td>
                <td>Female</td>
                <td>Male</td>
                <td>Female</td>
        </tr>
        @foreach($msmes as $msme)
        <tr >
                <td ><b>Name of Arc: </b>{{ $msme->arc_name }}</td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td ><b>Barangay/Municipality:  </b></td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td><b>Legislative District:  </b></td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr> 
        <tr >
                <td><b>MSME:  </b>{{ $msme->msme_name }}</td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td><b>No. of years assisted:  </b></td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td><b>Industry Focus:  </b></td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        
        <tr >
                <td><b>Income Generating Projects (IGPs):  </b></td>
                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
                
        <?php $a = 'a'; ?>
        @foreach($msme->projects as $project)
                <tr >
                        <td>&nbsp; {{ $a }}. {{ $project->igp_name }}</td>
                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                </tr>
                <?php $a++; ?>
        @endforeach
        
        @foreach($msme->attns as $category => $interventions)	
                <tr >
                        <td><b>{{ $category }} </b></td>
                        <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                </tr>
                <?php $c2 = 1; ?>
                @foreach( $interventions as $activity => $attendances)
                        <tr >
                                <td>&nbsp; <b> {{ $c2 }}. {{$activity}}  </b> </td>
                                <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
                        </tr>
                        <?php $a1 = 'a'; ?>
                        @foreach($attendances as $attendance)
                                <tr >
                                        <td style="padding-left: 30px;">{{ $a1.'. '.$attendance->activity }} </td>
                                        <td align="center">
                                                {{ date_format(date_create($attendance->activity_date), 'M d, Y') }} 
                                        </td>
                                        <td align="center">{{ number_format($attendance->fb_male) }}</td>
                                        <td align="center">{{ number_format($attendance->fb_female) }}</td>
                                        <td align="center">{{ number_format($attendance->lo_male) }}</td>
                                        <td align="center">{{ number_format($attendance->lo_female) }}</td>
                                        <td align="right">{{ '? ' . number_format($attendance->cost_carp, 2) }}</td>
                                        <td align="right">{{ '? ' . number_format($attendance->cost_others, 2) }}</td>
                                        <td>{{ $attendance->remarks }}</td>
                                </tr>
                        <?php $a1++; ?>
                        @endforeach
                <?php $c2++; ?>
                @endforeach
        @endforeach
        <tr >
                <td><b>Investments: </b></td>
                <td align="center">{{'? ' . number_format($msme->investments, 2) }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td><b>Sales Generated: </b></td>
                <td align="center">{{ '? ' . number_format($msme->sales, 2)  }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td><b>Jobs Generated: </b></td>
                <td align="center"></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        <tr >
                <td><b>Entrepreneurs Developed: </b></td>
                <td align="center">{{ number_format($msme->entreps) }}</td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
        </tr>
        @endforeach
    </table>
</html>