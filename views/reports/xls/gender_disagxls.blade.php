<html>
    <style>
        td.header {
           text-align: center;
           
        }
    </style>
    <table>
        <tr><td class="header" colspan="4">Annex 2</td></tr>
        <tr><td class="header" colspan="4"><strong>Gender-Disaggregated Data on DTI-CARP Services</strong></td></tr>
	<tr><td class="header" colspan="4">{{ Session::get('province') }}</td></tr>
        <tr><td class="header" colspan="4">
            @if(Input::get('report_start_month') == Input::get('report_end_month'))
                    {{ DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @else
                    {{ DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year') }}
            @endif
        </td></tr>
	<tr><td class="header" colspan="4"></td></tr>		
        <tr>
                <td ><strong>Indicators</strong></td>
                <td ><strong>Total</strong></td>
                <td ><strong>Male</strong></td>
                <td ><strong>Female</strong></td>
        </tr>
        <tr>
		<td>No. of Entrepreneurs Developed</td>
		<td>{{ $entrepreneurs }}</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>  No. of FBs Served</td>
		<td>{{ $msmes_dev_fb_m + $msmes_dev_fb_f }}</td>
		<td>{{ $msmes_dev_fb_m }}</td>
		<td>{{ $msmes_dev_fb_f }}</td>
	</tr>
	<tr>
		<td>No. of MSMEs Developed</td>
		<td>{{ $msmes_dev }}</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>  No. of FBs Served</td>
		<td>{{ $msmes_dev_fb_m + $msmes_dev_fb_f }}</td>
		<td>{{ $msmes_dev_fb_m }}</td>
		<td>{{ $msmes_dev_fb_f }}</td>
	</tr>
	<tr>
		<td>  No. of LOs/NCBs Served</td>
		<td></td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>No. of MSMEs Assisted</td>
		<td>{{ $msmes_asst }}</td>
		<td></td>
		<td></td>
	</tr>
	<tr>
		<td>  No. of FBs Served</td>
		<td>{{ $msmes_asst_fb_m + $msmes_asst_fb_f }}</td>
		<td>{{ $msmes_asst_fb_m }}</td>
		<td>{{ $msmes_asst_fb_f }}</td>
	</tr>
	<tr>
		<td>  No. of LOs/NCBs Served</td>
		<td>{{ $msmes_asst_lo_m + $msmes_asst_lo_f }}</td>
		<td>{{ $msmes_asst_lo_m }}</td>
		<td>{{ $msmes_asst_lo_f }}</td>
	</tr>
        @foreach( $interattns as $intervention => $attendance )
	    @if( !(in_array($intervention,array('Product Developed', 'Prototype Executed', 'Packaging and Label Developed'))) )
	    <tr>
		    @if($intervention == 'Consultancy')
		    <td>No. of Man-months Consultancy</td>
		    <td>{{ $attendance['manmonths'] }}</td>
		    @else
		    <td>No. of {{ $intervention }}</td>
		    <td>{{ $attendance['count'] }}</td>
		    @endif
		    <td></td>
		    <td></td>
	    </tr>
	    <tr>
		    <td>  No. of FBs Served</td>
		    <td>{{ $attendance['fb_male'] + $attendance['fb_female'] }}</td>
		    <td>{{ $attendance['fb_male']  }}</td>
		    <td>{{ $attendance['fb_female'] }}</td>
	    </tr>
	    <tr>
		    <td>  No. of LOs/NCBs Served</td>
		    <td>{{ $attendance['lo_male'] + $attendance['lo_female'] }}</td>
		    <td>{{ $attendance['lo_male']  }}</td>
		    <td>{{ $attendance['lo_female'] }}</td>
	    </tr>
	    @endif
	@endforeach
    </table>
</html>