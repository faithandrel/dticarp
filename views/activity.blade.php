
@section('inner_content')

     <div class="row">
		<div class="col-sm-12">
			<h3 class="page-header"><i class="fa fa-angle-double-right"></i><a href="{{ url('regional/home')}}">
			@if(Session::get('access') == 3)
			{{ Session::get('province') }}
			@elseif(Session::get('access') == 2)
			{{ Session::get('region') }}
			@endif
			</a>&nbsp;
			<i class="fa fa-angle-right"></i><span> Activities</span>
		</div>
	 </div>
			
		<div class="row activities-panel-wrapper" role="tabpanel">
			<div class="form-select form-group-sm inter_dropdown col-lg-12">
				{{ Form::label('inter_dropdown', 'Interventions', array('class'=>'control-label')) }}
                {{ Form::select('inter_dropdown', $inter_dropdown , NULL,  array('class'=>'form-control', 'id'=>'inter_dropdown' )  ) }}
			</div>
			<div class="activities-panel" role="tabpanel">
			<!-- Tab panes -->
			<div class="tab-content col-lg-9" >
				<?php  $isFirst = true; ?>
				@foreach($inter_dropdown as $key => $value) 
				
					<div role="tabpanel" class="tab-pane @if($isFirst) active @else fade @endif" id="{{$key}}">
						 <div class="row">
							<div class="col-lg-12"> 
								@if ($act_count[$key] !=0)
								<div class="dataTable_wrapper">
									<table class="table  table-bordered table-hover" id="dataTables-{{$key}}">
										<thead>
											<tr>
												<th class="actions-1"></th>
												<th>{{ $value }}</th>
												<th>Date Conducted</th>
												<th>Venue</th>
												<th class="actions-1"></th>
											</tr>
										</thead>
									<tbody id="char"> 
									
									@foreach($activities[$key] as $activity)
									<tr class="odd gradeX">
										<td>
											<span data-toggle="tooltip" title={{ "'View attendances for ".$activity->name."'" }}>
												<button type="button" data-toggle="modal" data-target="#get-attn-modal" act-name="{{ $activity->name }}"  class="btn btn-primary get-attn-trigger" act-id={{ $activity->id }} act-wsales={{ $activity->w_sales }}><i class="fa fa-file-text"></i></button>
											</span>
										</td>
										<td >{{ $activity->name }}
											<span data-toggle="modal" data-target="#edit-act-modal"   >
													<button type="button" data-toggle="tooltip" title={{ "'Edit ".$activity->name."'"}} class="btn btn-default edit-act-trigger" act-name="{{ $activity->name }}" act-id={{ $activity->id }} act-wsales={{ $activity->w_sales }}><i class="fa fa-pencil-square-o"></i></button>
										   </span>
										</td>
										<td align="center" >
										@if ($activity->date_constructed == $activity->date_finished)
											{{ date_format(date_create($activity->date_constructed), 'M d, Y') }}
										@else
											{{ date_format(date_create($activity->date_constructed), 'M d, Y') }}
											-
											{{ date_format(date_create($activity->date_finished), 'M d, Y') }}
										@endif
										</td>
										
										<td >{{ $activity->venue }}</td>
										{{ Form::open(array('url' => 'provincial/activities/'.$activity->id, 'method' => 'delete')) }}
											{{ Form::hidden('act_inter', $activity->intervention_id ) }}
										<td class="actions-2">
											<span data-toggle="tooltip" title={{ "'Delete ".$activity->name."'" }} >	
											<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $activity->name }}?' )" ><i class="fa fa-trash"></i></button>
											</span>
											<!--      
											<span data-toggle="tooltip" title={{ "'Add Attendance for ".$activity->name."'" }}>
													<button type="button" data-toggle="modal" data-target="#add-attn-modal"  class="btn btn-default add-attn-trigger" act-name="{{ $activity->name }}" act-id={{ $activity->id }} act-wsales={{ $activity->w_sales }}><i class="fa fa-plus"></i> Attendance</button>
											</span>	 -->
										</td>
										{{ Form::close() }}
									</tr>
									@endforeach 
                                    </tbody>
                                </table>
                            </div>
							@else
								<div class="no-data">
									<i class="fa fa-ban"></i>
									<br><span>No data available</span>
									<br>
								</div>
							@endif
						</div>
					</div>
				</div>
				<?php  $isFirst = false; ?>
				@endforeach
			</div>
			
			<ul class="list-group col-xs-3 inter" id="inter" style="font-size: 9pt" role="tablist">
				<?php  $isFirst = true; ?>
			<!--	<a   class="list-group-item head-inter" align="center" data-toggle="collapse" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne"><b>INTERVENTIONS</b></a>
				<div id="collapseOne" class="panel-collapse" role="tabpanel" aria-labelledby="headingOne"> -->
				@foreach($inter_dropdown as $key => $value) 
					<a href="#{{$key}}" role="presentation" class="list-group-item" aria-controls="{{$key}}" role="tab" data-toggle="tab"><span style="float: left; margin-right: 10px;" class="badge">{{ $act_count[$key] }}</span>{{ $value }}</a>
					<?php  $isFirst = false; ?>
				@endforeach
				<!-- <div> -->
			</ul>
			</div>
		</div>
		<!----------------------------------- edit activity ---------------------------------->
		<!-- add activity -->
		<div id="edit-act-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-sm">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit Activity</span></h4>
			    </div>
				{{ Form::open(array('url' => 'provincial/activities', 'method' => 'put', 'id'=>'edit-act-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('edit_act_inter', 'Intervention', array('class'=>'control-label')) }}
								{{ Form::select('edit_act_inter', $inter_dropdown, NULL,  array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('edit_act_name', 'Name', array('class'=>'control-label')) }}
								{{ Form::text('edit_act_name', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								{{ Form::label('edit_date_constructed', 'Date Conducted', array('class'=>'control-label')) }}
								{{ Form::text('edit_date_constructed', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">	
								{{ Form::label('edit_date_finished', 'Date Finished', array('class'=>'control-label')) }}
								{{ Form::text('edit_date_finished', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div id="edit-man-months" class="row" style="display:none;">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('edit_manmonths', 'Man-Months', array('class'=>'control-label')) }}
								{{ Form::text('edit_manmonths', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-group">
								{{ Form::label('edit_venue', 'Venue', array('class'=>'control-label')) }}
								{{ Form::text('edit_venue', NULL, array('class'=>'form-control')) }}
							</div>
						</div>
					</div>
			    </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
                
            <!-- add attendance 
            <div id="add-attn-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                            <div class="modal-header">
                            
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">&raquo; <i class="fa fa-file"></i><span id="add-attn-title">  Add Attendance to </span></h4>
                            </div>
                            <div class="modal-body">
                                    {{ Form::open(array('url' => 'provincial/attendances', 'class'=>'form-horizontal', 'role'=>'form')) }}
                                    <div class="row">
                                            <div class="col-lg-4">
                                                    {{ Form::hidden('act_id', NULL, array('class'=>'form-control')) }}
                                                    <div class="form-group">	
                                                            {{ Form::label('fb_male', 'Farmer Beneficiaries (FB) Male', array('class'=>'control-label')) }}
                                                            {{ Form::text('fb_male', NULL, array('class'=>'form-control')) }}
                                                    </div>
                                            </div>
                                            <div class="col-lg-4">
                                                    <div class="form-group">		
                                                            {{ Form::label('lo_male', 'Lot Owners (LO) Male', array('class'=>'control-label')) }}
                                                            {{ Form::text('lo_male', NULL, array('class'=>'form-control')) }}
                                                    </div>
                                            </div>
                                            <div class="col-lg-4">
                                                    <div class="form-group">		
                                                            {{ Form::label('ncb_male', 'Non-CARP Business (NCB) Male', array('class'=>'control-label')) }}
                                                            {{ Form::text('ncb_male', NULL, array('class'=>'form-control')) }}
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="row">
                                            <div class="col-lg-4">
                                                    <div class="form-group">	
                                                            {{ Form::label('fb_female', 'Farmer Beneficiaries (FB) Female', array('class'=>'control-label')) }}
                                                            {{ Form::text('fb_female', NULL, array('class'=>'form-control')) }}
                                                    </div>
                                            </div>
                                            <div class="col-lg-4">
                                                    <div class="form-group">		
                                                            {{ Form::label('lo_female', 'Lot Owners (LO) Female', array('class'=>'control-label')) }}
                                                            {{ Form::text('lo_female', NULL, array('class'=>'form-control')) }}
                                                    </div>
                                            </div>
                                            <div class="col-lg-4">
                                                    <div class="form-group">		
                                                            {{ Form::label('ncb_female', 'Non-CARP Business (NCB) Female', array('class'=>'control-label')) }}
                                                            {{ Form::text('ncb_female', NULL, array('class'=>'form-control')) }}
                                                    </div>
                                            </div>
                                    </div>
                                    <div class="form-group act-sales">		
                                            {{ Form::label('sales', 'Sales', array('class'=>'control-label')) }}
                                            <div class="input-group">
                                            <span class="input-group-addon">P</span>
                                                {{ Form::text('sales', NULL, array('class'=>'form-control')) }}
                                            </div>
                                    </div>
                                    <div class="form-group">
                                            {{ Form::label('msme_id', 'Select MSME', array('class'=>'control-label')) }}
                                            {{ Form::select('msme_id', $msme_dropdown, 1,  array('class'=>'form-control')) }}
                                    </div>
                            </div>
                            <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Add</button>
                                    {{ Form::close() }}
                            </div>
                            </div>
                    </div>
            </div>
            
            <!-- edit attendance
            <div id="edit-attn-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                        <div class="modal-content">
                        <div class="modal-header">
                        
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <h4 class="modal-title">&raquo; <i class="fa fa-file"></i><span id="add-attn-title"> Edit Attendance </span></h4>
                        </div>
                        <div class="modal-body">
                                {{ Form::open(array('url' => 'provincial/attendances', 'method' => 'put', 'id'=>'edit-attn-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
                                <div class="row">
                                        <div class="col-lg-4">
                                                {{ Form::hidden('act_id', NULL, array('class'=>'form-control')) }}
                                                <div class="form-group">	
                                                        {{ Form::label('edit_fb_male', 'Farmer Beneficiaries (FB) Male', array('class'=>'control-label')) }}
                                                        {{ Form::text('edit_fb_male', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                        <div class="col-lg-4">
                                                <div class="form-group">		
                                                        {{ Form::label('edit_lo_male', 'Lot Owners (LO) Male', array('class'=>'control-label')) }}
                                                        {{ Form::text('edit_lo_male', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                        <div class="col-lg-4">
                                                <div class="form-group">		
                                                        {{ Form::label('edit_ncb_male', 'Non-CARP Business (NCB) Male', array('class'=>'control-label')) }}
                                                        {{ Form::text('edit_ncb_male', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                                <div class="row">
                                        <div class="col-lg-4">
                                                <div class="form-group">	
                                                        {{ Form::label('edit_fb_female', 'Farmer Beneficiaries (FB) Female', array('class'=>'control-label')) }}
                                                        {{ Form::text('edit_fb_female', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                        <div class="col-lg-4">
                                                <div class="form-group">		
                                                        {{ Form::label('edit_lo_female', 'Lot Owners (LO) Female', array('class'=>'control-label')) }}
                                                        {{ Form::text('edit_lo_female', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                        <div class="col-lg-4">
                                                <div class="form-group">		
                                                        {{ Form::label('edit_ncb_female', 'Non-CARP Business (NCB) Female', array('class'=>'control-label')) }}
                                                        {{ Form::text('edit_ncb_female', NULL, array('class'=>'form-control')) }}
                                                </div>
                                        </div>
                                </div>
                                 <div class="form-group act-sales">		
                                        {{ Form::label('edit_sales', 'Sales', array('class'=>'control-label')) }}
                                        <div class="input-group">
                                        <span class="input-group-addon">P</span>
                                            {{ Form::text('edit_sales', NULL, array('class'=>'form-control')) }}
                                        </div>
                                </div>
                                <div class="form-group">
                                        {{ Form::label('edit_msme_id', 'Select MSME', array('class'=>'control-label')) }}
                                        {{ Form::select('edit_msme_id', $msme_dropdown, 1,  array('class'=>'form-control')) }}
                                </div>
                        </div>
                        <div class="modal-footer">
                                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Edit</button>
                                {{ Form::close() }}
                        </div>
                        </div>
                </div>
            </div>
            
            <!-- get attendances -->
            <div id="get-attn-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-md">
                        <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title"><i class="fa fa-users"></i><span id="get-attn-title">  </span></h4>
                        </div>
                        <div class="modal-body" >
							<div class="no-data no-data-attn">
								<i class="fa fa-ban"></i>
								<br><span>No data available</span>
								<br>
							</div>
                            <table id="attendances" class="table table-striped table-bordered table-hover">
                                <thead>
                                    <tr >
                                        <th>MSME</th>
                                        <th>FB Male</th>
                                        <th>FB Female</th>
                                        <th>LO Male</th>
                                        <th>LO Female</th>
                                        <th>NCB Male</th>
                                        <th>NCB Female</th>
                                        <th class="act-sales">Sales</th>
                                        <!-- <th>Actions</th> -->
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
							
                        </div>
                        <div class="modal-footer">
                               <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
                        </div>
                        </div>
                </div>
            </div>
@stop

@section('additional_scripts')
    @parent
    <script>
     /*   $(".add-attn-trigger").click( function() {
            var act_id = $(this).attr("act-id");
            var act_name = $(this).attr("act-name");
            var w_sales = $(this).attr("act-wsales");
            $('input[name="act_id"]').val(act_id);
            $('#add-attn-title').html('Add Attendance to ' + act_name);
            showSalesField(w_sales);
            
        });
        */
        $(".get-attn-trigger").click( function() {
            $('#attendances tbody').html("");
            var act_id = $(this).attr("act-id");
			var act_name = $(this).attr("act-name");
            var w_sales = $(this).attr("act-wsales");
           
			 $('#get-attn-title').html('  Attendance: '+act_name); 
			 
            @if(Session::get('access') == 2)
                var link = '{{ url('regional/activities') }}' + '/' + act_id
            @else
                var link = '{{ url('provincial/activities') }}' + '/' + act_id
            @endif
            
			$('#attendances').hide();
			$('.no-data-attn').show();
            $.ajax({ 
                type: 'GET', 
                url:  link,
                dataType: 'json',
                success: function (data) {
                    $.each(data, function(index, value) { 
						if(data)
						{	
							var table_row = "<tr><td>"+value.msme_name+"</td>" +
                                        "<td align='center'>" + value.fb_male+ "</td>" +
                                        "<td align='center'>" + value.fb_female+ "</td>" +
                                        "<td align='center'>" + value.lo_male+ "</td>" +
                                        "<td align='center'>" + value.lo_female+ "</td>" +
                                        "<td align='center'>" + value.ncb_male+ "</td>" +
                                        "<td align='center'>" + value.ncb_female+ "</td>";
								if(w_sales == '1')
								{
									table_row += "<td  align='right' class='act-sales'> ₱ " + numberWithCommas(parseFloat(value.sales).toFixed(2)) + "</td>";
									$('#get-attn-title').html(' Sales & Attendance: '+ act_name);
								}
								//  table_row += "<td><span data-toggle='tooltip'>" +
									//        "<button type='submit' class='btn btn-default add-attn-trigger' data-dismiss='modal' data-toggle='modal' data-target='#edit-attn-modal' onclick='editAttn("+value.id + ")'>" +
									 //       "<i class='fa fa-pencil-square-o'></i></button></span>" + "</tr>";
									
									table_row += "</tr>";
								$('#attendances').show();
								$('.no-data-attn').hide();
								$('#attendances tbody').append(table_row);
						}
						
                    });
                }
            });
            
            showSalesField(w_sales);
        });
		
		
		 $(".edit-act-trigger").click( function() {
            $('#attendances tbody').html("");
            var act_id = $(this).attr("act-id");
			var act_name = $(this).attr("act-name");
            var w_sales = $(this).attr("act-wsales");
           
			 $('#edit-act-title').html(' Edit '+act_name); 
			 
            @if(Session::get('access') == 2)
                var link = '{{ url('regional/activities') }}' + '/' + act_id 
            @else
                var link = '{{ url('provincial/activities') }}' + '/' + act_id 
            @endif
         
            $.ajax({ 
                type: 'GET', 
                url:  link + '/edit',
                dataType: 'json',
                success: function (data) { 
                       	$("#edit_act_inter").val(data.intervention_id);
						$("#edit_act_name").val(data.name);
						date = new Date(data.date_constructed);
						date = new Date(date.setDate(date.getDate()));
						$("#edit_date_constructed").val($.datepicker.formatDate('M d, yy', date));
						date = new Date(data.date_finished);
						date = new Date(date.setDate(date.getDate()));
						$("#edit_date_finished").val($.datepicker.formatDate('M d, yy', date));
						$("#edit_manmonths").val(data.man_months);
						$("#edit_venue").val(data.venue);		
						$("#edit_sales").val(data.sales);
						if (data.intervention_id == '13') {
							$('#edit-man-months').show();
						}
						$("#edit-act-form").attr('action', link);
                 
                }
            });
            
            showSalesField(w_sales);
        });
       /* function editAttn(attn_id) {
            var link = '{{ url('provincial/attendances') }}' + '/' + attn_id;

            $.ajax({ 
                type: 'GET', 
                url:  link + "/edit",
                dataType: 'json',
                success: function (data) {
                    $("#edit_fb_male").val(data.fb_male);
                    $("#edit_fb_female").val(data.fb_female);
                    $("#edit_lo_male").val(data.lo_male);
                    $("#edit_lo_female").val(data.lo_female);
                    $("#edit_ncb_male").val(data.ncb_male);
                    $("#edit_ncb_female").val(data.ncb_female);
                    $("#edit_msme_id").val(data.msme_id);
                    $("#edit_sales").val(data.sales);
                    $("#edit-attn-form").attr('action', link);
                }
            });
        }*/
        
        function showSalesField(wsales) {
            if (wsales == 0) { //if activity has sales
                $('.act-sales').hide();
            }
            else {
                $('.act-sales').show();
            }
        }
		$(document).ready(function() {
			@foreach($inter_dropdown as $key => $value) 
				$('#dataTables-{{ $key }}').DataTable({
						responsive: true,
						'iDisplayLength': 10
				});
			@endforeach
		});
		 $(function () { 
			$('#inter a:nth-child( {{ Session::pull("tab_activity", "default") }} )').tab('show')
			
		  })
		
		$('#inter_dropdown').change(function(){  
			if (this.value)
			{
				$('#inter a:nth-child('+this.value+')').tab('show')
			}
		});	
			
		</script>
    </script>
@stop
