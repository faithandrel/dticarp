 @extends('national/modules')
 @section('inner_content')
    <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-angle-double-right"></i><span> {{ Session::get('province') }} Accomplishments</span>				
			</h3>	
		</div>
		
	    </div>
            <!-- /.row -->
	   

	   <?php /*@foreach($current as $key => $result)
			<strong>{{ $key }} </strong> - {{ var_dump($result) }} <br/>
	    @endforeach */?>
		<div class="row header-form">
			{{ Form::open(array('url' => 'national/accomplishments', 'class'=>'form-horizontal', 'role'=>'form')) }}
			 <div class="form-group-sm col-sm-8">
                {{ Form::label('region', 'Regions', array('class'=>'control-label')) }}
                {{ Form::select('region', $regions_dropdown , date('Y'),  array('class'=>'form-control', 'id'=>'acc_year','onchange'=>'this.form.submit()')) }}
			</div>
			 <div class="form-group-sm col-sm-4">
                {{ Form::label('year', 'Year', array('class'=>'control-label')) }}
                {{ Form::select('year', $years_dropdown , date('Y'),  array('class'=>'form-control', 'id'=>'acc_year','onchange'=>'this.form.submit()')) }}
			</div>
			{{ Form::close();}}
		</div>
		@if(isset($annual))
		<div class="row table-wrapper-target">
		<div class="col-lg-12">
			<table class="table-bordered  table-hover table-target">
				<thead>
					<tr>
						<th rowspan="2">Indicators</th>
						<th rowspan="2">Annual Target</th>
						<th rowspan="2">This Quarter</th>
						<th rowspan="2">Accomplishment Todate</th>
						<th rowspan="2">%  Accomplishment</th>
					</tr>

				</head>
				<tbody>
				    <tr>
						<td>Investments (P M))</td>
						<td align="right">{{ '₱ ' . number_format($annual['investments'], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($current['investments'], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($todate['investments'], 2)  }}</td>
						<td align="right">{{ $percentage['investments'] }}%</td> </tr>
				    <tr>
						<td>Sales (P M)</td>
						<td align="right">{{ '₱ ' . number_format($annual['sales'], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($current['sales'], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($todate['sales'], 2)  }}</td>
						<td align="right">{{ $percentage['sales'] }}%</td> </tr>
				    <tr>
				    <td>Jobs Generated</td>
						<td align="right">{{ number_format($annual['jobs'] )  }}</td>
						<td align="right">{{ number_format($current['jobs'])  }}</td>
						<td align="right">{{ number_format($todate['jobs'])   }}</td>
						<td align="right">{{ $percentage['jobs'] }}%</td>  </tr>
				    <tr>
						<td>Entrepreneurs Developed</td>
						<td align="right">{{ number_format($annual['entrepreneurs'])   }}</td>
						<td align="right">{{ number_format($current['entrepreneurs'])   }}</td>
						<td align="right">{{ number_format($todate['entrepreneurs'])   }}</td>
						<td align="right">{{ $percentage['entrepreneurs'] }}%</td> </tr> </tr>
				    <tr>
						<td>Number of ARCs Assisted</td>
						<td align="right">{{ number_format($annual['arcs_assisted'])  }}</td>
						<td align="right">{{ number_format($current['arcs_assisted'])  }}</td>
						<td align="right">{{ number_format($todate['arcs_assisted'])  }}</td>
						<td align="right">{{ $percentage['arcs_assisted'] }}%</td>   
				    </tr>
					<tr>
						<td>Number of Non-ARCs Assisted</td>
						<td align="right">{{ number_format($annual['non_arcs_assisted']) }}</td>
						<td align="right">{{ number_format($current['non_arcs_assisted'])  }}</td>
						<td align="right">{{ number_format($todate['non_arcs_assisted'])  }}</td>
						<td align="right" >{{ $percentage['non_arcs_assisted'] }}%</td> </tr>
				    <tr>
						<td>Number of MSMEs Developed</td>
						<td align="right">{{ number_format($annual['msmes_dev'])  }}</td>
						<td align="right">{{ number_format($current['msmes_dev'])  }}</td>
						<td align="right">{{ number_format($todate['msmes_dev'])  }}</td>
						<td align="right">{{ $percentage['msmes_dev'] }}%</td></tr>
				    <tr>
						<td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
						<td align="right">{{ number_format($annual['msmes_dev_fb'])  }}</td>
						<td align="right">{{ number_format($current['msmes_dev_fb'])  }}</td>
						 <td align="right">{{ number_format($todate['msmes_dev_fb'])  }}</td>
						<td align="right">{{ $percentage['msmes_dev_fb'] }}%</td>  </tr>
				    <tr>
						<td> &nbsp; &nbsp; Landowners served</td>
						<td align="right">{{ number_format($annual['msmes_dev_lo'])  }}</td>
						<td></td>
						<td></td>
						<td></td>
				    </tr>
				    <tr>
						<td> &nbsp; &nbsp;  Sales Generated</td>
						 <td align="right">{{ '₱ ' . number_format($annual['established'], 2)  }}</td>
						 <td align="right">{{ '₱ ' . number_format($current['established'], 2)  }}</td>
						 <td align="right">{{ '₱ ' . number_format($todate['established'], 2)  }}</td>
						<td align="right">{{ $percentage['established'] }}%</td>  </tr>
				    <tr>
						<td> &nbsp; &nbsp;  ARCs served</td>
						 <td align="right">{{ number_format( $annual['msmes_dev_arcs']) }}</td>
						 <td align="right">{{ number_format($current['msmes_dev_arcs'])  }}</td>
						 <td align="right">{{ number_format($todate['msmes_dev_arcs'])  }}</td>
						<td align="right">{{ $percentage['msmes_dev_arcs'] }}%</td>   </tr>
				    <tr>
						<td>Number of MSMEs Assisted</td>
						 <td align="right">{{ number_format($annual['msmes_asst'])  }}</td>
						 <td align="right">{{ number_format($current['msmes_asst'])  }}</td>
						 <td align="right">{{ number_format($todate['msmes_asst'])  }}</td>
						<td align="right">{{ $percentage['msmes_asst'] }}%</td>     </tr>
				    <tr>
						<td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
						<td align="right">{{ number_format($annual['msmes_asst_fb'] ) }}</td>
						<td align="right">{{ number_format($current['msmes_asst_fb'])  }}</td>
						<td align="right">{{ number_format($todate['msmes_asst_fb'])  }}</td>
						<td align="right">{{ $percentage['msmes_asst_fb'] }}%</td>    </tr>
				    <tr>
						<td> &nbsp; &nbsp; Landowners served</td>
						<td align="right">{{ number_format($annual['msmes_asst_lo'])  }}</td>
						<td align="right">{{ number_format($current['msmes_asst_lo'])  }}</td>
						<td align="right">{{ number_format($todate['msmes_asst_lo'])  }}</td>
						<td align="right">{{ $percentage['msmes_asst_lo'] }}%</td>    </tr>
				    <tr>
					 <td> &nbsp; &nbsp;  Monitored Sales</td>
						<td align="right">{{ '₱ ' . number_format($annual['monitored'], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($current['monitored'], 2)  }}</td>
						<td align="right">{{ '₱ ' . number_format($todate['monitored'], 2)  }}</td>
						<td align="right">{{ $percentage['monitored'] }}%</td>    </tr>
				    <tr>
					<td> &nbsp; &nbsp;  ARCs served</td>
						 <td align="right">{{ number_format($annual['msmes_asst_arcs'])  }}</td>
						 <td align="right">{{ number_format($current['msmes_asst_arcs'])  }}</td>
						 <td align="right">{{ number_format($todate['msmes_asst_arcs'])  }}</td>
						<td align="right">{{ $percentage['msmes_asst_arcs'] }}%</td>    
					</tr>
				    @foreach ($inter_targets as $inter_target)
				    @if($inter_target->intervention_id == 13)
				    <tr>
						<td>Number of Man-months Consultancy</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['manmonths'],5) }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['manmonths'],5)  }}</td>
						<td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['manmonths'] }}%</td>
				     </tr>
				    @else
				    <tr>
						<td>Number of {{ $inter_target->intervention()->pluck('intervention') }}</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['count'] )  }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['count']) }}</td>
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['count'])  }}</td>
						<td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['count'] }}%</td>
				   </tr>
				    @endif
				    <tr>
						<td> &nbsp; &nbsp; Farmer-beneficiaries served</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['fbs']) }}</td>
						 <td align="right">{{  $percentage['interattns'][$inter_target->intervention_id]['fbs'] }}%</td>
				      </tr>
				    <tr>
					     <td> &nbsp; &nbsp; Landowners served</td>
						<td align="right">{{ $inter_target->los }}</td>
					    <td align="right">{{ $current['interattns'][$inter_target->intervention_id]['los'] }}</td>
						<td align="right">{{ $todate['interattns'][$inter_target->intervention_id]['los'] }}</td>
					    <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['los'] }}%</td>
				    @if($inter_target->intervention()->pluck('w_sales'))
				    <tr>
					    <td> &nbsp; &nbsp; Landowners served</td>
						<td align="right">{{ $inter_target->los }}</td>
					    <td align="right">{{ $current['interattns'][$inter_target->intervention_id]['los'] }}</td>
						<td align="right">{{ $todate['interattns'][$inter_target->intervention_id]['los'] }}</td>
					    <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['los'] }}%</td>
				    </tr>
				    @endif
				    <tr>
					    <td> &nbsp; &nbsp; Landowners served</td>
						 <td align="right">{{ number_format($annual['interattns'][$inter_target->intervention_id]['los'])  }}</td>
						 <td align="right">{{ number_format($current['interattns'][$inter_target->intervention_id]['los']) }}</td>
						 <td align="right">{{ number_format($todate['interattns'][$inter_target->intervention_id]['los'])  }}</td>
						 <td align="right">{{ $percentage['interattns'][$inter_target->intervention_id]['los']  }}%</td>
				    </tr>
				    @endforeach
				</tbody>
			</table>
		</div>
	</div>
		
	@elseif ( !isset($annual) && Input::get('region') != 0)
	<div class="no-data">
		<i class="fa fa-ban"></i>
		<br><span>No Annual Target set</span>
		<br>
	</div>
	
	@else 
		<div class="col-lg-12 home-logo" >
			<img class="dti-logo" src="{{ url('/img/dti-logo.jpg') }}"> <br>
			<img class="carp-logo" src="{{ url('/img/carp-logo.jpg') }}">
	    </div>
	@endif

@stop

@section('additional_scripts')
@parent
<script>

</script>
@stop