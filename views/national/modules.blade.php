@extends('base')
@section('sidebar')
	    <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
						<li class="sidebar-account">
							<a href="#">
								<table>
									<tr>
										<td rowspan="2"><i class="fa fa-user fa-4x" style="color: #ec6967"></i> </td>
										<td><span class="account-name">{{ Session::get('username') }}</span></td>
										
									</tr>
									
									<tr>
										<td><span class="account-label">
											National Account		
										</span></td>
										<span class="caret-profile" ><i class="fa fa-caret-down" style="float: right"></i></span>
									</tr>
									
								</table>
								
							</a>
							<ul class="nav nav-second-level sidebar-account-second-level">
									<li><a href="#/" data-toggle="modal" data-target="#profile-modal"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
									<li><a href="#/" data-toggle="modal" data-target="#change-pass-modal"><i class="fa fa-gear fa-fw"></i> Change Password</a></li>
									<li class="divider"></li>
									<li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
									</li>
							</ul>
						</li>
						<li>
							<a  href="{{ url('national/home') }}" ><i class="fa fa-trophy fa-fw"></i>&nbsp; ACCOMPLISHMENTS<span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="/#" ><i class="fa fa-bar-chart fa-fw"></i>&nbsp; REPORTS<span class="fa arrow"></span></a>
								<ul class="nav nav-second-level">
									<li>
										<a target="_blank" href="{{ url('national/reports/accomplishment-arcs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Accomplishments Per Arc</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/summary-accomplishments') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Accomplishments</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/trainings') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Trainings</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/entrepreneurs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Entrepreneurs Developed</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/jobs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Jobs Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/sales') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Sales Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/investments') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Investments Generated</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/gender-disaggregated') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Gender-Disaggregated Data on Program Services</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/assisted-arcs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; List of Assisted ARCs</a>
									</li>
									<li>
										<a target="_blank" href="{{ url('national/reports/trade_fairs') }}"><i class="fa fa-file-text-o fa-fw"></i>&nbsp; Trade Fairs</a>
									</li>
								</ul>
						</li>
						<li>
							<a href="#/" ><i class="fa fa-archive fa-fw"></i>&nbsp; REGION LIBRARY<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="{{ url('national/regions') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View Regions</a>
								</li>
								<li>
									<a href="#/" data-toggle="modal" data-target="#add-reg-modal" ><i class="fa fa-plus fa-fw"></i>&nbsp; Add Region</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#/" ><i class="fa fa-archive fa-fw"></i>&nbsp; PROVINCE LIBRARY<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="{{ url('national/provinces') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View Provinces</a>
								</li>
								<li>
									<a href="#/" data-toggle="modal" data-target="#add-prov-modal" ><i class="fa fa-plus fa-fw"></i>&nbsp; Add Province</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#/" ><i class="fa fa-archive fa-fw"></i>&nbsp; USER LIBRARY<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="{{ url('national/users') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View Users</a>
								</li>
								<li>
									<a href="#/" data-toggle="modal"  class="add-user-trigger"  data-target="#add-user-modal"><i class="fa fa-user-plus fa-fw"></i>&nbsp; Add User</a>
								</li>
							</ul>
						</li>
						<li>
							<a href="#/" ><i class="fa fa-archive fa-fw"></i>&nbsp; INTERVENTION LIBRARY<span class="fa arrow"></span></a>
							<ul class="nav nav-second-level">
								<li>
									<a href="{{ url('national/interventions') }}"><i class="fa fa-table fa-fw"></i>&nbsp; View Interventions</a>
								</li>
								<li>
									<a href="#/" data-toggle="modal" data-target="#add-inter-modal"><i class="fa fa-user-plus fa-fw"></i>&nbsp; Add Intervention</a>
								</li>
							</ul>
						</li>
								</ul>
							</div>
							<!-- /.sidebar-collapse -->
						</div>
            <!-- /.navbar-static-side -->
        </nav>
	    
	<!-- region modals -->
	<div id="add-reg-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New Region</span></h4>
		    </div>
		    <div class="modal-body">
			{{ Form::open(array('url' => 'national/regions', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="form-group">
				{{ Form::label('region_name', 'Name', array('class'=>'control-label')) }}
				{{ Form::text('region_name', NULL, array('class'=>'form-control')) }}
			    </div>
		    </div>
		    <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			{{ Form::close() }}
		    </div>
		</div>
	    </div>
	</div>
	
	<div id="edit-reg-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit Region</span></h4>
		    </div>
		    <div class="modal-body">
			{{ Form::open(array('url' => 'national/regions', 'method' => 'put', 'id'=>'edit-reg-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="form-group">
				{{ Form::label('edit_reg_name', 'Name', array('class'=>'control-label')) }}
				{{ Form::text('edit_reg_name', NULL, array('class'=>'form-control')) }}
			    </div>
		    </div>
		    <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			{{ Form::close() }}
		    </div>
		</div>
	    </div>
	</div>
	<!-- province modals -->    
	<div id="add-prov-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New Province</span></h4>
		    </div>
		    <div class="modal-body">
			{{ Form::open(array('url' => 'national/provinces', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="form-group">
				{{ Form::label('prov_name', 'Name', array('class'=>'control-label')) }}
				{{ Form::text('prov_name', NULL, array('class'=>'form-control')) }}
			    </div>
			    <div class="form-group">
				{{ Form::label('region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('region', $regions_dropdown, NULL,  array('class'=>'form-control')) }}
			    </div>
		    </div>
		    <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			{{ Form::close() }}
		    </div>
		</div>
	    </div>
	</div>
	
	<div id="edit-prov-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="fa fa-pencil fa-fw"></i><span>  Edit Province</span></h4>
		    </div>
		    <div class="modal-body">
			{{ Form::open(array('url' => 'national/provinces', 'method' => 'put', 'id'=>'edit-prov-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="form-group">
				{{ Form::label('edit_prov_name', 'Name', array('class'=>'control-label')) }}
				{{ Form::text('edit_prov_name', NULL, array('class'=>'form-control')) }}
			    </div>
			    <div class="form-group">
				{{ Form::label('edit_prov_region', 'Region', array('class'=>'control-label')) }}
				{{ Form::select('edit_prov_region', $regions_dropdown, NULL,  array('class'=>'form-control')) }}
			    </div>
		    </div>
		    <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			{{ Form::close() }}
		    </div>
		</div>
	    </div>
	</div>
	
	<!-- intervention modals -->    
	<div id="add-inter-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
	    <div class="modal-dialog modal-sm">
		<div class="modal-content">
		    <div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			<h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  New Intervention</span></h4>
		    </div>
		    <div class="modal-body">
			{{ Form::open(array('url' => 'national/interventions', 'class'=>'form-horizontal', 'role'=>'form')) }}
			    <div class="form-group">
				{{ Form::label('inter_name', 'Name', array('class'=>'control-label')) }}
				{{ Form::text('inter_name', NULL, array('class'=>'form-control')) }}
			    </div>
			    <div class="form-group">	
				{{ Form::label('w_sales', 'With Sales?') }}
				{{ Form::checkbox('w_sales', 1) }}
			    </div>
			    <div class="form-group">
				{{ Form::label('category', 'Category', array('class'=>'control-label')) }}
				{{ Form::select('category', $category_dropdown, NULL,  array('class'=>'form-control')) }}
			    </div>
		    </div>
		    <div class="modal-footer">
			<button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			<button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			{{ Form::close() }}
		    </div>
		</div>
	    </div>
	</div>
	
	@include('usermodals')
@stop		
@section('content')
	<div id="page-wrapper">
            @if($message = Session::get('message'))
               <div class="alert-wrapper">
						<div  class="alert alert-{{ $message['type'] }} alert-dismissable">
							<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
							{{ $message['content'] }}
						</div>
					</div>
            @endif
        @yield('inner_content')
        </div>
@stop

@section('additional_scripts')
    <script>
	$('select[name="access"], select[name="edit_access"]').change( function() {
	    check_access($(this).val());
	});
	
	function check_access(access) {
	    if (access == '2') {
		$('.reg-dropdown').show();
		$('.prov-dropdown').hide();
	    }
	    else if (access == '3') {
		$('.reg-dropdown').hide();
		$('.prov-dropdown').show();
	    }
	    else if (access == '1') {
		$('.reg-dropdown').hide();
		$('.prov-dropdown').hide();
	    }
	}
    </script>
@stop