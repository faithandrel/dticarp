@extends('national/modules')
		
@section('inner_content')
    <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-angle-double-right"></i><span>Interventions</span></h3>					
		</div>
	    </div>
        
	    <div class="row">
            <div class="col-lg-12">
                <div class="dataTable_wrapper">
                    <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                        <thead>
                            <tr>
								<th class="actions-1"></th>
                                <th>Intervention</th>
								<th class="actions-1">Actions</th>
                            </tr>
                      </thead>
                       <tbody id="char">
					@foreach($interventions as $intervention)
					    <tr class="odd gradeX">
						<td >
							<span data-toggle="tooltip" title={{ "'Edit ".$intervention->intervention."'" }} >
								<button type="button" data-toggle="modal" data-target="#edit-inter-modal" class="btn btn-primary edit-inter-trigger" inter-id={{ $intervention->id }}><i class="fa fa-pencil-square-o"></i></button>
						    </span>
						</td>
						<td >{{ $intervention->intervention }}</td>
						{{ Form::open(array('url' => 'national/interventions/'.$intervention->id, 'method' => 'delete')) }}
						<td >
							<span data-toggle="tooltip" title={{ "'Delete ".$intervention->intervention."'" }} >
								<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $intervention->intervention }}?' )" ><i class="fa fa-trash"></i></button>
							</span>
						</td>
						{{ Form::close() }}
					    </tr>
					@endforeach
                                        
                       </tbody>
                    </table>
                </div>
            </div>
        </div>
    
    <!--edit intervention modal-->
    <div id="edit-inter-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-plus fa-fw"></i><span>  Edit Intervention</span></h4>
                </div>
                <div class="modal-body">
                    {{ Form::open(array('url' => 'national/interventions', 'method' => 'put', 'id'=>'edit-inter-form', 'class'=>'form-horizontal', 'role'=>'form')) }}
                        <div class="form-group">
                            {{ Form::label('edit_inter_name', 'Name', array('class'=>'control-label')) }}
                            {{ Form::text('edit_inter_name', NULL, array('class'=>'form-control')) }}
                        </div>
                        <div class="form-group">	
                            {{ Form::label('edit_w_sales', 'With Sales?') }}
                            {{ Form::checkbox('edit_w_sales', 1) }}
                        </div>
                        <div class="form-group">
                            {{ Form::label('edit_category', 'Category', array('class'=>'control-label')) }}
                            {{ Form::select('edit_category', $category_dropdown, NULL,  array('class'=>'form-control')) }}
                        </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
                    <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
@stop

@section('additional_scripts')
@parent
<script>
    $(".edit-inter-trigger").click( function() {
        $("#edit_w_sales").prop( "checked", false );
        
        var inter_id = $(this).attr("inter-id");
	var link = '{{ url('national/interventions') }}' + '/' + inter_id
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) {
		$("#edit_inter_name").val(data.intervention);
                if (data.w_sales) {
                    $("#edit_w_sales").prop( "checked", true );
                }
		$("#edit_category").val(data.inter_category_id);
                $("#edit-inter-form").attr('action', link);
            }
        });
    });
</script>
@stop