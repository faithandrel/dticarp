@extends('national/modules')
		
@section('inner_content')
            <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-angle-double-right"></i><span>Regions</span></h3>					
		</div>
	    </div>
            <!-- /.row -->
	    <div class="row">
                <div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
											<th class="actions-1"></th>
                                            <th>Regions</th>
											<th class="actions-1">Actions</th>
															</tr>
														</thead>
														<tbody id="char">
										@foreach($regions as $region)
											<tr class="odd gradeX">
											<td class="actions-1">
												<span data-toggle="tooltip" title={{ "'Edit ".$region->region."'" }} >
													<button type="button" data-toggle="modal" data-target="#edit-reg-modal" class="btn btn-primary edit-reg-trigger" reg-id={{ $region->id }}><i class="fa fa-pencil-square-o"></i></button>
												</span>
											</td>
											<td >{{ $region->region }}</td>
											{{ Form::open(array('url' => 'national/regions/'.$region->id, 'method' => 'delete')) }}
											<td class="actions-1">
												<span data-toggle="tooltip" title={{ "'Delete ".$region->region."'" }} >
													<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $region->region }}?' )" ><i class="fa fa-trash"></i></button>
												</span>
											</td>
											{{ Form::close() }}
											</tr>
										@endforeach
                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
@stop

@section('additional_scripts')
@parent
<script>
    $(".edit-reg-trigger").click( function() {
        var reg_id = $(this).attr("reg-id");
	var link = '{{ url('national/regions') }}' + '/' + reg_id
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) {
		$("#edit_reg_name").val(data.region);
                $("#edit-reg-form").attr('action', link);
            }
        });
    });
</script>
@stop