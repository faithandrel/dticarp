@extends('national/modules')
@section('inner_content')
    <div class="row">
		<div class="col-lg-12">
		    <h2 class="page-header"><span>Welcome {{ Session::get('username') }}!</span></h2>
		</div>
	</div>
 
	<div class="row">
		<div class="col-lg-12 home-logo" >
			<img class="dti-logo" src="{{ url('/img/dti-logo.jpg') }}"> <br>
			<img class="carp-logo" src="{{ url('/img/carp-logo.jpg') }}">
	    </div>
		 </div>
	</div>
@stop