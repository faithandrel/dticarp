@extends('national/modules')

@section('inner_content')
	    <div class="row">
		<div class="col-lg-12">
		    <h3 class="page-header"><i class="fa fa-angle-double-right"></i><span>Provinces</span></h3>					
		</div>
	    </div>
            <!-- /.row -->
	    <div class="row">
                <div class="col-lg-12">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
											<th class="actions-1"></th>
                                            <th>Provinces</th>
                                            <th>Regions</th>
											<th class="actions-1"></th>
                                        </tr>
                                    </thead>
                                    <tbody id="char">
										@foreach($provinces as $province)
											<tr class="odd gradeX">
											<td >
												<span data-toggle="tooltip" title={{ "'Edit ".$province->province."'" }} >
													<button type="button" data-toggle="modal" data-target="#edit-prov-modal" class="btn btn-primary edit-prov-trigger" prov-id={{ $province->id }}><i class="fa fa-pencil-square-o"></i></button>
												</span>
											</td>
											<td >{{ $province->province }}</td>
											<td >{{ $province->region()->pluck('region') }}</td>
											{{ Form::open(array('url' => 'national/provinces/'.$province->id, 'method' => 'delete')) }}
											<td >
												<span data-toggle="tooltip" title={{ "'Delete ".$province->province."'" }} >
													<button type="submit" class="btn btn-danger" onclick="return confirm('Delete {{ $province->province }}?' )" ><i class="fa fa-trash"></i></button>
												</span>
											</td>
											{{ Form::close() }}
											</tr>
										@endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                </div>
            </div>
@stop

@section('additional_scripts')
@parent
<script>
    $(".edit-prov-trigger").click( function() {
        var prov_id = $(this).attr("prov-id");
	var link = '{{ url('national/provinces') }}' + '/' + prov_id
        
        $.ajax({ 
            type: 'GET', 
            url:  link + '/edit',
            dataType: 'json',
            success: function (data) {
		$("#edit_prov_name").val(data.province);
		$("#edit_prov_region").val(data.region_id);
                $("#edit-prov-form").attr('action', link);
            }
        });
    });
</script>
@stop