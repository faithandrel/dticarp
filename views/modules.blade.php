@extends('base')
@section('sidebar')
			<div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        @if(Session::get('access') == 1)
				<li>
					<a href=""><i class="fa fa-user fa-fw"></i> {{ Session::get('username') }}
						<span class="account-label"> National Account </span>
					</a>
				</li>
				<li>
					<a href="home"><i class="fa fa-plus fa-fw"></i> Add Region</a>
				</li> 
				<li>
					<a href="users/create"><i class="fa fa-user-plus fa-fw"></i> Add User</a>
				</li>
			@endif
			
			@if(Session::get('access') == 2) 
				<li>
					<a href=""><i class="fa fa-user fa-fw"></i> {{ Session::get('username') }}
						<span class="account-label"> Regional Account </span>
					</a>
				</li>
				<li>
					<a href="home"><i class="fa fa-plus fa-fw"></i> Add Province</a>
				</li> 
				<li>
					<a href="users/create"><i class="fa fa-user-plus fa-fw"></i> Add Provincial User</a>
				</li>
			@endif
			
			@if(Session::get('access') == 3)
				<li>
					<a href=""><i class="fa fa-user fa-fw"></i> {{ Session::get('username') }}
						<span class="account-label">
							Provincial Account 
						</span>
					</a>
				</li>
				<li>
					<a href="#" data-toggle="modal" data-target="#add-arc-modal" ><i class="fa fa-file-text fa-fw" ></i> Add ARC</a>
				</li>
			@endif

                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
@stop		
@section('content')
	<div id="page-wrapper">
            @if($message = Session::get('message'))
                <div class="alert alert-{{ $message['type'] }} alert-dismissable">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    {{ $message['content'] }}
                </div>
            @endif
        @yield('inner_content')
        </div>
@stop