@extends('modules')

@section('inner_content')
    <div class="row">
	<div class="col-lg-12">
	    <h3 class="page-header"><i class="fa fa-plus fa-fw"></i><span>Add Region</span></h3>
	    <div id="add-arc">
		{{ Form::open(array('url' => 'national/submit-region', 'class'=>'form-horizontal', 'role'=>'form')) }}
		    <div class="form-group">
			{{ Form::label('region_name', 'Name', array('class'=>'control-label')) }}
			{{ Form::text('region_name', NULL, array('class'=>'form-control')) }}
		    </div>
			{{ Form::submit('Add', array('class'=>'btn btn-default')) }}
		   
		{{ Form::close() }}
	    </div>
	    DTI-CARP | user: {{ Session::get('username') }}  access: {{ Session::get('access') }}
				
	</div>
    </div>
            <!-- /.row -->
@stop