<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>DTI-CARP Monitoring System</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="{{ asset('bower_components/metisMenu/dist/metisMenu.min.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
	
	<!-- DataTables CSS -->
    <link href="{{ asset('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.css') }}" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="{{ asset('bower_components/datatables-responsive/css/dataTables.responsive.css') }}" rel="stylesheet">

	<!-- Custom CSS -->
    <link href="{{ asset('dist/css/style.css') }}" rel="stylesheet">
	
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
	
	<link href="{{ asset('dist/css/jquery-ui.css') }}" rel="stylesheet">	
	
	<link href='http://fonts.googleapis.com/css?family=Ubuntu' rel='stylesheet' type='text/css'>
</head>

<body>
	@if(Session::get('username'))
	<!-- Navigation  -->
		<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom: 0">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
					<a class="navbar-brand" href="#">
						<img class="logo" src="{{ url('/img/logo.png') }}"><span>
						CARP Monitoring System</span></a>
			</div>
				<!-- /.navbar-header -->

			<ul class="nav navbar-top-links navbar-right">
				<!-- /.dropdown -->
				<li class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#">
						<i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>
					</a>
					<ul class="dropdown-menu dropdown-user">
						<li><a href="#" data-toggle="modal" data-target="#profile-modal"><i class="fa fa-user fa-fw"></i> User Profile</a></li>
						<li><a href="#" data-toggle="modal" data-target="#change-pass-modal"><i class="fa fa-gear fa-fw"></i> Change Password</a></li>
						<li class="divider"></li>
						<li><a href="{{ url('logout') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
						</li>
					</ul>
						<!-- /.dropdown-user -->
				</li>
					<!-- /.dropdown -->
			</ul>
				<!-- /.navbar-top-links -->
			<div id="loader">
				<i class="fa fa-spinner fa-pulse"></i>
			</div>
			
	@endif
    @yield('sidebar')  
    @yield('content')  
  
<!--- modals ---->
	
<!------------- user profile ---------------->
		<div id="profile-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-sm">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-user fa-fw"></i><span>  User Profile</span></h4>
			    </div>
			     <div class="modal-body">
					<ul class="list-group">
					  <li class="list-group-item"><b>Username: </b>{{ Session::get('username') }}</li>
					  <li class="list-group-item"><b>Name: </b>{{ Session::get('username') }}</li>
					 @if ( Session::get('access') == 3 ) <li class="list-group-item"><b>Province: </b>{{ Session::get('province') }}</li> @endif
					 @if ( Session::get('access') >= 2 ) <li class="list-group-item"><b>Region: </b>{{ Session::get('region') }}</li> @endif
					 @if ( Session::get('access') == 1 ) <li class="list-group-item">National Account</li> @endif
					</ul>
				 </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
			    </div>
			    </div>
		    </div>
		</div>

<!------------- change pass ---------------->
		<div id="change-pass-modal" class="modal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
		    <div class="modal-dialog modal-sm">
			    <div class="modal-content">
			    <div class="modal-header">
				    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				    <h4 class="modal-title"><i class="fa fa-gear fa-fw"></i><span> Change Password</span></h4>
			    </div>
				{{ Form::open(array('url' => 'change_password', 'name'=>'change_pass', 'class'=>'form-horizontal', 'role'=>'form')) }}
			     <div class="modal-body">
							<div class="form-group">	
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-lock"></i></span>
									{{ Form::password('current_pass', array('onKeyUp' => 'error()', 'required'=>'required', 'class' => 'form-control', 'placeholder'=>'Current Password', 'required' => 'required')) }}                                
								</div>
							</div>
							<div class="form-group check-pass">	
								<div style="margin-top: -15px; float: right; color: red; font-size: 10pt" id="check-pass"></div>
							</div>
							<div class="form-group">	
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-sign-in"></i></span>
								  {{ Form::password('new_pass', array('onKeyUp' => 'verify.check()', 'required'=>'required', 'class' => 'form-control', 'placeholder'=>'New Password', 'required' => 'required')) }}                               
								</div>
							</div>
							
							<div class="form-group">	
								<div class="input-group">
								  <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
								{{ Form::password('confirm_pass', array('onKeyUp' => 'verify.check()', 'required'=>'required', 'class' => 'form-control', 'placeholder'=>'Confirm Password', 'required' => 'required')) }}                                
								</div>
							</div>
							<div class="form-group">	
								<div style="margin-top: -15px; float: right; font-size: 10pt"id="password_result">&nbsp;</div>
							</div>
				 </div>
			    <div class="modal-footer">
				    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times fa-fw"></i> Close</button>
					<button type="button" onclick="checkPw()" class="btn btn-primary btn-save-pass"><i class="fa fa-floppy-o fa-fw"></i> Save</button>
			    </div>
				{{ Form::close() }}
			    </div>
		    </div>
		</div>
	
	
  
    <!-- jQuery -->
    <script  type="text/javascript" src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script  type="text/javascript" src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script  type="text/javascript" src="{{ asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Custom Theme JavaScript -->
    <script  type="text/javascript" src="{{ asset('dist/js/sb-admin-2.js') }}"></script>
	
	<!-- DataTables JavaScript -->
    <script  type="text/javascript" src="{{ asset('bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script  type="text/javascript" src="{{ asset('bower_components/datatables-plugins/integration/bootstrap/3/dataTables.bootstrap.min.js') }}"></script>

	<script  type="text/javascript" src="{{ asset('dist/js/jquery-ui.js') }}"></script>	
	
	<script type="text/javascript">
	// --------------- fade effect
		$(document).ready(function() { 
			$(".panel").hide().fadeIn("slow");
			$(".table-wrapper-accomplishment").hide().fadeIn("slow");
			$(".table-wrapper-target").hide().fadeIn("slow");
			$(".dataTable_wrapper").hide().fadeIn("slow");
			$("#inter").hide().fadeIn("slow");
			$( ".login" ).keydown(function() {
				$(".error").fadeOut("slow");
			});
			$( ".login" ).keydown(function() {
				$(".error").fadeOut("slow");
			});

			$('#dataTables-example').DataTable({
					responsive: false,
					'iDisplayLength': 25
			});
			
			$(".alert").fadeOut(6000);
			$( ".responsive-wrapper" ).scroll(function() {
				$(".scroll-right").fadeOut(4000);
			})
			
			$("#loader").hide();
			$(window).load(function() {
				$("#loader").fadeOut("slow");
			})
		});
	// --------------- tooltip
		$(function () {
			$('[data-toggle="tooltip"]').tooltip()
		})
	// --------------- datepicker
		$(function() {
			$( "#date_registered, #date_assisted, #date_developed, #date_started, #edit_date_assisted, #edit_date_registered" ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			
			$( "#edit_date_developed" ).datepicker({
			 showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#edit_date_started" ).datepicker({
			 showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#edit_date_constructed" ).datepicker({
			 showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#edit_date_finished" ).datepicker({
			  showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});		
			
			$( "#date_constructed" ).datepicker({
			  showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#date_ended" ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#date_finished" ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#igp_end_date" ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
			$( "#edit_igp_end_date" ).datepicker({
				showButtonPanel: true,
				dateFormat: "M d, yy",
				changeMonth: true,
				changeYear: true,
				showOtherMonths: true,
				selectOtherMonths: true
			});
		});

	//---------------- change pass
			verify = new verifynotify();
			verify.field1 = document.change_pass.new_pass;
			verify.field2 = document.change_pass.confirm_pass;
			verify.result_id = "password_result";
			verify.match_html = "<span><i class='fa fa-check'></i> Thank you, your passwords match!<\/span>";
			verify.nomatch_html = "<span  class='error'><i class='fa fa-warning'></i> Please make sure your passwords match.<\/span>";
			
			// Update the result message
			verify.check();
			$(".check-pass").hide();
			
			function checkPw()
			{
				pw1 = change_pass.new_pass.value;
				pw2 = change_pass.confirm_pass.value;
				current = change_pass.current_pass.value;
				
				
				if ( current == '' ) 
				{	
					$(".check-pass").show();
					$("#check-pass").html("Enter your current password.");
				}
				else if ( pw1 == pw2 && pw1 != ''  )
				{	
					var link = '{{ url('checkPass') }}' + '/' + current + '/' + pw2;
					$.get( link, function( data ) {
						if(data == 0)
						{
							$(".check-pass").show();
							$("#check-pass").html("<span  class='error'><i class='fa fa-times'></i> Your Password was Incorrect.</span>");
						}
						else if(data == 2)
						{
							$(".check-pass").show();
							$("#check-pass").html("<span  class='error'><i class='fa fa-times'></i>Password must differ from old password.</span>");
						}
						else 
						{
							document.forms["change_pass"].submit();	
							return false;
						}
					});
					
					//document.forms["change_pass"].submit();	
					//
				}
			}
		
		//-------------------- verifynotify password
		function verifynotify(field1, field2, result_id, match_html, nomatch_html) {
		 this.field1 = field1;
		 this.field2 = field2;
		 this.result_id = result_id;
		 this.match_html = match_html;
		 this.nomatch_html = nomatch_html;

		 this.check = function() {

		   // Make sure we don't cause an error
		   // for browsers that do not support getElementById
		   if (!this.result_id) { return false; }
		   if (!document.getElementById){ return false; }
		   r = document.getElementById(this.result_id);
		   if (!r){ return false; }

		   if (this.field1.value != "" && this.field1.value == this.field2.value) {
			 r.innerHTML = this.match_html;
		   } else {
			 r.innerHTML = this.nomatch_html;
		   }
		 }
		}
		
		//------------------ add input comma
		$('input.number').keyup(function (event) {
		if (event.which >= 37 && event.which <= 40) {
			event.preventDefault();
		}
			var currentVal = $(this).val();
			var testDecimal = testDecimals(currentVal);
			if (testDecimal.length > 1) {
				console.log("You cannot enter more than one decimal point");
				currentVal = currentVal.slice(0, -1);
			}
			$(this).val(replaceCommas(currentVal));
		});

		function testDecimals(currentVal) {
			var count;
			currentVal.match(/\./g) === null ? count = 0 : count = currentVal.match(/\./g);
			return count;
		}

		function replaceCommas(yourNumber) {
			var components = yourNumber.toString().split(".");
			if (components.length === 1) 
				components[0] = yourNumber;
			components[0] = components[0].replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
			if (components.length === 2)
				components[1] = components[1].replace(/\D/g, "");
			return components.join(".");
		}
		
		function numberWithCommas(x) {
			return x.toString().replace(/\B(?=(?:\d{3})+(?!\d))/g, ",");
		}
		//----------------------- end add input comma
		
		
    </script>
        
    @section('additional_scripts')
    @show
    
</body>
</html>