@extends('base')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
					<div class="login-logo">
                       <img src="../img/logo.png">
                    </div>
                    <div class="panel-heading">
                        <h3 class="panel-title">CARP MONITORING SYSTEM</h3>
                    </div>
                    <div class="panel-body">
                        {{ Form::open(array('url' => 'login')) }}
                            <fieldset>
							@if($message2 = Session::get('message2')) <span  class='error'>	{{ $message2['content'] }}</span>    @endif
                                <div class="form-group">
				    {{ Form::text('login_username', NULL , array('class' => 'form-control login', 'placeholder'=>'username', 'required' => 'required')) }}
							   </div>
							@if($message1 = Session::get('message1')) <span  class='error'>	{{ $message1['content'] }}</span>    @endif
                                <div class="form-group">
				    {{ Form::password('login_password', array('class' => 'form-control login', 'placeholder'=>'password', 'required' => 'required')) }}    
				</div>
                                {{ Form::submit('Login', array('class'=>'btn-login btn btn-lg btn-success btn-block')) }}
                            </fieldset>
                        {{ Form::close() }}
                    </div>
                </div>
            </div>
        </div>
    </div>	

@stop