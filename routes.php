<?php

Route::get('/', 'HomeController@index');

Route::post('login', 'HomeController@login');

Route::get('logout', 'HomeController@logout');

Route::get('checkPass/{pass}/{new_pass}',  'UsersController@checkPass');

Route::post('change_password',  'UsersController@changePass');



Route::group(array('before' => 'auth|is_nat_reg'), function()
{
	if(Session::get('access') == 1) Route::resource('national/users',  'UsersController');
	else if (Session::get('access') == 2) Route::resource('regional/users',  'UsersController');
	
	Route::get('check_username/{username}',  'UsersController@checkUsername');

	Route::post('deactivate_user/{id}', 'UsersController@deactivateUser');

	Route::post('activate_user/{id}', 'UsersController@activateUser');
	
	Route::post('users/reset_pass', 'UsersController@resetPass');
	
});

Route::group(array('prefix' => 'provincial', 'before' => 'auth|is_prov'), function()
{
	
	Route::get('home', 'ProvincialController@home');
	Route::resource('arcs',  'ArcController');
	Route::resource('msmes',  'MsmeController');
	Route::resource('msme_members', 'MsmeMemberController');
	Route::resource('igp', 'IgpController');
	Route::post('deactivate_igp/{id}', 'IgpController@deactivateIgp');
	Route::resource('jobs', 'JobController');
	Route::resource('igp_sales',  'IgpSalesController');
	Route::resource('entrepreneurs',  'EntrepreneurController');
	Route::resource('investments', 'InvestmentController');
	Route::resource('activities',  'ActivityController');
	Route::resource('attendances',  'AttendanceController');
	Route::get('inter_targets/{id}', 'TargetController@interTargets');
        Route::resource('targets',  'TargetController');
	Route::get('repairdropdown', 'ProvincialController@dropdown');
	Route::get('igp_dropdown', 'IgpController@igpDropdown');
	Route::get('accomplishments', 'ProvincialController@accomplishments');
	Route::post('accomplishments', 'ProvincialController@accomplishments');
	
        Route::group(array('prefix' => 'reports'), function()
        {
            Route::get('investments', 'ReportsController@investmentsGeneratedPage');
            Route::post('investments', 'ReportsController@investmentsGeneratedPage');
            Route::post('investments-xls', 'ReportsController@investmentsGeneratedXls');
            
            Route::get('sales', 'ReportsController@salesGeneratedPage');
            Route::post('sales', 'ReportsController@salesGeneratedPage');
            Route::post('sales-xls', 'ReportsController@salesGeneratedXls');
            Route::get('sales-test', 'ReportsController@salesGenerated');
            
            Route::get('trainings', 'ReportsController@sumOfTrainingsPage');
            Route::post('trainings', 'ReportsController@sumOfTrainingsPage');
            Route::post('trainings-xls', 'ReportsController@sumOfTrainingsXls');
			
            Route::get('entrepreneurs', 'ReportsController@entrepreneursDevelopedPage');
            Route::post('entrepreneurs', 'ReportsController@entrepreneursDevelopedPage');
            Route::post('entrepreneurs-xls', 'ReportsController@entrepreneursDevelopedXls');
            
            Route::get('summary-accomplishments', 'ReportsController@summaryOfAccompPage');
            Route::post('summary-accomplishments', 'ReportsController@summaryOfAccompPage');
            Route::post('summary-accomplishments-xls', 'ReportsController@summaryOfAccompXls');
            
            Route::get('accomplishment-arcs',  'ReportsController@accomplishmentPerArcPage');
            Route::post('accomplishment-arcs',  'ReportsController@accomplishmentPerArcPage');
			
	    Route::get('assisted-arcs', 'ReportsController@assistedArcsPage');
            Route::post('assisted-arcs', 'ReportsController@assistedArcsPage');
            Route::post('assisted-arcs-xls', 'ReportsController@assistedArcsXls');
			
			Route::get('trade_fairs', 'ReportsController@tradeFairsPage');
            Route::post('trade_fairs', 'ReportsController@tradeFairsPage');
            Route::post('trade_fairs-xls', 'ReportsController@tradeFairsXls');
            
            Route::get('gender-disaggregated', 'ReportsController@genderDisAgPage');
            Route::post('gender-disaggregated', 'ReportsController@genderDisAgPage');
            Route::post('gender-disaggregated-xls', 'ReportsController@genderDisAgXls');
			
			Route::get('jobs', 'ReportsController@jobsGeneratedPage');
            Route::post('jobs', 'ReportsController@jobsGeneratedPage');
            Route::post('jobs-xls', 'ReportsController@jobsGeneratedXls');
        });

});

Route::group(array('prefix' => 'regional', 'before' => 'auth|is_reg'), function()
{
	Route::get('home', 'RegionalController@home');
    Route::post('accomplishments', 'RegionalController@home'); 
        Route::resource('activities',  'ActivityController');
       
        Route::group(array('prefix' => 'reports'), function()
        {
            Route::get('investments', 'ReportsController@investmentsGeneratedPageRegional');
            Route::post('investments', 'ReportsController@investmentsGeneratedPageRegional');
            Route::post('investments-xls', 'ReportsController@investmentsGeneratedRegionalXls');
			
	    Route::get('trade_fairs', 'ReportsController@tradeFairsPage');
            Route::post('trade_fairs', 'ReportsController@tradeFairsPage');
            Route::post('trade_fairs-xls', 'ReportsController@tradeFairsXls');
			
	    Route::get('trainings', 'ReportsController@sumOfTrainingsPage');
            Route::post('trainings', 'ReportsController@sumOfTrainingsPage');
            Route::post('trainings-xls', 'ReportsController@sumOfTrainingsXls');
			
	    Route::get('entrepreneurs', 'ReportsController@entrepreneursDevelopedPage');
            Route::post('entrepreneurs', 'ReportsController@entrepreneursDevelopedPage');
            Route::post('entrepreneurs-xls', 'ReportsController@entrepreneursDevelopedXls');
            
            Route::get('summary-accomplishments', 'ReportsController@summaryOfAccompPageRegional');
            Route::post('summary-accomplishments', 'ReportsController@summaryOfAccompPageRegional');
            Route::post('summary-accomplishments-xls', 'ReportsController@summaryOfAccompRegionalXls');
            
            Route::get('sales', 'ReportsController@salesGeneratedPageRegional');
            Route::post('sales', 'ReportsController@salesGeneratedPageRegional');
	    Route::post('sales-xls', 'ReportsController@salesGeneratedRegionalXls');
            
	    Route::get('jobs', 'ReportsController@jobsGeneratedPage');
            Route::post('jobs', 'ReportsController@jobsGeneratedPage');
            Route::post('jobs-xls', 'ReportsController@jobsGeneratedXls');
            
            Route::get('gender-disaggregated', 'ReportsController@genderDisAgPageRegional');
            Route::post('gender-disaggregated', 'ReportsController@genderDisAgPageRegional');
            Route::post('gender-disaggregated-xls', 'ReportsController@genderDisAgRegionalXls');
            
            Route::get('assisted-arcs', 'ReportsController@assistedArcsPageRegional');
            Route::post('assisted-arcs', 'ReportsController@assistedArcsPageRegional');
            Route::post('assisted-arcs-xls', 'ReportsController@assistedArcsRegionalXls');
            
            Route::get('accomplishment-arcs',  'ReportsController@accomplishmentPerArcPage');
            Route::post('accomplishment-arcs',  'ReportsController@accomplishmentPerArcPage');
			
        });
        
	Route::post('submit-province', 'RegionController@addProvince');
});

Route::group(array('prefix' => 'national', 'before' => 'auth|is_nat'), function()
{
	Route::get('home', 'NationalController@home');
	Route::post('accomplishments', 'NationalController@home'); 
	Route::resource('provinces',  'ProvinceController');
	Route::resource('regions',  'RegionController');
	Route::resource('interventions',  'InterventionController');
	Route::post('submit-region', 'NationalController@addRegion');
	Route::get('province-dropdown', 'NationalController@get_json_provinces');
        
	Route::group(array('prefix' => 'reports'), function()
        {
            Route::get('investments', 'ReportsController@investmentsGeneratedPageRegional');
            Route::post('investments', 'ReportsController@investmentsGeneratedPageRegional');
            Route::post('investments-xls', 'ReportsController@investmentsGeneratedRegionalXls');
			
			Route::get('trade_fairs', 'ReportsController@tradeFairsPage');
            Route::post('trade_fairs', 'ReportsController@tradeFairsPage');
            Route::post('trade_fairs-xls', 'ReportsController@tradeFairsXls');
			
			Route::get('trainings', 'ReportsController@sumOfTrainingsPage');
            Route::post('trainings', 'ReportsController@sumOfTrainingsPage');
            Route::post('trainings-xls', 'ReportsController@sumOfTrainingsXls');
			
			Route::get('entrepreneurs', 'ReportsController@entrepreneursDevelopedPage');
            Route::post('entrepreneurs', 'ReportsController@entrepreneursDevelopedPage');
            Route::post('entrepreneurs-xls', 'ReportsController@entrepreneursDevelopedXls');
			
			Route::get('jobs', 'ReportsController@jobsGeneratedPage');
            Route::post('jobs', 'ReportsController@jobsGeneratedPage');
            Route::post('jobs-xls', 'ReportsController@jobsGeneratedXls');
            
            Route::get('sales', 'ReportsController@salesGeneratedPageRegional');
            Route::post('sales', 'ReportsController@salesGeneratedPageRegional');
            Route::post('sales-xls', 'ReportsController@salesGeneratedRegionalXls');
	    
	    Route::get('gender-disaggregated', 'ReportsController@genderDisAgPageRegional');
            Route::post('gender-disaggregated', 'ReportsController@genderDisAgPageRegional');
            Route::post('gender-disaggregated-xls', 'ReportsController@genderDisAgRegionalXls');
            
            Route::get('summary-accomplishments', 'ReportsController@summaryOfAccompPageRegional');
            Route::post('summary-accomplishments', 'ReportsController@summaryOfAccompPageRegional');
            Route::post('summary-accomplishments-xls', 'ReportsController@summaryOfAccompRegionalXls');
            
            Route::get('assisted-arcs', 'ReportsController@assistedArcsPageRegional');
            Route::post('assisted-arcs', 'ReportsController@assistedArcsPageRegional');
            Route::post('assisted-arcs-xls', 'ReportsController@assistedArcsRegionalXls');
            
            Route::get('accomplishment-arcs',  'ReportsController@accomplishmentPerArcPage');
            Route::post('accomplishment-arcs',  'ReportsController@accomplishmentPerArcPage');
        });
});
