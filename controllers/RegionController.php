<?php

class RegionController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $data['regions_dropdown'] = $this->get_reg_array();
            $data['provinces_dropdown'] = $this->get_prov_array();
			$data['regions'] = Region::all();
			$data['category_dropdown'] = $this->get_cat_array();
            return View::make('national/region', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('region_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('national/home')->withErrors($validator);
            }
            else
            {
                $new_reg = new Region;
                $new_reg->region = Input::get('region_name');
                $new_reg->created_by = $this->user->id;
                $new_reg->save();
                return Redirect::to('national/regions')->with('message',
                                                            array('type'=>'info', 'content'=>'New Region was added.'));
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	    return Region::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	    $validator = Validator::make(Input::all(), array('edit_reg_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('national/regions')->withErrors($validator);
            }
            else
            {
                $region = Region::find($id);
                $old_name = $region->region;
                $region->region = Input::get('edit_reg_name');
                $region->updated_by = $this->user->id;
                $region->save();
                return Redirect::to('national/regions')->with('message',
                                                            array('type'=>'info', 'content'=> $old_name.' was changed to '.$region->region));
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    $region = Region::find($id);
	    $name =  $region->region;
	    $region->delete();
	    return Redirect::to('national/regions')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
	}


}
