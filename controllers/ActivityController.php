<?php

class ActivityController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
			$data['arc_dropdown'] = $this->get_arc_array();
            $data['inter_dropdown'] = $this->get_inter_array();
			$data['cat_dropdown'] = $this->get_cat_array();
            $data['msme_dropdown'] = $this->get_msme_array();
			
            if(Session::get('access') == 2)
            {
				foreach($data['inter_dropdown'] as $key => $value) 
				{ 
					$data['activities'][$key] = Activity::where('region_id', '=', $this->user->region_id)
										->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
										->where('interventions.id', '=', $key)
										->orderBy('date_constructed', 'DESC')
										->select('activities.*', 'interventions.intervention', 'interventions.w_sales')->get();
												$data['regions_dropdown'] = $this->get_reg_array();
												$data['provinces_dropdown'] = $this->get_prov_array();
					$data['act_count'][$key] = count($data['activities'][$key]);
				}
				
				return View::make('regional/activity', $data);
            }
            else
            {
				foreach($data['inter_dropdown'] as $key => $value) 
				{  
					   $data['activities'][$key] = Activity::where('province_id', '=', $this->user->province_id)
										->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
										->where('interventions.id', '=', $key)
										->orderBy('date_constructed', 'DESC')
										->select('activities.*', 'interventions.intervention', 'interventions.w_sales', 'interventions.inter_category_id' )
										->orderBy('activities.intervention_id')->get();
										
						$data['act_count'][$key] = count($data['activities'][$key]);	
				}		
				
                return View::make('provincial/activity', $data);
            }
        //group by intervention
	}

	
	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('act_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/activities')->withErrors($validator);
            }
            else
            {
                $activity = new Activity;
                $activity->name = Input::get('act_name');
                $activity->intervention_id = Input::get('act_inter');
                $activity->date_constructed = date_format(date_create(Input::get('date_constructed')),'Y-m-d'); 
                $activity->date_finished = date_format(date_create(Input::get('date_finished')),'Y-m-d'); 
                $activity->venue = Input::get('venue');
                if(Input::get('manmonths')) $activity->man_months = Input::get('manmonths');
                if(Session::get('access') == 2)  $activity->region_id = $this->user->region_id;
                else $activity->province_id = $this->user->province_id;
                $activity->province_id = $this->user->province_id;
                $activity->created_by = $this->user->id;
                $activity->save();
				
				Session::put('tab_activity', Input::get('act_inter'));
				$path = Input::get('path');
				
                if(Session::get('access') == 2)
                {	
                    return Redirect::to('regional/activities')->with('message',
                                                            array('type'=>'info', 'content'=>'New Activity was added.'));
                }
                else
                {
					if ($path == 'provincial/accomplishments')
					{
						 return Redirect::to($path)->with('message',
                                                            array('type'=>'info', 'content'=>'New Activity was added.'));
					}
					else
					{
						 return Redirect::to('provincial/activities')->with('message',
                                                            array('type'=>'info', 'content'=>'New Activity was added.'));
					}
                }
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id) //get activity attendances
	{
	    $activity = Activity::find($id);
            return $activity->attendances()->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')->select('attendances.*', 'msmes.msme_name')->get();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		return $activity = Activity::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$validator = Validator::make(Input::all(), array('edit_act_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/activities')->withErrors($validator);
            }
            else
            {
              
				$activity = Activity::find($id);
                $old_name =  $activity->name;
                $activity->name = Input::get('edit_act_name');
                $activity->intervention_id = Input::get('edit_act_inter');
                $activity->date_constructed = date_format(date_create(Input::get('edit_date_constructed')),'Y-m-d'); 
                $activity->date_finished = date_format(date_create(Input::get('edit_date_finished')),'Y-m-d'); 
                $activity->venue = Input::get('edit_venue');
                if(Input::get('edit_manmonths')) $activity->man_months = Input::get('edit_manmonths');
                $activity->updated_by = $this->user->id;
                $activity->save();
				
				Session::put('tab_activity', Input::get('edit_act_inter'));
				
                if(Session::get('access') == 2)
                {
                    return Redirect::to('regional/activities')->with('message',
                                                            array('type'=>'info', 'content'=>$old_name.' was updated.'));
                }
                else
                {
                    return Redirect::to('provincial/activities')->with('message',
                                                            array('type'=>'info', 'content'=>$old_name.' was added.'));
                }
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$act = Activity::find($id);
	    $name =  $act->name;
	    $act->delete();
		
		Session::put('tab_activity', Input::get('act_inter'));
	    return Redirect::to('provincial/activities')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted from Activity Library'));
	}


}
