<?php

class NationalController extends BaseController {

	public function home()
	{
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		$data['category_dropdown'] = $this->get_cat_array();
		$data['years_dropdown'] = $this->get_years_dropdown();
		
		Session::put('region', $this->user->region['region']);
		
		$year = (int) date('Y');
		$region = 0;
		if (Request::has('year')) $year = Input::get('year');
		
		
		$data['provinces'] = Province::where('region_id', '=', $this->user->region_id)
									->lists('province','id'); 
		$data['province_count'] = count($data['provinces']);
		
		$data['targets'] = Target::where('year', '=', $year)
					->leftJoin('provinces', 'provinces.id', '=', 'targets.province_id')
					->where('region_id', '=', $this->user->region_id)
					->get(); 
		
		$id = Target::where('year', '=', $year)->pluck('id');
		$data['inter_targets'] = InterTarget::where('target_id', '=', $id)->get();  
		
		
		if (Request::has('region')) 
		{
			$region = Input::get('region'); 
			$results = $this->summaryOfAccompRegional($region);
			
			//$data['interventions'] = $interventions;
			$data['annual'] = $results['annual'];
			$data['current'] = $results['current'];
			$data['todate'] = $results['todate'];
			$data['percentage'] = $results['percentage'];
		}
		
		//return $results['annual'];
		Input::flash();
		return View::make('national/accomplishments', $data)->with('input', Input::old());

	}
        
        public function addRegion()
        {
            $validator = Validator::make(Input::all(), array('region_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('national/home')->withErrors($validator);
            }
            else
            {
                $new_reg = new Region;
                $new_reg->region = Input::get('region_name');
                $new_reg->created_by = $this->user->id;
                $new_reg->save();
                return Redirect::to('national/home')->with('message',
                                                            array('type'=>'info', 'content'=>'New Region was added.'));
            }
        }
	
	public function get_json_provinces()
	{
		$region_id = Input::get('option');
		return Province::where('region_id', '=', $region_id)->lists('id','province');
	}
}
