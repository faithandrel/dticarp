<?php

class InterventionController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $data['regions_dropdown'] = $this->get_reg_array();
            $data['provinces_dropdown'] = $this->get_prov_array();
            $data['category_dropdown'] = $this->get_cat_array();
	    $data['interventions'] = Intervention::all();
            return View::make('national/intervention', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('inter_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('national/interventions')->withErrors($validator);
            }
            else
            {
                $intervention = new Intervention;
                $intervention->intervention = Input::get('inter_name');
                if(Input::get('w_sales')) $intervention->w_sales = Input::get('w_sales');
                $intervention->inter_category_id = Input::get('category');
                $intervention->created_by = $this->user->id;
                $intervention->save();
                return Redirect::to('national/interventions')->with('message',
                                                            array('type'=>'info', 'content'=>'New Intervention was added.'));
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	    return Intervention::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	    $validator = Validator::make(Input::all(), array('edit_inter_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('national/interventions')->withErrors($validator);
            }
            else
            {
                $intervention = Intervention::find($id);
                $intervention->intervention = Input::get('edit_inter_name');
                if(Input::get('edit_w_sales')) $intervention->w_sales = Input::get('edit_w_sales');
                $intervention->inter_category_id = Input::get('edit_category');
                $intervention->save();
                return Redirect::to('national/interventions')->with('message',
                                                            array('type'=>'info', 'content'=>'Intervention was updated.'));
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
