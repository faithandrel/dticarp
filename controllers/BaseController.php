<?php

class BaseController extends Controller {

	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	
	protected $user = FALSE;
	
	public function __construct()
	{
		$this->user = User::whereUsername(Session::get('username'))->first();
	}
	
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}
	
	protected function get_prov_array()
	{
		$provinces_dropdown = array();
		
		if(Session::get('access') == 2)
		{
			$user_region = Region::find($this->user->region_id);
			$provinces = $user_region->provinces()->get();
		}
		else
		{
			
			$provinces = Province::all();
		}
		
		$provinces_dropdown[0] = '';
		
		foreach ($provinces as $province)
		{
			
			$provinces_dropdown[$province->id] = $province->province;
		}
		
		return $provinces_dropdown;
	}
	
	protected function get_reg_array()
	{
		$regions_dropdown = array();
		
		if(Session::get('access') == 2)
		{
			$user_region = Region::find($this->user->region_id);
			$regions = Region::where('id', '=', $this->user->region_id)->take(1)->get();
		}
		else
		{
			$regions = Region::all();
			$regions_dropdown[0] = '';
		}
		
		foreach ($regions as $region)
		{
			$regions_dropdown[$region->id] = $region->region;
		}
		
		return $regions_dropdown;
	}
	
	protected function get_arc_array()
	{
		$arc_dropdown = array();
		$arcs = Arc::where('province_id', '=', $this->user->province_id)->get();
		$arc_dropdown['blank'] = '';
		$arc_dropdown[0] = 'Non-ARC';
		foreach ($arcs as $arc)
		{
			$arc_dropdown[$arc->id] = $arc->name;
		}
		
		
		return $arc_dropdown;
	}
	
	protected function get_inter_array()
	{
		$inter_dropdown = array();
		$interventions = Intervention::all();
		
		foreach ($interventions as $intervention)
		{
			$inter_dropdown[$intervention->id] = $intervention->intervention;
		}
		return $inter_dropdown;
	}
	
	protected function get_msme_array()
	{
		$msme_dropdown = array();
		$msmes = Msme::where('province_id', '=', $this->user->province_id)->get();
		$msme_dropdown[''] = '';
		foreach ($msmes as $msme)
		{
			$msme_dropdown[$msme->id] = $msme->msme_name;
		}
		return $msme_dropdown;
	}
	
	protected function get_cat_array()
	{
		$cat_dropdown = array();
		$categories = Category::all();
		
		foreach ($categories as $category)
		{
			$cat_dropdown[$category->id] = $category->name;
		}
		return $cat_dropdown;
	}
	
	protected function get_months_dropdown()
	{
		$calendar = cal_info(0);
		return $calendar['months'];
	}
	
	protected function get_years_dropdown()
	{
		$years = array();
		$year_today = (int) date('Y');
		for($i=$year_today-10; $i < $year_today+10; $i++)
		{
			$years[$i] = $i;
		}
		return $years;
	}
	
	protected function get_months_quarter($q)
	{
		switch ($q) {
		case 1:
			$quarter['start_month'] = 1;
			$quarter['end_month'] = 3;
			return $quarter;
		case 2:
			$quarter['start_month'] = 4;
			$quarter['end_month'] = 6;
			return $quarter;
		case 3:
			$quarter['start_month'] = 7;
			$quarter['end_month'] = 9;
			return $quarter;
		case 4:
			$quarter['start_month'] = 10;
			$quarter['end_month'] = 12;
			return $quarter;
		}
	}
	protected function get_report_period()
	{
		if(Input::get('report_start_month') == Input::get('report_end_month'))
		{
			return DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year');
		}		
		else {
			return DateTime::createFromFormat('!m', Input::get('report_start_month'))->format('F')." ".Input::get('report_start_year').' - '.DateTime::createFromFormat('!m', Input::get('report_end_month'))->format('F')." ".Input::get('report_end_year');
		}
	}
	protected function get_quarter_dropdown()
	{
		$quarter_dropdown[1] = '1st Quarter';
		$quarter_dropdown[2] = '2nd Quarter';
		$quarter_dropdown[3] = '3rd Quarter';
		$quarter_dropdown[4] = '4th Quarter';
		return $quarter_dropdown;
	}
	
	protected function summaryOfAccomp($province_id)
	{
		$year = (int) date('Y');
		if (Request::has('year')) $year = Input::get('year');
		
		if (Input::has('report_start_month'))
		{
			$start_date = $year.'-'.Input::get('report_start_month').'-1';
			$end_date = $year.'-'.Input::get('report_end_month').'-31';
		}
		else
		{
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$start_date = $year.'-'. $quarter['start_month'].'-1';
			$end_date = $year.'-'.$quarter['end_month'].'-31';
			Input::merge(array('report_start_month' => $quarter['start_month'] , 'year' => $year, 'report_end_month' => $quarter['end_month']));
		}
		
		$data['interventions'] = Intervention::all();

		
		$data['target'] =  Target::where('year', '=', $year)->where('province_id', '=', $province_id)->first();
		if($data['target'] ) //if annual target is set
		{
			//initialize data
			$id = $data['target']->id;
			$data['inter_targets'] = array();
			$data['annual'] = $data['target']->toArray();
			$data['current'] = array('sales'=>(double)0, 'established'=>(double)0,'monitored'=>(double)0,'interattns'=>array());
			$data['todate'] = array('sales'=>(double)0, 'established'=>(double)0,'monitored'=>(double)0,'interattns'=>array());
			$data['percentage'] = array('sales'=>(double)0, 'established'=>(double)0,'monitored'=>(double)0,'interattns'=>array());
			$data['investments'] = (double) 0;
			
			
			$intertargets = InterTarget::where('target_id', '=', $id)->get();  
			$start_date = $year.'-'.Input::get('report_start_month').'-1';
			$end_date = $year.'-'.Input::get('report_end_month').'-31';
			
			$interattns = array();
			$interattns_todate = array();
			foreach ($data['interventions'] as $intervention) //get attendances of each intervention
			{
				$fb_male = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('fb_male');
				$fb_female = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('fb_female');
				$lo_male = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('lo_male');
				$lo_female = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('lo_female');
								
				$arcs = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('arcs.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
							->select('activities.date_finished', 'activities.name', 'arcs.name as arc_name', 'msmes.msme_name')
							->groupBy('msmes.arc_id')->get();
				
				$inter_wattn = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->groupBy('activities.id')->get();
							
				$interattns[$intervention->id]['count'] = $inter_wattn->count();
				
				if($intervention->w_sales)
				{
					$interattns[$intervention->id]['sales'] = Attendance::where('activities.intervention_id', '=', $intervention->id)
								->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->sum('sales');
					$data['current']['sales'] += (double) $interattns[$intervention->id]['sales'];
				}
				$interattns[$intervention->id]['fbs'] =  (int) $fb_male + $fb_female;
				$interattns[$intervention->id]['los'] =  (int) $lo_male + $lo_female;
				$interattns[$intervention->id]['arcs'] =  (int) $arcs->count();
				
				//accomp to date
				$fb_male_todate = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('fb_male');
				$fb_female_todate = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('fb_female');
				$lo_male_todate = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('lo_male');
				$lo_female_todate = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->sum('lo_female');
				$arcs_todate = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('arcs.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
							->select('activities.date_finished', 'activities.name', 'arcs.name as arc_name', 'msmes.msme_name')
							->groupBy('msmes.arc_id')->get();
				
				$inter_wattn_todate = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->groupBy('activities.id')->get();
							
				$interattns_todate[$intervention->id]['count'] = $inter_wattn_todate->count();
				if($intervention->w_sales)
				{
					$interattns_todate[$intervention->id]['sales'] = Attendance::where('activities.intervention_id', '=', $intervention->id)
								->where('msmes.province_id', '=', $province_id)
								->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->sum('sales');
					$data['todate']['sales'] += (double) $interattns_todate[$intervention->id]['sales'];
				}
				$interattns_todate[$intervention->id]['fbs'] =  (int) $fb_male_todate + $fb_female_todate;
				$interattns_todate[$intervention->id]['los'] =  (int) $lo_male_todate + $lo_female_todate;
				$interattns_todate[$intervention->id]['arcs'] =  (int) $arcs_todate->count();
				
				if($intervention->id == 13) //if consultancy get man-months
				{
					$interattns[$intervention->id]['manmonths'] = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
											->whereBetween('activities.date_finished', array($start_date, $end_date))
											->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
											->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
											->distinct('attendances.activity_id')->sum('activities.man_months');
					$interattns_todate[$intervention->id]['manmonths'] = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
											->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
											->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
											->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
											->distinct('attendances.activity_id')->sum('activities.man_months');
				}
			}
			
			$data['current']['interattns'] = $interattns;
			$data['todate']['interattns'] = $interattns_todate;
			$data['inter_targets'] = $intertargets;
			
			//get sales
			$data['current']['monitored'] = IgpSales::where('msmes.province_id', '=', $province_id)->whereBetween('igp_sales.report_date', array($start_date, $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'igp_sales.msme_id')
								->sum('igp_sales.monitored');
			$data['current']['established'] = IgpSales::where('msmes.province_id', '=', $province_id)->whereBetween('igp_sales.report_date', array($start_date, $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'igp_sales.msme_id')
								->sum('igp_sales.established');
			$data['current']['sales'] += $data['current']['monitored']  + $data['current']['established'];
			
			//get sales to date
			$data['todate']['monitored'] = IgpSales::where('msmes.province_id', '=', $province_id)->whereBetween('igp_sales.report_date', array($year.'-1-1', $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'igp_sales.msme_id')
								->sum('igp_sales.monitored');
			$data['todate']['established'] = IgpSales::where('msmes.province_id', '=', $province_id)->whereBetween('igp_sales.report_date', array($year.'-1-1', $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'igp_sales.msme_id')
								->sum('igp_sales.established');
			$data['todate']['sales'] += $data['todate']['monitored'] + $data['todate']['established'];
			
			//get investments
			$data['current']['investments'] = Investment::where('msmes.province_id', '=', $province_id)->whereBetween('investments.date_monitored', array($start_date, $end_date))
								->where('igprojects.date_started', '<=',  $end_date )
								->whereBetween('igprojects.date_finished', array($start_date,'2999-01-01'))
								->leftJoin('igprojects', 'igprojects.id', '=', 'investments.igp_id')
								->leftJoin('msmes', 'msmes.id', '=', 'igprojects.msme_id')
								->sum('investments.investment_amount');
			$data['todate']['investments'] = Investment::where('msmes.province_id', '=', $province_id)->whereBetween('investments.date_monitored', array($year.'-1-1', $end_date))
								->where('igprojects.date_started', '<=',  $end_date )
								->whereBetween('igprojects.date_finished', array($year.'-1-1','2999-01-01'))
								->leftJoin('igprojects', 'igprojects.id', '=', 'investments.igp_id')
								->leftJoin('msmes', 'msmes.id', '=', 'igprojects.msme_id')
								->sum('investments.investment_amount');
								
			//ARCs assisted
			$arcs_assisted_current =  Attendance::where('arcs.province_id', '=', $province_id)
							->where('msmes.arc_id', '<>', 0)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
							->select('activities.date_finished', 'activities.name', 'arcs.name as arc_name', 'msmes.msme_name')
							->groupBy('msmes.arc_id')->get();
			$arcs_assisted_todate =  Attendance::where('arcs.province_id', '=', $province_id)
							->where('msmes.arc_id', '<>', 0)
							->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
							->select('activities.date_finished', 'activities.name', 'arcs.name as arc_name', 'msmes.msme_name')
							->groupBy('msmes.arc_id')->get();
			$data['current']['arcs_assisted'] = $arcs_assisted_current->count();
			$data['todate']['arcs_assisted'] = $arcs_assisted_todate->count();
			
			//Non-ARCs assisted
			$nonarcs_assisted_current =  Attendance::where('msmes.province_id', '=', $province_id)->where('msmes.arc_id', '=', 0)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->select('activities.date_finished', 'activities.name', 'msmes.msme_name')
							->groupBy('attendances.msme_id')->get();
			$nonarcs_assisted_todate =  Attendance::where('msmes.province_id', '=', $province_id)->where('msmes.arc_id', '=', 0)
							->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->select('activities.date_finished', 'activities.name', 'msmes.msme_name')
							->groupBy('attendances.msme_id')->get();
			$data['current']['non_arcs_assisted'] = $nonarcs_assisted_current->count();
			$data['todate']['non_arcs_assisted'] = $nonarcs_assisted_todate->count();
			$data['non_arc'] = $nonarcs_assisted_current;
			
			//entrep developed
			$data['current']['entrepreneurs'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
								->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->count();
			$data['todate']['entrepreneurs'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
								->whereBetween('entrepreneurs.date_developed', array($year.'-1-1', $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->count();
							
			//MSMEs developed
			$data['current']['msmes_dev'] = $data['current']['entrepreneurs'];
			$data['todate']['msmes_dev'] = $data['todate']['entrepreneurs'];
			
			//MSMEs dev ARCs served
			$data['current']['msmes_dev_arcs'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
								->where('msmes.arc_id', '<>', 0)
								->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')
								->distinct('msmes.arc_id')->count();
			$data['todate']['msmes_dev_arcs'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
								->where('msmes.arc_id', '<>', 0)
								->whereBetween('entrepreneurs.date_developed', array($year.'-1-1', $end_date))
								->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')
								->distinct('msmes.arc_id')->count();
								
			
			$msmes_fb_m_current = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->sum('fb_male');
			$msmes_fb_f_current = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->sum('fb_female');
			$msmes_fb_m_todate = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($year.'-1-1', $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->sum('fb_male');
			$msmes_fb_f_todate = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($year.'-1-1', $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->sum('fb_female');
			
			$data['current']['msmes_dev_fb'] = $msmes_fb_m_current + $msmes_fb_f_current;
			$data['todate']['msmes_dev_fb'] = $msmes_fb_m_todate + $msmes_fb_f_todate;
			
			
			//MSMEs Assisted
			$msmes_assisted_current =  Attendance::where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->select('activities.date_finished', 'activities.name', 'msmes.msme_name', 'msmes.id as msme_id')
							->groupBy('attendances.msme_id')->get();
			$msmes_assisted_todate =  Attendance::where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->select('activities.date_finished', 'activities.name', 'msmes.msme_name', 'msmes.id as msme_id')
							->groupBy('attendances.msme_id')->get();
			$data['current']['msmes_asst'] = $msmes_assisted_current->count();
			$data['todate']['msmes_asst'] = $msmes_assisted_todate->count();
			
			$data['current']['msmes_asst_arcs'] = $arcs_assisted_current->count();
			$data['todate']['msmes_asst_arcs'] = $arcs_assisted_todate->count();
			
			//MSMEs Assisted FBs and LOs
			
			$data['current']['msmes_asst_fb']= 0;
			$data['current']['msmes_asst_lo']= 0;
			$data['todate']['msmes_asst_fb']= 0;
			$data['todate']['msmes_asst_lo']= 0;
			
			$data['current']['jobs']= 0;
			$data['todate']['jobs']= 0;
			
			foreach($msmes_assisted_current as $attn)
			{
				$current_fb_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($start_date, $end_date))
								->orderBy('report_date', 'desc')->pluck('fb_male');
				if($current_fb_male == 0) //if members log wasn't edited in the given dates, get latest
				{
					$current_fb_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->orderBy('report_date', 'desc')->pluck('fb_male');
				}
				$data['current']['msmes_asst_fb'] += $current_fb_male;
				
				$current_fb_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($start_date, $end_date))
								->orderBy('report_date', 'desc')->pluck('fb_female');
				if($current_fb_female == 0)
				{
					$current_fb_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->orderBy('report_date', 'desc')->pluck('fb_female');
				}
				$data['current']['msmes_asst_fb'] += $current_fb_female;
				
				$current_lo_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($start_date, $end_date))
								->orderBy('report_date', 'desc')->pluck('lo_male');
				if($current_lo_male == 0)
				{
					$current_lo_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->orderBy('report_date', 'desc')->pluck('lo_male');
				}
				$data['current']['msmes_asst_lo'] += $current_lo_male;
				
				
				$current_lo_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($start_date, $end_date))
								->orderBy('report_date', 'desc')->pluck('lo_female');
				if($current_lo_female == 0)
				{
					$current_lo_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
								->orderBy('report_date', 'desc')->pluck('lo_female');
				}
				$data['current']['msmes_asst_lo'] += $current_lo_female;
				
				$data['current']['jobs']  += Job::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($start_date, $end_date))
								->orderBy('report_date', 'desc')->pluck('number');
			}
			
			foreach($msmes_assisted_todate as $attn)
			{
				$data['todate']['msmes_asst_fb']  += MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($year.'-1-1', $end_date))
								->orderBy('report_date', 'desc')->pluck('fb_male');
				$data['todate']['msmes_asst_fb']  += MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($year.'-1-1', $end_date))
								->orderBy('report_date', 'desc')->pluck('fb_female');
				$data['todate']['msmes_asst_lo']  += MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($year.'-1-1', $end_date))
								->orderBy('report_date', 'desc')->pluck('lo_male');
				$data['todate']['msmes_asst_lo']  += MsmeMember::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($year.'-1-1', $end_date))
								->orderBy('report_date', 'desc')->pluck('lo_female');
								
				$data['todate']['jobs']  += Job::where('msme_id', '=', $attn->msme_id)
								->whereBetween('report_date', array($year.'-1-1', $end_date))
								->orderBy('report_date', 'desc')->pluck('number');
			}
			
								
			//calculate percentage - this must be after annual/target, current, todate are queried
			
			$data['percentage'] = array();
			$data['percentage']['interattns'] = array();
			$data['annual']['interattns'][13]['manmonths'] = $data['annual']['man_months'];
			
			foreach($intertargets as $intertarget)
			{
				$data['annual']['interattns'][$intertarget->intervention_id]['count'] = $intertarget->number;
				$data['annual']['interattns'][$intertarget->intervention_id]['fbs'] = $intertarget->fbs;
				$data['annual']['interattns'][$intertarget->intervention_id]['los'] = $intertarget->los;
				$data['annual']['interattns'][$intertarget->intervention_id]['sales'] = $intertarget->sales;
				$data['annual']['interattns'][$intertarget->intervention_id]['arcs'] = $intertarget->arcs;
			}
			
			foreach ($data['todate'] as $field => $value)
			{
				if(array_key_exists($field, $data['annual']) AND !is_array($value))
				{
					if($data['annual'][$field]) $data['percentage'][$field] = round( ($value/$data['annual'][$field])*100 );
					else $data['percentage'][$field] = 0;
				}
			}
			
			foreach ($data['todate']['interattns'] as $field => $value_array)
			{
				foreach ($value_array as $key => $value)
				{
					if($data['annual']['interattns'][$field][$key])
						$data['percentage']['interattns'][$field][$key] = round( ($value/$data['annual']['interattns'][$field][$key])*100 );
					else
						$data['percentage']['interattns'][$field][$key] = 0;
				}
			}
			
			return $data;
		}
		else return false;
	}
	
	protected function summaryOfAccompRegional($region_id)
	{
		$year = (int) date('Y');
		if (Request::has('year')) $year = Input::get('year');
		
		$provinces = Province::where('region_id', '=', $region_id)
					->lists('province','id');
									
		$target_count = 0;
		foreach($provinces as $id => $province)
		{
			$target = Target::where('year', '=', $year)->where('province_id', '=', $id)->first();
			if(!$target_count AND $target) //first target exists
			{
				$target_count++;
				$results = $this->summaryOfAccomp($id);
				$results['provinces'][$province] = $results['current'];
			}
			else if($new_target = Target::where('year', '=', $year)->where('province_id', '=', $id)->first())
			{
				$target_count++;
				$new_result = $this->summaryOfAccomp($id);
				$results['provinces'][$province] = $new_result['current'];
				//add up target, totals for this quarter, up to date
				foreach( $new_result['annual'] as $field => $value )
				{
					if(is_array($value))
					{
						foreach($value as $inter => $value_array)
						{
							//echo "<br/>hey:".$inter." ".var_dump($indicator);
							foreach ($value_array as $key => $number)
							{
								$results['annual']['interattns'][$inter][$key] += $number;
							}
						}
					}
					else
					{
						$results['annual'][$field] += $value;
					}
				}
				foreach( $new_result['current'] as $field => $value )
				{
					if(is_array($value))
					{
						foreach($value as $inter => $value_array)
						{
							//echo "<br/>hey:".$inter." ".var_dump($indicator);
							foreach ($value_array as $key => $number)
							{
								$results['current']['interattns'][$inter][$key] += $number;
							}
						}
					}
					else
					{
						$results['current'][$field] += $value;
					}
				}
				foreach( $new_result['todate'] as $field => $value )
				{
					if(is_array($value))
					{
						foreach($value as $inter => $value_array)
						{
							//echo "<br/>hey:".$inter." ".var_dump($indicator);
							foreach ($value_array as $key => $number)
							{
								$results['todate']['interattns'][$inter][$key] += $number;
							}
						}
					}
					else
					{
						$results['todate'][$field] += $value;
					}
				}
			}
		}
		
		//correct intervention count
		
		$quarter = $this->get_months_quarter(ceil(date('m')/3));
		$start_date = $year.'-'. $quarter['start_month'].'-1';
		$end_date = $year.'-'.$quarter['end_month'].'-31';
		$interventions = Intervention::lists('id');
		foreach($interventions as $intervention)
		{
			$attn_quarter = Attendance::where('activities.intervention_id', '=', $intervention )->where('provinces.region_id', '=', $region_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
							->groupBy('activities.id')->get();
			$results['current']['interattns'][$intervention]['count'] = $attn_quarter->count();
			$attn_todate = Attendance::where('activities.intervention_id', '=', $intervention )->where('provinces.region_id', '=', $region_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
							->groupBy('activities.id')->get();
			$results['todate']['interattns'][$intervention]['count'] = $attn_todate->count();
			
			if($intervention == 13) //if consultancy get man-months
			{
				$results['current']['interattns'][$intervention]['manmonths'] =  Attendance::where('activities.intervention_id', '=', $intervention)->where('provinces.region_id', '=', $region_id)
										->whereBetween('activities.date_finished', array($start_date, $end_date))
										->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
										->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
										->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
										->distinct('attendances.activity_id')->sum('activities.man_months');
				$results['todate']['interattns'][$intervention]['manmonths'] = Attendance::where('activities.intervention_id', '=', $intervention)->where('provinces.region_id', '=', $region_id)
										->whereBetween('activities.date_finished', array($year.'-1-1', $end_date))
										->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
										->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
										->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
										->distinct('attendances.activity_id')->sum('activities.man_months');
			}
		}
		//calculate regional percentage
			
		$results['percentage'] = array();
		$results['percentage']['interattns'] = array();
		
		
		$results['annual']['interattns'][13]['manmonths'] = $results['annual']['man_months'];
		 
	
		foreach ($results['todate'] as $field => $value)
		{
			if(array_key_exists($field, $results['annual']) AND !is_array($value))
			{
				if($results['annual'][$field]) $results['percentage'][$field] = round( ($value/$results['annual'][$field])*100 );
				else $results['percentage'][$field] = 0;
			}
		}
		
		foreach ($results['todate']['interattns'] as $field => $value_array)
		{
			foreach ($value_array as $key => $value)
			{
				if($results['annual']['interattns'][$field][$key])
					$results['percentage']['interattns'][$field][$key] = round( ($value/$results['annual']['interattns'][$field][$key])*100 );
				else
					$results['percentage']['interattns'][$field][$key] = 0;
			}
		}
		
		return $results;
	}
}
