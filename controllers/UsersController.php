<?php

class UsersController extends BaseController {
	
	public function index()
	{
		if(Session::get('access') == 2)
		{
			$data['users'] = User::where('region_id', '=', $this->user->region_id)
					->where('access_level', '=', 3)->get();
		}
		else if(Session::get('access') == 1)
		{
			$data['users'] = User::all();
		}
		
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['category_dropdown'] = $this->get_cat_array();
		
		if(Session::get('access') == 2)
		{
			return View::make('regional/user', $data);
		}
		else if(Session::get('access') == 1)
		{
			return View::make('national/user', $data);
		}
	}
	
	
	public function store()
	{
		$user = new User;
		$user->username = Input::get('username');
	  //  return Input::get('pass_admin');
		$user->password = Hash::make(Input::get('pass_admin'));
		$user->status = Input::get('status');
		
		if (Input::get('status') == NULL) $user->status = 0;
		$user->account_name = Input::get('name');
		if(Session::get('access') == 2)
		{
			$user->access_level = 3;
			$user->region_id = $this->user->region_id;
			if (!empty(Input::get('province'))) $user->province_id = Input::get('province');
			else return Redirect::to('regional/users')->with('message',
                                                array('type'=>'danger', 'content'=> 'Please choose a province for the user.'));
			
		}
		else
		{
			$user->access_level = Input::get('access');
			if ($user->access_level == 3)
			{
				if (!empty(Input::get('province')))
				{
					$user->province_id = Input::get('province');
					$user->region_id = Province::where('id', '=', $user->province_id)->pluck('region_id');
				}
				else return Redirect::to('national/users')->with('message',
							array('type'=>'danger', 'content'=> 'Please choose a province for the user.'));
				
			}
			else
			{
				if (!empty(Input::get('region'))) $user->region_id = Input::get('region');
				else return Redirect::to('national/users')->with('message',
							array('type'=>'danger', 'content'=> 'Please choose a region for the user.'));
			}
		}
		
		
		$user->save();
		
		if(Session::get('access') == 2)
		{
			return Redirect::to('regional/users')->with('message',
                                                            array('type'=>'info', 'content'=> $user->username.' was added.'));
		}
		else if(Session::get('access') == 1)
		{
			return Redirect::to('national/users')->with('message',
                                                            array('type'=>'info', 'content'=> $user->username.' was added.'));
		}
		
	}
	
	public function checkUsername($username)
	{
		return $count = count(User::whereUsername($username)->get()); 
	}
	
	public function update($id)
	{
		$user = User::find($id);
		$user->username = Input::get('edit_username');
		if(Input::get('edit_password')) $user->password = Hash::make(Input::get('edit_password'));
		$user->status = Input::get('edit_status');
		$user->account_name = Input::get('edit_name');
		$user->province_id = Input::get('edit_province');
		
		if(Session::get('access') == 2)
		{
			$user->access_level = 3;
			$province = Province::find($user->province_id);
			$user->region_id = $province->region()->pluck('id');
			
		}
		else
		{
			$user->access_level = Input::get('edit_access');
			if($user->access_level == 3)
			{
				$province = Province::find($user->province_id);
				$user->region_id = $province->region()->pluck('id');
			}
			else $user->region_id = Input::get('edit_region');
		}
		
		$user->save();
		
		if(Session::get('access') == 2)
		{
			return Redirect::to('regional/users')->with('message',
                                                            array('type'=>'info', 'content'=> $user->username.' was updated.'));
		}
		else if(Session::get('access') == 1)
		{
			return Redirect::to('national/users')->with('message',
                                                            array('type'=>'info', 'content'=> $user->username.' was updated.'));
		}
	}
	
	public function show($id)
	{
	    return User::find($id);
	}
	
	public function checkPass($pass, $newpass)
	{
		if (Auth::attempt(array('username' => Session::get('username'), 'password' => $pass)))
		{
			if (Hash::check($newpass, Hash::make($pass)))
			{
				return 2;
			}
			else return 1;
		}
		
		else return 0;
	}
	
	public function changePass()
	{
		$id = User::whereUsername(Session::get('username'))->pluck('id');
		$user = User::find($id);
		$user->password = Hash::make(Input::get('confirm_pass'));
		$user->save();
		
		return Redirect::to('provincial/home')->with('message',
                                                     array('type'=>'warning', 'content'=> 'Your password was updated.'));
	}
	
	public function deactivateUser($id)
	{
		$user = User::find($id);
		$user->status = 0;
		$user->save();
		
		if(Session::get('access') == 2)
		{
			return Redirect::to('regional/users')->with('message',
                                                            array('type'=>'info', 'content'=> 'User was deactivated.'));
		}
		else if(Session::get('access') == 1)
		{
			return Redirect::to('national/users')->with('message',
                                                            array('type'=>'info', 'content'=> 'User was deactivated.'));
		}
	}
	
	public function activateUser($id)
	{
		$user = User::find($id);
		$user->status = 1;
		$user->save();
		
		if(Session::get('access') == 2)
		{
			return Redirect::to('regional/users')->with('message',
                                                            array('type'=>'info', 'content'=> 'User was activated.'));
		}
		else if(Session::get('access') == 1)
		{
			return Redirect::to('national/users')->with('message',
                                                            array('type'=>'info', 'content'=> 'User was activated.'));
		}
	}
	
	public function resetPass()
	{
		$id = Input::get('id');
		$user = User::find($id);
		
	    $user->password = Hash::make(Input::get('new_pass'));
		$user->save();
		
		if(Session::get('access') == 2)
		{
			return Redirect::to('regional/users')->with('message',
                                                            array('type'=>'info', 'content'=> 'Reset.'));
		}
		else if(Session::get('access') == 1)
		{
			return Redirect::to('national/users')->with('message',
                                                            array('type'=>'info', 'content'=> 'Reset.'));
		}
	}
	
	public function destroy($id)
	{
		$user = User::find($id);
		$name =  $user->username;
		$user->delete();
		
		if(Session::get('access') == 2)
		{
			return Redirect::to('regional/users')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
		}
		else if(Session::get('access') == 1)
		{
			return Redirect::to('national/users')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
		}
	    
	}
}
