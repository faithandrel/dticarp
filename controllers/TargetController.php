<?php

class TargetController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
            $data['arc_dropdown'] = $this->get_arc_array();
            $data['inter_dropdown'] = $this->get_inter_array();
            $data['years_dropdown'] = $this->get_years_dropdown();
             $data['interventions'] = Intervention::all();
			
			$year = (int) date('Y');
			if (Request::has('year')) $year = Input::get('year');
		
			$data['targets'] = Target::where('year', '=', $year)
								->where('province_id', '=', $this->user->province_id)
								->get(); 
			
			$id = Target::where('year', '=', $year)
                                      ->where('province_id', '=', $this->user->province_id)->pluck('id');
			
			$data['inter_targets'] = InterTarget::where('target_id', '=', $id)->get();  
			
			Input::flash();
            return View::make('provincial/targets', $data)->with('input', Input::old());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
            $post_data = Input::all();
            foreach($post_data as $key => $value) 
            {
               $post_data[$key] = preg_replace('/\D/', '', $value); 
            }
            //save target
            $target = new Target;
            $target->fill($post_data);
            $target->man_months = Input::get('man_months');
            $target->province_id = $this->user->province_id;
            $target->created_by = $this->user->id;
            $target->save();
            
            $intertargets = array();
            
            foreach($post_data as $key => $value) //put intertargets in array
            { 
                if(is_numeric($key[0]) && !is_numeric($key[1]))
                {   
                    $intertargets[$key[0]][substr($key, 1)] = preg_replace('/\D/', '', $value); 
                }
				else if(is_numeric($key[1]))
                {   
                    $intertargets[$key[0].$key[1]][substr($key, 2)] = preg_replace('/\D/', '', $value); 
                }
				
            }
			 
            foreach ($intertargets as $intervention => $values ) //save intertargets
            {
                $intertarget = new InterTarget;
                $intertarget->fill($values);
                $intertarget->target_id = $target->id;
                $intertarget->intervention_id = $intervention;
                $intertarget->save();
            }
	    
            return Redirect::to('provincial/targets')->with('message',
                                                            array('type'=>'info', 'content'=>'Annual Target for '.Input::get('year').' was added.'));
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($year)
	{
		return Target::where('year', '=', $year)->where('province_id', '=', $this->user->province_id)->get();
	}

	public function interTargets($id)
	{
		return InterTarget::where('target_id', '=', $id)
							->get();
	}
	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
			$post_data = Input::except('_method','_token','year');
            foreach($post_data as $key => $value) 
            {
               $post_data[$key] = preg_replace('/\D/', '', $value); 
            }
            //update target
			
			$target = Target::find($id);
			$target->fill($post_data);
			$target->province_id = $this->user->province_id;
                        $target->man_months = Input::get('man_months');
			$target->updated_by = $this->user->id;
            $target->save();
         
            $intertargets = array();
            
            foreach($post_data as $key => $value) //put intertargets in array
            {
                if(is_numeric($key[0]) && !is_numeric($key[1]))
                {   
                    $intertargets[$key[0]][substr($key, 1)] = $value; 
                }
				else if(is_numeric($key[1]))
                {   
                    $intertargets[$key[0].$key[1]][substr($key, 2)] = $value; 
                }
            }
			
            foreach ($intertargets as $intervention => $values ) //save intertargets
            {
                
				$intertarget = InterTarget::find($values['id']);
                $intertarget->fill($values);
                $intertarget->save();
            }
	    
            return Redirect::to('provincial/targets')->with('message',
                                                            array('type'=>'info', 'content'=>'Annual Target for '.Input::get('year').' was updated.'));
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}


}
