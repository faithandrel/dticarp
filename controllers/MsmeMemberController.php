<?php

class MsmeMemberController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
				
				$msme_id = Input::get('msme_id');
                $log_count = Input::get('log_count');
				
				for ( $c = 1; $c <= $log_count ; $c++)
				{ 
					if (Input::get('report_date_member_'.$c) == NULL)
					{
						continue;
					}
						$new_members = new MsmeMember;
						$new_members->report_date = date_format(date_create(Input::get('report_date_member_'.$c)),'Y-m-d');
						$new_members->fb_male = preg_replace('/\D/', '', Input::get('fb_male_'.$c));
						$new_members->fb_female = preg_replace('/\D/', '', Input::get('fb_female_'.$c));
						$new_members->lo_male = preg_replace('/\D/', '', Input::get('lo_male_'.$c));
						$new_members->lo_female = preg_replace('/\D/', '', Input::get('lo_female_'.$c));
						$new_members->ncb_male = preg_replace('/\D/', '', Input::get('ncb_male_'.$c));
						$new_members->ncb_female = preg_replace('/\D/', '', Input::get('ncb_female_'.$c));
						$new_members->msme_id = $msme_id;
						$new_members->created_by = $this->user->id;
						$new_members->save();
				}
				 $edit_log_count = Input::get('edit_log_count');
				
				for ( $c = 1; $c <= $edit_log_count ; $c++)
				{ 
					if (Input::get('edit_report_date_member_'.$c) == NULL)
					{
						continue;
					}
						$id = Input::get('edit_id_'.$c);
						$members[$c] = MsmeMember::find($id);
						$members[$c]->report_date = date_format(date_create(Input::get('edit_report_date_member_'.$c)),'Y-m-d');
						$members[$c]->fb_male = preg_replace('/\D/', '', Input::get('edit_fb_male_'.$c)); 	
						$members[$c]->fb_female = preg_replace('/\D/', '', Input::get('edit_fb_female_'.$c));	
						$members[$c]->lo_male = preg_replace('/\D/', '', Input::get('edit_lo_male_'.$c));	
						$members[$c]->lo_female = preg_replace('/\D/', '', Input::get('edit_lo_female_'.$c));	
						$members[$c]->ncb_male = preg_replace('/\D/', '', Input::get('edit_ncb_male_'.$c));	
						$members[$c]->ncb_female = preg_replace('/\D/', '', Input::get('edit_ncb_female_'.$c));
						$members[$c]->updated_by = $this->user->id;
						$members[$c]->save();
				}
			
				return Redirect::to('provincial/accomplishments')->with('message',
                                                            array('type'=>'info', 'content'=> '<i class="fa fa-info-circle"></i> &nbsp; Members Log was updated.'));
			//}
			
			//else {
			//		return Redirect::to('provincial/accomplishments')->with('message',
				//array('type'=>'danger', 'content'=>'<i class="fa fa-exclamation-triangle"></i> 
			//														&nbsp; Empty Inputs: No new members added.'));
			//}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($msme_id)
	{
		
		return MsmeMember::where('msme_id', '=', $msme_id)
							->whereBetween('report_date', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($msme_id)
	{
        $acc_year = Session::get('acc_year');
		return MsmeMember::where('msme_id', '=', $msme_id)
							->whereBetween('report_date', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validator = Validator::make(Input::all(), array('edit_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/home')->withErrors($validator);
            }
            else
            {
                $msme_member = MsmeMember::find($id);
                $old_name =  $msme_member->name;
                $msme_member->name = Input::get('edit_name');
				$msme_member->gender = Input::get('edit_gender');
				$msme_member->address = Input::get('edit_address');
				$msme_member->contact_no = Input::get('edit_contact_no');
				$msme_member->date_registered = date_format(date_create(Input::get('edit_date_registered')), 'Y-m-d');
				$msme_member->membership = Input::get('edit_membership');
				$msme_member->status = Input::get('edit_status');
				$msme_member->remarks = Input::get('edit_remarks');
				$msme_member->developed = Input::get('edit_developed');
				$msme_member->date_developed = date_format(date_create(Input::get('edit_date_developed')), 'Y-m-d');
                $msme_member->updated_by = $this->user->id;
                $msme_member->save();
                return Redirect::to('provincial/msme_members')->with('message',
                                                            array('type'=>'info', 'content'=> $old_name.' was changed to '.$msme_member->name));
            }
	    
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    $msme = MsmeMember::find($id);
	    $name =  $msme->name;
	    $msme->delete();
	    return Redirect::to('provincial/msme_members')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
	}


}
