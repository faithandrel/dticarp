<?php

class EntrepreneurController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
			//$validator = Validator::make(Input::all(), array('report_date_1' => 'required' ));
            //if($validator->fails())
            //{
            //        return Redirect::to('provincial/msmes')->withErrors($validator);
           // }
           // else
           // { 
				
				$msme_id = Input::get('msme_id');
                $entrep_count = Input::get('entrep_count');

				for ( $c = 1; $c <= $entrep_count ; $c++)
				{ 
					if (Input::get('date_developed_'.$c) == NULL)
					{
						continue;
					}
						$new_entrep = new Entrepreneur;
						$new_entrep->date_developed = date_format(date_create(Input::get('date_developed_'.$c)),'Y-m-d');
						$new_entrep->first_name = Input::get('first_name_'.$c);
						$new_entrep->last_name = Input::get('last_name_'.$c);
						$new_entrep->project = Input::get('project_'.$c);
						$new_entrep->msme_id = $msme_id;
						$new_entrep->fb_male = Input::get('fb_male_'.$c);
						$new_entrep->fb_female = Input::get('fb_female_'.$c);
						$new_entrep->created_by = $this->user->id;
						$new_entrep->save();
				}
				$edit_sales_count = Input::get('edit_entrep_count');
				
				for ( $c = 1; $c <= $edit_sales_count ; $c++)
				{ 
					if (Input::get('edit_date_developed_'.$c) == NULL)
					{
						continue;
					}
						$id = Input::get('id_'.$c);
						$entrep[$c] = Entrepreneur::find($id);
						$entrep[$c]->date_developed = date_format(date_create(Input::get('edit_date_developed_'.$c)),'Y-m-d');
						$entrep[$c]->first_name = Input::get('edit_first_name_'.$c);
						$entrep[$c]->last_name = Input::get('edit_last_name_'.$c);	
						$entrep[$c]->project = Input::get('edit_project_'.$c);
						$entrep[$c]->fb_male = Input::get('edit_fb_male_'.$c);
						$entrep[$c]->fb_female = Input::get('edit_fb_female_'.$c);
						$entrep[$c]->updated_by = $this->user->id;
						$entrep[$c]->save();
				}
				
				return Redirect::to('provincial/accomplishments')->with('message',
                                                            array('type'=>'info', 'content'=>'Entrepreneurs List was updated.'));
			//}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($msme_id)
	{
		$acc_year = Session::get('acc_year');
		return Entrepreneur::where('msme_id', '=', $msme_id)
							//->where(DB::raw('YEAR(date_developed)'), '=', $acc_year)
							 ->whereBetween('date_developed', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($msme_id)
	{
		$acc_year = Session::get('acc_year');
		return Entrepreneur::where('msme_id', '=', $msme_id)
							 ->whereBetween('date_developed', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$entrep = Entrepreneur::find($id);
	    $entrep->delete();
	}


}
