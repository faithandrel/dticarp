<?php

class ProvinceController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $data['regions'] = $this->get_reg_array();
	    $data['provinces'] = Province::all();
            
            $data['regions_dropdown'] = $this->get_reg_array();
            $data['provinces_dropdown'] = $this->get_prov_array();
            $data['category_dropdown'] = $this->get_cat_array();
	    return View::make('national/province', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('prov_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/provinces')->withErrors($validator);
            }
            else
            {
                $new_prov = new Province;
                $new_prov->province = Input::get('prov_name');
                $new_prov->region_id = Input::get('region');
                $new_prov->created_by = $this->user->id;
                $new_prov->save();
                return Redirect::to('national/provinces')->with('message',
                                                            array('type'=>'info', 'content'=>'New Province was added.'));
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	    return Province::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	    $validator = Validator::make(Input::all(), array('edit_prov_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('national/provinces')->withErrors($validator);
            }
            else
            {
                $province = Province::find($id);
                $old_name =  $province->province;
                $province->province = Input::get('edit_prov_name');
                $province->region_id = Input::get('edit_prov_region');
                $province->updated_by = $this->user->id;
                $province->save();
                return Redirect::to('national/provinces')->with('message',
                                                            array('type'=>'info', 'content'=> $old_name.' was updated'));
            }
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    $province = Province::find($id);
	    $name =  $province->province;
	    $province->delete();
	    return Redirect::to('national/provinces')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
	}


}
