<?php

class AttendanceController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		//
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		if(Input::get('fb_male') != 0 || Input::get('fb_female') != 0 || Input::get('lo_male') != 0 ||
					Input::get('lo_male') != 0 || Input::get('lo_female') != 0 || Input::get('ncb_female') != 0 ||
						Input::get('ncb_male') != 0 )
		{
			Input::merge(array('fb_male'=>preg_replace('/\D/', '', Input::get('fb_male')), 
								'fb_female'=>preg_replace('/\D/', '', Input::get('fb_female')), 
								'lo_male'=>preg_replace('/\D/', '', Input::get('lo_male')),  
								'lo_female'=>preg_replace('/\D/', '', Input::get('lo_female')),  
								'ncb_female'=>preg_replace('/\D/', '', Input::get('ncb_female')), 
								'ncb_male'=>preg_replace('/\D/', '', Input::get('ncb_male')), 
                                'cost_carp'=>preg_replace('/\D/', '', Input::get('cost_carp')),  
								'cost_others'=>preg_replace('/\D/', '', Input::get('cost_others')),  
								'sales'=>preg_replace('/\D/', '', Input::get('sales'))
								));
								
            $post_data = Input::all();
            $attn = new Attendance;
            $attn->fill($post_data);
            $attn->remarks = $post_data['attn_remarks'];
            $attn->msme_id = Session::get('msme_dropdown');
            $attn->activity_id = Input::get('activity_id');
            $attn->created_by = $this->user->id;
            $attn->save();
			
			return Redirect::to('provincial/accomplishments')->with('message',
                                                       array('type'=>'info', 'content'=>'<i class="fa fa-info-circle"></i> &nbsp; An Attendance was updated.'));
		}
		else {
			return Redirect::to('provincial/accomplishments')->with('message',
                                                       array('type'=>'danger', 'content'=>'<i class="fa fa-exclamation-triangle"></i> 
																&nbsp; Empty Inputs: Attendance not saved.'));
		}
			
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		$msme_id = Session::get('msme_dropdown');
		return Attendance::where('msme_id', '=', $msme_id)
							->where('activity_id', '=', $id)
							->get();
	  //  return Attendance::find($id);
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{	
		$msme_id = Session::get('msme_dropdown');
		return Attendance::where('msme_id', '=', $msme_id)
							->where('activity_id', '=', $id)
							->get();
	  //  return Attendance::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		if(Input::get('fb_male') != 0 || Input::get('fb_female') != 0 || Input::get('lo_male') != 0 ||
				Input::get('lo_male') != 0 || Input::get('lo_female') != 0 || Input::get('ncb_female') != 0 ||
					Input::get('ncb_male') != 0 || Input::get('cost_carp') != 0 || Input::get('cost_others') != 0 ||
						Input::get('sales') != 0 || Input::get('attn_remarks') != null )
		{
			Input::merge(array('fb_male'=>preg_replace('/\D/', '', Input::get('fb_male')), 
								'fb_female'=>preg_replace('/\D/', '', Input::get('fb_female')), 
								'lo_male'=>preg_replace('/\D/', '', Input::get('lo_male')),  
								'lo_female'=>preg_replace('/\D/', '', Input::get('lo_female')),  
								'ncb_female'=>preg_replace('/\D/', '', Input::get('ncb_female')), 
								'ncb_male'=>preg_replace('/\D/', '', Input::get('ncb_male')), 
                                'cost_carp'=>preg_replace('/\D/', '', Input::get('cost_carp')),  
								'cost_others'=>preg_replace('/\D/', '', Input::get('cost_others')),  
								'sales'=>preg_replace('/\D/', '', Input::get('sales'))
								));
								
            $post_data = Input::all();
			$attn = Attendance::find($id);
            $attn->fill($post_data);
            $attn->remarks = $post_data['attn_remarks'];
            $attn->updated_by = $this->user->id;
            $attn->save();
       
			return Redirect::to('provincial/accomplishments')->with('message',
                                                       array('type'=>'info', 'content'=>'An Attendance was updated.'));
		}
		else {
			$attn = Attendance::find($id);
			$attn->delete();
			return Redirect::to('provincial/accomplishments')->with('message',
                                                       array('type'=>'warning', 'content'=>'<i class="fa fa-exclamation-triangle"></i> 
																&nbsp; Empty Inputs: Attendance deleted.'));
		}
		
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$attn = Attendance::find($id);
	    $attn->delete();
	}


}
