<?php

class ProvincialController extends BaseController {

	
	public function home()
	{
		$data['arcs'] = Arc::where('province_id', '=', $this->user->province_id)->get();
		$data['arc_dropdown'] = $this->get_arc_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['quarter_dropdown'] = $this->get_quarter_dropdown();

		Session::put('province', $this->user->province['province']);
		Session::put('region', $this->user->province->region['region']);
		
		Session::put('start_date', date("Y-m-d"));
		Session::put('end_date', '2999-01-01');
		Session::put('acc_year',  date("Y"));
		
	    return View::make('provincial/accomplishments', $data);
	}
	
    public function dropdown()
	{
		$input = Input::get('option');
		if ($input != 'blank') { 
					$msme = Msme::where('arc_id', '=', $input)
						->where('province_id', '=', $this->user->province_id)->get()->lists('id','msme_name'); 
						}
	
		return Response::json($msme); 
	}   
	
	public function accomplishments()
	{
	
		$data['arc_dropdown'] = $this->get_arc_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['quarter_dropdown'] = $this->get_quarter_dropdown();
		if( count ($data['arc_dropdown']) == 2) 
		{
			unset($data['arc_dropdown']);
			$data['arc_dropdown'] = array();
		}
		if (Request::has('arc_dropdown'))
		{
			$msme_id = Input::get('msme_dropdown');
			$arc_id = Input::get('arc_dropdown');
			$acc_year = Input::get('acc_year');
			$acc_quarter = Input::get('acc_quarter');
			Session::put('msme_dropdown', $msme_id);
			Session::put('arc_dropdown', $arc_id);
			Session::put('acc_year', $acc_year);
			Session::put('acc_quarter', $acc_quarter);
		}
		else {
			$msme_id = Session::get('msme_dropdown');
			$arc_id = Session::get('arc_dropdown');
			$acc_year = Session::get('acc_year');
			$acc_quarter = Session::get('acc_quarter');
			Input::merge(array('msme_dropdown' => $msme_id, 'arc_dropdown' => $arc_id, 'acc_year' => $acc_year,'acc_quarter' => $acc_quarter ));
			
		}

		//get quarter months
		$quarter = $this->get_months_quarter($acc_quarter);
		$start_month = $quarter['start_month'];
		$end_month = $quarter['end_month'];
		
		$start_date = $acc_year.'-'.$start_month.'-1';
		$end_date = $acc_year.'-'.$end_month.'-31';
		Session::put('start_month', $start_month);
		Session::put('end_month', $end_month);
		Session::put('start_date', $start_date);
		Session::put('end_date', $end_date);
		
		$data['msme'] = Msme::find($msme_id);
		$data['igps'] = Igproject::where('date_started', '<=',  $end_date )
							->whereBetween('date_finished', array($start_date,'2999-01-01'))
							->having('msme_id', '=', $msme_id)					
							->get();
									
		$category = $this->get_cat_array();
		
		foreach($category as $key => $value) 
        {  
			 $data['activities'][$value] = Attendance::where('msme_id', '=', $msme_id)
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
								//->where(DB::raw('YEAR(activities.date_constructed)'), '=', $acc_year)
								->whereBetween('date_finished', array($start_date, $end_date))
								->where('interventions.inter_category_id', '=', $key)
								->orderBy('activities.intervention_id')
								->select('attendances.*', 'activities.*', 'interventions.*', 'activities.id as act_id')
								->get();
								
			$data['category'][$value] = Activity::where(function($query)
						       {
								$query->where('province_id', '=', $this->user->province_id)
							       ->OrWhere('region_id', '=', $this->user->region_id);
						       })
							->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
													    ->where('interventions.inter_category_id', '=', $key)
													   // ->where(DB::raw('YEAR(activities.date_constructed)'), '=', $acc_year)
													   ->whereBetween('date_finished', array($start_date, $end_date))
													    ->select('activities.id', 'activities.name', 'w_sales')
													    ->get();
		}
		
		$data['investments'] = Investment::leftJoin('igprojects', 'igprojects.id', '=', 'investments.igp_id')
									->where('igprojects.msme_id', '=', $msme_id)
									->whereBetween('date_monitored', array($start_date, $end_date))
									->sum('investment_amount');
		
		//activties sales
		$intervention_wsales = Intervention::where('w_sales', '<>', 0)->select('id', 'intervention')->get();
			$activity_sales = 0;
			$sales = 0;
			foreach ($intervention_wsales as $intervention)
			{
				
				$attn = Attendance::where('attendances.msme_id', '=', $msme_id)->where('activities.intervention_id', '=', $intervention->id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->select('attendances.sales', 'activities.intervention_id')->get();
				if($attn->count())
				{
					foreach($attn as $attendance)
					{
						$sales += (double) $attendance->sales;
					}
				}
				
				$attns[$intervention->id][] = $attn;
			}
			$activity_sales += $sales;
		
		//end activties sales

		$monitored = IgpSales::where('msme_id', '=', $msme_id)->whereBetween('report_date', array($start_date, $end_date))->sum('monitored');
		$established = IgpSales::where('msme_id', '=', $msme_id)->whereBetween('report_date', array($start_date, $end_date))->sum('established');
		$data['sales'] = $monitored + $established + $activity_sales;
		
		$data['entrepreneurs'] = Entrepreneur::where('msme_id', '=', $msme_id)->whereBetween('date_developed', array($start_date, $end_date))->count();
		
		$job_igps = Job::select('igp_id')->where('msme_id', '=', $msme_id)
											->whereBetween('report_date', array($start_date, $end_date))
											->groupBy('igp_id')
											->get();
		$data['total_jobs'] = 0;
		foreach ($job_igps as $igp)
		{
			$max_date = Job::where('igp_id', '=', $igp->igp_id)->where('msme_id', '=', $msme_id)->whereBetween('report_date', array($start_date, $end_date))->max('report_date');
			$data['jobs'] = Job::where('igp_id', '=', $igp->igp_id)->where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('number');
			$data['total_jobs'] += $data['jobs'];
		}
		
		$max_date = MsmeMember::where('msme_id', '=', $msme_id)->whereBetween('report_date', array($start_date, $end_date))->max('report_date');
		$data['fb_male'] = MsmeMember::where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('fb_male');
		$data['fb_female'] = MsmeMember::where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('fb_female');
		$data['lo_male'] = MsmeMember::where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('lo_male');
		$data['lo_female'] = MsmeMember::where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('lo_female');
		$data['ncb_male'] = MsmeMember::where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('ncb_male');
		$data['ncb_female'] = MsmeMember::where('msme_id', '=', $msme_id)->where('report_date', '=', $max_date)->pluck('ncb_female');
		$data['fb_total'] = $data['fb_male'] + $data['fb_female'];
		$data['lo_total'] = $data['lo_male'] + $data['lo_female'];
		$data['ncb_total'] = $data['ncb_male'] + $data['ncb_female'];
		
		Input::flash();
		return View::make('provincial/accomplishments', $data)
			->withInput(Input::old());
	}    
}
