<?php

class IgpController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
		$data['arcs'] = Arc::where('province_id', '=', $this->user->province_id)->get();
		$data['arc_dropdown'] = $this->get_arc_array();
			
		$data['msme_dropdown'] = $this->get_msme_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		$data['msme_dropdown'][''] = 'All';
		if (Request::has('msme'))
		{
			$data['igps'] = Igproject::where('msme_id', '=', Input::get('msme'))->get();
			$igp_ids = $data['igps']->lists('id');
		
			if ($igp_ids)
			{
				$data['sources'] = Investment::whereIn('igp_id', $igp_ids)->get();
			}
		}
		else {
			$data['igps'] = Igproject::leftjoin('msmes', 'msmes.id', '=', 'igprojects.msme_id')
										->where('msmes.province_id', '=', $this->user->province_id)
										->select('igprojects.*', 'msmes.id as msme_id', 'msmes.msme_name')
										->get();
			
			$igp_ids = $data['igps']->lists('id');
			if ($igp_ids)
			{
				$data['sources'] = Investment::whereIn('igp_id', $igp_ids)->get();
			}
		}
		Input::flash();
		return View::make('provincial/igp', $data)->withInput(Input::old());
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('igp_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/accomplishments')->withErrors($validator);
            }
            else
            {
                $new_igp = new Igproject;
                $new_igp->igp_name = Input::get('igp_name');
				$new_igp->date_started = date_format(date_create(Input::get('date_started')),'Y-m-d');
				if(Input::get('igp_end_date')==NULL)
				{
					$new_igp->date_finished = date_format(date_create('2999-01-01'),'Y-m-d'); 
				}
				else{
					$new_igp->date_finished = date_format(Input::get('igp_end_date'),'Y-m-d'); 
				}
				$new_igp->remarks = Input::get('remarks');
				$new_igp->msme_id = Input::get('msme_id');
				$new_igp->igp_products = Input::get('igp_products');
				$new_igp->product_category = '';
				//$new_igp->product_sales = Input::get('product_sales');
				//$new_igp->product_remarks = Input::get('product_remarks');
                $new_igp->created_by = $this->user->id;
                $new_igp->save();
				$LastInsertId = $new_igp->id;
				
				$msme_id = Input::get('msme_id');
                $source_count = Input::get('source_count');
				
				for ( $c = 1; $c <= $source_count ; $c++)
				{ 
					if (Input::get('investment_source_'.$c) == NULL)
					{
						continue;
					}
						$new_inv = new Investment;
						$new_inv->igp_id = $LastInsertId;
						$new_inv->investment_source = Input::get('investment_source_'.$c);
						$new_inv->investment_amount = preg_replace('/\D/', '', Input::get('investment_amount_'.$c));
						$new_inv->date_monitored = date_format(date_create(Input::get('date_monitored_'.$c)),'Y-m-d');
						$new_inv->save();
				}
              //  return Redirect::to('provincial/msmes/'.Input::get('msme_id'))
			  
				return Redirect::to('provincial/accomplishments')->with('message',
                                                            array('type'=>'info', 'content'=>'New IGP was added.'));
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//return Igproject::where('msme_id', '=', $id)->get(); 
		return Igproject::find($id);
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
        return Igproject::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validator = Validator::make(Input::all(), array('edit_igp_name' => 'required' ));
          if($validator->fails())
            {
                    return Redirect::to('provincial/home')->withErrors($validator);
            }
            else
            {
				$igp = Igproject::find($id);
				$old_name =  $igp->igp_name;
                $igp->igp_name = Input::get('edit_igp_name');
				$igp->date_started = date_format(date_create(Input::get('edit_date_started')),'Y-m-d'); 
				if(Input::get('edit_igp_end_date')=='(Ongoing)' || Input::get('edit_igp_end_date')==NULL)
				{
					$igp->date_finished = date_format(date_create('2999-01-01'),'Y-m-d'); 
				}
				else{ 
					$igp->date_finished = date_format(date_create(Input::get('edit_igp_end_date')),'Y-m-d'); 
				}
				$igp->remarks = Input::get('edit_remarks');
				$igp->igp_products = Input::get('edit_igp_products');
				$igp->product_category = '';
				$igp->product_sales = Input::get('edit_product_sales');
				//$igp->product_remarks = Input::get('edit_product_remarks');
                $igp->updated_by = $this->user->id;
                $igp->save();
				$LastUpdatedId = $igp->id;
				
				$msme_id = Input::get('edit_msme_id');
				
				$source_count = Input::get('edit_source_count');
				
				for ( $c = 0; $c < $source_count ; $c++)
				{ 
					if (Input::get('edit_investment_source_'.$c) == NULL)
					{
						continue;
					}
						$id = Input::get('id_'.$c);
						$inv[$c] = Investment::find($id);
						$inv[$c]->investment_source = Input::get('edit_investment_source_'.$c);
						$inv[$c]->investment_amount = preg_replace('/\D/', '', Input::get('edit_investment_amount_'.$c));
						$inv[$c]->date_monitored = date_format(date_create(Input::get('edit_date_monitored_'.$c)),'Y-m-d'); 
						
						$inv[$c]->save();
				}
				
				$source_count = Input::get('source_count');
				
				for ( $c = 1; $c <= $source_count ; $c++)
				{ 
					if (Input::get('investment_source_'.$c) == NULL)
					{
						continue;
					}
						$new_inv = new Investment;
						$new_inv->igp_id = $LastUpdatedId;
						$new_inv->investment_source = Input::get('investment_source_'.$c);
						$new_inv->investment_amount = preg_replace('/\D/', '', Input::get('investment_amount_'.$c));
						$new_inv->date_monitored = date_format(date_create(Input::get('date_monitored_'.$c)),'Y-m-d');
						$new_inv->save();
				
				}
               // return Redirect::to('provincial/msmes/'.$msme_id)->with('message',
                                    //  array('type'=>'info', 'content'=> $igp->igp_name.' was updated.'));
					$url = Input::get('url');
					return Redirect::to('provincial/'.$url)->with('message',
															  array('type'=>'info', 'content'=> 'IGP '.$igp->igp_name.' was updated.'));								  
			
				
			}
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$msme_id = Input::get('msme_id');
	    $igp = Igproject::find($id);
	    $igp_name =  $igp->igp_name;
	    $igp->delete();
		
		$inv = Investment::where('igp_id', '=', $id)->delete();
	
	  //  return Redirect::to('provincial/msmes/'.$msme_id)->with('message',
         //                                                   array('type'=>'warning', 'content'=> $igp_name.' was deleted'));
		 return Redirect::to('provincial/igp')->with('message',
                                                           array('type'=>'warning', 'content'=> $igp_name.' was deleted'));
	}

	public function igpDropdown()
	{	
	
		return $data['igp_dropdown'] = Igproject::where('date_started', '<=',  Session::get('end_date') )
							->whereBetween('date_finished', array( Session::get('start_date') ,'2999-01-01'))
							->having('msme_id', '=', Session::get('msme_dropdown'))					
							->get()->lists('id', 'igp_name' );
	}
	public function deactivateIgp($id)
	{	
		$validator = Validator::make(Input::all(), array('date_ended' => 'required' ));
        if($validator->fails())
        {
            return Redirect::to('provincial/igp')->withErrors($validator);
        }
        else
		{
			$msme_id = Input::get('msme_id');
			$igp = Igproject::find($id);
			$igp->date_finished = date_format(date_create(Input::get('date_ended')),'Y-m-d'); 
			$igp->updated_by = $this->user->id;
            $igp->save();
			 return Redirect::to('provincial/igp?msme='.$msme_id)->with('message',
                                                           array('type'=>'info', 'content'=> $id.' project was ended'));
		}			
	}

}
