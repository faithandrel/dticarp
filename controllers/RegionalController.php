<?php

class RegionalController extends BaseController {

	public function home()
	{
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		$data['years_dropdown'] = $this->get_years_dropdown();
		
		Session::put('region', $this->user->region['region']);
		
		$year = (int) date('Y');
		$province = 0;
		if (Request::has('year')) $year = Input::get('year');
		if (Request::has('province')) $province = Input::get('province'); 
		
		$data['provinces'] = Province::where('region_id', '=', $this->user->region_id)
									->lists('province','id'); 
		$data['province_count'] = count($data['provinces']);
		
		$data['targets'] = Target::where('year', '=', $year)
					->leftJoin('provinces', 'provinces.id', '=', 'targets.province_id')
					->where('region_id', '=', $this->user->region_id)
					->get(); 
		
		$id = Target::where('year', '=', $year)->pluck('id');
		$data['inter_targets'] = InterTarget::where('target_id', '=', $id)->get();  
		
		//$results = $this->summaryOfAccompRegional($this->user->region_id);
		
		
		$results = $this->summaryOfAccomp($province);
		
		
		//$data['interventions'] = $interventions;
		$data['annual'] = $results['annual'];
		$data['current'] = $results['current'];
		$data['todate'] = $results['todate'];
		$data['percentage'] = $results['percentage'];
		
		//return $results['annual'];
		Input::flash();
		return View::make('regional/accomplishments', $data)->with('input', Input::old());
	}

}
