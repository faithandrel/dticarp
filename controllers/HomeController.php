<?php

class HomeController extends BaseController {

	public function index()
	{
		if (Auth::check())
		{
			if(Session::get('access') == 3) return Redirect::to('provincial/home');
			else if (Session::get('access') == 2) return Redirect::to('regional/home');
			else if (Session::get('access') == 1) return Redirect::to('national/home');
			else return View::make('login');
		}
		else return View::make('login');
	}
	
	public function login()
	{
		if (Auth::attempt(array('username' => Input::get('login_username'),
					'password' => Input::get('login_password'))))
		{

			$status = User::whereUsername(Input::get('login_username'))->pluck('status');
			if($status == 1)
			{
				Session::put('username', Input::get('login_username'));
				Session::put('access', User::whereUsername(Input::get('login_username'))->pluck('access_level'));
				return Redirect::intended('/');
			}
				return Redirect::to('/')->withInput(Input::old())->with('message2', array('type'=>'error', 'content'=> '<i class="fa fa-times"></i> Account was deactivated'));
		}
		else 
		{
			if(User::whereUsername(Input::get('login_username'))->count())
			{
				Input::flash();
				return Redirect::to('/')->withInput(Input::old())->with('message1', array('type'=>'error', 'content'=> '<i class="fa fa-times"></i> Invalid Password'));
			}
			else {
				return Redirect::to('/')->withInput(Input::old())->with('message2', array('type'=>'error', 'content'=> '<i class="fa fa-times"></i> Invalid Username'));
			}
		}
	}
	
	public function logout()
	{
		Auth::logout();
		Session::flush();
		return Redirect::to('/');
	}
}
