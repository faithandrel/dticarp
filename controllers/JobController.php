<?php

class JobController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
			//$validator = Validator::make(Input::all(), array('report_date_1' => 'required' ));
            //if($validator->fails())
            //{
            //        return Redirect::to('provincial/msmes')->withErrors($validator);
           // }
           // else
           // { 
				
				$msme_id = Input::get('msme_id');
                $job_count = Input::get('job_count');

				for ( $c = 1; $c <= $job_count ; $c++)
				{ 
					if (Input::get('report_date_job_'.$c) == NULL)
					{
						continue;
					}
						$new_job = new Job;
						$new_job->msme_id = $msme_id;
						$new_job->report_date = date_format(date_create(Input::get('report_date_job_'.$c)),'Y-m-d');
						$new_job->number = Input::get('number_'.$c);
						$new_job->igp_id = Input::get('igp_id_'.$c);
						$new_job->save();
				}
				
				 $edit_job_count = Input::get('edit_job_count');
				
				for ( $c = 1; $c <= $edit_job_count ; $c++)
				{
					if (Input::get('edit_report_date_job_'.$c) == NULL)
					{
						continue;
					}
						 $id = Input::get('id_'.$c);
						$job[$c] = Job::find($id);
						$job[$c]->report_date = date_format(date_create(Input::get('edit_report_date_job_'.$c)),'Y-m-d');
						$job[$c]->number = Input::get('edit_number_'.$c);
						$job[$c]->igp_id = Input::get('edit_igp_id_'.$c);	
						$job[$c]->updated_by = $this->user->id;
						$job[$c]->save();
				}
				
				return Redirect::to('provincial/accomplishments')->with('message',
                                                            array('type'=>'info', 'content'=>'Jobs log was updated.'));
			//}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($msme_id)
	{
		
		return Job::where('msme_id', '=', $msme_id)
							->whereBetween('report_date', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($msme_id)
	{
		
		return Job::where('msme_id', '=', $msme_id)
							->where('report_date', '<=', Session::get('end_date'))
							->get();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$job = Job::find($id);
	    $job->delete();
	}


}
