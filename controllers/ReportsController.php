<?php

class ReportsController extends BaseController {
	
	//Annex 9 - Accomplishments per ARC
	public function accomplishmentPerArcPage()
	{
		$data['region_dropdown'] = $this->get_reg_array();
		$data['province_dropdown'] = $this->get_prov_array();
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['msmes'] = array();
		
		if(Session::get('access') == 3)
		{
			if(Input::get('province')) $data['province'] = (int)Input::get('province');
			else  $data['province'] = (int)$this->user->province_id;
		}
		else
		{
			if(Session::get('access') == 2) 
			{
				$region_id = (int)$this->user->region_id;
			}
			else if(Session::get('access') == 1)
			{
				if(Input::has('region')) $region_id = (int)Input::get('region');
				else $region_id = Region::pluck('id');
			}
			$data['region'] = $region_id;
			if(Input::has('province')) $data['province'] = (int)Input::get('province');
			else $data['province'] = Province::where('region_id', '=', $region_id)->pluck('id');
		}
		//create report
	    if ( Input::get('action')  == 'create' ) 
		{

			$data['msmes'] = $this->accompPerArc($data['province']);
			
			Input::flash();
			return View::make('reports/accomplishment_per_arc', $data)->with('input', Input::old());
		}
		
		//download xls
		 if ( Input::get('action') == 'xls' )
		{
			$period = $this->get_report_period();
		
			$filename = 'Annex 9 Accomplishment Per ARC_'.$period.'_'.Session::get('province');
			Excel::create($filename, function($excel) {	

				$excel->sheet('Annex 9', function($sheet) {
					
					if(Session::get('access') == 3)
					{
						if(Input::get('province')) $province_id = (int)Input::get('province');
						else  $province_id = (int)$this->user->province_id;
					}
					else
					{
						if(Input::has('province')) $province_id = (int)Input::get('province');
						else $province_id = Province::where('region_id', '=', $region_id)->pluck('id');
					}
					$data['province'] = $province_id;
					
					$data['msmes'] = $this->accompPerArc($province_id);
					$sheet->loadView('reports/xls/per_arcxls', $data);

				});
			})->download('xls');
		}

		//downlod pdf
		else if ( Input::get('action') == 'pdf' )
		{
			Excel::create('Investment', function($excel) {		

				$excel->sheet('Sheetname', function($sheet) {
				
					$sheet->setOrientation('landscape');
					
					$calendar = cal_info(0);
					$data['months_dropdown'] = $calendar['months'];
					$years = array();
					$year_today = (int) date('Y');
					for($i=$year_today-10; $i < $year_today+10; $i++)
					{
						$years[$i] = $i;
					}
					$data['years_dropdown'] = $years;
					$data['msmes'] = $this->accompPerArc();
					$sheet->loadView('reports/accomplishment_per_arc', $data);

				});
			})->download('pdf');
		}
		
		else {
			//get current quarter
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$default_start_month = $quarter['start_month'];
			$default_end_month = $quarter['end_month'];
				
			//default 
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $default_start_month , 'report_end_year' => date('Y'), 'report_end_month' => $default_end_month));
				
			$data['msmes'] = $this->accompPerArc($data['province']);
			
			Input::flash();
			return View::make('reports/accomplishment_per_arc', $data)->with('input', Input::old());
		}
	}
	
	public function accompPerArc($province_id)
	{
		$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
		$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		
		$msmes = Msme::where('msmes.province_id', '=', $province_id)
                                    ->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
                                    ->select('msmes.*', 'arcs.name as arc_name')->get();
		
		foreach($msmes as $msme) //get igps
		{
			$msme->investments = (double) 0;
			/*$projects = Igproject::where('msme_id', '=', $msme->id)->get();
			foreach($projects as $project)
			{
				$msme->investments += Investment::where('igp_id', '=', $project->id)->whereBetween('date_monitored', array($start_date, $end_date))->sum('investment_amount');
			}*/
			
			$projects  = Igproject::where('date_started', '<=',  $end_date )
							->whereBetween('date_finished', array($start_date,'2999-01-01'))
							->having('msme_id', '=', $msme->id)					
							->get();
							
			foreach($projects as $project)
			{
				$msme->investments += Investment::where('igp_id', '=', $project->id)->whereBetween('date_monitored', array($start_date, $end_date))->sum('investment_amount');
			}
			
		//activties sales
		
			$intervention_wsales = Intervention::where('w_sales', '<>', 0)->select('id', 'intervention')->get();
			$activity_sales = 0;
			$sales = 0;
			foreach ($intervention_wsales as $intervention)
			{
				
				$attn = Attendance::where('attendances.msme_id', '=', $msme->id)->where('activities.intervention_id', '=', $intervention->id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->select('attendances.sales', 'activities.intervention_id')->get();
				if($attn->count())
				{
					foreach($attn as $attendance)
					{
						$sales += (double) $attendance->sales;
					}
				}
				
				$attns[$intervention->id][] = $attn;
			}
			$activity_sales += $sales;
		
		//end activties sales
			
			
			$monitored = IgpSales::where('msme_id', '=', $msme->id)->whereBetween('report_date', array($start_date, $end_date))->sum('monitored');
			$established = IgpSales::where('msme_id', '=', $msme->id)->whereBetween('report_date', array($start_date, $end_date))->sum('established');
			$msme->sales = $monitored + $established + $activity_sales;
			$msme->projects = $projects;
			$msme->entreps = Entrepreneur::where('msme_id', '=', $msme->id)->whereBetween('date_developed', array($start_date, $end_date))->count();
			
			//jobs
			$job_igps = Job::select('igp_id')->where('msme_id', '=', $msme->id)
											->whereBetween('report_date', array($start_date, $end_date))
											->groupBy('igp_id')
											->get();
			$msme->jobs = 0;
			foreach ($job_igps as $igp)
			{
				$max_date = Job::where('igp_id', '=', $igp->igp_id)->where('msme_id', '=', $msme->id)->whereBetween('report_date', array($start_date, $end_date))->max('report_date');
				$jobs = Job::where('igp_id', '=', $igp->igp_id)->where('msme_id', '=', $msme->id)->where('report_date', '=', $max_date)->pluck('number');
				$msme->jobs += $jobs;
			}
			//end jobs
			
			$unsorted_attns = Attendance::where('attendances.msme_id', '=', $msme->id)->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
								->leftJoin('inter_categories', 'inter_categories.id', '=', 'interventions.inter_category_id')
								->select('attendances.*', 'activities.name as activity','activities.date_constructed as activity_date', 'activities.date_finished as activity_date_end' ,
									 'interventions.intervention', 'inter_categories.name as category')->get();
			
			$attns = array();
			foreach($unsorted_attns as $attn) //sort attendances
			{
				$attns[$attn->category][$attn->intervention][] = $attn;
			}
			$msme->attns = $attns;
		}
		
		return $msmes;
	}
	
	private function summaryOfAccompRegionalPrep()
	{
		if(Session::get('access') == 2) $region_id = (int)$this->user->region_id;
		else if(Session::get('access') == 1)
		{
			if(Input::has('region')) $region_id = (int)Input::get('region');
			else $region_id = Region::pluck('id');
		}
		
		$year = (int) date('Y');
		if (Request::has('year')) $year = Input::get('year');
		
		$data['target'] = Target::where('year', '=', $year)
					->leftJoin('provinces', 'provinces.id', '=', 'targets.province_id')
					->where('region_id', '=', $region_id)
					->first(); 
		
		$id = Target::where('year', '=', $year)->pluck('id');
		$data['inter_targets'] = InterTarget::where('target_id', '=', $id)->get();
		
		$results = $this->summaryOfAccompRegional($region_id);
		
		$data['annual'] = $results['annual'];
		$data['current'] = $results['current'];
		$data['todate'] = $results['todate'];
		$data['percentage'] = $results['percentage'];
		$data['provinces_current'] = $results['provinces'];
		
		$data['region'] = $region_id;
		
		return $data;
	}
	
	public function summaryOfAccompPage()
	{
		if(Session::get('access') == 3)
		{
			$province_id = (int)$this->user->province_id;
		}
		else $province_id = (int)Input::get('report_province');
		
		$data = $this->summaryOfAccomp($province_id);
		
		if(!$data)
		{
			return Redirect::to('provincial/targets')->with('message',
                                        array('type'=>'danger', 'content'=>'Annual Target for this year has not been set.'));
		}
		else
		{
			$data['months_dropdown'] = $this->get_months_dropdown();
			$data['years_dropdown'] = $this->get_years_dropdown();
			
			Input::flash();
			return View::make('reports/accomplishment', $data)->with('input', Input::old());
		}
	}
	
	public function summaryOfAccompPageRegional()
	{
		$data = $this->summaryOfAccompRegionalPrep();
		
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();

		Input::flash();
		return View::make('reports/accomplishment_regional', $data)->with('input', Input::old());
	}
	
	public function summaryOfAccompXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 8 Summary of Accomplishments'.$period.'_'.Session::get('province');
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 8', function($sheet) {
				if(Session::get('access') == 3)
				{
					$province_id = (int)$this->user->province_id;
				}
				else $province_id = (int)Input::get('report_province');
				$results = $this->summaryOfAccomp($province_id);
				$sheet->loadView('reports/xls/accomplishmentxls', $results);
			});
		    
		})->download('xls');
	}
	
	public function summaryOfAccompRegionalXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 8 Summary of Accomplishments'.$period;
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 8', function($sheet) {
				$results = $this->summaryOfAccompRegionalPrep();
				$sheet->loadView('reports/xls/accomplishmentxls_regional', $results);
			});
		    
		})->download('xls');
		
		//$query = $this->summaryOfAccompRegionalPrep();
		//return View::make('reports/xls/accomplishmentxls_regional', $query);
	}
	
	//  Entrepreneurs
	private function entrepreneursDeveloped()
	{
		if(Session::get('access') == 3)
		{
			$region_id = (int)$this->user->region_id;
			$data['provinces'] = Province::where('id', '=', (int)$this->user->province_id)->get();
			$provinces_list = Province::where('id', '=', (int)$this->user->province_id)->lists('id');
		}
		else 
		{
			if(Input::has('region'))
			{
				$region_id = Input::get('region');
			}
			else 
			{
				$region_id = (int)$this->user->region_id;
			}
			$data['provinces'] = Province::where('provinces.region_id', '=', $region_id)
											->leftJoin('msmes', 'msmes.province_id', '=', 'provinces.id')
											->leftJoin('entrepreneurs', 'entrepreneurs.msme_id', '=', 'msmes.id')
											->distinct()->select('provinces.id', 'provinces.province')->get();
			$provinces_list = Province::where('region_id', '=', $region_id)->lists('id');
		}	
		
		$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
		$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		
		$data['grand_total'] = 0;
		foreach($data['provinces'] as $province)
		{
			$data['total'][$province->id] = 0;
			$data['entrepreneurs'][$province->id] = Entrepreneur::where('msmes.province_id', '=', $province->id)->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
						->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
						->select('entrepreneurs.*', 'msmes.msme_name', 'arcs.name as arc_name')->get();
			$count = count($data['entrepreneurs'][$province->id]);
			
			foreach($data['entrepreneurs'][$province->id] as $entrep){ $data['total'][$entrep->arc_name][$province->id] = 0; }
			
		//	foreach($data['entrepreneurs'][$province->id] as $entrep)
			//{ 
			//	 $data['total'][$entrep->arc_name][$province->id]++;
				// $data['count'][$entrep->arc_name][$province->id] = $data['total'][$entrep->arc_name][$province->id];
			//}
			$data['total'][$province->id] += $count;
			$data['grand_total'] += $data['total'][$province->id];
		}

		return $data;
	}
	
	public function entrepreneursDevelopedPage()
	{
		$data['region_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
	
		
		if (Input::has('report_start_month'))
		{
			$query = $this->entrepreneursDeveloped();
			$data['results'] = $query;
		}
		else
		{
			//get current quarter
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$default_start_month = $quarter['start_month'];
			$default_end_month = $quarter['end_month'];
			
			//default 
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $default_start_month , 'report_end_year' => date('Y'), 'report_end_month' => $default_end_month));
			$query = $this->entrepreneursDeveloped();
			$data['results'] = $query;
		}
		
		Input::flash();
		return View::make('reports/entrepreneurs', $data)->with('input', Input::old());
	}
	
	public function entrepreneursDevelopedXls()
	{
		$period = $this->get_report_period();
		
		$filename = 'Annex 6 Entrepreneurs Developed_'.$period.'_'.Session::get('province');
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 6', function($sheet) {
				$data['results'] = $this->entrepreneursDeveloped();
				$sheet->loadView('reports/xls/entrepreneursxls', $data);
			});
		    
		})->download('xls');
	}
	
	private function assistedArcs($province_id)
	{
		if (Input::has('report_start_month')) //initialize dates
		{
			$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
			$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		}
		else
		{
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$start_date = date('Y').'-'. $quarter['start_month'].'-1';
			$end_date = date('Y').'-'.$quarter['end_month'].'-31';
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $quarter['start_month'] , 'report_end_year' => date('Y'), 'report_end_month' => $quarter['end_month']));
		}
		
		$msmes = Attendance::where('msmes.province_id', '=', $province_id)
				->whereBetween('activities.date_finished', array($start_date, $end_date))
				->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
				->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
				->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
				->select('arcs.id as arc_id','arcs.name as arc_name','msmes.msme_name', 'msmes.msme_fullname')
				->groupBy('attendances.msme_id')->get();
		
		$arcs = array();
		$data['grand_total'] = (int) 0;
		
		foreach ($msmes as $msme)
		{
			if($msme->arc_id == 0) $arcs['Non-ARC'][] = $msme->msme_fullname."(".$msme->msme_name.")";
			else
			{
				$arcs[$msme->arc_name][] = $msme->msme_fullname."(".$msme->msme_name.")";
				$data['grand_total']++;
			}
		}
		
		$data['arcs'] = $arcs;
		
		return $data;
	}
	
	private function assistedArcsRegional()
	{
		if(Session::get('access') == 2) $region_id = (int)$this->user->region_id;
		else if(Session::get('access') == 1)
		{
			if(Input::has('region')) $region_id = (int)Input::get('region');
			else $region_id = Region::pluck('id');
		}
		
		$provinces = Province::where('region_id', '=', $region_id)
					->lists('province','id');
		$data['grand_total'] = (double) 0;
		foreach($provinces as $province_id => $province)
		{
			$data['results'][$province] = $this->assistedArcs($province_id);
			$data['grand_total'] += $data['results'][$province]['grand_total'];
		}
		
		$data['region'] = $region_id;
		
		return $data;
	}
	
	public function assistedArcsPage()
	{
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		
		if(Session::get('access') == 3)
		{
			$province_id = (int)$this->user->province_id;
		}
		else $province_id = (int)Input::get('report_province');
		
		$data['results'] = $this->assistedArcs($province_id);
		
		Input::flash();
		return View::make('reports/arc_list', $data)->with('input', Input::old());
	}
	
	public function assistedArcsPageRegional()
	{
		$data = $this->assistedArcsRegional();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();

		Input::flash();
		return View::make('reports/arc_list_regional', $data)->with('input', Input::old());
	}
	
	public function assistedArcsXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 1 List of Assisted ARCs_'.$period.'_'.Session::get('province');
			
		Excel::create($filename, function($excel) {
					
			$excel->sheet('Annex 3', function($sheet) {
				
				if(Session::get('access') == 3)
				{
					$province_id = (int)$this->user->province_id;
				}
				else $province_id = (int)Input::get('report_province');
				
				$data['results'] = $this->assistedArcs($province_id);
				
				$sheet->loadView('reports/xls/arc_listxls', $data);
			});
		    
		})->download('xls');
		//return View::make('report', $data);
	}
	
	public function assistedArcsRegionalXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 1 List of Assisted ARCs_'.$period.'_'.Session::get('province');
			
		Excel::create($filename, function($excel) {
					
			$excel->sheet('Annex 3', function($sheet) {
				
				$data = $this->assistedArcsRegional();
				$sheet->loadView('reports/xls/arc_listxls_regional', $data);
			});
		    
		})->download('xls');
		//return View::make('report', $data);
	}
	
	//Annex 3 - Investments
	
	private function investmentsGenerated($province_id)
	{
		if (Input::has('report_start_month')) //initialize dates
		{
			$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
			$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		}
		else
		{
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$start_date = date('Y').'-'. $quarter['start_month'].'-1';
			$end_date = date('Y').'-'.$quarter['end_month'].'-31';
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $quarter['start_month'] , 'report_end_year' => date('Y'), 'report_end_month' => $quarter['end_month']));
		}
		
		$total = (double) 0; //total investment
		
		$projects = Igproject::where('msmes.province_id', '=', $province_id)
					->where('igprojects.date_started', '<=',  $end_date )
					->whereBetween('igprojects.date_finished', array($start_date,'2999-01-01'))
					->leftJoin('msmes', 'msmes.id', '=', 'igprojects.msme_id')->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
					->select('igprojects.*', 'msmes.msme_name', 'arcs.name as arc_name')
					->orderBy('arcs.id') //ly
					->get();
		
		//raw sql query					
		//$projects = DB::select(DB::raw("SELECT  A.*, B.msme_name, C.name as 'arc_name' FROM `igprojects` A 
		//				LEFT JOIN `msmes` B ON B.id = A.msme_id
		//				LEFT JOIN `arcs` C ON C.id = B.arc_id
		//				WHERE B.province_id = ".$province_id." AND
		//				A.date_started BETWEEN '".$start_date."' AND '".$end_date."'"));
		foreach($projects as $project)
		{
			$project->investments = Investment::where('igp_id', '=', $project->id)->whereBetween('date_monitored', array($start_date, $end_date))->get();
			foreach ($project->investments as $investment)
			{
				$total += $investment->investment_amount;
			}
		}
		
		$data['total'] = $total;
		$data['projects'] = $projects;
		
		return $data;
	}
	
	public function investmentsGeneratedRegional()
	{
		if(Session::get('access') == 2) $region_id = (int)$this->user->region_id;
		else if(Session::get('access') == 1)
		{
			if(Input::has('region')) $region_id = (int)Input::get('region');
			else $region_id = Region::pluck('id');
		}
		
		$provinces = Province::where('region_id', '=', $region_id)
					->lists('province','id');
		$data['grand_total'] = (double) 0;
		foreach($provinces as $province_id => $province)
		{
			$data['investments'][$province] = $this->investmentsGenerated($province_id);
			$data['grand_total'] += $data['investments'][$province]['total'];
		}
		
		$data['region'] = $region_id;
		
		return $data;
	}
	
	public function investmentsGeneratedPage()
	{
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		
		if(Session::get('access') == 3)
		{
			$province_id = (int)$this->user->province_id;
		}
		else $province_id = (int)Input::get('report_province');
	
		$query = $this->investmentsGenerated($province_id);
		$data['results'] = $query;
		
		Input::flash();
		return View::make('reports/investments', $data)->with('input', Input::old());
	}
	
	public function investmentsGeneratedPageRegional()
	{
		$data = $this->investmentsGeneratedRegional();
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();

		Input::flash();
		return View::make('reports/investments_regional', $data)->with('input', Input::old());
	}
	
	public function investmentsGeneratedXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 3 Investments Generated_'.$period.'_'.Session::get('province');
			
		Excel::create($filename, function($excel) {
					
			$excel->sheet('Annex 3', function($sheet) {
				
				$province_id = (int)$this->user->province_id;
				
				$query = $this->investmentsGenerated($province_id);
				//$data['projects'] = $query['projects'];
				//$data['total'] = $query['total'];
				
				$sheet->loadView('reports/xls/investmentxls', $query);
			});
		    
		})->download('xls');
		//return View::make('report', $data);
	}
	
	public function investmentsGeneratedRegionalXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 3 Investments Generated_'.$period;
			
		Excel::create($filename, function($excel) {		
			$excel->sheet('Annex 3', function($sheet) {
				$query = $this->investmentsGeneratedRegional();
				$sheet->loadView('reports/xls/investmentxls_regional', $query);
			});
		    
		})->download('xls');
		
		//$query = $this->investmentsGeneratedRegional();
		//return View::make('reports/xls/investmentxls_regional', $query);
	}
	
	//Annex 4 - Sales Generated
	
	private function salesGenerated($province_id)
	{
		if (Input::has('report_start_month'))
		{
			$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
			$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		}
		else
		{
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$start_date = date('Y').'-'. $quarter['start_month'].'-1';
			$end_date = date('Y').'-'.$quarter['end_month'].'-31';
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $quarter['start_month'] , 'report_end_year' => date('Y'), 'report_end_month' => $quarter['end_month']));
		}
		
		$msmes = Msme::where('msmes.province_id', '=', $province_id)
                                    ->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
                                    ->select('msmes.*', 'arcs.name')->get();
		$intervention_wsales = Intervention::where('w_sales', '<>', 0)->select('id', 'intervention')->get();
		
		$totals = array('igp_sales'=>(double)0, 'monitored'=>(double)0, 'established'=>(double)0);
		foreach( $intervention_wsales as $intervention ) //init total
		{
			$totals[$intervention->id] = (double)0;
		}
		
		
		foreach ($msmes as $msme)
		{
			$attns = array();
			/*$msme->igp = $msme->igprojects()->select('igprojects.igp_name', 'igprojects.product_sales')
							->whereBetween('igprojects.date_started', array($start_date, $end_date))->get();
			*/
			$msme->igp  = Igproject::where('date_started', '<=',  $end_date )
							->whereBetween('date_finished', array($start_date,'2999-01-01'))
							->having('msme_id', '=', $msme->id)	
							->get();			
							
			$totals['igp'][$msme->id] = (double)0;
			$totals['activities'][$msme->id] = array();
			$msme->monitored = IgpSales::where('msme_id', '=', $msme->id)->whereBetween('report_date', array($start_date, $end_date))->sum('monitored');
			$msme->established = IgpSales::where('msme_id', '=', $msme->id)->whereBetween('report_date', array($start_date, $end_date))->sum('established');
			$msme->sales = $msme->monitored + $msme->established;
	
			foreach ($intervention_wsales as $intervention)
			{
				$totals['activities'][$msme->id][$intervention->id] = (double)0;
				
				$attn = Attendance::where('attendances.msme_id', '=', $msme->id)->where('activities.intervention_id', '=', $intervention->id)
								->whereBetween('activities.date_finished', array($start_date, $end_date))
								->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
								->select('attendances.sales', 'activities.intervention_id')->get();
				if($attn->count())
				{
					foreach($attn as $attendance)
					{
						$totals['activities'][$msme->id][$intervention->id] += (double) $attendance->sales;
						$msme->sales += (double) $attendance->sales;
						$totals[$intervention->id] += (double) $attendance->sales;
					}
				}
				
				$attns[$intervention->id][] = $attn;
			}
			$msme->attn = $attns;
			$totals['igp_sales'] += $msme->sales;
			$totals['monitored'] += $msme->monitored;
			$totals['established'] += $msme->established;
		}

		$results['msmes'] = $msmes;
		$results['interventions'] = $intervention_wsales;
		$results['totals'] = $totals;
		
		return $results;
	}
	
	private function salesGeneratedRegional()
	{
		if(Session::get('access') == 2) $region_id = (int)$this->user->region_id;
		else if(Session::get('access') == 1)
		{
			if(Input::has('region')) $region_id = (int)Input::get('region');
			else $region_id = Region::pluck('id');
		}
		
		$provinces = Province::where('region_id', '=', $region_id)
					->lists('province','id');
		$intervention_wsales = Intervention::where('w_sales', '<>', 0)->lists('id');
		
		$data['totals'] = array('igp_sales'=>(double)0, 'monitored'=>(double)0, 'established'=>(double)0);
		foreach($intervention_wsales as $intervention)
		{
			$data['totals'][$intervention] = (double)0;
		}
		foreach($provinces as $province_id => $province)
		{
			$data['sales'][$province] = $this->salesGenerated($province_id);
			foreach($data['totals'] as $key => $value)
			{
				if(!is_numeric($key)) $data['totals'][$key] += $data['sales'][$province]['totals'][$key];
				
			}
			foreach($intervention_wsales as $intervention)
			{
				$data['totals'][$intervention] +=  $data['sales'][$province]['totals'][$intervention];
			}
		}
		
		$data['region'] = $region_id;
		
		return $data;
	}
	
	public function salesGeneratedPage()
	{
		$data['regions_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		
		$province_id = (int)$this->user->province_id;
		
		$results = $this->salesGenerated($province_id);
			
		$data['msmes'] = $results['msmes'];
		$data['totals'] = $results['totals'];
		
		$data['interventions'] =  $results['interventions'];
		
		Input::flash();
		return View::make('reports/sales', $data)->with('input', Input::old());
	}
	
	public function salesGeneratedPageRegional()
	{
		$data = $this->salesGeneratedRegional();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();
		
		Input::flash();
		return View::make('reports/sales_regional', $data)->with('input', Input::old());
	}
	
	public function salesGeneratedXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 4 Sales Generated_'.$period.'_'.Session::get('province');
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 4', function($sheet) {
				$province_id = (int)$this->user->province_id;
				$results = $this->salesGenerated($province_id);
				$sheet->loadView('reports/xls/salesxls', $results);
			});
		    
		})->download('xls');
		//$results = $this->salesGenerated();
		//return View::make('reports/xls/salesxls', $results);
	}
	
	public function salesGeneratedRegionalXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 4 Sales Generated_'.$period.'_';
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 4', function($sheet) {
				$data = $this->salesGeneratedRegional();
				$sheet->loadView('reports/xls/salesxls_regional', $data);
			});
		    
		})->download('xls');
		//$results = $this->salesGeneratedRegional();
		//return View::make('reports/xls/salesxls_regional', $results);
	}
	
	//Annex 7 - Summary of Trainings
	
	protected function sumOfTrainings()
	{
		
		if(Session::get('access') == 3)
		{
			$region_id = (int)$this->user->region_id;
			$data['provinces'] = Province::where('id', '=', (int)$this->user->province_id)->get();
			$provinces_list = Province::where('id', '=', (int)$this->user->province_id)->lists('id');
		}
		else 
		{
			if(Input::has('region'))
			{
				$region_id = Input::get('region');
			}
			else 
			{
				$region_id = (int)$this->user->region_id;
			}
			$data['provinces'] = Province::where('region_id', '=', $region_id)->get();
			
			$provinces_list = Province::where('region_id', '=', $region_id)->lists('id');
			if ($provinces_list == []) $provinces_list = array(0);
		}	
		
		$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
		$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-30';
		
		$data['interventions'] = Activity::whereBetween('activities.date_finished', array($start_date, $end_date))
					->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
					->where('interventions.inter_category_id', '=', 3)
					->leftJoin('provinces', 'provinces.id', '=', 'activities.province_id')
					->whereIn('provinces.id',  $provinces_list)
					//->whereIn('provinces.id', array(1))
					->orderBy('activities.intervention_id')
					->distinct()->select('interventions.intervention', 'interventions.id')
					->get();
		
				$data['grand_count'] = 0;
				$data['grand_fb_m_total'] = 0;
				$data['grand_fb_f_total'] = 0;
				$data['grand_lo_m_total'] = 0;
				$data['grand_lo_f_total'] = 0;
				$data['grand_ncb_m_total'] = 0;
				$data['grand_ncb_f_total'] = 0;
				$data['grand_total_attn'] = 0;
				$data['grand_carp'] = 0;
				$data['grand_others'] = 0;
				$data['grand_total_cost'] = 0;
				
		foreach($data['interventions'] as $intervention)			
		{
				$data['count'][$intervention->id] = 0; 
				$data['fb_m_total'][$intervention->id] = 0;
				$data['fb_f_total'][$intervention->id] = 0;
				$data['lo_m_total'][$intervention->id] = 0;
				$data['lo_f_total'][$intervention->id] = 0;
				$data['ncb_m_total'][$intervention->id] = 0;
				$data['ncb_f_total'][$intervention->id] = 0;
				$data['total_attn'][$intervention->id] = 0;
				$data['carp'][$intervention->id] = 0;
				$data['others'][$intervention->id] = 0;
				$data['total_cost'][$intervention->id] = 0;
				
			foreach($data['provinces'] as $province)
			{
				
				
				$data['activities'][$intervention->id][$province->id] = Activity::whereBetween('activities.date_finished', array($start_date, $end_date))
						->leftJoin('attendances', 'attendances.activity_id', '=', 'activities.id')
						->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
						->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
						->where('msmes.province_id', '=', $province->id)
						->leftJoin('interventions', 'interventions.id', '=', 'activities.intervention_id')
						->where('interventions.id', '=',  $intervention->id)
						->distinct()->select('activities.*', 'interventions.id as intervention_id', 'interventions.intervention')
						->get();
						
				foreach($data['activities'][$intervention->id][$province->id] as $activity)
				{
					$activity->fb_m = 0;
					$activity->fb_f = 0;
					$activity->lo_m = 0;
					$activity->lo_f = 0;
					$activity->ncb_m = 0;
					$activity->ncb_f = 0;
					$activity->carp = 0;
					$activity->others = 0;
					
					
					$attendances = Attendance::where('attendances.activity_id', '=', $activity->id)
									->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
									->where('msmes.province_id', '=', $province->id)
									->get();
									
					foreach($attendances as $attendance)
					{
					
						$activity->fb_m += $attendance->fb_male;
						$activity->fb_f += $attendance->fb_female;
						$activity->lo_m += $attendance->lo_male;
						$activity->lo_f += $attendance->lo_female;
						$activity->ncb_m += $attendance->ncb_male;
						$activity->ncb_f += $attendance->ncb_female;
						$activity->carp += $attendance->cost_carp;
						$activity->others += $attendance->cost_others;
						$activity->province = $attendance->province;
					}
					$activity->total_attn = $activity->fb_m+$activity->fb_f+$activity->lo_m+$activity->lo_f+$activity->ncb_m+$activity->ncb_f;
					$activity->total_cost = $activity->carp + $activity->others;
					$data['fb_m_total'][$intervention->id] += $activity->fb_m;
					$data['fb_f_total'][$intervention->id] += $activity->fb_f;
					$data['lo_m_total'][$intervention->id] += $activity->lo_m;
					$data['lo_f_total'][$intervention->id] += $activity->lo_f;
					$data['ncb_m_total'][$intervention->id] += $activity->ncb_m;
					$data['ncb_f_total'][$intervention->id] += $activity->ncb_f;
					$data['total_attn'][$intervention->id] += $activity->total_attn;
					$data['count'][$intervention->id]++;
					$data['carp'][$intervention->id] += $activity->carp;
					$data['others'][$intervention->id] += $activity->others;
					$data['total_cost'][$intervention->id] += $activity->total_cost;
				}
			}
				$data['grand_count'] += $data['count'][$intervention->id];
				$data['grand_fb_m_total'] += $data['fb_m_total'][$intervention->id];
				$data['grand_fb_f_total'] += $data['fb_f_total'][$intervention->id];
				$data['grand_lo_m_total'] += $data['lo_m_total'][$intervention->id];
				$data['grand_lo_f_total'] += $data['lo_f_total'][$intervention->id];
				$data['grand_ncb_m_total'] += $data['ncb_m_total'][$intervention->id];
				$data['grand_ncb_f_total'] += $data['ncb_f_total'][$intervention->id];
				$data['grand_total_attn'] += $data['total_attn'][$intervention->id];
				$data['grand_carp'] += $data['carp'][$intervention->id];
				$data['grand_others'] += $data['others'][$intervention->id];
				$data['grand_total_cost'] += $data['total_cost'][$intervention->id];
		}
		return $data;
	}
	
	public function sumOfTrainingsPage()
	{
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();
		
		
		
		
		if (Input::has('report_start_month'))
		{
			$data['results'] = $this->sumOfTrainings();
		}
		else
		{
			//get current quarter
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$default_start_month = $quarter['start_month'];
			$default_end_month = $quarter['end_month'];
			
			//default 
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $default_start_month , 'report_end_year' => date('Y'), 'report_end_month' => $default_end_month));
			
		    $data['results'] = $this->sumOfTrainings();
		
		}
		
		Input::flash();
		return View::make('reports/trainings', $data)->with('input', Input::old());
	}
	
	public function sumOfTrainingsXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 7 Summary of Trainings_'.$period.'_'.Session::get('province');
	//return	$results = $this->sumOfTrainings();
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 7', function($sheet) {
				 $data['results'] = $this->sumOfTrainings();
				$sheet->loadView('reports/xls/trainingsxls', $data);
			});
		    
		})->download('xls');
		// $results['activities'] = $this->sumOfTrainings();
		//return View::make('reports/xls/trainingsxls', $results);
	}
	
	//tradeFairs
	
	protected function tradeFairs()
	{
		
		$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
		$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-30';
		if(Session::get('access') == 3)
		{
			$tradefairs['provinces'] = Province::where('id', '=', (int)$this->user->province_id)->get();
			$tradefairs['region'] = 1;
		}
		else 
		{
			if(Input::has('region'))
			{
				$tradefairs['region'] = Input::get('region');
			}
			else 
			{
				$tradefairs['region'] = (int)$this->user->region_id;
			}
			$tradefairs['provinces'] = Province::where('region_id', '=', $tradefairs['region'])->get();
		}	
			$tradefairs['grand_count'] = 0;
			$tradefairs['grand_total_sales'] = 0;
			$tradefairs['grand_total_fb_male'] = 0;
			$tradefairs['grand_total_fb_female'] = 0;
			$tradefairs['grand_total_lo_male'] = 0;
			$tradefairs['grand_total_lo_female'] = 0;
			$tradefairs['grand_count'] = 0;
			
			foreach ($tradefairs['provinces'] as $province)
			{	
				$tradefairs['tradefairs'][$province->id] = Attendance::select('attendances.*', 'activities.*', 'arcs.name as arc_name', 'msmes.msme_name')
						->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
						->where('activities.intervention_id', '=', 1)
						->whereBetween('activities.date_finished', array($start_date, $end_date))
						->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
						->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
						->where('msmes.province_id', '=', $province->id )
						//->Where('arcs.id', '=', 'msmes.arc_id')
						->get();
						
						$tradefairs['total_sales'][$province->id] = 0;
						$tradefairs['total_fb_male'][$province->id] = 0;
						$tradefairs['total_fb_female'][$province->id] = 0;
						$tradefairs['total_lo_male'][$province->id] = 0;
						$tradefairs['total_lo_female'][$province->id] = 0;
						foreach ($tradefairs['tradefairs'][$province->id] as $tradefair)
						{
							$tradefairs['total_sales'][$province->id] += $tradefair->sales;
							$tradefairs['total_fb_male'][$province->id] += $tradefair->fb_male;
							$tradefairs['total_fb_female'][$province->id] += $tradefair->fb_female;
							$tradefairs['total_lo_male'][$province->id] += $tradefair->lo_male;
							$tradefairs['total_lo_female'][$province->id] += $tradefair->lo_female;
						}
			
						$tradefairs['count'][$province->id] = count($tradefairs['tradefairs'][$province->id]);	
						
						$tradefairs['grand_total_sales'] += $tradefairs['total_sales'][$province->id];
						$tradefairs['grand_total_fb_male'] += $tradefairs['total_sales'][$province->id];
						$tradefairs['grand_total_fb_female'] += $tradefairs['total_sales'][$province->id];
						$tradefairs['grand_total_lo_male'] += $tradefairs['total_sales'][$province->id];
						$tradefairs['grand_total_lo_female'] += $tradefairs['total_sales'][$province->id];
						$tradefairs['grand_count'] += count($tradefairs['tradefairs'][$province->id]);
			}
			return $tradefairs;
	
		
	}
	
	public function tradeFairsPage()
	{
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();
	
		if (Input::has('report_start_month'))
		{
			$data['results'] = $this->tradeFairs();
		}
		else
		{
			//get current quarter
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$default_start_month = $quarter['start_month'];
			$default_end_month = $quarter['end_month'];
			
			//default 
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $default_start_month , 'report_end_year' => date('Y'), 'report_end_month' => $default_end_month));
			
			 $data['results'] = $this->tradeFairs();
		}
		
		Input::flash();
		return View::make('reports/trade_fairs', $data)->with('input', Input::old());
	}
	
	public function tradeFairsXls()
	{
		$period = $this->get_report_period();
		$filename = 'Trade Fairs_'.$period.'_'.Session::get('province');
		
		//return $data['result'] = $this->tradeFairs();
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Trade Fairs', function($sheet) {
				$data['results'] = $this->tradeFairs();
				$sheet->loadView('reports/xls/tradefairsxls', $data);
			});
		    
		})->download('xls');
		
		//return View::make('reports/xls/trainingsxls', $results);
	}
	
	private function genderDisAg($province_id)
	{
		
		if (Input::has('report_start_month'))
		{
			$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
			$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		}
		else
		{
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$start_date = date('Y').'-'. $quarter['start_month'].'-1';
			$end_date = date('Y').'-'.$quarter['end_month'].'-31';
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $quarter['start_month'] , 'report_end_year' => date('Y'), 'report_end_month' => $quarter['end_month']));
		}
		
		//MSMEs Assisted
		$msmes_assisted =  Attendance::where('msmes.province_id', '=', $province_id)
						->whereBetween('activities.date_finished', array($start_date, $end_date))
						->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
						->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
						->select('activities.date_finished', 'activities.name', 'msmes.msme_name', 'msmes.id as msme_id')
						->groupBy('attendances.msme_id')->get();
		
		$data['msmes_asst'] = $msmes_assisted->count();

		//MSMEs Assisted FBs and LOs
		$data['msmes_asst_fb_m']= 0;
		$data['msmes_asst_lo_m']= 0;
		$data['msmes_asst_fb_f']= 0;
		$data['msmes_asst_lo_f']= 0;
		
		foreach($msmes_assisted as $attn)
		{
			$current_fb_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->whereBetween('report_date', array($start_date, $end_date))
							->orderBy('report_date', 'desc')->pluck('fb_male');
			if($current_fb_male == 0) //if members log wasn't edited in the given dates, get latest
			{
				$current_fb_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->orderBy('report_date', 'desc')->pluck('fb_male');
			}
			
			$current_fb_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->whereBetween('report_date', array($start_date, $end_date))
							->orderBy('report_date', 'desc')->pluck('fb_female');
			if($current_fb_female == 0)
			{
				$current_fb_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->orderBy('report_date', 'desc')->pluck('fb_female');
			}
			
			$current_lo_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->whereBetween('report_date', array($start_date, $end_date))
							->orderBy('report_date', 'desc')->pluck('lo_male');
			if($current_lo_male == 0)
			{
				$current_lo_male = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->orderBy('report_date', 'desc')->pluck('lo_male');
			}
			
			$current_lo_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->whereBetween('report_date', array($start_date, $end_date))
							->orderBy('report_date', 'desc')->pluck('lo_female');
			if($current_lo_female == 0)
			{
				$current_lo_female = MsmeMember::where('msme_id', '=', $attn->msme_id)
							->orderBy('report_date', 'desc')->pluck('lo_female');
			}
				
			$data['msmes_asst_fb_m']  += $current_fb_male;				
			$data['msmes_asst_fb_f']  += $current_fb_female;
			$data['msmes_asst_lo_m']  += $current_lo_male;
			$data['msmes_asst_lo_f']  += $current_lo_female;
		}
			
		$interventions = Intervention::all();
		$interattns = array();
		foreach ($interventions as $intervention) //get attendances of each intervention
		{
			$interattns[$intervention->intervention]['fb_male'] = Attendance::where('activities.intervention_id', '=', $intervention->id)
							->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->sum('fb_male');
			$interattns[$intervention->intervention]['fb_female'] = Attendance::where('activities.intervention_id', '=', $intervention->id)
							->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->sum('fb_female');
			$interattns[$intervention->intervention]['lo_male'] = Attendance::where('activities.intervention_id', '=', $intervention->id)
							->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->sum('lo_male');
			$interattns[$intervention->intervention]['lo_female'] = Attendance::where('activities.intervention_id', '=', $intervention->id)
							->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->sum('lo_female');
			
			$activities = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->groupBy('activities.id')->get();
							
			$interattns[$intervention->intervention]['count'] = $activities->count();
			if($intervention->id== 13) //if consultancy get man-months
			{
				$interattns[$intervention->intervention]['manmonths'] = Attendance::where('activities.intervention_id', '=', $intervention->id)->where('msmes.province_id', '=', $province_id)
										->whereBetween('activities.date_finished', array($start_date, $end_date))
										->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
										->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
										->distinct('attendances.activity_id')->sum('activities.man_months');
			}
		}
		$interventions_prod = Intervention::where('inter_category_id', '=', 2)->get();
		$interattns['Product Development'] = array('fb_male'=>(int)0, 'fb_female'=> (int)0,
							   'lo_male'=>(int)0, 'lo_female'=> (int)0, 'count'=>(int)0);
		foreach($interventions_prod as $prod_dev) //total prod dev attendances
		{
			$interattns['Product Development']['fb_male'] += $interattns[$prod_dev->intervention]['fb_male'];
			$interattns['Product Development']['fb_female'] += $interattns[$prod_dev->intervention]['fb_female'];
			$interattns['Product Development']['lo_male'] += $interattns[$prod_dev->intervention]['lo_male'];
			$interattns['Product Development']['lo_female'] += $interattns[$prod_dev->intervention]['lo_female'];
			$interattns['Product Development']['count'] += $interattns[$prod_dev->intervention]['count'];
		}
		$data['interattns'] = $interattns;
		
		$data['entrepreneurs'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->count();
		
		$data['msmes_dev_fb_m'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->sum('fb_male');
		$data['msmes_dev_fb_f'] = Entrepreneur::where('msmes.province_id', '=', $province_id)
							->whereBetween('entrepreneurs.date_developed', array($start_date, $end_date))
							->leftJoin('msmes', 'msmes.id', '=', 'entrepreneurs.msme_id')->sum('fb_female');
			
		$data['msmes_dev'] = $data['entrepreneurs'];
						
		return $data;
	}
	
	private function genderDisAgRegional()
	{
		if(Session::get('access') == 2) $region_id = (int)$this->user->region_id;
		else if(Session::get('access') == 1)
		{
			if(Input::has('region')) $region_id = (int)Input::get('region');
			else $region_id = Region::pluck('id');
		}
		
		$data['provinces'] = Province::where('region_id', '=', $region_id)
					->lists('province','id');
					
		$data['grand_total']['msmes_asst'] = 0;
		$data['grand_total']['msmes_asst_fb_m'] = 0;
		$data['grand_total']['msmes_asst_fb_f'] = 0;
		$data['grand_total']['msmes_asst_lo_m'] = 0;
		$data['grand_total']['msmes_asst_lo_f'] = 0;
		
		$data['grand_total']['entrepreneurs'] = 0;
		$data['grand_total']['msmes_dev_fb_m'] = 0;
		$data['grand_total']['msmes_dev_fb_f'] = 0;
		
		foreach($data['provinces'] as $province_id => $province)
		{
			$data['gender_disag'][$province] = $this->genderDisAg($province_id);
			$data['grand_total']['msmes_asst'] += $data['gender_disag'][$province]['msmes_asst'];
			$data['grand_total']['msmes_asst_fb_m'] += $data['gender_disag'][$province]['msmes_asst_fb_m'] ;
			$data['grand_total']['msmes_asst_fb_f'] += $data['gender_disag'][$province]['msmes_asst_fb_f'];
			$data['grand_total']['msmes_asst_lo_m'] += $data['gender_disag'][$province]['msmes_asst_lo_m'];
			$data['grand_total']['msmes_asst_lo_f'] += $data['gender_disag'][$province]['msmes_asst_lo_f'];
			
			$data['grand_total']['entrepreneurs'] = $data['gender_disag'][$province]['entrepreneurs'];
			$data['grand_total']['msmes_dev_fb_m'] = $data['gender_disag'][$province]['msmes_dev_fb_m'];
			$data['grand_total']['msmes_dev_fb_f'] = $data['gender_disag'][$province]['msmes_dev_fb_f'];
		}
		
		//correct intervention count
		
		if (Input::has('report_start_month'))
		{
			$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
			$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		}
		else
		{
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$start_date = date('Y').'-'. $quarter['start_month'].'-1';
			$end_date = date('Y').'-'.$quarter['end_month'].'-31';
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $quarter['start_month'] , 'report_end_year' => date('Y'), 'report_end_month' => $quarter['end_month']));
		}
		
		$data['interventions'] = Intervention::lists('intervention','id');
		
		$data['grand_total']['Product Development']['count'] = 0;
		$data['grand_total']['Product Development']['fb_male'] = 0;
		$data['grand_total']['Product Development']['fb_female'] = 0;
		$data['grand_total']['Product Development']['lo_male'] = 0;
		$data['grand_total']['Product Development']['lo_female'] = 0;
		
		foreach($data['interventions'] as $intervention_id => $intervention)
		{
			$inter_count = Attendance::where('activities.intervention_id', '=', $intervention_id )->where('provinces.region_id', '=', $this->user->region_id)
							->whereBetween('activities.date_finished', array($start_date, $end_date))
							->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
							->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
							->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
							->groupBy('activities.id')->get();
			$data['grand_total'][$intervention]['fb_male'] =  Attendance::where('activities.intervention_id', '=', $intervention_id )->where('provinces.region_id', '=', $this->user->region_id)
									->whereBetween('activities.date_finished', array($start_date, $end_date))
									->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
									->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
									->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
									->sum('fb_male');
			$data['grand_total'][$intervention]['fb_female'] =  Attendance::where('activities.intervention_id', '=', $intervention_id )->where('provinces.region_id', '=', $this->user->region_id)
									->whereBetween('activities.date_finished', array($start_date, $end_date))
									->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
									->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
									->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
									->sum('fb_female');
			$data['grand_total'][$intervention]['lo_male'] =  Attendance::where('activities.intervention_id', '=', $intervention_id )->where('provinces.region_id', '=', $this->user->region_id)
									->whereBetween('activities.date_finished', array($start_date, $end_date))
									->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
									->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
									->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
									->sum('lo_male');
			$data['grand_total'][$intervention]['lo_female'] =  Attendance::where('activities.intervention_id', '=', $intervention_id )->where('provinces.region_id', '=', $this->user->region_id)
									->whereBetween('activities.date_finished', array($start_date, $end_date))
									->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
									->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
									->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
									->sum('lo_female');
									
			$data['grand_total'][$intervention]['count'] = $inter_count->count();
			
			if($intervention == 'Consultancy') //if consultancy get man-months
			{
				$data['grand_total'][$intervention]['manmonths'] =  Attendance::where('activities.intervention_id', '=', $intervention_id)->where('provinces.region_id', '=', $this->user->region_id)
										->whereBetween('activities.date_finished', array($start_date, $end_date))
										->leftJoin('activities', 'activities.id', '=', 'attendances.activity_id')
										->leftJoin('msmes', 'msmes.id', '=', 'attendances.msme_id')
										->leftJoin('provinces', 'provinces.id', '=', 'msmes.province_id')
										->distinct('attendances.activity_id')->sum('activities.man_months');
			}
			if( in_array($intervention,array('Product Developed', 'Prototype Executed', 'Packaging and Label Developed')) )
			{
				$data['grand_total']['Product Development']['count'] += $data['grand_total'][$intervention]['count'];
				$data['grand_total']['Product Development']['fb_male'] += $data['grand_total'][$intervention]['fb_male'];
				$data['grand_total']['Product Development']['fb_female'] += $data['grand_total'][$intervention]['fb_female'];
				$data['grand_total']['Product Development']['lo_male'] += $data['grand_total'][$intervention]['lo_male'];
				$data['grand_total']['Product Development']['lo_female'] += $data['grand_total'][$intervention]['lo_female'];
			}
		}
		
		$data['region'] = $region_id;
		
		return $data;
	}
	
	public function genderDisAgPage()
	{
		$province_id = (int)$this->user->province_id;
		
		$data = $this->genderDisAg($province_id);
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();

		//return $interattns;
		Input::flash();
		return View::make('reports/gender_disag', $data)->with('input', Input::old());
	}
	
	public function genderDisAgPageRegional()
	{
		$data = $this->genderDisAgRegional();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();
		
		Input::flash();
		return View::make('reports/gender_disag_regional', $data)->with('input', Input::old());
	}
	
	public function genderDisAgXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 2 Gender-Disaggregated Data'.$period.'_'.Session::get('province');
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 2', function($sheet) {
				$province_id = (int)$this->user->province_id;
				$results = $this->genderDisAg($province_id);
				$sheet->loadView('reports/xls/gender_disagxls', $results);
			});
		    
		})->download('xls');
	}
	
	public function genderDisAgRegionalXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 2 Gender-Disaggregated Data'.$period;
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 2', function($sheet) {
				$results = $this->genderDisAgRegional();
				$sheet->loadView('reports/xls/gender_disagxls_regional', $results);
			});
		    
		})->download('xls');
	}
	
	//  Jobs
	private function jobsGenerated()
	{
		if(Session::get('access') == 3)
		{
			$region_id = (int)$this->user->region_id;
			$data['provinces'] = Province::where('id', '=', (int)$this->user->province_id)->get();
			$provinces_list = Province::where('id', '=', (int)$this->user->province_id)->lists('id');
		}
		else 
		{
			if(Input::has('region'))
			{
				$data['region'] = Input::get('region');
			}
			else 
			{
				$data['region'] = (int)$this->user->region_id;
			}
			$region_id = $data['region'];
			$data['provinces'] = Province::where('provinces.region_id', '=', $region_id)
											->leftJoin('msmes', 'msmes.province_id', '=', 'provinces.id')
											->leftJoin('jobs', 'jobs.msme_id', '=', 'msmes.id')
											->distinct()->select('provinces.id', 'provinces.province')->get();
			$provinces_list = Province::where('region_id', '=', $region_id)->lists('id');
		}	
		
		$start_date = Input::get('report_start_year').'-'.Input::get('report_start_month').'-1';
		$end_date = Input::get('report_end_year').'-'.Input::get('report_end_month').'-31';
		
		$data['grand_total'] = 0;
		foreach($data['provinces'] as $province)
		{
			$data['total'][$province->id] = 0;
			//$data['jobs'][$province->id]
			$data['msmes'][$province->id] = Job::where('msmes.province_id', '=', $province->id)->whereBetween('jobs.report_date', array($start_date, $end_date))
										->leftJoin('msmes', 'msmes.id', '=', 'jobs.msme_id')->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
										->select( 'msmes.id', 'jobs.igp_id')
										->groupBy('jobs.igp_id')
										->orderBy('arcs.id')
										->get();
			
			foreach($data['msmes'][$province->id] as $msme)
			{
				
				$max = Job::where('msme_id', '=', $msme->id)->where('igp_id', '=', $msme->igp_id)->whereBetween('jobs.report_date', array($start_date, $end_date))
									->max('jobs.report_date');
				
				$data['jobs'][$province->id][$msme->id.'_'.$msme->igp_id] = Job::where('jobs.msme_id', '=', $msme->id)
										->where('igp_id', '=', $msme->igp_id)
										->where('jobs.report_date', '=', $max)
										->leftJoin('igprojects', 'igprojects.id', '=', 'jobs.igp_id')
										->leftJoin('msmes', 'msmes.id', '=', 'jobs.msme_id')
										->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
										->select('jobs.*', 'igprojects.igp_name', 'msmes.msme_name', 'arcs.name as arc_name')
										->get();	

				$data['total'][$province->id] += Job::where('jobs.msme_id', '=', $msme->id)
										->where('igp_id', '=', $msme->igp_id)
										->where('jobs.report_date', '=', $max)
										->leftJoin('igprojects', 'igprojects.id', '=', 'jobs.igp_id')
										->leftJoin('msmes', 'msmes.id', '=', 'jobs.msme_id')
										->leftJoin('arcs', 'arcs.id', '=', 'msmes.arc_id')
										->select('jobs.*', 'igprojects.igp_name', 'msmes.msme_name', 'arcs.name as arc_name')
										->pluck('number');												
			}
			$data['grand_total'] += $data['total'][$province->id];
		}

		return $data;
	}
	
	public function jobsGeneratedPage()
	{
		$data['region_dropdown'] = $this->get_reg_array();
		$data['provinces_dropdown'] = $this->get_prov_array();
		
		$data['months_dropdown'] = $this->get_months_dropdown();
		$data['years_dropdown'] = $this->get_years_dropdown();
		$data['region_dropdown'] = $this->get_reg_array();
		
		if (Input::has('report_start_month'))
		{
			$query = $this->jobsGenerated();
			$data['results'] = $query;
		}
		else
		{
			//get current quarter
			$quarter = $this->get_months_quarter(ceil(date('m')/3));
			$default_start_month = $quarter['start_month'];
			$default_end_month = $quarter['end_month'];
			
			//default 
			Input::merge(array('report_start_year' => date('Y'), 'report_start_month' => $default_start_month , 'report_end_year' => date('Y'), 'report_end_month' => $default_end_month));
			$query = $this->jobsGenerated();
		    $data['results'] = $query;
		}
		
		Input::flash();
		return View::make('reports/jobs', $data)->with('input', Input::old());
	}
	
	public function jobsGeneratedXls()
	{
		$period = $this->get_report_period();
		$filename = 'Annex 5 Jobs Generated'.$period.'_'.Session::get('province');
		
		Excel::create($filename, function($excel) {
		
			$excel->sheet('Annex 5', function($sheet) {
				$data['results'] = $this->jobsGenerated();
				$sheet->loadView('reports/xls/jobsxls', $data);
			});
		    
		})->download('xls');
	}
}
