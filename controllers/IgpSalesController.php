<?php

class IgpSalesController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{	
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
			//$validator = Validator::make(Input::all(), array('report_date_1' => 'required' ));
            //if($validator->fails())
            //{
            //        return Redirect::to('provincial/msmes')->withErrors($validator);
           // }
           // else
           // { 
				
				$msme_id = Input::get('msme_id');
                $sales_count = Input::get('sales_count');
				
				for ( $c = 1; $c <= $sales_count ; $c++)
				{ 
					if (Input::get('report_date_'.$c) == NULL)
					{
						continue;
					}
						$new_sales = new IgpSales;
						$new_sales->report_date = date_format(date_create(Input::get('report_date_'.$c)),'Y-m-d');
						$new_sales->monitored = preg_replace('/\D/', '', Input::get('monitored_'.$c));
						$new_sales->established = preg_replace('/\D/', '', Input::get('established_'.$c));
						$new_sales->remarks = Input::get('remarks_'.$c);
						$new_sales->msme_id = $msme_id;
						$new_sales->created_by = $this->user->id;
						$new_sales->save();
				}
				$edit_sales_count = Input::get('edit_sales_count');
				
				for ( $c = 1; $c <= $edit_sales_count ; $c++)
				{ 
					if (Input::get('edit_report_date_'.$c) == NULL)
					{
						continue;
					}
						$id = Input::get('id_'.$c);
						$sales[$c] = IgpSales::find($id);
						$sales[$c]->report_date = date_format(date_create(Input::get('edit_report_date_'.$c)),'Y-m-d');
						$sales[$c]->monitored = preg_replace('/\D/', '', Input::get('edit_monitored_'.$c));
						$sales[$c]->established = preg_replace('/\D/', '', Input::get('edit_established_'.$c));
						$sales[$c]->remarks = Input::get('edit_remarks_'.$c);
						$sales[$c]->updated_by = $this->user->id;
						$sales[$c]->save();
				}
				
				return Redirect::to('provincial/accomplishments')->with('message',
                                                            array('type'=>'info', 'content'=>'Sales were updated.'));
			
			//}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($msme_id)
	{
		return IgpSales::where('msme_id', '=', $msme_id)
							->whereBetween('report_date', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($msme_id)
	{
		return IgpSales::where('msme_id', '=', $msme_id)
							->whereBetween('report_date', array(Session::get('start_date'), Session::get('end_date')))
							->get();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{ 
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		$sales = IgpSales::find($id);
	    $sales->delete();
	}


}
