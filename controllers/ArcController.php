<?php

class ArcController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
	    $data['arcs'] = Arc::where('province_id', '=', $this->user->province_id)->get();
	    
	    $data['arc_dropdown'] = $this->get_arc_array();
	    $data['inter_dropdown'] = $this->get_inter_array();
	    return View::make('provincial/arc', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('arc_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/arcs')->withErrors($validator);
            }
            else
            {
                $new_arc = new Arc;
                $new_arc->name = Input::get('arc_name');
                $new_arc->province_id = $this->user->province_id;
                $new_arc->created_by = $this->user->id;
                $new_arc->save();
                return Redirect::to('provincial/arcs')->with('message',
                                                            array('type'=>'info', 'content'=>'New ARC was added.'));
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
            
	    return Arc::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validator = Validator::make(Input::all(), array('edit_arc_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/arcs')->withErrors($validator);
            }
            else
            {
                $arc = Arc::find($id);
                $old_name =  $arc->name;
                $arc->name = Input::get('edit_arc_name');
                $arc->updated_by = $this->user->id;
                $arc->save();
                return Redirect::to('provincial/arcs')->with('message',
                                                            array('type'=>'info', 'content'=> $old_name.' was changed to '.$arc->name));
            }
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    $arc = Arc::find($id);
	    $name =  $arc->name;
	    $arc->delete();
	    return Redirect::to('provincial/arcs')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
	}


}
