<?php

class InvestmentController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('investment_source_1' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/msmes')->withErrors($validator);
            }
            else
            {
				
				$msme_id = Input::get('msme_id');
                $source_count = Input::get('source_count');
				
				for ( $c = 1; $c <= $source_count ; $c++)
				{ 
					if (Input::get('investment_source_'.$c) == NULL)
					{
						continue;
					}
						$new_inv = new Investment;
						$new_inv->igp_id = Input::get('igp_id');
						$new_inv->investment_source = Input::get('investment_source_'.$c);
						$new_inv->investment_amount = Input::get('investment_amount_'.$c);
						$new_inv->date_monitored = date_format(date_create(Input::get('date_monitored_'.$c)),'Y-m-d');
						$new_inv->save();
				
				}
				
                return Redirect::to('provincial/msmes/'.$msme_id)->with('message',
                                                            array('type'=>'info', 'content'=>'New Funds Source was added.'));
			
			}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($igp_id)
	{
		return Investment::where('igp_id', '=', $igp_id)
				->whereBetween('date_monitored', array(Session::get('start_date'), Session::get('end_date')))
							->get();
			
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($igp_id)
	{
	    return Investment::where('igp_id', '=', $igp_id)
							->get();
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validator = Validator::make(Input::all(), array('edit_investment_source_0' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/home')->withErrors($validator);
            }
            else
            {
			 	 $source_count = Input::get('edit_source_count');
				 $msme_id = Input::get('msme_id');
				
				for ( $c = 0; $c < $source_count ; $c++)
				{ 
					if (Input::get('edit_investment_source_'.$c) == NULL)
					{
						continue;
					}
						$id = Input::get('id_'.$c);
						$inv[$c] = Investment::find($id);
						$inv[$c]->investment_source = Input::get('edit_investment_source_'.$c);
						$inv[$c]->investment_amount = Input::get('edit_investment_amount_'.$c);
						$inv[$c]->date_monitored = date_format(date_create(Input::get('edit_date_monitored_'.$c)),'Y-m-d'); 
						
						$inv[$c]->save();
				}
				
                return Redirect::to('provincial/msmes/'.$msme_id)->with('message',
                                                            array('type'=>'info', 'content'=> 'Funds Source was Updated.'));
            }
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    $inv = Investment::find($id);
	    //$inv_source =  $inv->investment_source;
	    $inv->delete();
		
	 //   return Redirect::to('provincial/msmes')->with('message',
      //                                                      array('type'=>'warning', 'content'=> $inv_source.' was deleted'));
	}


}
