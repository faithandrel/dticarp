<?php

class MsmeController extends BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$data['msmes'] = Msme::where('province_id', '=', $this->user->province_id)->get();
		
		$data['arc_dropdown'] = $this->get_arc_array();
		$data['inter_dropdown'] = $this->get_inter_array();
		
		return View::make('provincial/msme', $data);
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
	    
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
	    $validator = Validator::make(Input::all(), array('msme_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/msmes')->withErrors($validator);
            }
            else
            {
                $new_msme = new Msme;
                $new_msme->msme_name = Input::get('msme_name');
				$new_msme->msme_fullname = Input::get('msme_fullname');
				$new_msme->address_region = Input::get('address_region');
				$new_msme->address_province = Input::get('address_province');
				$new_msme->address_city = Input::get('address_city');
				$new_msme->address_brgy = Input::get('address_brgy');
				$new_msme->district = Input::get('district');
				$new_msme->industry = Input::get('industry');
				$new_msme->date_assisted = date_format(date_create(Input::get('date_assisted')), 'Y-m-d');
				$new_msme->remarks = Input::get('remarks');
				$new_msme->arc_id = Input::get('arc_id');
                $new_msme->province_id = $this->user->province_id;
				$new_msme->region_id =  $this->user->province->region_id;
                $new_msme->created_by = $this->user->id;
                $new_msme->save();
                return Redirect::to('provincial/msmes')->with('message',
                                                            array('type'=>'info', 'content'=>'New MSME was added.'));
            }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	//for navigation
		$data['arc_dropdown'] = $this->get_arc_array();
		$data['inter_dropdown'] = $this->get_inter_array();
	//for navigation	
	
		$data['msme'] = Msme::find($id);
		
		$data['igps'] = Igproject::where('msme_id', '=', $id)->get();
		
		$igp_ids = $data['igps']->lists('id');
		
		if ($igp_ids)
		{
			$data['sources'] = Investment::whereIn('igp_id', $igp_ids)->get();
		}
		return View::make('provincial/msme_detail', $data);
		
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{  
	   return Msme::find($id);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
            $validator = Validator::make(Input::all(), array('edit_msme_name' => 'required' ));
            if($validator->fails())
            {
                    return Redirect::to('provincial/home')->withErrors($validator);
            }
            else
            {
                $msme = Msme::find($id);
                $old_name =  $msme->msme_name;
                $msme->msme_name = Input::get('edit_msme_name');
				$msme->msme_fullname = Input::get('edit_msme_fullname');
				$msme->address_region = Input::get('edit_address_region');
				$msme->address_province = Input::get('edit_address_province');
				$msme->address_city = Input::get('edit_address_city');
				$msme->address_brgy = Input::get('edit_address_brgy');
				$msme->district = Input::get('edit_district');
				$msme->industry = Input::get('edit_industry');
				$msme->date_assisted = date_format(date_create(Input::get('edit_date_assisted')), 'Y-m-d');
				$msme->remarks = Input::get('edit_msme_remarks');
				$msme->arc_id = Input::get('edit_arc_id');
                $msme->updated_by = $this->user->id;
                $msme->save();
                return Redirect::to('provincial/msmes')->with('message',
                                                            array('type'=>'info', 'content'=> $msme->msme_name.' was updated.'));
            }
	    
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	    $msme = Msme::find($id);
	    $name =  $msme->name;
	    $msme->delete();
	    return Redirect::to('provincial/msmes')->with('message',
                                                            array('type'=>'warning', 'content'=> $name.' was deleted'));
	}


}
